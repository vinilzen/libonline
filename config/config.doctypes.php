<?
// VARIABLES
// .docx formats
$this->docFormats = array(
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/octet-stream'
);
// Maximum bytes to upload .docx-document
$this->maxVolDocx = 1048576;

// Image format
$this->imageFormats = array('image/jpeg', 'image/png', 'image/gif');
// Maximum bytes to upload image
$this->maxVolImage = 3145728;

// Video formats
$this->multimediaFormats = array('audio/mp3');
// Maximum bytes to upload video
$this->maxVolMultimedia = 5242880;

// Drop Caps Image Formats
$this->dropCapsFormatsImage = array('image/jpeg', 'image/png', 'image/gif');
// Maximum bites to upload drop caps image
$this->maxVolDropCapsFormatsImage = 1048576;

?>