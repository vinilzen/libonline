<?
global
$maxFirstParts,

//$styleRules,
//$tableRules,
//$proofErrRule,
//$boldLineRule,
//$italLineRule,
//$colorRule,
//$justificationRule,
$reassignmentsRule,
//$insetRule,
//$fallBackRule,
//$footNoteRules,

$patternAuthor,
$patternName,
$specialSymbols,
$patternSerial,
$patternVolume,
$patternDateCreateRu,
$patternDateCreateEn,
$patternGenreRu,
$patternGenreEn,
$patternSpeech,
$patternIntroduction,
$patternAnnotation,
$patternAboutAuthor,
$patternFromAuthor,
$patternDedication,
$patternFirstSentence,

$patternFootNoteBegin,
$patternFootNoteMiddle,
$patternFootNoteFinal,
$patternFootNote,
$patterFootNoteContent,

$skipTags;

// VARIABLES
// Number of puts-in numbers in potential titles
$this->numPutsIn = 10;

// Minimum difference sizes value of potential title and general mass text
$this->differenceTitileSize = 5;

// Max share of potential titles in general mass text
$this->maxShareTitle = 15;

// Max symbols in potential name of author or name of book
$this->maxAuthorOrNameLetters = 30;

// Max symbols in potential title
$this->maxTitleLetters = 100;

// Max value of first parts to inspect as author or book-name
$this->maxFirstPats = 30;
$maxFirstParts = $this->maxFirstPats;

// Max value of text parts
$this->maxFirstPats = 1500;

// Min score to detect author
$this->concurrencyAuthor = 20;

// Min score to detect bookname
$this->concurrencyName = 20;

// Minimum difference sizes value of potential title
$this->differenceTitleSize = 5;

// PATTERNS
$patternRusBigLetter = '(А|Б|В|Г|Д|Е|Ё|Ж|З|И|Й|К|Л|М|Н|О|П|Р|С|Т|У|Ф|Х|Ц|Ч|Ш|Щ|Э|Ю|Я)';
$patternEngBigLetter = '[A-Z]';
$patternRusSmallLetter = '(а|б|в|г|д|е|ё|ж|з|и|й|к|л|м|н|о|п|р|с|т|у|ф|х|ц|ч|ш|щ|ъ|ы|ь|э|ю|я)';
$patternEngSmallLetter = '[a-z]';
$patternAllLetters = $patternRusBigLetter.'*'.$patternEngBigLetter.'*'.$patternRusSmallLetter.'*'.$patternEngSmallLetter.'*';

$patternRusWords = '(\s?'.$patternRusSmallLetter.'+\s?)*';
$patternEngWords = '(\s?'.$patternEngSmallLetter.'+\s?)*';
$patternQuote = '("|«|\'|`|“|„)';
$patternUnquote = '("|»|\'|`|”|‟)';
$patternSpots = '(\(|\)|\{|\}|,|\'|;|-|=|–|—)*';
$this->patternSpots = $patternSpots.$patternQuote.'?'.$patternUnquote.'?\s?';

$this->exceptWords = array(
    'от',
    'на',
    'в',
    'к',
    'с'
);

// Patterns to detect key-words in text parts
$patternAuthor = '/^\s?([А,а]втор|[A,a]uthor|АВТОР|AUTHOR).*/';

// Pattern to detect no letter line
$this->patternLetter = '/'.$patternAllLetters.'[0-9]*/';

// Patterns to detect arabic and roman numbers in potential titles
$this->patternArabNum = '/^\s*(\d+\.?){1,'.$this->numPutsIn.'}\s?/';
//$this->patternRomNum = '/^\s*I*V?I*X*L*C*D*M*\s?/';
$this->patternRomNum = '/^\s*((M{1,3})|(D{1,3})|(C{1,3})|(L{1,3})|(X{1,3})|(V{1,3})|(I{1,3}))\s*'.$patternAllLetters.'/';

// Parts patterns to detect puts-in numeric numbers in potential titles
$this->patternBgnArabNum = '(\d+\.?)';
$this->patternEndArabNum =  '\s*'.$patternAllLetters.'$';

// Pattern to detect three stars title
$this->patternTreeStars = '/^.{1,2}\s*\*\s*\*\s*\*\s*$/';

// Pattern to detect potential name of author write union line with name of book
$patternName = '/^\D{2,}\s\D{2,}\.\s/';

// Pattern to detect special symbols in potential name of book
$specialSymbols = '/\w\s?\d|:|-|—|;|«|-I|-V|-X|!$|\?/';

// Pattern to detect serial number
$patternSerial = '/([С,с]ерия|[S,s]erial|СЕРИЯ|SERIAL)/';

// Pattern to detect volume
$patternVolume = '/([Т,т]ом|ТОМ|[V,v]olume|VOLUME)(:|#|№|-|\s|–|—)?\s?(\d+|'.$patternAllLetters.')/';

// Patterns to detect date create document
$patternDateCreateRu = '/(([1-2]?[0-9])|(3[0,1])\s(([Я,я]нв(\.)?(арь|аря)?)|(ЯНВ(\.)?(АРЬ|АРЯ)?)|([Ф,ф]ев(\.)?(раль|раля)?)|(ФЕВ(\.)?(РАЛЬ|РАЛЯ)?)|([М,м]ар(\.)?(т|та)?)|(МАР(\.)?(Т|ТА)?)|([А,а]пр(\.)?(ель|еля)?)|(АПР(\.)?(ЕЛЬ|ЕЛЯ)?)|([М,м]а(й|я)?)|(МА(Й|Я)?)|([И,и]юн(ь|я)?)|(ИЮН(Ь|Я)?)|([И,и]юл(ь|я)?)|(ИЮЛ(Ь|Я)?)|([А,а]вг(\.)?(уст|уста)?)|(АВГ(\.)?(УСТ|УСТА)?)|([С,с]ен(т)?(\.)?(ябрь|ября)?)|(СЕН(Т)?(\.)?(ЯБРЬ|ЯБРЯ)?)|([О,о]кт(\.)?(ябрь|ября)?)|(ОКТ(\.)?(ЯБРЬ|ЯБРЯ)?)|([Н,н]ояб(\.)?(рь|ря)?)|(НОЯБ(\.)?(РЬ|РЯ)?)|([Д,д]ек(\.)?(абрь|абря)?)|(ДЕК(\.)?(АБРЬ|АБРЯ)?))\s)?(((1[0-9]?)|(20))[0-9]{2}\s?((г|гг)|(год(а)?(ы)?)|(Г|ГГ)|(ГОД(А)?(Ы)?))?)\.?/';
$patternDateCreateEn = '/(([1-2]?[0-9])|(3[0,1])\s(([J,j]an(\.)?(uary)?)|(JAN(\.)?(UARY)?)|([F,f]eb(\.)?(ruary)?)|(FEB(\.)?(RUARY)?)|([M,m]ar(\.)?(ch)?)|(MAR(\.)?(CH)?)|([A,a]pr(\.)?(il)?)|(APR(\.)?(IL)?)|([M,m]ay)|(MAY)?)|([J,j]un(e)?)|(JUN(E)?)|([J,j]ul(y)?)|(JUL(Y)?)|([A,a]ug(\.)?(ust)?)|(AUG(\.)?(UST)?)|([S,s]ep(t)?(\.)?(ember)?)|(SEP(T)?(\.)?(EMBER)?)|([O,o]ct(\.)?(ober)?)|(OCT(\.)?(OBER)?)|([N,n]ov(\.)?(ember)?)|(NOV(\.)?(EMBER)?)|([D,d]es(\.)?(ember)?)|(DES(\.)?(EMBER)?))\s)?(((1[0-9]?)|(20))[0-9]{2}\s?((y(ear)?)|(Y(EAR)?))?)\.?/';

// Patterns to detect genre
$patternGenreRu = '^.{1,2}\s*([Б,б]аллад[а,ы]|[Б,б]асн[я,и]|[Б,б]ылин[а,ы]|[В,в]идени[я,е]|[В,в]одевил[ь,и]|[Д,д]рам[а,ы]|[И,и]нтермеди[я,и]|[К,к]омеди[я,и]|[М,м]иф(ы)?|[Н,н]овелл[а,ы]|[О,о]д[а,ы]|[О,о]пус(ы)?|[О,о]черк(и)?|[П,п]ароди[я,и]|[П,п]овест[ь,и]|[П,п]ослание|[П,п]оэм[а,ы]|[П,п]ьес[а,ы]|[Р,р]ассказ(ы)?|[Р,р]оман(ы)?|[С,с]казк([а,и])?|[С,с]кетч(и)?|[С,с]тансы|[Т,т]рагеди[я,и]|[Ф,ф]арс(ы)?|[Э,э]леги[я,и]|[Э,э]пиграмм[а,ы]|[Э,э]пическ[ие,ое]|[Э,э]попе[я,и]|[Э,э]пос(ы)?|[Э,э]ссе)';
$patternGenreEn = '^.{1,2}\s*([B,b]tallad|[B,b]ylina|[C,c]omedy|[D,d]rama|[E,e]legy|[E,e]pic|[E,e]pigram|[E,e]pistle|[E,e]popee|[E,e]pos|[E,e]ssay|[E,e]ssay|[F,f]able|[F,f]arce|[I,i]ntermezzo|[М,м]yth|[N,n]arrative|[N,n]ovel|[O,o]de|[O,o]pus|[P,p]arody|[P,p]iece|[P,p]oem|[R,r]omance|[S,s]ketch|[S,s]tanzas|[S,s]tory|[T,t]ale|[T,t]ragedy|[V,v]audeville|[V,v]ision)s?)';

// Pattern to detect speech
$patternSpeech = '/:\s?'.$patternQuote.'('.$patternRusBigLetter.'|'.$patternEngBigLetter.')+((('.$patternRusWords.')|('.$patternRusWords.'))'.$patternSpots.')*'.$patternUnquote.'/';

// Pattern to detect introduction
$patternIntroduction = '^.{1,2}\s*([В,в]ведени[е,я,ем])|(ВВЕДЕНИ[Е,Я,ЕМ])|([П,п]редислови[е,я,ем])|(ПРЕДИСЛОВИ[Е,Я,ЕМ])|([I,i]ntroduction(s)?)|(INTRODUCTION(S)?)|([P,p]reface(s)?)|(PREFACE(S)?)|([F,f]oreword(s)?)|(FOREWORD(S)?)';

// Pattern to detect annotation
$patternAnnotation = '^.{1,2}\s*([А,а]ннотаци[я,ей,и])|(АННОТАЦИ[Я,ЕЙ,И])|([A,a]nnotation(s)?)|(ANNOTATION(S)?)|([N,n]ote(s)?)|(NOTE(S)?)';

// Pattern to detect "about author"
$patternAboutAuthor = '^.{1,2}\s*([О,о]б\sавтор[е,ах])|(ОБ\sАВТОР[Е,АХ])|([A,a]bout\sauthor(s)?)|(ABOUT\sAUTHOR(S)?)';

// Pattern to detect "from author"
$patternFromAuthor = '^.{1,2}\s*(([О,о]бращение)|(ОБРАЩЕНИЕ))?(\s?([К,к]\sчитател[ю,ям])|(К\sЧИТАТЕЛ[Ю,ЯМ])|(([О,о]т\sавтор[а,ов])|(ОТ\sАВТОР[А,ОВ])))|([О,о]т\sавтор[а,ов])|(ОТ\sАВТОР[А,ОВ])|([F,f]rom\sauthor(s)?)|(FROM\sAUTHOR(S)?)|([A,a]uthor\sappeal)|(AUTHOR\sAPPEAL)';

// Pattern to detect dedication
$patternDedication = '^.{1,2}\s*([П,п]освящ(ение|ается|аю|ем))|(ПОСВЯЩ(ЕНИЕ|АЕТСЯ|АЮ|ЕМ)|([D,d]edication(s)?)|(DEDICATION(S)?))';

// Patter to detect titles as three stars
$this->patternThreeStars = '/^\s*\*{3}$|(\s*\*){3}$/';

// Patterns to detect key-words title in text parts
$this->patternTitle = '/(([В,в]ступление|[Г,г]лава|[Ч,ч]асть|[V,v]al(ue)?|[C,c]hapter|ВСТУПЛЕНИЕ|ГЛАВА|ЧАСТЬ|VAL(UE)?|CHAPTER)|'.$patternFromAuthor.'|'.$patternDedication.'|'.$patternIntroduction.'|'.$patternAnnotation.'|'.$patternAboutAuthor.')\s?\d?/';

// Pattern to detect footnotes
$patternFootNoteBegin = '(\(|\{|\[)\s*';
$patternFootNoteMiddle = '\d+';
$patternFootNoteFinal = '\s*(\)|\}|\])';
$patternFootNote = '/'.$patternFootNoteBegin.$patternFootNoteMiddle.$patternFootNoteFinal.'$/';
$patterFootNoteContent = '/^\s*'.$patternFootNoteBegin.$patternFootNoteMiddle.$patternFootNoteFinal.'\s*'.$patternAllLetters.'/';

// EXTERNAL LIBS
// External libs to detect peoples names, family's, end of family's and geographic lands, country's, city's, towns etc.
$this->libNames = array(
    'lib/names.lib'
);

$this->libFamily_s = array(
    'lib/family1.lib',
    'lib/family2.lib',
    'lib/family3.lib',
    'lib/family4.lib',
    'lib/family5.lib',
    'lib/family6.lib',
    'lib/family7.lib',
    'lib/family8.lib',
    'lib/family9.lib',
    'lib/family10.lib',
    'lib/family11.lib',
    'lib/family12.lib'
);

$this->libFamilyEnds = array(
    'lib/family_ends.lib'
);

$this->libLands = array(
    'lib/lands.lib'
);

$this->libThesaurus = array(
    'lib/thesaurus.lib'
);

// Valid structure elements
$this->validElements = array(
    'name',
    'author',
    'element',
    'inset',
    'title'
);

// RULES
// Rules to detect name of author anf boookname
// Rules to detect author
$this->rulesDetectAuthor = array(
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['bfrRet'];
        },
        'score' => 1 // Associated score value by return line before text-part
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['title'];
        },
        'score' => 2 // Associated score value if text-part marking as title
    ),
    array(
        'rule' => function($pos){
            global $textParts, $familyEnds, $detectPattern;
            return $detectPattern($textParts[$pos]['content'], $familyEnds);
        },
        'score' => 3 // Associated score value if text-part contains knowing end of family
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['italic'];
        },
        'score' => 5 // Associated score value if text-part marking as italic line
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['color'];
        },
        'score' => 7 // Associated score value if text-part marking as colored
    ),
//    array(
//        'rule' => function($pos){
//            global $textParts, $maxSize, $minSize;
//            return (($textParts[$pos-1]['size'] == $maxSize && $maxSize != $minSize) || ($textParts[$pos+1]['size'] == $maxSize && $maxSize != $minSize));
//        },
//        'score' => 11 // Associated score value if previous or next text-part is maximum font size
//    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos-1]['score_name'] > 40;
        },
        'score' => 11 // Associated score value if previous text-part have a score of name > 5
    ),
    array(
        'rule' => function($pos){
            global $textParts, $family_s, $detectPattern;
            return $detectPattern($textParts[$pos]['content'], $family_s);
        },
        'score' => 13 // Associated score value if text-part contains knowing family word
    ),
    array(
        'rule' => function($pos){
            global $textParts, $patternAuthor;
            return preg_match($patternAuthor, $textParts[$pos]['content']);
        },
        'score' => 17 // Associated score value if text-part coincident with $patternAuthor
    ),
    array(
        'rule' => function($pos){
            global $textParts, $names, $detectPattern;
            return $detectPattern($textParts[$pos]['content'], $names);
        },
        'score' => 19 // Associated score value if text-part contains knowing name word
    )
);

// Rules to detect bookname
$this->rulesDetectBookName = array(
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['bold'];
        },
        'score' => 1 // Associated score value if text-part marking as bold
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['title'];
        },
        'score' => 2 // Associated score value if text-part marking as title
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['bfrRet'];
        },
        'score' => 3 // Associated score value by return line before text-part
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return (preg_match('/[0-9]+/', $textParts[$pos]['content']));
        },
        'score' => 5 // Associated score value by return line before next text-part
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return $textParts[$pos]['color'];
        },
        'score' => 7 // Associated score value if text-part marking as colored
    ),
    array(
        'rule' => function($pos){
            global $textParts, $maxSize, $minSize;
            return ($textParts[$pos]['size'] == $maxSize && $maxSize != $minSize);
        },
        'score' => 11 // Associated score value if text-part is maximum font size
    ),
    array(
        'rule' => function($pos){
            global $textParts;
            return (mb_strtoupper($textParts[$pos]['content'], 'UTF-8') == $textParts[$pos]['content']);
        },
        'score' => 13 // Associated score value if text-part is writing caps letter
    ),
    array(
        'rule' => function($pos){
            global $textParts, $lands, $detectPattern;
            return $detectPattern($textParts[$pos]['content'], $lands);
        },
        'score' => 17 // Associated score value if text-part contains knowing land, city's, towns etc
    )
);

// Rules to detect other text-parts
$this->otherRules = array(
    array(
        'val' => 'serial',
        'have_content' => false,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternSerial;
            return (mb_strlen($textPart['content'], 'UTF-8') <= 30 && preg_match($patternSerial, $textPart['content']));
        }
    ),
    array(
        'val' => 'volume',
        'have_content' => false,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternVolume;
            return (mb_strlen($textPart['content'], 'UTF-8') <= 15 && preg_match($patternVolume, $textPart['content']));
        }
    ),
    array(
        'val' => 'date_create',
        'have_content' => false,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternDateCreateRu, $patternDateCreateEn, $maxFirstParts;
            $content = $textPart['content'];
            return (
                mb_strlen($content, 'UTF-8') <= 20 &&
                    ($pos <= $maxFirstParts || $pos >= count($countTextPart)-$maxFirstParts) &&
                    (preg_match($patternDateCreateRu, $content) || preg_match($patternDateCreateEn, $content))
            );
        }
    ),
    array(
        'val' => 'epigraph',
        'have_content' => false,
        'rule' => function($textPart, $pos, $countTextPart){
            global $epigraphs, $epigraphCount, $epigraphFlag;
            if($textPart['justification'] == 'right' && $textPart['italic'] && mb_strlen($textPart['content'], 'UTF-8') < 150){
                $epigraphFlag = true;
                $epigraphs[$epigraphCount][] = $pos;
                return true;
            }else{
                $epigraphCount = $epigraphFlag ? $epigraphCount + 1 : $epigraphCount;
                $epigraphFlag = false;
                return false;
            }
        }
    ),
    array(
        'val' => 'genre',
        'have_content' => false,
        'rule' => function($textPart, $pos, $countTextPart){
            global $maxFirstParts, $patternGenreRu, $patternGenreEn;
            $patternGenreRuCaps = mb_strtoupper($patternGenreRu, 'UTF-8');
            $patternGenreEnCaps = mb_strtoupper($patternGenreEn, 'UTF-8');
            $content = $textPart['content'];
            return ($pos <= $maxFirstParts && mb_strlen($content, 'UTF-8') < 50 && (
                preg_match('/^.*'.$patternGenreRu.'/', $content) ||
                    preg_match('/^.*'.$patternGenreEn.'/', $content) ||
                    preg_match('/^.*'.$patternGenreRuCaps.'/', $content) ||
                    preg_match('/^.*'.$patternGenreEnCaps.'/', $content) ));
        }
    ),
    array(
        'val' => 'introduction',
        'have_content' => true,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternIntroduction;
            $content = $textPart['content'];
            return (mb_strlen($content, 'UTF-8') <= 15 && preg_match('/'.$patternIntroduction.'/', $content));
        }
    ),
    array(
        'val' => 'annotation',
        'have_content' => true,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternAnnotation;
            $content = $textPart['content'];
            return (mb_strlen($content, 'UTF-8') <= 20 && preg_match('/'.$patternAnnotation.'/', $content));
        }
    ),
    array(
        'val' => 'about_author',
        'have_content' => true,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternAboutAuthor;
            $content = $textPart['content'];
            return (mb_strlen($content, 'UTF-8') <= 15 && preg_match('/'.$patternAboutAuthor.'/', $content));
        }
    ),
    array(
        'val' => 'from_author',
        'have_content' => true,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternFromAuthor;
            $content = $textPart['content'];
            return (mb_strlen($content, 'UTF-8') <= 15 && preg_match('/'.$patternFromAuthor.'/', $content));
        }
    ),
    array(
        'val' => 'dedication',
        'have_content' => true,
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternDedication;
            $content = $textPart['content'];
//            echo $content.'<br>';
            return (mb_strlen($content, 'UTF-8') <= 20 && preg_match('/'.$patternDedication.'/', $content));
        }
    )
);

// Rules to detect inline element
$this->inlineRules = array(
    array(
        'val' => 'speech',
        'rule' => function($textPart, $pos, $countTextPart){
            global $patternSpeech, $detectInlineElements;
            return $detectInlineElements($textPart['content'], $patternSpeech, 1);
        }
    )
);

// Rules to detect reassignments elements
$reassignmentsRule = function($reassignPhrase, $type, $startPos){
    $score = 0;
    $maxScore = 0;
    $minDiff = NULL;
    $returnedType = false;
    for($i=0;$i<count($reassignPhrase->was_it);$i++){
        if($reassignPhrase->was_it[$i]->was == $type){
            $middleScore = $reassignPhrase->was_it[$i]->score;
            if($difference = abs($reassignPhrase->was_it[$i]->start_pos-$startPos)) $middleScore = $middleScore/$difference;
            if($middleScore > $maxScore){
                $maxScore = $middleScore;
                $minDiff = $difference;
            }
            $score += $middleScore;
        }
    }
    if($score > 3){
        for($i=0;$i<count($reassignPhrase->turn_to);$i++){
            if(abs($reassignPhrase->turn_to[$i]->start_pos-$startPos) == $minDiff) $returnedType = $reassignPhrase->turn_to[$i]->turn;
        }
    }
    return $returnedType;
};

// TRIGGERS
// Trigger to enable detect size-titles
define('TRIGGER_DETECT_SIZE_TITLE', true);

// Trigger to enable detect titles as bold and return line before
define('TRIGGER_DETECT_BOLD_TITLE', true);

// Trigger to enable detect titles as puts-in numbers
define('TRIGGER_DETECT_NUMBER_TITLE', true);

// Trigger to enable detect titles as contains key-words
define('TRIGGER_DETECT_KEYWORDS_TITLE', true);

// Trigger to enable detect titles as three stars
define('TRIGGER_DETECT_THREESTARS_TITLE', true);

// Trigger to enable second try detect author and bookname wrote in single line
define('TRIGGER_SECOND_TRY', true);

// Trigger to enable detect other structure element
define('TRIGGER_DETECT_OTHER_ELEMENTS', true);

// Trigger to enable detect inline element
define('TRIGGER_DETECT_INLINE_ELEMENTS', true);

// Trigger to enable thesaurus
define('TRIGGER_THESAURUS', true);

// Trigger to enable define elements based on reassignments statistic
define('TRIGGER_DETECT_REASSIGNMENTS', true);

// Trigger to enable define book name or author from filename
define('TRIGGER_DETECT_FROM_FILENAME', true);

// Trigger to enable define footnotes
define('TRIGGER_DETECT_FOOTNOTES', true);
?>