<?

define('SERVER_ROOT', '/local/home/LibOnline/www/');
define('SERVER_URL',  'http://libonline/');
define('SERVER_HOST', 'libonline');

define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'libonline');

define('DEFAULT_FUNCTION', '_default');

// charsets
define('CHARSET_DB', 'UTF8');
define('CHARSET_HTML', 'utf-8');
define('CHARSET_CODE', 'UTF-8');