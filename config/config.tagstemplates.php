<?
$this->ext = '/.xml$/';
$this->aliasing = array(
    '/\/IMAGES\//' => PATH_UPLOAD_IMAGES,
    '/\/MULTIMEDIA\//' => PATH_UPLOAD_MULTIMEDIA,
    '/\/CLIPART\//' => PATH_CLIPART,
    '/\/CLIPART_VIDEO\//' => PATH_CLIPART_VIDEO,
    '/\/BASE_THEMES\//' => PATH_BASE_TEMPLATES,
    '/\/USER_THEMES\//' => PATH_USER_TEMPLATES,
    '/\/BASE_DROP_CAPS\//' => PATH_BASE_DROP_CAPS,
    '/\/USER_DROP_CAPS\//' => PATH_USER_DROP_CAPS
);

$pageTags = array(
    'cover_author_style',
    'cover_name_style',
    'illustration',
    'cover_pic'
);

$templateTags = array(
    'illustration',
    'blockquotes',
    'text_style',
    'page_style',
    'annotation',
    'title_style',
    'inset',
    'latex'
);

$illustrationTags = array(
    'description',
    'title',
    'fullpage'
);

$this->skipSubTags = array(
    'root' => array(
        'audio',
        'video',
        'illustration',
        'inset',
        'divscript',
        'cover_pic',
        'dropcap',
        'blockquotes',
        'serial',
        'volume',
        'date_create',
        'epigraph',
        'genre',
        'introduction',
        'annotation',
        'about_author',
        'from_author',
        'dedication',
        'introduction_content',
        'latex',
        'table',
        'annotation_content',
        'about_author_content',
        'from_author_content',
        'dedication_content',
        'custom_widgets',
        'colon_title'
    ),

    'root->title_style' => array(
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
        'h7',
        'h8',
    ),

    'root->custom_widgets' => array(
        'latex',
        'table',
        'video'
    )
);

$this->multiTags = array(
    'root->special_page->first_page->page' => $pageTags,
    'root->special_page->last_page->page' => $pageTags,
    'root->special_page->page_templates->template' => $templateTags,
    'root->illustration->illustration' => $illustrationTags
);

$this->skipTags = array(
    'id',
    'delimiter',
    'selected',
    'column',
);

$this->cssRootTags = array(
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'h7',
    'h8',
    'page_style',
    'style',
    'cover_author_style',
    'cover_name_style',
    'text_style',
    'colon_number'
);


$this->cssTags = array(
    'animation-delay',
    'background',
    'background-attachment',
    'background-clip',
    'background-color',
    'background-image',
    'background-origin',
    'background-position',
    'background-position-x',
    'background-position-y',
    'background-repeat',
    'background-size',
    'border',
    'border-bottom',
    'border-bottom-color',
    'border-bottom-left-radius',
    'border-bottom-right-radius',
    'border-bottom-style',
    'border-bottom-width',
    'border-collapse',
    'border-color',
    'border-image',
    'border-left',
    'border-left-color',
    'border-left-style',
    'border-left-width',
    'border-radius',
    'border-right',
    'border-right-color',
    'border-right-style',
    'border-right-width',
    'border-spacing',
    'border-style',
    'border-top',
    'border-top-color',
    'border-top-left-radius',
    'border-top-right-radius',
    'border-top-style',
    'border-top-width',
    'border-width',
    'bottom',
    'box-shadow',
    'box-sizing',
    'caption-side',
    'clear',
    'clip',
    'color',
    'column-count',
    'column-gap',
    'column-rule',
    'column-width',
    'columns',
    'content',
    'counter-increment',
    'counter-reset',
    'cursor',
    'direction',
    'display',
    'empty-cells',
    'filter',
    'float',
    'font',
    'font-family',
    'font-size',
    'font-stretch',
    'font-style',
    'font-variant',
    'font-weight',
    'hasLayout',
    'height',
    'hyphens',
    'image-rendering',
    'left',
    'letter-spacing',
    'line-height',
    'list-style',
    'list-style-image',
    'list-style-position',
    'list-style-type',
    'margin',
    'margin-bottom',
    'margin-left',
    'margin-right',
    'margin-top',
    'max-height',
    'max-width',
    'min-height',
    'min-width',
    'opacity',
    'orphans',
    'outline',
    'outline-color',
    'outline-offset',
    'outline-style',
    'outline-width',
    'overflow',
    'overflow-x',
    'overflow-y',
    'padding',
    'padding-bottom',
    'padding-left',
    'padding-right',
    'padding-top',
    'page-break-after',
    'page-break-before',
    'page-break-inside',
    'position',
    'quotes',
    'resize',
    'right',
    'scrollbar-3dlight-color',
    'scrollbar-arrow-color',
    'scrollbar-base-color',
    'scrollbar-darkshadow-color',
    'scrollbar-face-color',
    'scrollbar-highlight-color',
    'scrollbar-shadow-color',
    'scrollbar-track-color',
    'tab-size',
    'table-layout',
    'text-align',
    'text-align-last',
    'text-decoration',
    'text-decoration-color',
    'text-decoration-line',
    'text-decoration-style',
    'text-indent',
    'text-overflow',
    'text-shadow',
    'text-transform',
    'top',
    'transform',
    'transform-origin',
    'transform-style',
    'transition',
    'transition-delay',
    'transition-property',
    'transition-timing-function',
    'unicode-bidi',
    'vertical-align',
    'visibility',
    'white-space',
    'widows',
    'width',
    'word-break',
    'word-spacing',
    'word-wrap',
    'writing-mode',
    'z-index',
    'zoom'
);

$this->defaultTags = array(
//        'dropcap',
    'blockquotes',
    'serial',
    'volume',
    'date_create',
    'epigraph',
    'genre',
    'introduction',
    'annotation',
    'about_author',
    'from_author',
    'dedication',
    'introduction_content',
    'annotation_content',
    'about_author_content',
    'from_author_content',
    'dedication_content'
);

$this->mergeWidgets = array(
    'custom_widgets'
);



$this->typeTemplates = array('book');
?>