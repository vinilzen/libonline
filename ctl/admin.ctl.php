<?
	require_once('ctl/base.ctl.php');
	require_once('view/admin.view.php');
	
	class AdminCtl extends BaseCtl{
		public function _default(){
            $this->redirectNotAdmin();

			$view = new AdminView();
			$view->_default();
		}
	}
?>