<?
    require_once('inc/constant.inc.php');

	abstract class BaseCtl{
		protected $params = array();
		protected $offset = 12;
		protected $step;
		protected $count;
        private $sessionTime = 604800;

        protected $resultJson = array();
        protected $bookUid;
        protected $themeUid;
        protected $data;
        protected $content;

		function __construct(){
            session_start();
            if($_SESSION['session_id']) $this->handlerSession();
            else {
                $msg = 'Please, enter your login!';
                if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                    header("HTTP/1.1 401 Unauthorized");
                    die(json_encode(array('error'=>$msg)));
                } else {
                    header('Location: '.Dispatcher::getURI('login', '', array('message' => $msg)));
                }
            }
            $this->params = Dispatcher::getParams();
        }

        private function sessionDestroy(){
            session_destroy();
            unset($_SESSION['session_id']);
            unset($_SESSION['session_time']);
            unset($_SESSION['uid']);

            $msg = 'Your session has expired!';

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                header("HTTP/1.1 401 Unauthorized");
                die(json_encode(array('error'=>$msg)));
            } else {
                //  setcookie('lib_user_uid');
                header('Location: '.Dispatcher::getURI('login', '', array('message' => $msg)));
            }
		}

		private function handlerSession(){
			if ($_SESSION['session_id'] != session_id() || time() > $_SESSION['session_time'] || !$_COOKIE['lib_user_uid'])
                $this->sessionDestroy();
            else $_SESSION['session_time'] = time() + $this->sessionTime;
		}

//		protected function pagenator(MainModel $model){
//			$this->count = $model->sqlCount()->getCount();
//			if(!$this->params['page']) $this->params['page'] = 1;
//			$this->step = ($this->params['page']-1)*$this->offset;
//		}

        protected function setBookParams($checkTheme=true, $addCheckParam=NULL){
            if(!isset($_COOKIE['lib_user_uid']) || !$_COOKIE['lib_user_uid']) $this->resultJson[]['error'] = 'Please, enable your COOKIE and login!';
            if(!$this->params['book_uid']) $this->resultJson[]['error'] = 'Not exist "book_uid" in incoming params';
            if($checkTheme && !$this->params['theme_uid']) $this->resultJson[]['error'] = 'Not exist "theme_uid" in incoming params';
            if($addCheckParam){
                if(is_array($addCheckParam)){
                    for($i=0;$i<count($addCheckParam);$i++){
                        if(!$this->params[$addCheckParam[$i]]) $this->resultJson[]['error'] = 'Not exist "'.$addCheckParam[$i].'" in incoming params';
                    }
                }elseif(!$this->params[$addCheckParam]) $this->resultJson[]['error'] = 'Not exist "'.$addCheckParam.'" in incoming params';
            }
            if($this->params['data'] && !json_decode($this->params['data'])) $this->resultJson[]['error'] = 'Invalid json format in "data" param';
            if(!sizeof($this->resultJson)){
                $this->bookUid = $this->params['book_uid'];
                $this->themeUid = $this->params['theme_uid'];
                $this->data = $this->params['data'];
                $this->content = $this->params['content'];
                return true;
            }else return false;
        }

        protected function showResult($die=false){
            header('Content-type: application/json');
            echo json_encode($this->resultJson);
            if($die) die;
        }

        protected function setFileName($bookUID){
            return $_SERVER['DOCUMENT_ROOT'].PATH_BOOKS.'book'.$bookUID.'.json';
        }

        protected function checkExistFile($file){
            return file_exists($file);
        }

        protected function getFolders($path, $type, $dirIgnore=false, $extCut=NULL, $ignoreExt=NULL){
            $result = array();
            if(!is_dir($path)) $result['error'] = 'Not exist folder "'.$path.'"';
            else{
                $objs = glob($path."*");
                $count = 0;
                foreach($objs as $obj){
                    if( (!$dirIgnore || !is_dir($obj)) && !preg_match('/\.'.$ignoreExt.'$/', $obj) ) {
                        $withExt = str_replace($path, '', $obj);
                        if($extCut) $withoutExt = str_replace($extCut, '', $withExt);

                        $result[$count]['name'] = $extCut ? $withoutExt : $withExt;
                        $result[$count]['type'] = is_dir($obj) ? 'dir' : $type;
                        $result[$count]['full_path'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $path);
                        $count++;
                    }
                }
            }
            return $result;
        }

        protected function redirectNotAdmin(){
            if(!$this->checkAdmin()){
                header('Location: '.Dispatcher::getURI('login', '', array('message' => 'Your must enter as administrator!')));
                return false;
            }else return true;
        }

        protected function checkAdmin(){
            return ($_COOKIE['lib_user_uid'] == ADMIN_PASS);
        }
	}

?>