<?
	require_once('ctl/base.ctl.php');
    require_once('models/books.model.php');
	
	class BooksCtl extends BaseCtl{
        private $book;
        private $statistic = array();

        function __construct(){
            parent::__construct();
            $this->book = new BooksModel();
        }

		public function _default(){
            $this->book->sqlSelect()->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])->exec();

            $books = $this->book->getField('book_uid')->value;
            $names = $this->book->getField('name')->value;
            $authors = $this->book->getField('author')->value;

            for($i=0;$i<count($books);$i++){
                $this->resultJson[] = array('book_uid' => $books[$i], 'name' => $names[$i], 'author' => $authors[$i]);
            }
            $this->showResult();
		}

//        public function addThesaurusWord(){
//            if($this->setBookParams(false, 'word')){
//                $this->book->sqlSelect('thesaurus_words')->
//                    sqlFilter('book_uid', $this->params['book_uid'])->
//                    sqlFilterAnd()->
//                    sqlFilter('user_uid', $_COOKIE['lib_user_uid'])->
//                    exec();
//
//                $words = json_decode($this->book->getField('thesaurus_words')->value[0]);
//
//                $match = false;
//                foreach($words as $word){
//                    $this->resultJson[] = $word;
//                    if($word == $this->params['word']) $match = true;
//                }
//
//                if(!$match){
//                    $this->resultJson[] = $this->params['word'];
//                    $this->book
//                        ->sqlUpdate(array('thesaurus_words' => json_encode($this->resultJson)))
//                        ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
//                        ->sqlFilterAnd()
//                        ->sqlFilter('book_uid', $this->bookUid)
//                        ->exec();
//                }
//            }
//            $this->showResult();
//        }

        public function insertBook($name, $author, $bookUID, $elements, $content, $statistic){
            $this->book
                ->sqlInsert(array(
                'user_uid' => $_COOKIE['lib_user_uid'],
                'book_uid' => $bookUID,
                'name' => $name,
                'author' => $author,
                'elements' => json_encode($elements),
                'content' => json_encode($content),
                'statistic' => json_encode($statistic)
            ))->exec();
        }

        public function saveBook(){
            if($this->setBookParams(false)){
                $this->book->sqlSelect()->sqlFilter('book_uid', $this->bookUid)->exec();
                $saveElements = json_decode($this->book->getField('elements')->value[0]);

                if(!$this->book->getField('id')->value[0]) $this->resultJson = array('error' => 'The Book UID '.$this->bookUid.' not exist in DB');
                else{
                    $updateFields = array();
                    if($this->params['name']) $updateFields['name'] = $this->params['name'];
                    if($this->params['author']) $updateFields['author'] = $this->params['author'];
                    if($this->params['elements']) $updateFields['elements'] = $this->params['elements'];
                    if($this->params['content']) $updateFields['content'] = json_encode($this->params['content']);
                    if($this->params['html']) $updateFields['html'] = $this->params['html'];

                    if(!sizeof($updateFields)) $this->resultJson = array('error' => 'Not exist no one of "name", "author", "elements" or "content" in incoming params');
                    elseif($updateFields['elements'] && !$elements = json_decode($updateFields['elements'])) $this->resultJson = array('error' => 'Invalid json format in "elements" param');
                    else{
                        if($saveElements && $elements){
                            $lastId = (int)$saveElements[count($saveElements)-1]->id + 1;
                            for($w=0;$w<count($elements);$w++){
                                if(!$elements[$w]->id){
                                    $elements[$w]->id = $lastId;
                                    $saveElements[] = $elements[$w];
                                    $lastId++;
                                }else{
                                    for($i=0;$i<count($saveElements);$i++){
                                        if($saveElements[$i]->id == $elements[$w]->id){
                                            if($elements[$w]->delete) array_splice($saveElements, $i, 1);
                                            else{
                                                $saveElements[$i]->type = $elements[$w]->type;
                                                $saveElements[$i]->pos_start = $elements[$w]->pos_start;
                                                $saveElements[$i]->pos_end = $elements[$w]->pos_end;

                                                if($elements[$w]->typeLvl) $saveElements[$i]->typeLvl = $elements[$w]->typeLvl;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            $updateFields['elements'] = json_encode($saveElements);
                        }
                        $this->book
                            ->sqlUpdate($updateFields)
                            ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
                            ->sqlFilterAnd()
                            ->sqlFilter('book_uid', $this->bookUid)
                            ->exec();
                        $this->resultJson = array('success' => 1);
                    }
                }
            }
            $this->showResult();
        }

        public function getContent(){
            if($this->setBookParams(false)){
                $this->book
                    ->sqlSelect(array('elements', 'content'))
                    ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
                    ->sqlFilterAnd()
                    ->sqlFilter('book_uid', $this->bookUid)
                    ->exec();
                $result =
                    '{'.
                        '"elements":'.$this->book->getField('elements')->value[0].','.
                        '"content":'.$this->book->getField('content')->value[0].
                    '}';
                if($result) echo $result;
                else{
                    $this->resultJson['error'] = 'Not exist book "'.$this->bookUid.'"';
                    $this->showResult();
                }
            }else $this->showResult();
        }

        public function setReassignments(){
            if($this->setBookParams(false, 'reassign')){
                if(!is_array($reassign = json_decode($this->params['reassign']))) $this->resultJson['error'] = 'Wrong format parameter "reassign"';
                else{
                    $this->book->sqlSelect('reassignments')
                        ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
                        ->sqlFilterAnd()
                        ->sqlFilter('book_uid', $this->bookUid)
                        ->exec();

                    $this->statistic = json_decode($this->book->getField('reassignments')->value[0]);
                    $errorFlag = false;

                    foreach($reassign as $key => $value){
                        $coincidence = false;
                        $endPos = $value->start_pos + mb_strlen($value->phrase);
                        if(count($this->statistic)){
                            for($i=0;$i<count($this->statistic);$i++){
                                if(
                                    !$value->phrase    ||
                                    !$value->turn      ||
                                    !$value->was  ||
                                    !$value->start_pos ||
                                    !is_int(intval($value->start_pos))
                                ){
                                    $this->resultJson[]['error'] = 'Incomplete or wrong params in position #'.$key;
                                    $errorFlag = true;
                                }elseif($value->phrase == $this->statistic[$i]->phrase){
                                    $coincidence = true;

                                    $concurFlag = false;
                                    foreach($this->statistic[$i]->turn_to as $k => $turned){
                                        if($turned->start_pos == $value->start_pos && $turned->turn == $value->turn){
                                            $concurFlag = true;
                                            $this->statistic[$i]->turn_to[$k]->score++;
                                            break;
                                        }
                                    }
                                    if(!$concurFlag){
                                        $this->statistic[$i]->turn_to[] = array(
                                            'turn' => $value->turn,
                                            'score' => 1,
                                            'start_pos' => $value->start_pos,
                                            'end_pos' => $endPos
                                        );
                                    }

                                    $concurFlag = false;
                                    foreach($this->statistic[$i]->was_it as $k => $was_it){
                                        if($was_it->start_pos == $value->start_pos && $was_it->was == $value->was){
                                            $concurFlag = true;
                                            $this->statistic[$i]->was_it[$k]->score++;
                                            break;
                                        }
                                    }
                                    if(!$concurFlag){
                                        $this->statistic[$i]->was_it[] = array(
                                            'was' => $value->was,
                                            'score' => 1,
                                            'start_pos' => $value->start_pos,
                                            'end_pos' => $endPos
                                        );
                                    }
                                }
                            }
                        }
                        if(!$coincidence){
                            $newPos = count($this->statistic);
                            $this->statistic[$newPos]->id = $newPos + 1;
                            $this->statistic[$newPos]->phrase = $value->phrase;
                            $this->statistic[$newPos]->turn_to = array(
                                array(
                                    'turn' => $value->turn,
                                    'score' => 1,
                                    'start_pos' => $value->start_pos,
                                    'end_pos' => $endPos
                                )
                            );
                            $this->statistic[$newPos]->was_it = array(
                                array(
                                    'was' => $value->was,
                                    'score' => 1,
                                    'start_pos' => $value->start_pos,
                                    'end_pos' => $endPos
                                )
                            );
                        }
                    }

                    if(!$errorFlag){
                        $this->book
                            ->sqlUpdate(array('reassignments' => json_encode($this->statistic)))
                            ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
                            ->sqlFilterAnd()
                            ->sqlFilter('book_uid', $this->bookUid)
                            ->exec();
                        $this->resultJson['success'] = true;
                    }
                }
            }
            $this->showResult();
        }

        public function getStatistic(){
            if($this->setBookParams(false)){
                $this->book
                    ->sqlSelect('statistic')
                    ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
                    ->sqlFilterAnd()
                    ->sqlFilter('book_uid', $this->bookUid)
                    ->exec();
                $this->resultJson = $this->book->getField('statistic')->value;
            }
            $this->showResult();
        }

        public function getReassignStatistic($insideReturn=false){
            $this->book->sqlSelect('reassignments')->exec();
            if($this->book->getField('reassignments')->value[0]){
                for($i=0;$i<$this->book->getNumRows();$i++){
                    $this->mountReassignStatistic(json_decode($this->book->getField('reassignments')->value[$i]));
                }
            }
            if($insideReturn) return $this->statistic;
            else{
                $this->resultJson = $this->statistic;
                $this->showResult();
            }
        }

        public function getBooksStatistic(){
            $this->redirectNotAdmin();
            $this->book->sqlSelect(array('book_uid','name','author','content','statistic'))->exec();
            for($i=0;$i<$this->book->getNumRows();$i++){
                $this->resultJson[$i]['book_uid'] = $this->book->getField('book_uid')->value[$i];
                $this->resultJson[$i]['name'] = $this->book->getField('name')->value[$i];
                $this->resultJson[$i]['author'] = $this->book->getField('author')->value[$i];

                $this->resultJson[$i]['statistic'] = array();

                foreach(json_decode($this->book->getField('statistic')->value[$i]) as $key => $val){
                    $content = json_decode($this->book->getField('content')->value[$i]);
                    $startPos = intval($val->pos_start);
                    $endPos = intval($val->pos_end);

                    $this->resultJson[$i]['statistic'][$key]['start_pos'] = $startPos;
                    $this->resultJson[$i]['statistic'][$key]['end_pos'] = $endPos;
                    $this->resultJson[$i]['statistic'][$key]['type'] = $val->type;
                    $this->resultJson[$i]['statistic'][$key]['content'] = mb_substr($content, $startPos-1, $endPos-$startPos+1, 'UTF-8');
                }
            }
            $this->showResult();
        }

        private function mountReassignStatistic($bookStatistic){
            foreach($bookStatistic as $statistic){
                $coincidence = false;
                if(count($this->statistic)){
                    for($i=0;$i<count($this->statistic);$i++){
                        if($statistic->phrase == $this->statistic[$i]->phrase){
                            $coincidence = true;

                            foreach($statistic->turn_to as $turnedOne){
                                $concurFlag = false;
                                foreach($this->statistic[$i]->turn_to as $k => $turnedTwo){
                                    if($turnedOne->turn == $turnedTwo->turn && $turnedOne->start_pos == $turnedTwo->start_pos){
                                        $concurFlag = true;
                                        $this->statistic[$i]->turn_to[$k]->score++;
                                        break;
                                    }
                                }
                                if(!$concurFlag){
                                    $count = count($this->statistic[$i]->turn_to);
                                    $this->statistic[$i]->turn_to[$count] = new stdClass();

                                    $this->statistic[$i]->turn_to[$count]->turn = $turnedOne->turn;
                                    $this->statistic[$i]->turn_to[$count]->score = $turnedOne->score;
                                    $this->statistic[$i]->turn_to[$count]->start_pos = $turnedOne->start_pos;
                                    $this->statistic[$i]->turn_to[$count]->end_pos = $turnedOne->end_pos;
                                }
                            }

                            foreach($statistic->was_it as $wasItOne){
                                $concurFlag = false;
                                foreach($this->statistic[$i]->was_it as $k => $wasItTwo){
                                    if($wasItOne->was == $wasItTwo->was && $wasItOne->start_pos == $wasItTwo->start_pos){
                                        $concurFlag = true;
                                        $this->statistic[$i]->was_it[$k]->score++;
                                        break;
                                    }
                                }
                                if(!$concurFlag){
                                    $count = count($this->statistic[$i]->was_it);
                                    $this->statistic[$i]->was_it[$count] = new stdClass();

                                    $this->statistic[$i]->was_it[$count]->was = $wasItOne->was;
                                    $this->statistic[$i]->was_it[$count]->score = $turnedOne->score;
                                    $this->statistic[$i]->was_it[$count]->start_pos = $wasItOne->start_pos;
                                    $this->statistic[$i]->was_it[$count]->end_pos = $wasItOne->end_pos;
                                }
                            }
                        }
                    }
                }
                if(!$coincidence){
                    $newId = count($this->statistic) + 1;
                    $statistic->id = $newId;
                    $this->statistic[] = $statistic;
                }
            }
        }
	}
?>