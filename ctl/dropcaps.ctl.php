<?
require_once('ctl/base.ctl.php');

class DropCapsCtl extends BaseCtl{
    private $path;
    private $nameConfig;
    private $configFile;
    private $config;
    private $userUID;

    public function getBaseConfig(){
        $this->path = PATH_DROP_CAPS.PATH_BASE_DROP_CAPS;
        $this->getConfig();
    }

    public function getCustomConfig(){
        $this->path = PATH_DROP_CAPS.PATH_USER_DROP_CAPS.$this->userUID.'/';
        $this->getConfig();
    }

    private function getConfig(){
        if($this->setAndCheckDropCap()){
            $result = '{';
            foreach($this->config as $liter => $value){
                $result .= "\"".$liter."\":\"".$value."\",";
            }
            $result = substr($result, 0, strlen($result)-1);
            $result .= '}';
            echo $result;
//            $this->showResult();
        }else $this->showResult();
    }

    public function validateConfig($name){
        $this->nameConfig = $name;
        if($this->setAndCheckDropCap()){
            if(count($this->config)){
                foreach($this->config as $tag => $val){
                    if($val){
                        if(!preg_match('/^\d{1,3}x\d{1,3};\d{1,3}x\d{1,3}$/', $val)){
                            $this->resultJson['error'] = 'In tag "'.$tag.'" contains non-format data';
                            return false;
                        }
                    }else{
                        $this->resultJson['error'] = 'The tag "'.$tag.'" cannot be empty';
                        return false;
                    }
                }
            }else{

            } $this->resultJson['error'] = 'The config cannot be empty';
        }else return false;
        return true;
    }

    private function setAndCheckDropCap(){
        $this->userUID = $_COOKIE['lib_user_uid'];
        if(!$this->path) $this->path = $this->userUID == ADMIN_PASS ? PATH_DROP_CAPS.PATH_BASE_DROP_CAPS : PATH_DROP_CAPS.PATH_USER_DROP_CAPS.$this->userUID.'/';
        $this->nameConfig = $this->nameConfig ? $this->nameConfig : $this->params['config'];
        $this->configFile = $_SERVER['DOCUMENT_ROOT'].$this->path.$this->nameConfig;

        if(!$this->params['config'] && !$this->nameConfig){
            $this->resultJson['error'] = 'Not exist param "config" in incoming params';
            return false;
        }elseif(!$this->checkExistFile($this->configFile)){
            $this->resultJson['error'] = 'Not exist "'.$this->path.$this->nameConfig.'" file drop cap config';
            return false;
        }elseif(!@$this->config = simplexml_load_file($this->configFile)){
            $this->resultJson['error'] = 'Error parsing file drop cap config "'.$this->path.$this->nameConfig.'"';
            return false;
        }else return true;
    }

    public static function setThemePath(){
        $path = self::checkAdmin() ? PATH_DROP_CAPS.PATH_BASE_DROP_CAPS : PATH_DROP_CAPS.PATH_USER_DROP_CAPS;
        return $path;
    }
}
?>