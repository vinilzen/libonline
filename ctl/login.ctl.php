<?
    require_once('models/uids.model.php');
    require_once('models/books.model.php');
    require_once('inc/constant.inc.php');
	require_once('view/login.view.php');
	
	class LoginCtl{
        private $sessionTime = 604800;

		public function _default(){
            $this->userSession();
            //setcookie("lib_user_uid", "", time() - 300);

//            $savedBooks = new BooksModel();
//            $savedBooks->sqlSelect('book_uid')->exec();
//            $objs = glob($_SERVER['DOCUMENT_ROOT'].PATH_UPLOAD.'*');
//            foreach($objs as $obj){
//                $time = (int)str_replace($_SERVER['DOCUMENT_ROOT'].PATH_UPLOAD.'book', '', $obj);
//                $timeDifference = time() - $time;
//                if(!in_array($time, $savedBooks->getField('book_uid')->value) && $timeDifference > 86400){
//                    @unlink($obj.'/word/document.xml');
//                    @rmdir($obj.'/word');
//                    @rmdir($obj);
//                }
//            }

            $view = new LoginView();
            $view->_default($_GET['message']);
		}

        public function enter(){
            if(!$_POST['login']) header('Location: '.Dispatcher::getURI('login', '', array('message' => 'You must enter login!')));
            else{
                $login = md5($_POST['login']);
                if($login == ADMIN_PASS){
                    setcookie("lib_user_uid", ADMIN_PASS, time() + $this->sessionTime, "/");
                    header('Location: '.Dispatcher::getURI('admin'));
                }else{
                    $uids = new UidsModel();
                    $uids->sqlSelect()->sqlFilter('uid', $login)->exec();

                    if(!$uids->getField('uid')->value[0]) $uids->sqlInsert(array('uid' => $login))->exec();
                    setcookie("lib_user_uid", $login, time() + $this->sessionTime, "/");
                    header('Location: '.Dispatcher::getURI('index'));
                }
            }
        }

        private function userSession(){
            session_start();
            $_SESSION['session_id'] = session_id();
			$_SESSION['session_time'] = time() + $this->sessionTime;
		}
	}
?>