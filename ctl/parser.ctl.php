<?
//    require_once('models/thesaurus.model.php');
require_once('ctl/base.ctl.php');
require_once('ctl/books.ctl.php');
//    require_once('ctl/thesaurus.ctl.php');

abstract class Element{
    protected static $elements = array();
    protected static $statistic = array();
    protected static $elementsCount = 0;
    protected static $content = '';
    protected static $inlineElements = array();
    protected static $inlineElementsCount = 0;
    protected static $name = NULL;
    protected static $authors = array();
//        protected static $thesaurus = array();
    protected static $currElement = NULL;

    protected static $parser = NULL;
    protected static $reassignmentsPhrases = NULL;
    protected static $footNotesTypeOne = array();
    protected static $footNotesTypeTwo = array();
    protected static $footNotesInlineContent = array();

    protected static $pathFile;

    protected $validElements = array();

    protected static $bookUID;

    function __construct($pathFile=NULL, $bookUID=NULL){
        require_once('config/config.parser.php');

        self::$pathFile = $pathFile ? $pathFile : self::$pathFile;

        if($bookUID) self::$bookUID = $bookUID;
        if(TRIGGER_DETECT_REASSIGNMENTS && !self::$reassignmentsPhrases){
            $books = new BooksCtl();
            self::$reassignmentsPhrases = $books->getReassignStatistic(true);
        }
    }

    public function resetData(){
        self::$elements = array();
        self::$elementsCount = 0;
        self::$inlineElements = array();
        self::$inlineElementsCount = 0;
        self::$name = NULL;
        self::$authors = array();
        self::$currElement = NULL;
    }

    public function detectBlockElements($data){
        include('config/config.parser.php');
        $curContent = $data['content'];

        if($data['potential_foot_note']){
            self::$footNotesInlineContent[] = array(
                'id' => $data['potential_foot_note'],
                'content' => $curContent
            );
            return;
        }

        $id = $data['id'];
        $curInlineElements = $data['inline'];
//            $thesaurus = $data['thesaurus'];
        $origin = $data['origin'];
        $level = $data['title'];
        $footNotes = $data['foot_note'];
        $footNotesInline = $data['foot_note_inline'];

        if($curContent){
            $type = false;
            foreach($this->validElements as $element){
                if($data[$element]){
                    $type = $element;

                    if($type == 'name') self::$name = $curContent;
                    elseif($type == 'author') self::$authors[] = $curContent;
                    elseif($type == 'title') $origin = $origin ? $origin : 'style system';

                    $type = $data['element'] ? $data['element'] : $type;

                    $this->setElement($id, $type, $curContent, $origin, $curInlineElements, $footNotes, $footNotesInline, $level);
                    self::$currElement = ($data['have_content'] && $data['element']) ? $data['element'].'_content' : NULL;
                    break;
                }
            }

            if(!$type){
                $type = self::$currElement ? self::$currElement : 'paragraph';
                $this->setElement($id, $type, $curContent, NULL, $curInlineElements, $footNotes, $footNotesInline);
            }
        }
    }

    protected function setElement($id, $type, $data, $origin, $inlineElements, $footNotes, $footNotesInline, $level=1){}

    public function setStatistic($type, $origin, $startPos, $endPos){
        $count = count(self::$statistic);
        self::$statistic[$count]['type'] = $type;
        self::$statistic[$count]['origin'] = $origin;
        self::$statistic[$count]['pos_start'] = $startPos;
        self::$statistic[$count]['pos_end'] = $endPos;
    }

    protected function setInlineElement($inlineElements, $posStart){
        if(!$inlineElements || !is_array($inlineElements) || !count($inlineElements)) return;

        foreach($inlineElements as $element){
            if($element['val'] && is_array($element['coordinates'])){
                foreach($element['coordinates'] as $value){
                    $newPosStart = $posStart + 1 + $value['pos_start'];
                    $newPosEnd = $posStart + 1 + $value['pos_end'];
                    $type = $element['val'];

                    $this->setStatistic($type, 'inline element system', $newPosStart, $newPosEnd);

                    self::$inlineElements[self::$inlineElementsCount]['id'] = self::$inlineElementsCount + 1;
                    self::$inlineElements[self::$inlineElementsCount]['type'] = $type;
                    self::$inlineElements[self::$inlineElementsCount]['pos_start'] = $newPosStart;
                    self::$inlineElements[self::$inlineElementsCount]['pos_end'] = $newPosEnd;
                    self::$inlineElementsCount++;
                }
            }
        }
    }

    public function setElementText($data){
        $tagsArray = array('/<b>/', '/<\/b>/', '/<i>/', '/<\/i>/', '/<span style=".*">/', '/<\/span>/');
        return preg_replace($tagsArray, '', $data);
    }

    public function startEndPos($data=NULL){
        $posStart = mb_strlen(self::$content, 'UTF-8');
        return ($data ? $posStart + mb_strlen($this->setElementText($data), 'UTF-8') : $posStart);
    }

    protected function setReassignType($data, $type, $posStart){
        global $reassignmentsRule;
        $newType = false;
        if(is_array(self::$reassignmentsPhrases) && count(self::$reassignmentsPhrases)){
            foreach(self::$reassignmentsPhrases as $currentPhrase){
                if($currentPhrase->phrase == $data){
                    $newType = $reassignmentsRule($currentPhrase, $type, $posStart);
                    break;
                }
            }
        }
        return $newType;
    }
}


class CharData extends Element{
    protected function setElement($id, $type, $data, $origin, $inlineElements, $footNotes, $footNotesInline, $level=1){
        $posStart = $this->startEndPos();
        $posEnd = $this->startEndPos($data);
        $newType = $this->setReassignType($data, $type, $posStart);

        self::$elements[self::$elementsCount]['id'] = $id ? $id : self::$elementsCount + 1;
        self::$elements[self::$elementsCount]['pos_start'] = $posStart + 1;
        self::$elements[self::$elementsCount]['pos_end'] = $posEnd;

//            $words = explode(' ', $data);
//            $prgfPos = 1;
//            $totalPos = $posStart + 1;

//            foreach($words as $word){
//                $currentWord = preg_replace('/'.$this->patternSpots.'/', '', str_replace('<br />', '', str_replace('/>', '', strip_tags($word))));
//
//                $prgfPos = $prgfPos + mb_strlen($word, 'UTF-8') + 1;
//                $totalPos = $totalPos + $prgfPos - 1;
//
//                if($currentWord){
//                    self::$thesaurus[] = array(
//                        'book_uid' => self::$bookUID,
//                        'word' => $currentWord,
//                        'paragraph_pos' => $prgfPos,
//                        'text_pos' => $totalPos
//                    );
//                }
//            }

        if($newType){
            self::$elements[self::$elementsCount]['type'] = $newType;
            $origin = 'self learning system';
        }else self::$elements[self::$elementsCount]['type'] = $type;

        if(self::$elements[self::$elementsCount]['type'] == 'title') self::$elements[self::$elementsCount]['typeLvl'] = $level;

        if($footNotes && is_array($footNotes)){
            foreach($footNotes as $footNote){
                self::$footNotesTypeOne[] = array(
                    'id' => $footNote['id'],
                    'pos_start' => $footNote['start_pos'] + $posStart
                );
            }
        }

        if($footNotesInline && is_array($footNotesInline)){
            foreach($footNotesInline as $footNote){
                self::$footNotesTypeTwo[] = array(
                    'id' => $footNote['id'],
                    'pos_start' => $footNote['start_pos'] + $posStart
                );
            }
        }

        self::$elementsCount++;
        self::$content .= $this->setElementText($data);

        if($inlineElements) $this->setInlineElement($inlineElements, $posStart);
        if($origin) $this->setStatistic($type, $origin, $posStart+1, $posEnd);
//            if($thesaurus) self::$thesaurus->countThesaurus($thesaurus, $posStart);
    }

    public function getContent(){
        return self::$content;
    }

    public function getElements(){
        return self::$elements;
    }

    public function getInlineElements(){
        return self::$inlineElements;
    }

    public function getName(){
        return self::$name;
    }

    public function getAuthors(){
        return self::$authors;
    }

//        public function getThesaurus(){
//            return self::$thesaurus;
//        }

    public function getStatistic(){
        return self::$statistic;
    }

    public function getFootNotesTypeOne(){
        return self::$footNotesTypeOne;
    }

    public function getFootNotesTypeTwo(){
        return self::$footNotesTypeTwo;
    }

    public function getFootNotesInlineContent(){
        return self::$footNotesInlineContent;
    }
}

//    class TableData extends Element{
//        private $countRows;
//        private $countCells;
//        private $countElement;

//        public function setTableElement(array $table, $tableSize){
//            self::$currElement = NULL;
//            $this->countRows = 0;
//            $this->countCells = 0;
//            $this->countElement = 0;
//
//            self::$elements[self::$elementsCount]['id'] = self::$elementsCount + 1;
//            self::$elements[self::$elementsCount]['type'] = 'table';
//            self::$elements[self::$elementsCount]['content'] = array();
//
//            $posStart = $this->startEndPos();
//            $posEnd = $posStart + $tableSize;
//            self::$elements[self::$elementsCount]['pos_start'] = $posStart + 1;
//            self::$elements[self::$elementsCount]['pos_end'] = $posEnd;
//
//            $this->setStatistic('table', 'style system', $posStart+1, $posEnd);
//
//            for($i=0;$i<count($table);$i++){
//                if($table[$i]['table']){
//                    switch ($table[$i]['table']){
//                        case 'row':
//                            if($table[$i]['sign']){
//                                $newRow = array();
//                                $newRow['id'] = $this->countRows + 1;
//                                $newRow['type'] = 'row';
//                                $newRow['content'] = array();
//                                self::$elements[self::$elementsCount]['content'][$this->countRows] = $newRow;
//                            }else{
//                                $this->countElement = 0;
//                                $this->countCells = 0;
//                                $this->countRows++;
//                            }
//                            break;
//                        case 'cell':
//                            if($table[$i]['sign']){
//                                $newCell = array();
//                                $newCell['id'] = $this->countCells + 1;
//
//                                if($table[$i]['colspan']) $newCell['colspan'] = $table[$i]['colspan'];
//                                if($table[$i]['rowspan']) $newCell['rowspan'] = $table[$i]['rowspan'];
//
//                                $newCell['type'] = 'cell';
//                                $newCell['content'] = array();
//
//                                self::$elements[self::$elementsCount]['content'][$this->countRows]['content'][$this->countCells] = $newCell;
//                            }else{
//                                $this->countElement = 0;
//                                $this->countCells++;
//                            }
//                            break;
//                    }
//                }
//                elseif($table[$i]['foot_note'] && is_array($table[$i]['foot_note'])) $this->loadFootNote($table[$i]);
//                else $this->detectBlockElements($table[$i]);
//            }
//            self::$elementsCount++;
//        }
//
//        protected function setElement($type, $data, $thesaurus=NULL, $origin=NULL, $inlineElements=NULL, $footNote=NULL, $level=1){
//            $cellContent = array();
//
//            $posStart = $this->startEndPos();
//            $posEnd = $this->startEndPos($data);
//            $newType = $this->setReassignType($data, $type, $posStart);
//
//            $cellContent['id'] = $this->countElement + 1;
//            $cellContent['pos_start'] = $posStart + 1;
//            $cellContent['pos_end'] = $posEnd;
//            $cellContent['type'] = $newType ? $newType : $type;
//
//            if($cellContent['type'] == 'title') $cellContent['typeLvl'] = $level;
//            if($footNote && is_array($footNote)) $cellContent['footNote'][] = $footNote;
//
//            $this->setInlineElement($inlineElements, $posStart);
//
//            self::$elements[self::$elementsCount]['content'][$this->countRows]['content'][$this->countCells]['content'][$this->countElement] = $cellContent;
//
//            $this->countElement++;
//            self::$content .= $this->setElementText($data);
//
//            if($origin) $this->setStatistic($type, $origin, $posStart+1, $posEnd);
//            if($thesaurus) self::$thesaurus->countThesaurus($thesaurus, $posStart);
//        }
//    }

class ParserCtl extends BaseCtl{
    private $spacePattern = '/\s{2,}/';

    private $libNames;
    private $libFamilyEnds;
    private $libFamily_s;
    private $libLands;
//        private $libThesaurus;

    private $patternLetter;
    private $patternTitle;
    private $patternArabNum;
    private $patternRomNum;
    private $patternBgnArabNum;
    private $patternEndArabNum;
    private $patternTreeStars;

    private $numPutsIn;
    private $differenceTitleSize;
    private $maxShareTitle;
    private $maxTitleLetters;
    private $maxAuthorOrNameLetters;
    private $maxFirstPats;
    private $maxTextParts;

    private $concurrencyAuthor;
    private $concurrencyName;

    private $rulesDetectAuthor;
    private $rulesDetectBookName;
    private $otherRules;
    private $inlineRules;

//        private $patternSpots;
//        private $thesaurus = array();
//        private $thesaurusCount = 0;

    public static function parser($fileHandle){
        global
        $preParse,
        $preParseInset,
        $preParseTable,

        $insetCount,

        $tableCount,
        $rowTableCont,
        $cellTableCount,
        $vMergeCount,

        $wpCount,
        $wpInsetCount,
        $wpTableCount,

        $wrCount,
        $wrInsetCount,
        $wrTableCount,

        $wtCount,

        $setWTValue,
        $setWPAttribute,
        $setWRAttribute;

        $insetCount = 0;

        $tableCount = 0;
        $rowTableCont = 0;
        $cellTableCount = 0;
        $vMergeCount = 0;

        $wpCount = 0;
        $wpInsetCount = 0;
        $wpTableCount = 0;

        $wrCount = 0;
        $wrInsetCount = 0;
        $wrTableCount = 0;

        $wtCount = 0;

        $preParse = array();
        $preParseInset = array();
        $preParseTable = array();

        $setWTValue = function($value){
            global
            $preParse,
            $preParseInset,
            $preParseTable,

            $insetTagFlag,
            $tableTagFlag,

            $insetCount,

            $tableCount,
            $rowTableCont,
            $cellTableCount,

            $wpCount,
            $wpInsetCount,
            $wpTableCount,

            $wrCount,
            $wrInsetCount,
            $wrTableCount,

            $wtCount;

            if($insetTagFlag) $preParseInset[$insetCount][$wpInsetCount]['w:r'][$wrInsetCount]['content'][$wtCount] = $value;
            elseif($tableTagFlag){
                $preParseTable
                [$tableCount]
                [$rowTableCont]
                [$cellTableCount]
                ['w:p']
                [$wpTableCount]
                ['w:r']
                [$wrTableCount]
                ['content']
                [$wtCount] = $value;
            }
            else $preParse[$wpCount]['w:r'][$wrCount]['content'][$wtCount] = $value;
        };

        $setWPAttribute = function(array $attr){
            global
            $preParse,
            $preParseInset,
            $preParseTable,

            $insetTagFlag,
            $tableTagFlag,

            $insetCount,

            $tableCount,
            $rowTableCont,
            $cellTableCount,

            $wpCount,
            $wpInsetCount,
            $wpTableCount;

            if($insetTagFlag) $preParseInset[$insetCount][$wpInsetCount][$attr['name']] = $attr['val'];
            elseif($tableTagFlag){
                $preParseTable
                [$tableCount]
                [$rowTableCont]
                [$cellTableCount]
                ['w:p']
                [$wpTableCount]
                ['attr']
                [$attr['name']] = $attr['val'];;
            }
            else $preParse[$wpCount]['attr'][$attr['name']] = $attr['val'];;
        };

        $setWRAttribute = function(array $attr){
            global
            $preParse,
            $preParseInset,
            $preParseTable,

            $insetTagFlag,
            $tableTagFlag,

            $insetCount,

            $tableCount,
            $rowTableCont,
            $cellTableCount,

            $wpCount,
            $wpInsetCount,
            $wpTableCount,

            $wrCount,
            $wrInsetCount,
            $wrTableCount;

            if($insetTagFlag) $preParseInset[$insetCount][$wpInsetCount]['w:r'][$wrInsetCount]['attr'][$attr['name']] = $attr['val'];
            elseif($tableTagFlag){
                $preParseTable
                [$tableCount]
                [$rowTableCont]
                [$cellTableCount]
                ['w:p']
                [$wpTableCount]
                ['w:r']
                [$wrTableCount]
                ['attr']
                [$attr['name']] = $attr['val'];
            }
            else $preParse[$wpCount]['w:r'][$wrCount]['attr'][$attr['name']] = $attr['val'];
        };

        $xml_parser = xml_parser_create();
        xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, false);
        xml_parser_set_option($xml_parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
        $data = stream_get_contents($fileHandle);

        xml_set_element_handler(
            $xml_parser,

            // Start Tag
            function($parser, $name, $attr){
                global
                $preParse,
                $preParseInset,
                $preParseTable,

                $ignoreTagFlag,
                $textTagFlag,
                $insetTagFlag,
                $ignoreInsetFlag,
                $tableTagFlag,
                $ignoreTableFlag,
                $mergeTagFlag,

                $insetCount,

                $tableCount,
                $rowTableCont,
                $cellTableCount,
                $vMergeCount,
                $vMergeCell,
                $vMergeRow,

                $wpCount,
                $wpInsetCount,
                $wpTableCount,

                $wrCount,
                $wrInsetCount,
                $wrTableCount,

                $wtCount,

                $footNoteId,

                $setWTValue,
                $setWPAttribute,
                $setWRAttribute;

                if(!$ignoreTagFlag){
                    switch ($name){
                        case 'w:txbxContent':
                            if($insetTagFlag) $ignoreInsetFlag = true;
                            else $insetTagFlag = true;
                            break;
                        case 'w:p':
                            $newArray = array('w:r' => array(), 'attr' => array());
                            if($insetTagFlag) $preParseInset[$insetCount][$wpInsetCount] = $newArray;
                            elseif($tableTagFlag &&
                                !is_array(
                                    $preParseTable
                                    [$tableCount]
                                    [$rowTableCont]
                                    [$cellTableCount]
                                    ['w:p']
                                    [$wpTableCount]
                                )
                            ){
                                $preParseTable
                                [$tableCount]
                                [$rowTableCont]
                                [$cellTableCount]
                                ['w:p']
                                [$wpTableCount] = $newArray;
                            }
                            elseif(!is_array($preParse[$wpCount])) $preParse[$wpCount] = $newArray;
                            break;
                        case 'w:jc':
                            $setWRAttribute(array('name' => 'justification', 'val' => $attr['w:val']));
                            break;
                        case 'w:r':
                            $newArray = array('attr' => $attr, 'content' => array());
                            if(
                                $insetTagFlag &&
                                !is_array($preParseInset[$insetCount][$wpInsetCount]['w:r'][$wrInsetCount])
                            ) $preParseInset[$insetCount][$wpInsetCount]['w:r'][$wrInsetCount] = $newArray;
                            elseif(
                                $tableTagFlag &&
                                !is_array(
                                    $preParseTable
                                    [$tableCount]
                                    [$rowTableCont]
                                    [$cellTableCount]
                                    ['w:p']
                                    [$wpTableCount]
                                    ['w:r']
                                    [$wrTableCount]
                                )
                            ){
                                $preParseTable
                                [$tableCount]
                                [$rowTableCont]
                                [$cellTableCount]
                                ['w:p']
                                [$wpTableCount]
                                ['w:r']
                                [$wrTableCount] = $newArray;
                            }
                            elseif(!is_array($preParse[$wpCount]['w:r'][$wrCount])) $preParse[$wpCount]['w:r'][$wrCount] = $newArray;
                            break;
                        case 'w:sz':
                            $setWRAttribute(array('name' => 'size', 'val' => $attr['w:val']));
                            break;
                        case 'w:b':
                            $setWRAttribute(array('name' => 'bold', 'val' => true));
                            break;
                        case 'w:i':
                            $setWRAttribute(array('name' => 'italic', 'val' => true));
                            break;
                        case 'w:color':
                            $setWRAttribute(array('name' => 'color', 'val' => $attr['w:val']));
                            break;
                        case 'w:t':
                            $textTagFlag = true;
                            $setWTValue(array('attr' => $attr, 'text' => ''));
                            break;
                        case 'w:br':
                            $setWTValue('br');
                            $wtCount++;
                            break;
                        case 'w:footnoteReference':
                            if(!$insetTagFlag && !$tableTagFlag)
                                $preParse[$wpCount]['w:r'][$wrCount]['content'][$wtCount]['foot_note'] = $attr['w:id'];
                            break;
                        case 'w:footnote':
                            if($attr['w:id']) $footNoteId = (int)$attr['w:id'];
                            break;
//                            case 'w:lastRenderedPageBreak':
//                                $setWTValue('PageBreak');
//                                $wtCount++;
//                                break;
                        case 'mc:Fallback':
                            $ignoreTagFlag = true;
                            break;
                        case 'w:tbl':
                            if($tableTagFlag) $ignoreTableFlag = true;
                            else{
                                $tableTagFlag = true;
                                $preParseTable[$tableCount] = array();
                            }
                            break;
                        case 'w:tr':
                            $preParseTable[$tableCount][$rowTableCont] = array();
                            break;
                        case 'w:tc':
//                                $preParseTable[$tableCount][$rowTableCont][$cellTableCount] = array();
                            $mergeTagFlag = false;
                            break;
                        case 'w:gridSpan':
                            $preParseTable[$tableCount][$rowTableCont][$cellTableCount]['attr']['colspan'] = $attr['w:val'];
                            break;
                        case 'w:vMerge':
                            if($attr['w:val'] == 'restart'){
                                $vMergeCount = 1;
                                $vMergeCell = $cellTableCount;
                                $vMergeRow = $rowTableCont;
                            }else{
                                $vMergeCount++;
                                $mergeTagFlag = true;
                            }
                            $preParseTable[$tableCount][$vMergeRow][$vMergeCell]['attr']['rowspan'] = $vMergeCount;
                            break;
                        case 'w:pStyle':
                            if(preg_match('/^[1-9]/', $attr['w:val']))
                                $setWPAttribute(array('name' => 'title', 'val' => (int)$attr['w:val']));
                            elseif(preg_match('/^Heading/', $attr['w:val']))
                                $setWPAttribute(array('name' => 'title', 'val' => (int)preg_replace('/^Heading/', '', $attr['w:val'])));
                            break;
                    }
                }
            },

            // Stop Tag
            function($parser, $name){
                global
                $ignoreTagFlag,
                $textTagFlag,
                $insetTagFlag,
                $ignoreInsetFlag,
                $tableTagFlag,
                $ignoreTableFlag,
                $mergeTagFlag,

                $insetCount,

                $tableCount,
                $rowTableCont,
                $cellTableCount,

                $wpCount,
                $wpInsetCount,
                $wpTableCount,

                $wrCount,
                $wrInsetCount,
                $wrTableCount,

                $wtCount,

                $footNoteId;

                if(!$ignoreTagFlag){
                    switch ($name){
                        case 'w:footnote':
                            $footNoteId = NULL;
                            break;
                        case 'w:txbxContent':
                            if($ignoreInsetFlag) $ignoreInsetFlag = true;
                            else{
                                $insetTagFlag = false;
                                $insetCount++;
                                $wpInsetCount = 0;
                            }
                            break;
                        case 'w:p':
                            if($insetTagFlag){
                                $wpInsetCount++;
                                $wrInsetCount = 0;
                            }elseif($tableTagFlag){
                                $wpTableCount++;
                                $wrTableCount = 0;
                            }else{
                                $wpCount++;
                                $wrCount = 0;
                            }
                            break;
                        case 'w:r':
                            if($insetTagFlag) $wrInsetCount++;
                            elseif($tableTagFlag) $wrTableCount++;
                            else $wrCount++;
                            $wtCount = 0;
                            break;
                        case 'w:t':
                            $textTagFlag = false;
                            $wtCount++;
                            break;
                        case 'w:tbl':
                            if($ignoreTableFlag) $ignoreTableFlag = false;
                            else{
                                $tableTagFlag = false;
                                $tableCount++;
                                $rowTableCont = 0;
                            }
                            break;
                        case 'w:tr':
                            $rowTableCont++;
                            $cellTableCount = 0;
                            break;
                        case 'w:tc':
                            if(!$mergeTagFlag){
                                $cellTableCount++;
                                $wpTableCount = 0;
                            }
                            break;
                    }
                }

                //Ignore tag
                if($name == 'mc:Fallback') $ignoreTagFlag = false;
            }
        );

        // Char Data
        xml_set_character_data_handler($xml_parser, function($parser, $data){
            global
            $preParse,
            $preParseInset,
            $preParseTable,

            $ignoreTagFlag,
            $textTagFlag,
            $insetTagFlag,
            $ignoreInsetFlag,
            $tableTagFlag,
            $ignoreTableFlag,

            $insetCount,

            $tableCount,
            $rowTableCont,
            $cellTableCount,

            $wpCount,
            $wpInsetCount,
            $wpTableCount,

            $wrCount,
            $wrInsetCount,
            $wrTableCount,

            $wtCount,

            $footNoteId;

            if(
                !$ignoreTagFlag &&
                !$ignoreTableFlag &&
                !$ignoreInsetFlag &&
                $textTagFlag
            ){
                if($insetTagFlag) $preParseInset[$insetCount][$wpInsetCount]['w:r'][$wrInsetCount]['content'][$wtCount]['text'] .= $data;
                elseif($tableTagFlag){
//                        echo $cellTableCount.' = '.$data.'<br>';
                    $preParseTable
                    [$tableCount]
                    [$rowTableCont]
                    [$cellTableCount]
                    ['w:p']
                    [$wpTableCount]
                    ['w:r']
                    [$wrTableCount]
                    ['content']
                    [$wtCount]
                    ['text'] .= $data;
                }else{
                    $preParse[$wpCount]['w:r'][$wrCount]['content'][$wtCount]['text'] .= $data;
                    if($footNoteId) $preParse[$wpCount]['w:r'][$wrCount]['content'][$wtCount]['id'] = $footNoteId;
                }
            }
        });


        xml_parse($xml_parser, $data, true);
        xml_parser_free($xml_parser);

        $insetParts = array();
        $tableParts = array();

        $textParts = self::recombineTextContent($preParse);

        if(count($preParseInset)){
            for($i=0;$i<count($preParseInset);$i++){
                $insetParts[] = self::recombineTextContent($preParseInset[$i]);
            }
        }

        if(count($preParseTable)){
            foreach($preParseTable as $tableCount => $table){
                $tableParts[$tableCount] = array();
                foreach($table as $rowCount => $row){
                    $tableParts[$tableCount][$rowCount] = array();
                    foreach($row as $cellCount => $cell){
                        $currentCell = array();
                        if(count($cell['attr'])){
                            foreach($cell['attr'] as $attr => $val){
                                $currentCell['attr'][$attr] = (int)$val;
                            }
                        }
                        $currentCell['content'] = self::recombineTextContent($cell['w:p']);
                        $tableParts[$tableCount][$rowCount][$cellCount] = $currentCell;
                    }
                }
            }
        }
        return array('text_parts' => $textParts, 'inset_parts' => $insetParts, 'table_parts' => $tableParts);
    }

    public function parseXML($bookUID=NULL, $fileName=NULL, $testMode=false, $statMode=false){
        global
        $textParts,

        $names,
        $familyEnds,
        $family_s,
        $lands,
        $patternName,

        $detectPattern,
        $detectInlineElements;

        $detectPattern = function($content, array $patterns){
            $result = false;
//                if($content == 'Пупкин Василий' && $test) var_dump($patterns);
            for($i=0;$i<count($patterns);$i++){
                if(preg_match($patterns[$i], $content)){
                    $result = true;
                    break;
                }
            }
            return $result;
        };

        $detectInlineElements = function($data, $pattern, $offset=0){
            if(preg_match_all($pattern, $data, $matches, PREG_SET_ORDER)){
                $parts = preg_split($pattern, $data);

                $result = array();
                $startOffset = 0;

                for($m=0;$m<count($matches);$m++){
                    $startPos = mb_strlen($parts[$m], 'UTF-8') + $startOffset + $offset;
                    $endPos = $startPos + mb_strlen($matches[$m][0], 'UTF-8') - $offset;
                    $startOffset += $endPos;

                    $result[$m] = array(
                        'pos_start' => $startPos,
                        'pos_end' => $endPos
                    );
                }
                return $result;
            }else return false;
        };

        $bookUID = $bookUID ? $bookUID : $this->params['book_uid'];
        if(!$bookUID){
            $this->resultJson = array('error' => 'Not set path to parsing file');
            $this->showResult(true);
        }

        require_once('config/config.parser.php');

        for($i=0;$i<count($this->libNames);$i++){
            $names[] = $this->addLib($this->openLib($this->libNames[$i]), '\s?', '\s?|\s?', '\s?');
        }

        for($i=0;$i<count($this->libFamilyEnds);$i++){
            $familyEnds[] = $this->addLib($this->openLib($this->libFamilyEnds[$i]), '^.*', '$|^.*', '$');
        }

        for($i=0;$i<count($this->libFamily_s);$i++){
            $family_s[] = $this->addLib($this->openLib($this->libFamily_s[$i]), '\s?', '\s?|\s?', '\s?');
        }

        for($i=0;$i<count($this->libLands);$i++){
            $lands[] = $this->addLib($this->openLib($this->libLands[$i]), '\s?', '\s?|\s?', '\s?');
        }

//            for($i=0;$i<count($this->libThesaurus);$i++){
//                $this->addThesaurusLib($this->openLib($this->libThesaurus[$i]));
//            }

        $pathFile = $_SERVER['DOCUMENT_ROOT'].PATH_UPLOAD.'book'.$bookUID.'/';

        if(!@$fileHandle = fopen($pathFile.'word/document.xml', "r")){
            $this->resultJson = array('error' => 'Cannot load XML file');
            $this->showResult(true);
        }

        $parts = self::parser($fileHandle);
        $textParts = $parts['text_parts'];
        $insetParts = $parts['inset_parts'];
        $tableParts = $parts['table_parts'];
        fclose($fileHandle);

        $footNotesParts = array();
        if(TRIGGER_DETECT_FOOTNOTES){
            if(@$fileHandle = fopen($pathFile.'word/footnotes.xml', "r")){
                $parts = self::parser($fileHandle);
                $footNotesParts = $parts['text_parts'];
            }
        }

        if(count($textParts) < $this->maxTextParts) //if not mach of text parts
            $textParts = $this->calculateTextSizes($textParts);

        //Detecting author and name of book
        $parts = count($textParts) < $this->maxFirstPats ? count($textParts) : $this->maxFirstPats;
        for($i=0;$i<$parts;$i++){
            $currentContent = $textParts[$i]['content'];
            if(
                mb_strlen($currentContent, 'UTF-8') < $this->maxAuthorOrNameLetters &&
                mb_strlen($currentContent, 'UTF-8') > 3 &&
                !preg_match($this->patternTitle, $currentContent
                )){
                //detecting author
                $this->detectScore($this->rulesDetectAuthor, $i, 'author_score', true);
                //detecting name
                $this->detectScore($this->rulesDetectBookName, $i, 'name_score', false);
            }
        }

        $maxScoreName = 0;
        $posAuthors = array();

        //define max score author and name
        for($i=0;$i<$parts;$i++){
            $scoreAuthor = $textParts[$i]['author_score'];
            $scoreName = $textParts[$i]['name_score'];
            if($scoreAuthor >= $this->concurrencyAuthor){
//                    $maxScoreAuthor = $scoreAuthor;
                $posAuthors[] = $i;
            }
            if($scoreName >= $maxScoreName && $scoreName >= $this->concurrencyName){
                $maxScoreName = $scoreName;
                $posName = $i;
            }
        }

        if(isset($posName) && in_array($posName, $posAuthors)){
            $mootPos = array_search($posName, $posAuthors);
            if($textParts[$mootPos]['author_score'] > $textParts[$mootPos]['name_score']) unset($posName);
            else array_splice($posAuthors, $mootPos, 1);
        }

//          Set author and name
        if(sizeof($posAuthors)){
            foreach($posAuthors as $value){
                $textParts[$value]['author'] = true;
                $textParts[$value]['origin'] = 'probabilistic system';
            }
        }
        if($maxScoreName > 3 && isset($posName)){
            $textParts[$posName]['name'] = true;
            $textParts[$posName]['origin'] = 'probabilistic system';
        }

        //If not confidence true set name or author - second try
        if(TRIGGER_SECOND_TRY && count($textParts) < $this->maxTextParts){
            if(!sizeof($posAuthors)){
                for($i=0;$i<$parts;$i++){
                    if(
                        $textParts[$i]['author_score'] < 10 &&
                        preg_match($patternName, $textParts[$i]['content']) &&
                        mb_strlen($textParts[$i]['content'], 'UTF-8') <= $this->maxAuthorOrNameLetters
                    ){
                        if(!$this->secondTry($i)) $this->secondTry($i, true);
                    }
                }
            }

            if($maxScoreName < 30 && isset($posName) && preg_match($patternName, $textParts[$posName]['content'])){
                if(!$this->secondTry($posName)) $this->secondTry($posName, true);
            }
        }

        if(count($textParts) < $this->maxTextParts){ //if not mach of text parts
            $textParts = $this->multiDetect($textParts, array(
//                  'thesaurus' => true,
                'detect_bold_title' => true,
                'detect_number_title' => true,
                'detect_three_stars_title' => true,
                'detect_keywords_title' => true,
                'detect_other_elements_title' => true,
                'detect_inline_elements' => true,
                'detect_foot_notes' => true
            ));
        }

        if(count($insetParts)){
            for($i=0;$i<count($insetParts);$i++){
                $insetParts[$i] = $this->multiDetect($insetParts[$i], array(
//                        'thesaurus' => true,
                    'detect_bold_title' => false,
                    'detect_number_title' => false,
                    'detect_keywords_title' => true,
                    'detect_three_stars_title' => false,
                    'detect_other_elements_title' => true,
                    'detect_inline_elements' => false,
                    'detect_foot_notes' => false
                ));
            }
        }

        if(count($tableParts)){
            for($t=0;$t<count($tableParts);$t++){
                for($r=0;$r<count($tableParts[$t]);$r++){
                    for($c=0;$c<count($tableParts[$t][$r]);$c++){
                        $tableParts[$t][$r][$c]['content'] = $this->multiDetect($tableParts[$t][$r][$c]['content'], array(
//                                'thesaurus' => true,
                            'detect_bold_title' => true,
                            'detect_number_title' => true,
                            'detect_keywords_title' => true,
                            'detect_three_stars_title' => true,
                            'detect_other_elements_title' => true,
                            'detect_inline_elements' => false,
                            'detect_foot_notes' => false
                        ));
                    }
                }
            }
        }

        if(count($footNotesParts)){
            for($i=0;$i<count($footNotesParts);$i++){
                $footNotesParts[$i] = $this->multiDetect($footNotesParts[$i], array(
//                        'thesaurus' => true,
                    'detect_bold_title' => false,
                    'detect_number_title' => false,
                    'detect_keywords_title' => false,
                    'detect_three_stars_title' => false,
                    'detect_other_elements_title' => true,
                    'detect_inline_elements' => false,
                    'detect_foot_notes' => false
                ));
            }
        }

        //forming final array content
        $elements = new CharData($pathFile, $bookUID);

        for($i=0;$i<count($textParts);$i++){
            $elements->detectBlockElements($textParts[$i]);
        }

        $allElements = $elements->getElements();
        if($inlineElements = $elements->getInlineElements()) $allElements[count($allElements)] = array('inline' => $inlineElements);
        $authors = $elements->getAuthors();
        $name = $elements->getName();

        if(count($insetParts)){
            $allInsetElements = array();
            $countInsets = 0;
            for($ins=0;$ins<count($insetParts);$ins++){

                $elements->resetData();
                $startPos = $elements->startEndPos() + 1;
                $content = '';

                for($i=0;$i<count($insetParts[$ins]);$i++){
                    $elements->detectBlockElements($insetParts[$ins][$i]);
                    $content .= $insetParts[$ins][$i]['content'];
                }

                $endPos = $startPos + mb_strlen($elements->setElementText($content), 'UTF-8') - 1;
                $elements->setStatistic('inset', 'style system', $startPos, $endPos);

                $currentInset = $elements->getElements();
                foreach($currentInset as $inset){
                    $inset['id'] = $countInsets + 1;
                    $allInsetElements[$countInsets] = $inset;
                    $countInsets++;
                }
            }
            $allElements[] = array('inset' => $allInsetElements);
        }

        if(count($tableParts)){
            $allTableElements = array();
            foreach($tableParts as $tableCount => $table){

                $startPos = $elements->startEndPos() + 1;
                $content = '';

                $allTableElements[$tableCount] = array(
                    'id' => $tableCount+1,
                    'rows' => array()
                );

                foreach($table as $rowCount => $row){
                    $allTableElements[$tableCount]['rows'][$rowCount] = array(
                        'id' => $rowCount+1,
                        'cells' => array()
                    );

                    foreach($row as $cellCount => $cell){
                        $elements->resetData();
                        $currentCell = array(
                            'id' => $cellCount+1,
                            'content' => array()
                        );

                        if($cell['attr']){
                            $currentCell['attr'] = $cell['attr'];
                        }
                        for($i=0;$i<count($cell['content']);$i++){
                            $elements->detectBlockElements($cell['content'][$i]);
                            $content .= $cell['content'][$i]['content'];
                            $currentCell['content'][$i] = $elements->getElements();
                        }

                        $allTableElements[$tableCount]['rows'][$rowCount]['cells'][$cellCount] = $currentCell;
                    }
                };
                $endPos = $startPos + mb_strlen($elements->setElementText($content), 'UTF-8') - 1;
                $elements->setStatistic('table', 'style system', $startPos, $endPos);
            }
            $allElements[] = array('tables' => $allTableElements);
        }

        if(TRIGGER_DETECT_FOOTNOTES){

            $footNotesTypeTwo = $elements->getFootNotesTypeTwo();
            $footNotesInlineContent = $elements->getFootNotesInlineContent();

            if(count($footNotesTypeTwo) && count($footNotesInlineContent)){
                $elements->resetData();
                for($i=0;$i<count($footNotesInlineContent);$i++){
                    $startPos = $elements->startEndPos() + 1;
                    $content = '';

                    $elements->detectBlockElements($footNotesInlineContent[$i]);
                    $content .= $footNotesInlineContent[$i]['content'];

                    $endPos = $startPos + mb_strlen($elements->setElementText($content), 'UTF-8') - 1;
                    $elements->setStatistic('footnote', 'response footnote system', $startPos, $endPos);
                }
                $allFootNotesInlineElements = $elements->getElements();
                for($i=0;$i<count($allFootNotesInlineElements);$i++){
                    foreach($footNotesTypeTwo as $footNote){
                        if($allFootNotesInlineElements[$i]['id'] == $footNote['id']){
                            $allFootNotesInlineElements[$i]['pos_in_text'] = $footNote['pos_start'];
                            break;
                        };
                    }
                }
                $allElements[] = array('footnotes_inline' => $allFootNotesInlineElements);
            }

            $footNotesTypeOne = $elements->getFootNotesTypeOne();

            if(count($footNotesParts) && count($footNotesTypeOne)){
                $elements->resetData();
                $startPos = $elements->startEndPos() + 1;
                $content = '';

                for($i=0;$i<count($footNotesParts);$i++){
                    $elements->detectBlockElements($footNotesParts[$i]);
                    $content .= $footNotesParts[$i]['content'];
                }

                $endPos = $startPos + mb_strlen($elements->setElementText($content), 'UTF-8') - 1;
                $elements->setStatistic('footnote', 'style system', $startPos, $endPos);
                $allFootNotesElements = $elements->getElements();

                for($i=0;$i<count($allFootNotesElements);$i++){
                    foreach($footNotesTypeOne as $footNote){
                        if($allFootNotesElements[$i]['id'] == $footNote['id']){
                            $allFootNotesElements[$i]['pos_in_text'] = $footNote['pos_start'];
                            break;
                        };
                    }
                }
                $allElements[] = array('footnotes_style' => $allFootNotesElements);
            }
        }

        $content = $elements->getContent();
        $statistic = $elements->getStatistic();

//            $thesaurusData = $elements->getThesaurus();
//            $thesaurus = new ThesaurusModel();
//            $thesaurus->multiplySql($thesaurusData)->exec();

        if(TRIGGER_DETECT_FROM_FILENAME && (!count($authors) || !$name)){
            if(!$name){
                $nameAndAuthor = $this->secondTry(NULL, false, $fileName);
                $nameAndAuthor = !$nameAndAuthor ? $this->secondTry(NULL, true, $fileName) : $nameAndAuthor;
                if(is_array($nameAndAuthor)){
                    $name = $nameAndAuthor['name'];
                    $authors[0] = $nameAndAuthor['author'];
                }
            }
        }

        $name = $name ? $name : $fileName;
        if(!count($authors)) $authors[0] = 'Unknown';

        $testMode = $this->params['test'] ? $this->params['test'] : $testMode;
        $testStatistic = $this->params['test_stat'] ? $this->params['test_stat'] : $statMode;
        if($testMode || $testStatistic){
            if($testMode){
                if($testMode != 'elements' && $testMode != 'content'){
                    $this->resultJson = array('error' => 'Wrong value test param');
                    $this->showResult();
                }elseif($testMode == 'elements'){
                    if($testMode){

//                            $this->showTest($allElements, $content, 'Elements');

//                            if(isset($allTableElements) && count($allTableElements)){
//                                echo '<h1>Tables</h1>';
//                                foreach($allTableElements as $table){
//                                    echo '<table border="1" width="100%">';
//                                    foreach($table['rows'] as $rows){
//                                        echo '<tr>';
//                                        foreach($rows['cells'] as $cells){
//                                            $rowSpan = $cells['attr']['rowspan'] ? 'rowspan='.$cells['attr']['rowspan'] : '';
//                                            $colSpan = $cells['attr']['colspan'] ? 'colspan='.$cells['attr']['colspan'] : '';
//
//                                            echo '<td height="30" '.$rowSpan.' '.$colSpan.'>';
//                                            for($i=0;$i<count($cells['content']);$i++){
//                                                $this->showTest($cells['content'][$i], $content, 'Table Elements', 3);
//                                            }
//                                            echo '</td>';
//                                        }
//                                        echo '</tr>';
//                                    }
//                                    echo '</table>';
//                                }
//                            }

//                            if(isset($allInsetElements) && count($allInsetElements)){
//                                echo '<h1>Insets</h1>';
//                                $this->showTest($allInsetElements, $content, 'Inset #'.($i+1), 2);
//                            }
//
//                            if(isset($allFootNotesElements) && count($allFootNotesElements))
//                                $this->showTest($allFootNotesElements, $content, 'FootNotes Style', 1);
//
//                            if(isset($allFootNotesInlineElements) && count($allFootNotesInlineElements))
//                                $this->showTest($allFootNotesInlineElements, $content, 'FootNotes Inline', 1);
//
//                            if(isset($inlineElements))
//                                $this->showTest($inlineElements, $content, 'Inline Elements', 1);
                    }
                }else echo $content;
            }
//                if($testStatistic) $this->showTest($statistic, $content, 'Statistic', 2);
        }else{
            //Remove temp dirs and files
            $this->removeTempDir($pathFile);

//                $thesaurus->saveThesaurus($authors[0], $name, mb_strlen($content, 'UTF-8'));

            $newBook = new BooksCtl();
            $newBook->insertBook($name, $authors[0], $bookUID, $allElements, $content, $statistic);
            $this->resultJson = array('book_uid' => $bookUID, 'name' => $name, 'author' => $authors[0]);
            $this->showResult();
        }
    }

    private static function recombineTextContent($preParse){
        global $patternFootNoteBegin, $patternFootNoteFinal, $patternFootNote;

        $count = 0;
        $textParts = array();
        $last_wp = 0;

        for($wp=0;$wp<count($preParse);$wp++){
            $wpAttr = $preParse[$wp]['attr'];

            if(count($preParse[$wp]['w:r'])){
                for($wr=0;$wr<count($preParse[$wp]['w:r']);$wr++){
                    for($wt=0;$wt<count($preParse[$wp]['w:r'][$wr]['content']);$wt++){

                        if(!is_string($preParse[$wp]['w:r'][$wr]['content'][$wt])){
                            $content = $preParse[$wp]['w:r'][$wr]['content'][$wt]['text'];

//                                $wtAttrBfrWr = $preParse[$wp]['w:r'][$wr-1]['content'][$wt]['attr'];
//                                $wtAttr = $preParse[$wp]['w:r'][$wr]['content'][$wt]['attr'];
//                                $wtAttrNxtWr = $preParse[$wp]['w:r'][$wr+1]['content'][$wt]['attr'];

                            $wrAttr = $preParse[$wp]['w:r'][$wr]['attr'];
                            $lastPos = count($preParse[$wp]['w:r'][$wr-1]['content'])-1;
                            $footNoteIdTypeOne = $preParse[$wp]['w:r'][$wr]['content'][$wt]['foot_note'];
                            $id = $preParse[$wp]['w:r'][$wr]['content'][$wt]['id'];

//                                echo $content.'<br>';

                            if(
                                $count != 0 &&
                                $last_wp == $wp &&
                                $preParse[$wp]['w:r'][$wr]['content'][$wt-1] != 'br' &&
                                $preParse[$wp]['w:r'][$wr-1]['content'][$lastPos] != 'br'
//                                    (
//                                        (sizeof($wrAttr) && ($wrAttr['w:rsidRPr'] || $wrAttr['w:rsidR'])) ||
//                                        ($preParse[$wp]['w:r'][$wr]['content'][$wt-1] == 'PageBreak') ||
//                                        (
//                                            $preParse[$wp]['w:r'][$wr]['content'][$wt-1] != 'br' &&
//                                            (
//                                                $wtAttr['xml:space'] == 'preserve' ||
//                                                ($wtAttrBfrWr['xml:space'] == 'preserve' && $wtAttrNxtWr['xml:space'] == 'preserve')
//                                            )
//                                        )
//                                    )
                            ){
                                $textParts[$count-1]['content'] .= $content;
                                $actualCount = $count-1;

                                if(preg_match($patternFootNote, $content)){
//                                        echo $textParts[$count-1]['content'].'<br>';
                                    $partContent = $textParts[$count-1]['content'];
                                    $footNoteIdTypeTwo = preg_replace('/.*'.$patternFootNoteBegin.'/', '', preg_replace('/'.$patternFootNoteFinal.'/', '', $partContent));
                                    $textParts[$count-1]['content'] = preg_replace($patternFootNote, '', $partContent);
                                    $textParts[$count-1]['foot_note_inline'][] = array(
                                        'id' => (int)$footNoteIdTypeTwo,
                                        'start_pos' => mb_strlen($textParts[$count-1]['content'], 'UTF-8') - 1
                                    );
                                }
                            }else{
                                $last_wp = $wp;
                                $textParts[$count]['content'] = $content;
                                $actualCount = $count;
                                $count++;
                            }

                            if($wpAttr){
                                foreach($wpAttr as $attr => $val){
                                    $textParts[$actualCount][$attr] = $val;
                                }
                            }
                            if($wrAttr){
                                foreach($wrAttr as $attr => $val){
                                    $textParts[$actualCount][$attr] = $val;
                                }
                            }

                            if($footNoteIdTypeOne){
                                $textParts[$actualCount]['foot_note'][] = array(
                                    'id' => (int)$footNoteIdTypeOne,
                                    'start_pos' => mb_strlen($textParts[$actualCount]['content'], 'UTF-8')
                                );
                            }
                            if($id) $textParts[$actualCount]['id'] = $id;
                            if($wpAttr['title']) $textParts[$actualCount]['title'] = $wpAttr['title'];
                        }
                    }
                }
            }
        }
        return $textParts;
    }

    private function calculateTextSizes($textParts){
        global $maxSize, $minSize;

        $maxSize = 0;
        $minSize = 0;

        //Calculation max and min sizes
        for($i=0;$i<count($textParts);$i++){
            $size = $textParts[$i]['size'];
            if($size && $minSize == 0) $minSize = $size;
            $maxSize = $size > $maxSize ? $size : $maxSize;
            $minSize = $minSize > $size ? $size : $minSize;
        }

        //Redifine NULL Sizes
        for($i=0;$i<count($textParts);$i++){
            if(!$textParts[$i]['size']) $textParts[$i]['size'] = $minSize;
        }

        //Calculation parts of font sizes
        $countSizes = 0;
        $totalScope = 0;

        if(TRIGGER_DETECT_SIZE_TITLE  && count($textParts) < $this->maxTextParts){
            $sizes = array();

            for($i=0;$i<count($textParts);$i++){
                if(!$textParts[$i]['name'] && !$textParts[$i]['author']){
                    $count = 0;
                    $flagExSize = false;
                    do{
                        if($sizes[$count]['size'] == $textParts[$i]['size']){
//                                $sizes[$count]['parts']++;
                            $sizes[$count]['scope'] += mb_strlen($textParts[$i]['content'], 'UTF-8');
                            $flagExSize = true;
                        }
                        $count++;
                    }while($count < count($sizes));

                    if(!$flagExSize){
                        $sizes[$countSizes]['size'] = $textParts[$i]['size'];
//                            $sizes[$countSizes]['parts'] = 1;
                        $sizes[$countSizes]['scope'] = mb_strlen($textParts[$i]['content'], 'UTF-8');
                        $countSizes++;
                    }
                    $totalScope += mb_strlen($textParts[$i]['content'], 'UTF-8');
                }
            }

            // Define titles as percents parts of general mass text and font size
            for($i=0;$i<count($sizes);$i++){
                $share = $sizes[$i]['scope']/$totalScope*100;
                $sizes[$i]['share'] = $share;
                $difference = $sizes[$i]['size'] - $minSize;
                if(abs($difference) > $this->differenceTitleSize && $sizes[$i]['share'] < $this->maxShareTitle){
                    $minLevel = 1;
                    $sizes[$i]['title'] = 1;
                }
            }

            // Set titles level
            if(isset($minLevel)){ // if exist at least one size title
                for($i=0;$i<count($sizes);$i++){
                    for($s=$i;$s>=0;$s--){
                        if($sizes[$i]['title'] && $sizes[$s]['title']){
                            $difference = $sizes[$i]['size'] - $sizes[$s]['size'];
                            if($difference > 0){
                                $sizes[$i]['title']++;
                                $curLevel = $sizes[$i]['title'];
                            }elseif($difference < 0){
                                $sizes[$s]['title']++;
                                $curLevel = $sizes[$s]['title'];
                            }
                            if(isset($curLevel)) $minLevel = $minLevel < $curLevel ? $curLevel : $minLevel;
                        }
                    }
                }
                for($i=0;$i<count($sizes);$i++){ //revers title levels
                    if($sizes[$i]['title']) $sizes[$i]['title'] = ($minLevel + 1) - $sizes[$i]['title'];
                }

                //Assignment titles and titles level to parts of text
                for($i=0;$i<count($textParts);$i++){
                    for($s=0;$s<count($sizes);$s++){
                        if(
                            !$textParts[$i]['title'] &&
                            $sizes[$s]['title'] &&
                            $textParts[$i]['size'] == $sizes[$s]['size'] &&
                            $this->validTitle($textParts[$i])
                        ){
                            $textParts[$i]['title'] = $sizes[$s]['title'];
                            $textParts[$i]['origin'] = 'size system';
                        }
                    }
                }
            }
        }
        return $textParts;
    }

    private function multiDetect($textParts, array $arrOn){
        global
        $currentParts,
        $epigraphs,
        $epigraphCount,
//                $detectInlineElements,

        $patternFootNoteBegin,
        $patternFootNoteMiddle,
        $patternFootNoteFinal,
        $patterFootNoteContent;

        $currentParts = $textParts;

        if(
            count($arrOn) &&
            (
//                    TRIGGER_THESAURUS ||
                TRIGGER_DETECT_BOLD_TITLE ||
                TRIGGER_DETECT_NUMBER_TITLE ||
                TRIGGER_DETECT_KEYWORDS_TITLE ||
                TRIGGER_DETECT_THREESTARS_TITLE ||
                TRIGGER_DETECT_OTHER_ELEMENTS ||
                TRIGGER_DETECT_FOOTNOTES
            )
        ){
            $toDeleteParts = array();
            $totalScope = 0;

            $maxPercent = 5;
            $boldScore = 0;
            $potentialTitleBold = array();

            $arabScope = 0;
            $romeScope = 0;
            $potentialTitleArab = array();
            $potentialTitleRome = array();

            $epigraphs = array();
            $epigraphCount = 0;

            $allCurrentContent = '';
            for($i=0;$i<count($currentParts);$i++){
                $totalScope += mb_strlen($currentParts[$i]['content'], 'UTF-8');
                $currentContent = preg_replace($this->spacePattern, ' ', $currentParts[$i]['content']);
                $allCurrentContent .= $currentContent;

//                    if(
//                        TRIGGER_THESAURUS &&
//                        count($this->thesaurus) &&
//                        $arrOn['thesaurus']
//                    ){
//                        foreach($this->thesaurus as $value){
//                            if($result = $detectInlineElements($currentContent, $value['pattern'])){
//                                $currentParts[$i]['thesaurus'][] = array(
//                                    'word' => $value['word'],
//                                    'positions' => $result
//                                );
//                            }
//                        }
//                    }

                if(
                    TRIGGER_DETECT_BOLD_TITLE &&
                    $this->validTitle($currentParts[$i]) &&
                    $currentParts[$i]['bold'] &&
                    $arrOn['detect_bold_title']
                ){ //Try define titles as bold and before return line before
                    $potentialTitle[] = $i;
                    $boldScore += mb_strlen($currentContent);
                }

                if(
                    TRIGGER_DETECT_NUMBER_TITLE &&
                    $this->validTitle($currentParts[$i]) &&
                    !isset($minLevel) &&
                    $arrOn['detect_number_title']
                ){ //Try define titles as numbers
                    if(preg_match($this->patternArabNum, $currentContent)){
                        $potentialTitleArab[] = $i;
                        $arabScope += mb_strlen($currentContent, 'UTF-8');
                    }

                    if(preg_match($this->patternRomNum, $currentContent)){
                        $potentialTitleRome[] = $i;
                        $romeScope += mb_strlen($currentContent, 'UTF-8');
                    }
                }

                if(
                    TRIGGER_DETECT_KEYWORDS_TITLE &&
                    $this->validTitle($currentParts[$i]) &&
                    preg_match($this->patternTitle, $currentContent) &&
                    $arrOn['detect_keywords_title']
                ){ //Try define titles as contains key-words
                    $currentParts[$i]['title'] = 1;
                    $currentParts[$i]['origin'] = 'keyword system';
                }

                if(
                    TRIGGER_DETECT_THREESTARS_TITLE &&
                    $this->validTitle($currentParts[$i]) &&
                    preg_match($this->patternTreeStars, $currentContent) &&
                    $arrOn['detect_three_stars_title']
                ){ //Try define titles as three starts
                    $currentParts[$i]['title'] = 1;
                    $currentParts[$i]['origin'] = 'three stars';
                }

                if(
                    TRIGGER_DETECT_OTHER_ELEMENTS &&
                    ($i<=$this->maxFirstPats || $i>=count($currentParts)-$this->maxFirstPats) &&
                    !$currentParts[$i]['name'] &&
                    !$currentParts[$i]['author'] &&
                    $arrOn['detect_other_elements_title']
                ){ //Try define other structure elements
                    for($rule=0;$rule<count($this->otherRules);$rule++){
                        if($this->otherRules[$rule]['rule']($currentParts[$i], $i, count($currentParts))){
                            $currentParts[$i]['element'] = $this->otherRules[$rule]['val'];
                            $currentParts[$i]['have_content'] = $this->otherRules[$rule]['have_content'];
                            $currentParts[$i]['origin'] = 'other elements system';
                            break;
                        }
                    }
                }

                if(
                    TRIGGER_DETECT_INLINE_ELEMENTS &&
                    !$currentParts[$i]['name'] &&
                    !$currentParts[$i]['author'] &&
                    $arrOn['detect_inline_elements']
                ){ //Try define inline elements
                    for($rule=0;$rule<count($this->inlineRules);$rule++){
                        if($result = $this->inlineRules[$rule]['rule']($currentParts[$i], $i, count($currentParts))){
                            $currentParts[$i]['inline'][] = array(
                                'val' => $this->inlineRules[$rule]['val'],
                                'coordinates' => $result
                            );
                        }
                    }
                }

                if(
                    TRIGGER_DETECT_FOOTNOTES &&
                    $this->validTitle($currentParts[$i]) &&
                    $arrOn['detect_foot_notes']
                ){
                    if(preg_match($patterFootNoteContent, $currentContent)){
                        $currentParts[$i]['content'] = preg_replace(
                            '/^\s*'.$patternFootNoteBegin.$patternFootNoteMiddle.$patternFootNoteFinal.'\s*/', '',
                            $currentContent
                        );
                        $currentParts[$i]['potential_foot_note'] = (int)preg_replace(
                            '/'.$patternFootNoteBegin.'/', '' , preg_replace(
                                '/'.$patternFootNoteFinal.'.*/', '', $currentContent)
                        );
                    }
                }
            }

            $boldShare = $boldScore/$totalScope*100;
            if($boldShare && $boldShare < $maxPercent){
                foreach($potentialTitleBold as $value){
                    $currentParts[$value]['title'] = 1;
                    $currentParts[$value]['origin'] = 'size title system';
                }
            }

            $arabNumShare = $arabScope/$totalScope*100;
            $romeShare = $romeScope/$totalScope*100;
            $topTitleLevel = $romeShare ? 1 : 0;

            if($arabNumShare && $arabNumShare < $maxPercent){
                foreach($potentialTitleArab as $value){

                    //response put-in title up to 10 put-ins
                    for($s=1;$s<=$this->numPutsIn;$s++){
                        $pattern = '/^\s*'.$this->patternBgnArabNum.'{'.$s.'}'.$this->patternEndArabNum.'/';
                        if(preg_match($pattern, $currentParts[$value]['content'])){
                            $currentParts[$value]['title'] = $topTitleLevel + $s;
                            $currentParts[$value]['origin'] = 'arab number system';
                            break;
                        }
                    }
                }
            }

            if($romeShare && $romeShare < $maxPercent){
                foreach($potentialTitleRome as $value){
                    $currentParts[$value]['title'] = 1;
                    $currentParts[$value]['origin'] = 'rome number system';
                }
            }

            //Joining epigraph parts
            if(count($epigraphs)){
                for($i=0;$i<count($epigraphs);$i++){
                    foreach($epigraphs[$i] as $value => $pos){
                        if($value != 0){
                            $currentParts[$epigraphs[$i][0]]['content'] .= '<br />'.$currentParts[$pos]['content'];
                            $toDeleteParts[] = $pos;
                        }
                    }
                }
            }

            if(count($toDeleteParts)){
                foreach($toDeleteParts as $value){
                    unset($currentParts[$value]);
                }
                $currentParts = array_values($currentParts);
            }
            return $currentParts;
        }
    }

    private function showCutText($startPos, $endPos, $content){
        $result = mb_substr($content, intval($startPos)-1, $endPos-$startPos+1, 'UTF-8');
        echo 'Text: '.$result.'<br />';
    }

    private function showTest($data, $content, $head, $headLevel=1){
        if(!sizeof($data)) return;
        $parts = $data['inline'] ? count($data)-1 : count($data);
        echo '<h'.$headLevel.'>'.$head.'</h'.$headLevel.'>';
        for($i=0;$i<$parts;$i++){
            if($data[$i]['type']){
                echo '<b>Element #'.$i.'</b><br />';
                if($data[$i]['id']) echo 'id: '.$data[$i]['id'].'<br />';
                if($data[$i]['origin']) echo 'origin: '.$data[$i]['origin'].'<br />';
                echo 'Type: '.$data[$i]['type'].'<br />';
                $this->showCutText($data[$i]['pos_start'], $data[$i]['pos_end'], $content);

//                    if($data[$i]['type'] == 'table') $this->showTable($data[$i]['content'], $content);


//                    if($data[$i]['footNote'] && is_array($data[$i]['footNote'])){
//                        echo '<h4 style="margin-left: 20px;">Foot Notes</h4>';
//                        foreach($data[$i]['footNote'] as $footNote){
//
//                            echo '<div style="margin: 0 0 35px 20px;"><i>';
//                            echo '<b>id:</b> '.$footNote['id'].'<br />';
//                            echo '<b>content:</b> '.$footNote['content'].'<br />';
//                            echo '<b>start pos:</b> '.$footNote['start_pos'].'<br />';
//                            echo '</i></div>';
//                        }
//                    }

                if($data[$i]['typeLvl']) echo 'Level: '.$data[$i]['typeLvl'].'<br />';
                if($data[$i]['pos_start']) echo 'Position Start: '.$data[$i]['pos_start'].'<br />';
                if($data[$i]['pos_end']) echo 'Position End: '.$data[$i]['pos_end'].'<br />';
                if($data[$i]['pos_in_text']) echo 'Position on the text: '.$data[$i]['pos_in_text'].'<br />';
                echo '----------------------------------------<br>';
            }
        }
    }

    private function secondTry($pos=NULL, $reverse=false, $content=NULL){
        global $textParts, $family_s, $names, $patternName, $detectPattern;

        if(!$pos && !$content) return false;

        $content = !$content ? $textParts[$pos]['content'] : $content;
        if($reverse){
            $potentialAuthor = preg_replace($patternName, '', $content);
            $potentialName = str_replace('. '.$potentialAuthor, '', $content);
        }else{
            $potentialName = preg_replace($patternName, '', $content);
            $potentialAuthor = str_replace('. '.$potentialName, '', $content);
        }

        if($detectPattern($potentialAuthor, $family_s) || $detectPattern($potentialAuthor, $names)){
            for($i=0;$i<count($textParts);$i++){
                if($textParts[$i]['name']) $textParts[$i]['name'] = false;
                if($textParts[$i]['author']) $textParts[$i]['author'] = false;
            }

            if($pos){
                $textParts[$pos]['author'] = true;
                $textParts[$pos]['content'] = $potentialAuthor;
                $textParts[$pos]['origin'] = 'second try system';
                array_unshift($textParts, array('name' => true, 'content' => $potentialName, 'origin' => 'second try system'));
                return true;
            }else return array('name' => $potentialName, 'author' => $potentialAuthor);
        }else return false;
    }

    public static function deleteParts(array $toDeleteParts, array $parts){
        foreach($toDeleteParts as $value){
            unset($parts[$value]);
        }
        return array_values($parts);
    }

    private function openLib($lib){
        if(!$extLib = fopen($lib, "r")){
            $this->resultJson = array('error' => 'Not found '.$lib.' library!');
            $this->showResult(true);
        }

        $content = stream_get_contents($extLib);
        return $content;
    }

    private function addLib($partLow, $bgnPattern, $mdlPattern, $endPattern, $upFirstLetter=false){
        $partHigh = mb_strtoupper($partLow, 'UTF-8');
        $data = '/'.$bgnPattern.preg_replace('/\n/', $mdlPattern, $partLow).$mdlPattern.preg_replace('/\n/', $mdlPattern, $partHigh);
        if($upFirstLetter) $data .= $mdlPattern.preg_replace('/\n/', $mdlPattern, mb_convert_case($partLow, 2, 'UTF-8'));
        $data .= $endPattern.'/';
        return preg_replace('/\r/', '', $data);
    }

//        private function addThesaurusLib($data){
//            $words = explode("\n", $data);
//            foreach($words as $word){
//                $this->thesaurus[$this->thesaurusCount] = array('word' => $word, 'pattern' => $this->addLib($word, '\s'.$this->patternSpots, $this->patternSpots.'|'.$this->patternSpots, $this->patternSpots.'\s', true));
//                $this->thesaurusCount++;
//            }
//        }

    private function validTitle($textPart){
        return (
            !$textPart['title'] &&
                !$textPart['name'] &&
                !$textPart['author'] &&
                !preg_match('/^(-|—|#)/', $textPart['content']) &&
                mb_strlen(preg_replace($this->spacePattern, '', $textPart['content']), 'UTF-8') < $this->maxTitleLetters &&
                preg_match($this->patternLetter, $textPart['content'])
        );
    }

    private function detectScore(array $rules, $pos, $nameScore){
        global $textParts;
        $score = 0;
        $content = $textParts[$pos]['content'];

        if(!preg_match($this->patternLetter, $content)){
            $textParts[$pos][$nameScore] = 0;
            return;
        }
        for($i=0;$i<count($rules);$i++){
            if($rules[$i]['rule']($pos)){
                $score += $rules[$i]['score'];
            }
        }
        $textParts[$pos][$nameScore] = $score;
    }

    private function removeTempDir($dir){
        if (@fopen($dir.'_rels/.rels', "r")) unlink($dir.'_rels/.rels');
        if ($objs = glob($dir."*")){
            foreach($objs as $obj){
                if(is_dir($obj)) $this->removeTempDir($obj.'/');
                else unlink($obj);
            }
        }
        rmdir($dir);
    }
}
?>