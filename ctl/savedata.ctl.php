<?	
require_once('ctl/base.ctl.php');
//    require_once('models/books.model.php');
require_once('models/themes_data.model.php');

class SavedataCtl extends BaseCtl{
//        private $bookModel;
    private $themeData;

    function __construct(){
        parent::__construct();
//            $this->bookModel = new BooksModel();
        $this->themeData = new ThemesDataModel();
    }

//        public function insertBook($name, $author, $bookUID, $widgets, $content){
//            $this->bookModel
//                ->sqlInsert(array(
//                    'user_uid' => $_COOKIE['lib_user_uid'],
//                    'book_uid' => $bookUID,
//                    'name' => $name,
//                    'author' => $author,
//                    'widgets' => json_encode($widgets),
//                    'content' => json_encode($content)
//                ))->exec();
//        }
//
//        public function saveBook(){
//            $bookUID = $this->params['book_uid'];
//            if(!$bookUID) $this->resultJson = array('error' => 'Not exist "book_uid" in incoming params');
//            else{
//                $this->bookModel->sqlSelect('id')->sqlFilter('book_uid', $bookUID)->exec();
//                if(!$this->bookModel->getField('id')->value[0]) $this->resultJson = array('error' => 'The Book UID '.$bookUID.' not exist in DB');
//                else{
//                    $updateFields = array();
//                    if($this->params['name']) $updateFields['name'] = $this->params['name'];
//                    if($this->params['author']) $updateFields['author'] = $this->params['author'];
//                    if($this->params['widgets']) $updateFields['widgets'] = json_encode($this->params['widgets']);
//                    if($this->params['content']) $updateFields['content'] = json_encode($this->params['content']);
//
//                    if(!sizeof($updateFields)) $this->resultJson = array('error' => 'Not exist no one of "name", "author", "widgets" or "content" in incoming params');
//                    else{
//                        $this->bookModel
//                            ->sqlUpdate($updateFields)
//                            ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
//                            ->sqlFilterAnd()
//                            ->sqlFilter('book_uid', $bookUID)
//                            ->exec();
//                        $this->resultJson = array('success' => 1);
//                    }
//                }
//            }
//            $this->showResult();
//        }

    public function insertTheme(){
        if($this->setBookParams()){
            $this->themeData->sqlSelect('id')->sqlFilter('book_uid', $this->bookUid)->sqlFilterAnd()->sqlFilter('theme_uid', $this->themeUid)->exec();
            if($this->themeData->getField('id')->value[0]) $this->resultJson['error'] = 'That pair of "book_uid" and "theme_uid" already exist!';
            else{
                $this->themeData->sqlInsert(
                    array(
                        'book_uid' => $this->bookUid,
                        'theme_uid' => $this->themeUid,
                        'theme_data' => $this->data,
                        'html_data' => $this->params['html']
                    )
                )->exec();
                $this->resultJson = array('success' => 1);
            }
        }
        $this->showResult();
    }

    public function updateData(){
        if($this->setBookParams()){

            $updateFields = array('theme_uid' => $this->themeUid);
            if($this->data) $updateFields['theme_data'] = $this->data;
            if($this->params['html']) $updateFields['html_data'] = $this->params['html'];

            $this->themeData->sqlUpdate($updateFields)
                        ->sqlFilter('book_uid', $this->bookUid)
                        ->sqlFilterAnd()
                        ->sqlFilter('theme_uid', $this->themeUid)
                        ->exec();
            $this->resultJson = array('success' => 1);
        }
        $this->showResult();
    }

    public function getHtml(){
        if($this->setBookParams()){
            $this->themeData
                ->sqlSelect('html_data')
                ->sqlFilter('book_uid', $this->bookUid)
                ->sqlFilterAnd()
                ->sqlFilter('theme_uid', $this->themeUid)
                ->exec();

            echo $this->themeData->getField('html_data')->value[0];
        }else $this->showResult();
    }

    public function adminCheck(){
        if(md5($this->params['password']) == ADMIN_PASS) $this->resultJson = array('success' => 1);
        else $this->resultJson = array('error' => 'Invalid password');
        $this->showResult();
    }
}
?>