<?
	require_once('ctl/base.ctl.php');
    require_once('models/scripts.model.php');
    require_once('models/themes_data.model.php');
	
	class ScriptsCtl extends BaseCtl{
        private $script;

        function __construct(){
            parent::__construct();
            $this->script = new ScriptsModel();
        }

        public function getScripts(){
            $this->script->sqlSelect()->exec();
            $result = array();
            for($i=0;$i<$this->script->getNumRows();$i++){
                $result[$i]['id'] = $this->script->getField('id')->value[$i];
                $result[$i]['script_name'] = $this->script->getField('name')->value[$i];
                $result[$i]['script_demo'] = $this->script->getField('demo')->value[$i];
                $result[$i]['script_conf'] = $this->script->getField('conf')->value[$i];
            }
            return $result;
        }

        public function getScriptsPublic(){
            $this->redirectNotAdmin();
            $this->resultJson = $this->getScripts();
            $this->showResult();
        }

        public function saveCurrentScript(){
            $this->redirectNotAdmin();
            if(!$idScript = $this->params['id']) $this->resultJson['error'] = 'Not exist "id" in incoming params';
            elseif(!$field = $this->params['script_field']) $this->resultJson['error'] = 'Not exist "script_field" in incoming params';
            else{
                $this->script->sqlUpdate(array($field => $this->params['value']))->sqlFilter('id',$idScript)->exec();
                $this->resultJson = array('success' => 1);
            }
            $this->showResult();
        }

        public function insertScripts(){
            $this->redirectNotAdmin();
            $updateFields = array();
            if($this->params['script_name']) $updateFields['script_name'] = $this->params['script_name'];
            if($this->params['script_demo']) $updateFields['script_demo'] = $this->params['script_demo'];
            if($this->params['script_conf']) $updateFields['script_conf'] = $this->params['script_conf'];

            if(!sizeof($updateFields))
                $this->resultJson = array('error' => 'Not exist no one of "script_name", "demo_script" or "conf_script" in incoming params');
            else{
                $this->script->sqlInsert($updateFields)->exec();
                $this->resultJson = array('success' => 1);
            }
            $this->showResult();
        }

        public function deleteScripts(){
            $this->redirectNotAdmin();
            if(!$idScript = $this->params['id']) $this->resultJson['error'] = 'Not exist "id" in incoming params';
            else{
                $this->script->sqlDelete($idScript)->exec();
                $this->resultJson = array('success' => 1);
            }
            $this->showResult();
        }

        public function saveScripts(){
            if($this->setBookParams(true, array('id','pos'))){
                $themeData = new ThemesDataModel();

                $themeData
                    ->sqlSelect('script_data')
                    ->sqlFilter('book_uid', $this->bookUid)
                    ->sqlFilterAnd()
                    ->sqlFilter('theme_uid', $this->themeUid)
                    ->exec();

                $data = json_decode($themeData->getField('script_data')->value[0]);
                $newData = array();

                if($data){
                    $concurFlag = false;
                    for($i=0;$i<count($data);$i++){
                        $newData[$i] = new stdClass();
                        $newData[$i]->pos = $data[$i]->pos;

                        if($data[$i]->pos == $this->params['pos']){
                            $concurFlag = true;
                            $newData[$i]->id = $this->params['id'];
                            $newData[$i]->data = $this->params['data_js'] ? $this->params['data_js'] : new stdClass();
                        }else{
                            $newData[$i]->id = $data[$i]->id;
                            $newData[$i]->data = $data[$i]->data;
                        }
                    }

                    if(!$concurFlag){
                        $nextLast = count($newData);
                        $newData[$nextLast] = new stdClass();

                        $newData[$nextLast]->id = $this->params['id'];
                        $newData[$nextLast]->pos = $this->params['pos'];
                        $newData[$nextLast]->data = $this->params['data_js'] ? $this->params['data_js'] : new stdClass();
                    }

                    $themeData->sqlUpdate(array('script_data' => json_encode($newData)))
                        ->sqlFilter('book_uid', $this->bookUid)
                        ->sqlFilterAnd()
                        ->sqlFilter('theme_uid', $this->themeUid)
                        ->exec();
                }else{
                    $newData[0] = new stdClass();
                    $newData[0]->pos = $this->params['pos'];
                    $newData[0]->id = $this->params['id'];
                    $newData[0]->data = new stdClass();

                    $themeData->sqlInsert(array(
                        'book_uid' => $this->bookUid,
                        'theme_uid' => $this->themeUid,
                        'script_data' => json_encode($newData)
                    ))->exec();
                }

                $this->resultJson['success'] = true;
            }
            $this->showResult();
        }
	}
?>