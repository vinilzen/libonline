<?
require_once('ctl/base.ctl.php');
require_once('ctl/scripts.ctl.php');
require_once('models/themes.model.php');
require_once('models/themes_data.model.php');

class TemplatesCtl extends BaseCtl{
    private $theme;
    private $userUID;
    private $themeData;

    private $path;
    private $name;
    private $template;
    private $allTemplates;
    private $templateFile;
    private $idealXML;

    private $ext;
    private $aliasing;
    private $defaultTags;
    private $mergeWidgets;
    private $skipTags;
    private $skipSubTags;
    private $cssRootTags;
    private $multiTags;
    private $cssTags;
    private $typeTemplates;

    function __construct(){
        parent::__construct();
        require_once('config/config.tagstemplates.php');
        $this->theme = new ThemesModel();

        foreach($this->aliasing as $alias => $val){
            if(preg_match('/USER_/', $alias) || $alias == '/\/IMAGES\//' || $alias == '/\/MULTIMEDIA\//') $this->aliasing[$alias] .= $_COOKIE['lib_user_uid'].'/';
            if(preg_match('/_THEMES/', $alias)) $this->aliasing[$alias] = PATH_TEMPLATES.$this->aliasing[$alias];
            if(preg_match('/_DROP_CAPS/', $alias)) $this->aliasing[$alias] = PATH_DROP_CAPS.$this->aliasing[$alias];
        }
    }

    public function _default(){
        if($this->setBookParams()){
            $themeData = new ThemesDataModel();
            $themeData->sqlSelect()->sqlFilter('book_uid', $this->bookUid)->sqlFilterAnd()->sqlFilter('theme_uid', $this->themeUid)->exec();
            $this->theme->sqlSelect()->sqlFilter('theme_uid', $this->themeUid)->exec();

            if($this->theme->getField('theme_name')->value[0]){
                $this->themeData = $themeData->getField('theme_data')->value[0];
                $scriptData = json_decode($themeData->getField('script_data')->value[0]);

                $this->userUID = $this->theme->getField('user_uid')->value[0];
                $this->name = $this->theme->getField('theme_name')->value[0].'.xml';

                if($this->setAndCheckTemplate($this->theme->getField('theme_type')->value[0])){
                    if($this->themeData) $result = $this->checkSubNode($this->template, json_decode($this->themeData));
                    else $result = $this->remountTemplate($this->template);

                    foreach($result as $key => $value){
                        if($key == 'id') $this->resultJson[$key] = intval($value);
                        elseif($key == 'name' || $key == 'icon' || $key == 'previews') $this->resultJson[$key] = strval($value);

                        elseif(in_array($key, $this->mergeWidgets)){
                            $this->resultJson[$key] = array();
                            foreach($value as $mergeKey => $mergeVal){
                                $placeholders = array();
                                foreach($mergeVal as $val){
                                    $placeholders[] = $val;
                                }
                                $this->resultJson[$key][$mergeKey] = $placeholders;
                            }
                        }

                        else $this->resultJson[$key] = $value;
                    }

                    foreach($this->defaultTags as $tag){
                        if(!in_array($tag, array_keys($this->resultJson))){
                            $this->resultJson[$tag] = $this->idealXML->$tag;
                        }
                    }

                    foreach($this->mergeWidgets as $tag){
                        if($this->idealXML->$tag){
                            foreach($this->idealXML->$tag as $mergeWidgets){
                                foreach($mergeWidgets as $keyWidget => $valWidget){
                                    foreach($valWidget as $widget){
                                        $this->resultJson[$tag][$keyWidget][] = $widget;
                                    }
                                }
                            }
                        }
                    }

                    $allScripts = new ScriptsCtl();
                    $this->resultJson['scripts'] = $allScripts->getScripts();

                    if(isset($scriptData) && $scriptData){
                        $dataScripts = array();
                        for($i=0;$i<count($scriptData);$i++){
                            $dataScripts[$i]['id'] = $scriptData[$i]->id;
                            $dataScripts[$i]['pos'] = $scriptData[$i]->pos;
                            $dataScripts[$i]['data'] = $scriptData[$i]->data;
                        }
                        $this->resultJson['scripts_data'] = $dataScripts;
                    }
                }
            }else $this->resultJson['error'] = 'Requested template number '.$this->themeUid.' not exist';
        }
        $this->showResult();
    }

    private function checkSubNode($node, $checkNode){
        $returnArray = array();
        foreach($node as $name => $val){
            $consFlag = false;
            foreach($checkNode as $subName => $subVal){
                if($name == $subName){
                    if(is_object($val) && is_object($subVal)) $returnArray[$name] = $this->checkSubNode($val, $subVal);
                    else $returnArray[$subName] = $subVal;
                    $consFlag = true;
                    break;
                }else $returnArray[$subName] = $subVal;
            }
            if(!$consFlag){
                if (is_object($val) && !sizeof($val)) $returnArray[$name] = $this->replaceAlias(strval($val));
                else $returnArray[$name] = $this->replaceAlias($val);
            }
        }
        return $returnArray;
    }

    private function setFinalArr(){
        $returnedArr = array();
        for($i=0;$i<count($this->theme->getField('theme_name')->value);$i++){
            $returnedArr[$i]['theme_uid'] = $this->theme->getField('theme_uid')->value[$i];
            $returnedArr[$i]['id'] = $this->theme->getField('theme_uid')->value[$i];
            $returnedArr[$i]['theme_name'] = $this->theme->getField('theme_name')->value[$i];
            $returnedArr[$i]['theme_path'] = $this->path;
            $returnedArr[$i]['url'] = $this->path;
        }
        return $returnedArr;
    }

    private function synchronize(){
        if(!$this->allTemplates) return $this->theme;
        for($i=0;$i<count($this->theme->getField('theme_name')->value);$i++){
            $coinFlag = false;
            for($f=0;$f<count($this->allTemplates);$f++){
                if($this->theme->getField('theme_name')->value[$i] == $this->allTemplates[$f]['name']){
                    $coinFlag = true;
                    break;
                }
            }
            if(!$coinFlag){
                $this->deleteTheme($this->theme->getField('theme_uid')->value[$i]);
                foreach($this->theme->getFields() as $field){
                    array_splice($this->theme->getField($field)->value, $i, 1);
                }
            }
        }

        for($f=0;$f<count($this->allTemplates);$f++){
            $coinFlag = false;
            for($i=0;$i<count($this->theme->getField('theme_name')->value);$i++){
                if($this->theme->getField('theme_name')->value[$i] == $this->allTemplates[$f]['name']){
                    $coinFlag = true;
                    break;
                }
            }
            if(!$coinFlag && !preg_match('/^ideal_.+$/', $this->allTemplates[$f]['name'])){
                $this->templateFile = $_SERVER['DOCUMENT_ROOT'].$this->path.$this->allTemplates[$f]['name'].'.xml';
                $this->deleteTheme();
            }
        }
        return $this->theme;
    }

    public function getTypeTemplates(){
        $this->resultJson = $this->typeTemplates;
        $this->showResult();
    }

    public function getBaseTemplates(){
        $this->path = PATH_TEMPLATES.PATH_BASE_TEMPLATES;
        $this->allTemplates = $this->getFolders($_SERVER['DOCUMENT_ROOT'].$this->path, 'template', true, '.xml');
        $this->theme->sqlSelect(array('theme_uid', 'theme_name'))->sqlFilter('user_uid', ADMIN_PASS)->exec();
        $this->synchronize();
        $this->resultJson = $this->setFinalArr($this->theme, $this->path);
        $this->showResult();
    }

    public function getUserTemplates(){
        $this->path = PATH_TEMPLATES.PATH_USER_TEMPLATES.$_COOKIE['lib_user_uid'].'/';
        $this->allTemplates = $this->getFolders($_SERVER['DOCUMENT_ROOT'].$this->path, 'template', true, '.xml');
        $this->theme->sqlSelect(array('theme_uid', 'theme_name'))->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])->exec();
        $this->synchronize();
        $this->resultJson = $this->setFinalArr($this->theme, $this->path);
        $this->showResult();
    }

    private function checkTags($sampleTags, $checkingTags, $reverse, $rootTag='root', $prevRoot=''){
        $simpleMainTag = NULL;
        $rootTag = $prevRoot ? $prevRoot.'->'.$rootTag : $rootTag;

        foreach($sampleTags as $samName => $samTag){
            $matchFlag = false;
            foreach($checkingTags as $name => $tag){

                if(preg_match('/^\w+1$/', $samName) && !$reverse) $simpleMainTag = $samTag;

                if($name == $samName){
                    $matchFlag = true;
                    if(sizeof($samTag) && !sizeof($tag))
                        $this->resultJson[]['error'] = $reverse ? 'In "'.$rootTag.'" content tag-name "'.$name.'" must be array' : 'In "'.$rootTag.'" content tag-name "'.$name.'" must not be array';
                    else $this->checkTags($samTag, $tag, $reverse, $name, $rootTag);
                }
            }

            if(!$matchFlag && $simpleMainTag){
                $matchFlag = true;
                $this->checkTags($samTag, $simpleMainTag, false, $samName, $rootTag);
            }

            if(!$matchFlag){
                foreach($this->cssRootTags as $tag){
                    if(preg_match('/'.$tag.'$/', $rootTag) && in_array($samName, $this->cssTags)){
                        $matchFlag = true;
                        break;
                    }
                }
            }

            if(!$matchFlag){
                foreach($this->multiTags as $tag => $skipTags){
                    if(preg_match('/^'.$tag.'\d+$/', $rootTag) && in_array($samName, $skipTags)){
                        $matchFlag = true;
                        break;
                    }
                }
            }

            if(
                !$matchFlag &&
                !in_array($samName, $this->skipTags) &&
                !(array_key_exists($rootTag, $this->skipSubTags) && in_array($samName, $this->skipSubTags[$rootTag]))
            ){
                $this->resultJson[]['error'] = $reverse ? 'In "'.$rootTag.'" missing required tag-name "'.$samName.'"' : 'In "'.$rootTag.'" the tag-name "'.$samName.'" doesn\'t support';
            }
        }
    }

    public function validateTemplate($name, $type){
        $saveThemeUID = $this->setThemeUID($name);
        if($this->setAndCheckTemplate($type)){
            $this->checkTags($this->idealXML, $this->template, true);
            $this->checkTags($this->template, $this->idealXML, false);
        }

        if(sizeof($this->resultJson)){
            $this->deleteTheme($saveThemeUID);
            return false;
        }else{
            if(!$saveThemeUID){
                $this->theme->sqlInsert(
                    array(
                        'theme_name' => str_replace('.xml', '', $name),
                        'theme_type' => $type,
                        'user_uid' => $_COOKIE['lib_user_uid']
                    )
                )->exec();
                $saveThemeUID = $this->theme->getLastId();
            }
            if($this->template->id) $this->template->id = $saveThemeUID;
            else $this->template->addChild('id', $saveThemeUID);
            $this->template->asXML($this->templateFile);
            return true;
        }
    }

    public function updateTemplate(){
        if($this->setBookParams(true, 'data')){
            $themeData = new ThemesDataModel();
            $themeData->sqlSelect('id')->sqlFilter('theme_uid', $this->themeUid)->sqlFilterAnd()->sqlFilter('book_uid', $this->bookUid)->exec();
            if(!$themeData->getField('id')->value[0]) $themeData->sqlInsert(array('book_uid' => $this->bookUid, 'theme_uid' => $this->themeUid, 'theme_data' => $this->data))->exec();
            else{
                $result = count($themeData->getField('theme_data')->value) ? $this->checkSubNode(json_decode($themeData->getField('theme_data')->value[0]), json_decode($this->data)) : $this->data;
                $themeData->sqlUpdate(array('theme_data' => $result))->sqlFilter('theme_uid', $this->themeUid)->sqlFilterAnd()->sqlFilter('book_uid', $this->bookUid)->exec();
            }
            $this->resultJson['success'] = true;
        }
        $this->showResult();
    }

    public function deleteTheme($saveThemeUID=NULL, $deleteFile=true){
        if($saveThemeUID){
            $themeData = new ThemesDataModel();
            $themeData->sqlSelect('id')->sqlFilter('theme_uid', $saveThemeUID)->exec();
            if(sizeof($themeData->getField('id')->value)) $themeData->sqlDelete($themeData->getField('id')->value)->exec();
            $this->theme->sqlDelete($saveThemeUID)->exec();
        }
        if($deleteFile) @unlink($this->templateFile);
    }

    public function setAndCheckTemplate($type=NULL){
        $this->userUID = $this->userUID ? $this->userUID : $_COOKIE['lib_user_uid'];
        $this->path = $this->userUID == ADMIN_PASS ? PATH_TEMPLATES.PATH_BASE_TEMPLATES : PATH_TEMPLATES.PATH_USER_TEMPLATES.$this->userUID.'/';

        if(!preg_match($this->ext, $this->name)) $this->name = $this->name.'.xml';
        $this->templateFile = $_SERVER['DOCUMENT_ROOT'].$this->path.$this->name;

        if(!$this->checkExistFile($this->templateFile)){
            $this->resultJson['error'] = 'Not exist "'.$this->path.$this->name.'" file template';
            return false;
        }elseif(!@$this->template = simplexml_load_file($this->templateFile)){
            $this->resultJson['error'] = 'Error parsing file template "'.$this->path.$this->name.'"';
            return false;
        }

        if($type){
            $idealTemplate = $_SERVER['DOCUMENT_ROOT'].PATH_TEMPLATES.PATH_BASE_TEMPLATES.'ideal_'.$type.'.xml';
            if(!in_array($type, $this->typeTemplates)){
                $this->resultJson['error'] = 'The type of template "'.$type.'" isn\'t supports';
                return false;
            }elseif(!$this->checkExistFile($idealTemplate)){
                $this->resultJson['error'] = 'Cannot load basing template "'.$idealTemplate.'" for validating your template';
                return false;
            }else{
                $this->idealXML = simplexml_load_file($idealTemplate);
//                switch($type){
//                    case 'book':
//                        $this->multiTags[] = 'first_page';
//                        $this->multiTags[] = 'last_page';
//                        $this->multiTags[] = 'page_templates';
//
//                        for($i=0;$i<100;$i++){
//                            $this->specialRootTags[] = 'page'.$i;
//                        }
//
//                        $this->skipTags[] = 'contents';
//                }
            }
        }
        return true;
    }

    private function remountTemplate($data){
        $result = array();
        foreach($data as $key => $val){
            if(count($val)) $result[$key] = $this->remountTemplate($val);
            elseif(is_object($val)) $result[$key] = $this->replaceAlias(strval($val));
            else $result[$key] = $this->replaceAlias($val);
        }
        return $result;
    }

    private function replaceAlias($value){
        foreach($this->aliasing as $alias => $val){
            if(preg_match($alias, $value)) $value = preg_replace($alias, $val, $value);
        }
        return $value;
    }

    public function setThemeUID($name){
        if(preg_match($this->ext, $name)) $name = str_replace('.xml', '', $name);
        $this->name = $name;
        $this->theme
            ->sqlSelect('theme_uid')
            ->sqlFilter('theme_name', $name)
            ->sqlFilterAnd()
            ->sqlFilter('user_uid', $_COOKIE['lib_user_uid'])
            ->exec();
        return $this->theme->getField('theme_uid')->value[0];
    }

    public static function setThemePath(){
        $path = self::checkAdmin() ? PATH_TEMPLATES.PATH_BASE_TEMPLATES : PATH_TEMPLATES.PATH_USER_TEMPLATES;
        return $path;
    }
}
?>