<?
	require_once('ctl/base.ctl.php');
	require_once('view/test.view.php');
    require_once('models/books.model.php');
	
	class TestCtl extends BaseCtl{
		public function _default(){
			$view = new TestView();
			$view->_default();
		}

        public function receiveRemote(){
//            $book = new BooksModel();
//            $data = $_POST['book_uid'];
//            $book->sqlInsert(array('book_uid' => $data))->exec();
            echo '1111';
        }

        public function sendRemote(){
            $host = 'localhost';
            $path = '/test/receiveRemote/';
            $data = "book_uid=23232323";

            $fp = fsockopen($host, 80, $errno, $errstr, 30);
            if(!$fp) echo "$errstr ($errno)<br />\n";
            else{
                $headers = "POST $path HTTP/1.1\n";
                $headers .= "Host: $host\n";
                $headers .= "Content-Type: application/x-www-form-urlencoded\n";
                $headers .= "Content-Length: ".strlen($data)."\n\n";
                $headers .= $data."\n";
                $headers .= "Connection: Close\n\n";

                fwrite($fp, $headers);
                $result = '';

                while(!feof($fp)){
                    $result .= fgets($fp, 1024);
                }

//                fclose($fp);

                echo $result;
            }
        }
	}
?>