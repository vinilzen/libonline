<?
require_once('models/files.model.php');

require_once('lib/pclzip.lib.php');
require_once('inc/constant.inc.php');
require_once('ctl/base.ctl.php');
require_once('ctl/parser.ctl.php');
require_once('ctl/templates.ctl.php');
require_once('ctl/dropcaps.ctl.php');

class UploadCtl extends BaseCtl{

    private $docFormats;
    private $maxVolDocx;

    private $imageFormats;
    private $maxVolImage;

    private $multimediaFormats;
    private $maxVolMultimedia;

    private $dropCapsFormatsImage;
    private $maxVolDropCapsFormatsImage;

    private $rootDir;
    private $subFolder;
    private $fullPath;
    private $shortPath;
    private $currentFile;

    private $file;
    private $destinationName;
    private $fileTable;

    function __construct(){
        parent::__construct();
        require('config/config.doctypes.php');
        $this->fileTable = new FilesModel();
    }

    public function uploadFile(){
        if($this->checkUpload('doc_file', $this->docFormats, $this->maxVolDocx)){
            $bookUID = time();
            $rootDir = $_SERVER['DOCUMENT_ROOT'].PATH_UPLOAD.'book'.$bookUID.'/';
            $fileName = 'book.zip';
            mkdir($rootDir);

            if(copy($this->file['tmp_name'], $rootDir.$fileName)){
                $archive = new PclZip($rootDir.$fileName);
                $archive->extract($rootDir);
                $parser = new ParserCtl();
                $parser->parseXML(
                    $bookUID, preg_replace('/\..*$/', '', $_FILES['doc_file']['name']),
                    $this->params['test'],
                    $this->params['test_stat']
                );
            }else{
                $this->resultJson['error'] = 'Error upload file';
                $this->showResult();
            }
        }
    }

    // Upload Image
    public function uploadImage(){
        $this->saveFiles('file_name', 'image', PATH_UPLOAD_IMAGES, $this->imageFormats, $this->maxVolImage);
        $this->writeFileInBase('image');
        $this->showResult();
    }

    // Upload Multimedia
    public function uploadMultimedia(){
        $this->saveFiles('file_name', 'multimedia', PATH_UPLOAD_MULTIMEDIA, $this->multimediaFormats, $this->maxVolMultimedia);
        $this->writeFileInBase('multimedia');
        $this->showResult();
    }

    // Upload Cliparts
    public function uploadClipArt(){
        if($this->redirectNotAdmin()){
            $this->saveFiles('file_name', 'clipart', PATH_CLIPART, $this->imageFormats, $this->maxVolImage, false);
            $this->writeFileInBase('clipart');
            $this->showResult();
        }
    }

    public function uploadMultimediaClipArt(){
        if($this->redirectNotAdmin()){
            $this->saveFiles('file_name', 'multimedia_clipart', PATH_CLIPART_MULTIMEDIA, $this->multimediaFormats, $this->maxVolMultimedia, false);
            $this->writeFileInBase('multimedia_clipart');
            $this->showResult();
        }
    }

    // Upload Template and any stuff
    public function uploadTemplate(){
        if($this->params['type']){
            if($name = $this->saveFiles('file_name', 'xml', TemplatesCtl::setThemePath(), array('text/xml'), 1048576, !$this->checkAdmin())){
                $template = new TemplatesCtl();
//                $template->validateTemplate($name, $this->params['type']);
//                die;
                if(!$template->validateTemplate($name, $this->params['type'])) $template->showResult(true);
            }
        }else $this->resultJson['error'] = 'You didn\'t define type of template';
        $this->showResult();
    }

    public function uploadImageTheme(){
        $this->saveFiles('file_name', 'image', TemplatesCtl::setThemePath(), $this->imageFormats, 104857, !$this->checkAdmin());
        $this->writeFileInBase('image_theme');
        $this->showResult();
    }

    public function uploadBaseImageTheme(){
        if($this->redirectNotAdmin()){
            $this->saveFiles('file_name', 'image', PATH_TEMPLATES.PATH_BASE_TEMPLATES, $this->imageFormats, 104857);
            $this->writeFileInBase('image_theme');
            $this->showResult();
        }
    }

    // Upload Drop-cap
    public function uploadDropCap(){
        $pathUpload = DropCapsCtl::setThemePath();
        $imageName = $_FILES['image_drop_cap']['name'];
        $configName = preg_replace('/\.png$|\.jpg$|\.jpeg$|\.gif$|/', '', $imageName).'.xml';

        if(!$imageName || !$configName) $this->resultJson['error'] = 'You must upload only pair files: image file and config file';
        else{
            if($this->saveFiles('image_drop_cap', 'drop cap', $pathUpload, $this->dropCapsFormatsImage, $this->maxVolDropCapsFormatsImage, !$this->checkAdmin())){
                if($this->saveFiles('config_drop_cap', 'drop cap', $pathUpload, array('text/xml'), 104857, !$this->checkAdmin(), $configName)){

                    $this->resultJson = array();

                    $dropCap = new DropCapsCtl();
                    if($dropCap->validateConfig($configName)){
                        $this->resultJson['success'] = true;
                        $this->resultJson['drop_cap_image'] = $imageName;
                        $this->resultJson['drop_cap_config'] = $configName;
                    }else{
                        $this->currentFile = $imageName;
                        $this->deleteFile($pathUpload, true);

                        $this->currentFile = $configName;
                        $this->deleteFile($pathUpload, true);

                        $this->resultJson = $dropCap->resultJson;
                    }
                }else{
                    $this->currentFile = $imageName;
                    $this->deleteFile($pathUpload, true);
                }
            }
        }
        $this->showResult();
    }

    // Create Folders
    public function createImageFolder(){
        $this->createFolder(PATH_UPLOAD_IMAGES);
    }

    public function createMultimediaFolder(){
        $this->createFolder(PATH_UPLOAD_MULTIMEDIA);
    }

    public function createClipArtFolder(){
        if($this->redirectNotAdmin()) $this->createFolder(PATH_CLIPART);
    }

    public function createClipArtMultimediaFolder(){
        if($this->redirectNotAdmin()) if($this->redirectNotAdmin()) $this->createFolder(PATH_CLIPART_MULTIMEDIA);
    }

    public function createTemplateFolder(){
        if($this->redirectNotAdmin()) $this->createFolder(PATH_TEMPLATES.PATH_BASE_TEMPLATES);
    }

    // Delete folders
    public function deleteImageFolder(){
        $this->deleteFolder(PATH_UPLOAD_IMAGES, true);
    }

    public function deleteMultimediaFolder(){
        if($this->redirectNotAdmin()) $this->deleteFolder(PATH_UPLOAD_MULTIMEDIA, true);
    }

    public function deleteClipArtFolder(){
        if($this->redirectNotAdmin()) $this->deleteFolder(PATH_CLIPART, true);
    }

    public function deleteThemeFolder(){
        $this->deleteFolder(TemplatesCtl::setThemePath());
    }

    // Delete files
    public function deleteDropCapFile(){
        $this->deleteFile(DropCapsCtl::setThemePath());
    }

    public function deleteImageFile(){
        $this->deleteFile(PATH_UPLOAD_IMAGES);
    }

    public function deleteMultimediaFile(){
        $this->deleteFile(PATH_UPLOAD_MULTIMEDIA);
    }

    public function deleteClipArtFile(){
        if($this->redirectNotAdmin()) $this->deleteFile(PATH_CLIPART);
    }

    public function deleteMultimediaClipArtFile(){
        if($this->redirectNotAdmin()) $this->deleteFile(PATH_UPLOAD_MULTIMEDIA);
    }

    public function deleteTemplate(){
        if(!$this->params['name']) $this->resultJson['error'] = 'The name of the template is not set';
        else{
            $template = new TemplatesCtl();
            if(!$themeUID = $template->setThemeUID($this->params['name'])) $this->resultJson['error'] = 'The template with the name "'.$this->params['name'].'" not exist';
            elseif(!$template->setAndCheckTemplate()) $template->showResult(true);
            else{
                $template->deleteTheme($themeUID);
                $this->resultJson['success'] = true;
                $this->resultJson['name'] = $this->params['name'];
            }
        }
        $this->showResult();
    }

    public function deleteBaseImageFile(){
        if($this->redirectNotAdmin()) $this->deleteFile(PATH_TEMPLATES.PATH_BASE_TEMPLATES);
    }

    // Get Files
    public function getBaseThemeFiles(){
        if($this->redirectNotAdmin()) $this->getFiles(PATH_TEMPLATES.PATH_BASE_TEMPLATES, 'theme');
    }

    public function getImage(){
        $this->getFiles(PATH_UPLOAD_IMAGES, 'image', NULL, $_COOKIE['lib_user_uid']);
    }

    public function getMultimedia(){
        $this->getFiles(PATH_UPLOAD_MULTIMEDIA, 'multimedia', NULL, $_COOKIE['lib_user_uid']);
    }

    public function getClipArts(){
        $this->getFiles(PATH_CLIPART, 'clipart', NULL, ADMIN_PASS);
    }

    public function getMultimediaClipArts(){
        $this->getFiles(PATH_CLIPART_MULTIMEDIA, 'multimedia_clipart', NULL, ADMIN_PASS);
    }

    public function getDropCapsBase(){
        $this->getFiles(PATH_DROP_CAPS.PATH_BASE_DROP_CAPS, 'drop_caps_file', 'xml');
    }

    public function getDropCapsCustom(){
        $this->getFiles(PATH_DROP_CAPS.PATH_USER_DROP_CAPS, 'drop_caps_file', 'xml');
    }

    public function setFileTags(){
        $params = json_decode($this->params['model']);
        if(!$params->id) $this->resultJson['error'] = 'You didn\'t select the "id" file';
        else{
            $this->fileTable->sqlSelect('user_uid')->sqlFilter('id', $params->id)->exec();
            if(
                $this->fileTable->getField('user_uid')->value[0] == ADMIN_PASS &&
                $_COOKIE['lib_user_uid'] != ADMIN_PASS
            ) $this->resultJson['error'] = 'Access denied';
            else{
                $this->fileTable->
                    sqlUpdate(array('tags' => json_encode($params->tags)))->
                    sqlFilter('id', $params->id)->
                    exec();
                $this->resultJson['success'] = true;
            }
        }
        $this->showResult();
    }

    // Private methods
    private function saveFiles($inputName, $typeFile, $pathUpload, array $formats, $maxVolSize, $makeFolders=true, $nameFile=NULL/*, $overwrite=false,*/){
        if($this->checkUpload($inputName, $formats, $maxVolSize)){
            $this->setFolders($pathUpload);
            $this->destinationName = $nameFile ? $nameFile : $this->file['name'];

            if($makeFolders && !is_dir($this->rootDir) && !@mkdir($this->rootDir)) $this->resultJson['error'] = 'Error create main folder';
            elseif($this->subFolder && !$this->checkFolderName()) $this->resultJson['error'] = 'You must enter only latin symbols';
            elseif($this->subFolder && !is_dir($this->fullPath) && !@mkdir($this->fullPath)) $this->resultJson['error'] = 'Error create sub-folder "'.$this->subFolder.'"';
//            elseif($overwrite && $this->checkExistFile($this->fullPath.$this->file['name'])) $this->resultJson['error'] = 'File already exist';
            elseif(@copy($this->file['tmp_name'], $this->fullPath.$this->destinationName)){
                    $this->resultJson['success'] = true;
                    $this->resultJson[$typeFile] = $this->file['name'];
            }else $this->resultJson['error'] = 'Error upload file';
        }
        return !$this->resultJson['error'] ? $this->file['name'] : false;
    }

    private function getFiles($pathUpload, $type, $ignoreExt=NULL, $userUID=NULL){
        $this->setFolders($pathUpload);
        if(is_dir($this->fullPath)){
            $result = $this->getFolders($this->fullPath, $type, false, null, $ignoreExt);

            if($userUID){
                $this->fileTable->sqlSelect()->
                    sqlFilter('user_uid', $userUID)->
                    sqlFilterAnd()->
                    sqlFilter('type', $type)->
                    sqlFilterAnd()->
                    sqlFilter('path', $this->shortPath.$this->subFolder)->
                    exec();

//                $toDelete = array();
                for($i=0;$i<$this->fileTable->getNumRows();$i++){
//                    $much = false;
                    foreach($result as $key => $file){
                        if($file['name'] == $this->fileTable->getField('filename')->value[$i]){
//                            $much = true;
                            $result[$key]['tags'] = json_decode($this->fileTable->getField('tags')->value[$i]);
                            $result[$key]['data'] = $this->fileTable->getField('data')->value[$i];
                            $result[$key]['id'] = $this->fileTable->getField('id')->value[$i];
                            break;
                        }
                    }
//                    if(!$much) $toDelete[] = $this->fileTable->getField('id')->value[$i];
                }
//                if(count($toDelete))  $this->fileTable->sqlDelete($toDelete);
            }
            $this->resultJson = $result;
        }
        $this->showResult();
    }

    private function createFolder($pathUpload){
        $this->setFolders($pathUpload);
        if(!$this->subFolder) $this->resultJson['error'] = 'The folder isn\'t set';
        elseif(!is_dir($this->rootDir) && !@mkdir($this->rootDir)) $this->resultJson['error'] = 'Error create main folder';
        elseif(!$this->checkFolderName()) $this->resultJson['error'] = 'You must enter only latin symbols';
        elseif(is_dir($this->fullPath)) $this->resultJson['error'] = 'Folder "'.$this->subFolder.'" already exist';
        elseif(!@mkdir($this->fullPath)) $this->resultJson['error'] = 'Error create sub-folder "'.$this->subFolder.'"';
        else{
            $this->resultJson['success'] = true;
            $this->resultJson['folder'] = $this->subFolder;
        }
        $this->showResult();
    }

    private function checkFolderName(){
        if(preg_match('/[А-Яа-я]+/', $this->subFolder)) return false;
        else return true;
    }

    private function deleteFolder($pathUpload, $useFileTable=false){
        $this->setFolders($pathUpload);
        if(!$this->subFolder) $this->resultJson['error'] = 'The folder isn\'t set';
        elseif(!is_dir($this->fullPath))$this->resultJson['error'] = 'The folder not exist';
        else{
            $objs = glob($this->fullPath."*");

            if($useFileTable){
                $this->fileTable->sqlSelect('id')->
                    sqlFilter('user_uid', $_COOKIE['lib_user_uid'])->
                    sqlFilterAnd()->
                    sqlFilter('path', $this->shortPath.$this->subFolder)->
                    exec();

                $idDelete = $this->fileTable->getField('id')->value[0];
                if($idDelete) $this->fileTable->sqlDelete($idDelete)->showSqlQuery();
            }

            foreach($objs as $obj){
                @unlink($obj);
            }
            if(!@rmdir($this->fullPath)) $this->resultJson['error'] = 'Error delete folder';
            else{
                $this->resultJson['success'] = true;
                $this->resultJson['folder'] = $this->subFolder;
            }
        }
        $this->showResult();
    }

    private function deleteFile($pathUpload, $silent = false){
        $this->setFolders($pathUpload);
        if(!$this->currentFile) $this->resultJson['error'] = 'The file is not set';
        elseif(!$this->checkExistFile($this->fullPath.$this->currentFile)) $this->resultJson['error'] = 'File not exist';
        elseif(!@unlink($this->fullPath.$this->currentFile)) $this->resultJson['error'] = 'Error delete file';
        else{
            if($this->params['id']) $this->fileTable->sqlDelete((int)$this->params['id'])->exec();
            $this->resultJson['success'] = true;
            $this->resultJson['file'] = $this->params['file_name'];
        }
        if(!$silent) $this->showResult();
    }

    private function setFolders($pathUpload){
        if($pathUpload == PATH_CLIPART ||
           $pathUpload == PATH_CLIPART_MULTIMEDIA ||
           $pathUpload == PATH_TEMPLATES.PATH_BASE_TEMPLATES ||
           $pathUpload == PATH_DROP_CAPS.PATH_BASE_DROP_CAPS
        ) $this->shortPath = $pathUpload;
        else $this->shortPath = $pathUpload.$_COOKIE['lib_user_uid'].'/';
        $this->rootDir = $_SERVER['DOCUMENT_ROOT'].$this->shortPath;
        $this->subFolder = $this->params['folder'] ? $this->params['folder'].'/' : '';
        $this->fullPath = $this->rootDir.$this->subFolder;
        $this->currentFile = $this->currentFile ? $this->currentFile : $this->params['file_name'];
    }

    private function checkUpload($name, array $types, $size){
        $this->file = $_FILES[$name];
        $this->file['name'] = addslashes($this->file['name']);
        if(!$this->file['name']){
            $this->resultJson['error'] = 'You didn\'t select a valid file';
            return false;
        }
        elseif(!in_array($this->file['type'], $types)){
            $this->resultJson['error'] = 'Not select any valid format of file or file too big';
            return false;
        }
        elseif($this->file['size'] > $size){
            $this->resultJson['error'] = 'The file is too big';
            return false;
        }
        else return true;
    }

    private function writeFileInBase($type){
        if($this->resultJson['success']){
            $this->fileTable->sqlSelect()->
                sqlFilter('user_uid', $_COOKIE['lib_user_uid'])->
                sqlFilterAnd()->
                sqlFilter('type', $type)->
                sqlFilterAnd()->
                sqlFilter('filename', $this->destinationName)->
                sqlFilterAnd()->
                sqlFilter('path', $this->shortPath.$this->subFolder)->
                exec();

            if(!$lastId = $this->fileTable->getField('id')->value[0]){
                $insertArray = array(
                    'user_uid' => $_COOKIE['lib_user_uid'],
                    'type' => $type,
                    'filename' => $this->destinationName,
                    'path' => $this->shortPath.$this->subFolder
                );
                if($this->params['data']) $insertArray['data'] = json_encode($this->params['data']);

                $this->fileTable->sqlInsert($insertArray)->exec();
                $lastId = $this->fileTable->getLastId();
            }
            $this->resultJson['id'] = $lastId;
        }
    }
}
?>