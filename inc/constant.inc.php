<?
define('PATH_STATIC',			    '/static/');
define('PATH_JS',   			    '/js/');
define('PATH_CSS',        		    '/css/');
define('PATH_IMG',				    '/images/');
define('PATH_BOOKS',                '/books/');

//uploads
define('PATH_UPLOAD',		        '/_upload/');
define('PATH_UPLOAD_IMAGES',        '/users_images/');
define('PATH_UPLOAD_MULTIMEDIA',    '/users_multimedia/');

//cliparts path
define('PATH_CLIPART',		        '/clipart/');
define('PATH_CLIPART_MULTIMEDIA',   '/clipart_multimedia/');
define('PATH_CLIPART_VIDEO',        SERVER_VIDEO.'clipart_video/');

//themes path
define('PATH_TEMPLATES',	        '/themes/');
define('PATH_BASE_TEMPLATES',	    'base_themes/');
define('PATH_USER_TEMPLATES',	    'users_themes/');

//drop_caps path
define('PATH_DROP_CAPS',	        '/drop_caps/');
define('PATH_BASE_DROP_CAPS',	    'base_drop_caps/');
define('PATH_USER_DROP_CAPS',	    'users_drop_caps/');

//admin password
define('ADMIN_PASS',                '21232f297a57a5a743894a0e4a801fc3');
?>