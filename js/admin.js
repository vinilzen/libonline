var BaseModel = Backbone.Model.extend({
    fog: function(){
        $('body').css({
            'postiton':'fixed',
            'overflow':'hidden'
        }).append(
            $('<div></div>').attr('id','fogLoad').append($('<div></div>').attr('class','preloaderRoll').css({
                'margin-left':document.width/2-24,
                'margin-top':document.height/2-24
            })
        ));
    }
});

var GetFiles = BaseModel.extend({
    initialize: function(){
        this.fog();
        this.get_files();
    },

    get_files: function(){
        var files_model = this;
        var get_host = this.get('external_host') ? this.get('external_host')+'upload/'+this.get('get_url')+'/' : '/upload/'+this.get('get_url')+'/';
        $.post(get_host,{folder:this.get('sub_folder'),user_uid:this.get('user_uid')}, function(files){
            var view = files_model.get('view');
            if(files.error){
                view.showAlert(files.error);
            }else{
                files_model.set('files',files);
                view.render(files_model);
            }
        });
    }
});

var GetTemplates = GetFiles.extend({
    get_files: function(){
        var files_model = this;
        var view = files_model.get('view');
        var files = new Array();

        if(!this.get('sub_folder')){
            $.post('/upload/getBaseThemeFiles/', function(folders){
                if(folders.error){
                    view.showAlert(folders.error);
                }else{
                    _.each(folders, function(name){
                        if(name.type == 'dir'){
                            files.push({
                                'name':name.name,
                                'type':'dir'
                            });
                        }
                    });
                }

                $.post('/templates/getBaseTemplates/', function(templates){
                    if(templates.error){
                        view.showAlert(templates.error);
                    }else{
                        _.each(templates, function(template){
                            files.push({
                                'name':template.theme_name,
                                'type':'template'
                            });
                        });

                        files_model.set('files',files);
                        view.render(files_model);
                    }
                });
            });
        }else{
            $.post('/upload/getBaseThemeFiles/', {folder:this.get('sub_folder')}, function(files){
                if(files.error){
                    view.showAlert(files.error);
                }else{
                    files_model.set('files',files);
                    view.render(files_model);
                }
            });
        }
    }
});

var BaseView = Backbone.View.extend({
    addFog: function(){
        $('body').scrollTop(0);
        $('body').css({
            'postiton':'fixed',
            'overflow':'hidden'
        }).append($('<div></div>').attr('id','fogLoad'));
    },

    removeFog: function(){
        $('#fogLoad').remove();
        $('body').css({
            'postiton':'',
            'overflow':'auto'
        })
    },

    addPage: function(count, id){
        var score_page = $('<div></div>').css({
            'width':'100%',
            'display':'none'
        }).attr({
                'id':id+'_score_page_'+count,
                'class':id+'_score_pages'
            });
        return score_page;
    },

    showPage: function(pos, id){
        $('.'+id+'_paginator').css('color','black');
        $('#'+id+'_pagenumber_'+pos).css('color','gray');

        $('.'+id+'_score_pages').hide();
        $('#'+id+'_score_page_'+pos).show();
    },

    showPageNumbers: function(pages, totalPages, id){
        var current_model = this;
        for(var i=1;i<=totalPages;i++){
            var page = $('<span></span>').css({
                'margin-right': '3px',
                'cursor':'pointer',
                'font-size':'12px'
            }).attr({
                    'class':id+'_paginator',
                    'id':id+'_pagenumber_'+i
                }).bind('click', {id:id,pos:i}, function(e){
                    current_model.showPage(e.data.pos, e.data.id);
                }).html(i);
            pages.append(page);
        }
    }
});

var ShowFiles = BaseView.extend({
    tagName: "table",
    className: "files",

    render: function(parent_model){
        var current_model = this;
        var files_cont = this.model.container.find('.admin_files');
        files_cont.html('');
        this.$el.html('');

        this.$el.css({
            'width':'100%',
            'margin-bottom': '20px'
        });

        if(parent_model.get('make_folder')){
            if(parent_model.get('sub_folder')){
                var openFolder = $('<div></div>').attr({
                    'class':'new_folder_icon',
                    'title':'Наверх'
                });
                var text_open_folder = '<b>../'+parent_model.get('sub_folder')+'</b>';
                openFolder.bind('dblclick', function(){
                    parent_model.set('sub_folder', '');
                    parent_model.get_files();
                });
            }else{
                openFolder = $('<div></div>').attr({
                    'class':'open_folder_icon',
                    'title':'Новая папка'
                }).css({
                        'cursor':'pointer'
                    });
                text_open_folder = $('<b>Создать новую папку</b>');
                text_open_folder.attr('class','newFolder');

                openFolder.click(function(){
                    current_model.makeFolder();
                });

                text_open_folder.click(function(){
                    current_model.makeFolder(parent_model);
                });
            }
        }

        this.tableFilesRow(false, openFolder, text_open_folder);
        var matchInvalidFiles = false;

        var input_files = typeof(parent_model.get('files')) == 'string' ? jQuery.parseJSON(parent_model.get('files')) : parent_model.get('files');
        _.each(input_files, function(file){
            var icon_delete = $('<div></div>').attr('class','icon-remove').css('cursor','pointer');
            icon_delete.bind('click', {name:file.name, type:file.type, id:file.id}, function(e){
                current_model.confirmDelete(parent_model, e.data.name, e.data.type, e.data.id);
            });

            if(file.type == 'dir'){
                var icon_file = $('<div></div>').attr({
                    'class':'folder_icon',
                    'title':'Содержимое папки'
                });
                icon_file.bind('dblclick', {folder:file.name}, function(e){
                    parent_model.set('sub_folder', e.data.folder);
                    parent_model.get_files();
                });
            }else{
                icon_file = current_model.iconFile(file, parent_model);
            }

            if(typeof(file.name) == 'string' && file.name.search('.mp4$') == -1 && parent_model.get('external_host') && file.type != 'dir'){
                var icon = $('<div></div>').attr('class','clock_icon');
                matchInvalidFiles = true;
            }else icon = icon_delete;
            current_model.tableFilesRow(file.id, icon_file, file.name, icon, parent_model);
        });

        if(matchInvalidFiles){
            setTimeout(function(){
                parent_model.get_files();
            },1000);
        }

        this.init_file_upload(parent_model, current_model.model.ids, current_model.model.max_loaded, current_model.model.formats);
        files_cont.append(this.$el);

        this.removeFog();
    },

    init_file_upload: function (parent_model, ids, max_loaded, formats){
        var current_model = this;
        $('#' + ids.dropzone).unbind();
        $('#' + ids.upload_alert + ' .close').unbind();

        $('#' + ids.dropzone).bind('click', function(){
            $('#' + ids.fileupload).click();
        });

        $('#' + ids.fileupload).fileupload({
            dataType: 'json',
            done: function (e, data) {
                if (data.result && data.result.error){
                    current_model.my_alert(data.result.error, ids);
                    return false;
                } else if (data.result) {
                    var errorMessages = '';
                    var messages = jQuery.parseJSON(data.jqXHR.responseText);
                    _.each(messages, function(message){
                        if(message.error) errorMessages += message.error + '<br><br>';
                    });

                    if(errorMessages) current_model.showAlert(errorMessages);
                    parent_model.get_files();
                }

                $('#' + ids.progress).fadeOut();
                $('#' + ids.dropzone).html('Перетащите файл сюда или выберите с диска');
            },
            progressall: function (e, data) {
                $('#' + ids.progress).show();
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#' + ids.progress +' .bar').css('width', progress + '%');
            },
            dropZone: $('#' + ids.dropzone),
            drop: function(e, data){
                if (data.files.length > max_loaded) {
                    current_model.my_alert('Вы можете добавлять файлы не больше '+max_loaded, ids);
                    return false;
                }

                $.each(data.files, function (index, file) {
                    if ( formats(file.name) ) {
                        $('#' + ids.dropzone).html(file.name);
                    } else {
                        current_model.my_alert('Недопустимый формат', ids);
                        return false;
                    }
                });
            },
            formData: {folder:parent_model.get('sub_folder'),id:parent_model.get('alt_id')},
            change: function(e, data){
                $.each(data.files, function (index, file) {
                    if ( formats(file.name) ) {
                        $('#' + ids.dropzone).html(file.name);
                    } else {
                        current_model.my_alert('Недопустимый формат', ids);
                        return false;
                    }
                });
            }
        }).error(function (jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'abort') {
                    current_model.my_alert('File Upload has been canceled', ids);
                }
            });

        if ($.browser.opera){
            $('#' + ids.fileupload).removeClass('hidden').attr('placeholder','Выберите файл на диске');
            $('#' + ids.dropzone).hide();
        }

        $('#' + ids.upload_alert).bind('click', function(){
            $('#' + ids.upload_alert).hide();
            $('#' + ids.upload_alert + ' h4, #' + ids.upload_alert + ' p').remove();
        });
    },

    my_alert: function(message, ids){
        $('#' + ids.upload_alert + ' h4, #upload_alert p').remove();
        var alert = '<h4>Warning!</h4><p>'+message+'</p>';
        $('#' + ids.upload_alert).append(alert).show();
    },

    tableFilesRow: function(id, icon_file, name, icon){
        this.$el.append($('<tr></tr>')
            .append($('<td></td>').css({'width':'50px','height':'48px','padding':'3px'}).append(icon_file))
            .append($('<td></td>').css('padding-left','10px').html(name))
            .append($('<td></td>').attr('class','controls').append(icon))
        );
    },

    confirmDelete: function(parent_model, name, type, id){
        var current_model = this;
        var but_cont = $('<div></div>').attr('id','subButtonsCont');
        var yes = $('<span></span>').attr('class','subButton').html('[Да]');
        var no = $('<span></span>').attr('class','subButton').html('[Нет]');

        no.click(function(){$('#blackShadow').remove();});
        yes.click(function(){
            if(type == 'dir'){
                var get_host = parent_model.get('external_host') ? parent_model.get('external_host')+'upload/'+parent_model.get('delete_folder_url')+'/':'/upload/'+parent_model.get('delete_folder_url')+'/';
                $.post(get_host, {
                    folder:name,
                    user_uid:parent_model.get('user_uid')
                }, function(response){
                    $('#blackShadow').remove();
                    if(response.error){
                        current_model.showAlert(response.error);
                    }else{
                        parent_model.set('sub_folder','');
                        parent_model.get_files();
                    }
                });
            }else{
                get_host = parent_model.get('external_host') ? parent_model.get('external_host')+'upload/'+parent_model.get('delete_file_url')+'/':'/upload/'+parent_model.get('delete_file_url')+'/';
                $.post(get_host  ,{
                    folder:parent_model.get('sub_folder'),
                    file_name:name,
                    user_uid:parent_model.get('user_uid'),
                    id:id
                }, function(response){
                    $('#blackShadow').remove();
                    if(response.error){
                        current_model.showAlert(response.error);
                    }else{
                        parent_model.get_files();
                    }
                });
            }
        });
        but_cont.append(yes).append(no);
        var type_show = type == 'dir' ? 'папку' : 'файл';
        var content = $('<div></div>').css({'width':'100%','padding-top':'25px'}).html('Вы действительно хотите удалить <br>'+type_show+' '+'"'+name+'"?').append(but_cont);
        current_model.showAlert(content);
    },

    makeFolder: function(parent_model){
        var current_model = this;
        var content = $('<div></div>');

        var descr = $('<div></div>').css({
            'width':'100%',
            'color':'gray',
            'font-style':'italic'
        }).html('Введите имя папки');
        var input = $('<input>').attr({
            'name':'folder',
            'id':'make_folder'
        }).css({
                'width':'90%',
                'margin-bottom':'10px'
            });

        var make = $('<span></span>').attr('class','subButton').html('[Создать]');
        var cancel = $('<span></span>').attr('class','subButton').html('[Отмена]');
        cancel.click(function(){$('#blackShadow').remove();});
        make.click(function(){
            $('#blackShadow').remove();
            var folderName = input.val();
            if(!folderName) current_model.showAlert('Вы не ввели название папки!');
            else if(folderName.match(/\s|[А-Яа-я]/)) current_model.showAlert('Недопустимые символы в названии!');
            else{
                var get_host = parent_model.get('external_host') ? parent_model.get('external_host')+'upload/'+parent_model.get('make_folder')+'/':'/upload/'+parent_model.get('make_folder')+'/';
                $.post(get_host, {
                    folder:folderName,
                    user_uid:parent_model.get('user_uid')
                }, function(response){
                    if(response.error){
                        current_model.showAlert(response.error);
                    }else{
                        parent_model.set('sub_folder',folderName);
                        parent_model.get_files();
                    }
                });
            }
        });

        content.append(descr).append(input).append(make).append(cancel);
        current_model.showAlert(content);
    },

    showAlert: function(content){
        var frame_bg = $('<div></div>').attr('id','blackShadow');
        var frame_out = $('<div></div>').attr('id','messageFrame');
        var frame_inner = $('<div></div>').attr('id','innerMessageFrame');
        frame_inner.html(content);
        var closeBtn = $('<div></div>').attr('id','closeButton').html('[X]').css('margin-top','0px');

        closeBtn.click(function(){frame_bg.remove();});
        $('body').scrollTo('0px', 500);
        $('body').prepend(frame_bg.append(frame_out.append(closeBtn).append(frame_inner)));
    },

    showImage: function(img){
        this.addFog();
        var current_model = this;
        var big_img = $(img).clone();
        var big_img_width = document.width* 0.4;

        big_img.css('width',big_img_width);

        big_img.bind('click',function(){
            current_model.removeFog();
            $('#image_frame').remove();
        });

        $('body').append($('<div></div>').css({
            'display':'table',
            'border':'10px solid white',
            'box-shadow':'0 0 10px rgba(0,0,0,0.8)',
            'position':'absolute',
            'z-index':999,
            'left':(document.width/2-big_img_width/2)-5,
            'top':'40px'
        }).attr('id','image_frame').append(big_img));
    }
});

var ShowImageClipart = ShowFiles.extend({
    iconFile: function(file, parent_model){
        var current_model = this;
        var icon_file = $('<img>').attr({
            'class':'media-object',
            'src':file.full_path + file.name
        }).css({
                'cursor':'pointer',
                'border':'1px solid #56695e'
        }).bind('click',function(){
            current_model.showImage(this);
        });
        return icon_file;
    }
});

var ShowVideoClipart = ShowFiles.extend({
    iconFile: function(file, parent_model){
        var current_model = this;
        var icon_file = $('<img>').attr({
            'class':'media-object',
            'src':'/img/video_icon.png'
        });

        if(file.name_img){
            icon_file.bind('click',function(){
                current_model.showImage(this);
            }).css({
                'cursor':'pointer',
                'border':'1px solid #56695e'
            }).attr('src',parent_model.get('external_host') + file.full_path_img + file.name_img);
        }

        return icon_file;
    },

    tableFilesRow: function(id, icon_file, name, icon, parent_model){
        var current_model = this;
        var cell_name = $('<td></td>').css('padding-left','10px').html(name);
        var row_obj = $('<tr></tr>')
            .append($('<td></td>').css({'width':'50px','height':'48px','padding':'3px'}).append(icon_file))
            .append(cell_name)
            .append($('<td></td>').attr('class','controls').append(icon));

        if(icon_file[0].className == 'media-object'){
            cell_name.attr({
                'class':'row_video_files'
            }).bind('click',{id:id},function(e){
                    $('.row_video_files').removeAttr('selected');
                    $(this).attr('selected',1).css('background-color','#cecee5');

                    var container_upload_image = $('#upload_clipart_image');
                    if(container_upload_image.attr('class') == 'hide') container_upload_image.show('fast');

                    parent_model.set('alt_id', e.data.id);
                    current_model.init_file_upload(parent_model, current_model.model.idsImg, current_model.model.max_loaded, current_model.model.formats_img);

                }).css({
                    'cursor':'pointer'
                }).bind('mouseover',function(){
                    var current_obj = $(this);
                    $('.row_video_files').each(function(){
                        if(!$(this).attr('selected')) $(this).css('background-color','');
                    });

                    if(!current_obj.attr('selected')) current_obj.css('background-color','#e5e5f1');
                });
        }
        this.$el.append(row_obj);
    }
});

var ShowMultimediaClipart = ShowFiles.extend({
    iconFile: function(file, parent_model){
        var icon_file = $('<img>').attr({
            'class':'media-object',
            'src':'/img/multimedia_icon.png'
        });
        return icon_file;
    }
});

var ShowTemplate = ShowFiles.extend({
    render: function(parent_model){
        var current_model = this;
        var files_cont = this.model.container.find('.admin_files');
        files_cont.html('');
        this.$el.html('');

        this.$el.css({
            'width':'100%',
            'margin-bottom': '20px'
        });

        if(parent_model.get('sub_folder')){
            $('#jumbotron_template').hide('fast');
            $('#jumbotron_template_image').show('fast');

            var openFolder = $('<div></div>').attr({
                'class':'open_folder_icon',
                'title':'Наверх'
            });
            var text_open_folder = '<b>../'+parent_model.get('sub_folder')+'</b>';
            openFolder.bind('dblclick', function(){
                parent_model.set('sub_folder', '');
                parent_model.get_files();
            });
        }else{
            $('#jumbotron_template').show('fast');
            $('#jumbotron_template_image').hide('fast');

            openFolder = $('<div></div>').attr({
                'class':'new_folder_icon',
                'title':'Новая папка'
            }).css({
                    'cursor':'pointer'
                });
            text_open_folder = $('<div>Создать новую папку</div>');
            text_open_folder.attr('class','newFolder');

            openFolder.click(function(){
                current_model.makeFolder();
            });

            text_open_folder.click(function(){
                current_model.makeFolder(parent_model);
            });
        }

        current_model.tableFilesRow(false, openFolder, text_open_folder);

        _.each(parent_model.get('files'), function(file){
            var icon_delete = $('<div></div>').attr('class','icon-remove').css('cursor','pointer');
            icon_delete.bind('click', {name:file.name, type:file.type}, function(e){
                current_model.confirmDelete(parent_model, e.data.name, e.data.type);
            });

            if(file.type == 'dir'){
                var icon_file = $('<div></div>').attr({
                    'class':'icon-folder',
                    'title':'Содержимое папки'
                });

                icon_file.bind('dblclick', {folder:file.name}, function(e){
                    parent_model.set('sub_folder', e.data.folder);
                    parent_model.get_files();
                });
            }else{
                if(parent_model.get('sub_folder')){
                    var icon_file = $('<img>').attr({
                        'class':'media-object',
                        'src':file.full_path + file.name
                    });
                }else{
                    var icon_file = $('<img>').attr({
                        'class':'media-object',
                        'src':'/img/template_icon.png'
                    });
                }
            }
            current_model.tableFilesRow(file.id, icon_file, file.name, icon_delete);
        });

        this.init_file_upload(parent_model, current_model.model.ids_template, current_model.model.max_loaded_xml, current_model.model.formats_xml);
        this.init_file_upload(parent_model, current_model.model.ids_template_image, current_model.model.max_loaded_img, current_model.model.formats_img);
        files_cont.append(this.$el);
        this.removeFog();
    },

    confirmDelete: function(parent_model, name, type){
        var current_model = this;
        var but_cont = $('<div></div>').attr('id','subButtonsCont');
        var yes = $('<span></span>').attr('class','subButton').html('[Да]');
        var no = $('<span></span>').attr('class','subButton').html('[Нет]');

        no.click(function(){$('#blackShadow').remove();});
        yes.click(function(){
            if(type == 'dir'){
                $.post('/upload/deleteThemeFolder/',{folder:name}, function(response){
                    $('#blackShadow').remove();
                    if(response.error){
                        current_model.showAlert(response.error);
                    }else{
                        parent_model.set('sub_folder','');
                        parent_model.get_files();
                    }
                });
            }else if(type == 'template'){
                $.post('/upload/deleteTemplate/',{name:name}, function(response){
                    $('#blackShadow').remove();
                    if(response.error){
                        current_model.showAlert(response.error);
                    }else{
                        parent_model.get_files();
                    }
                });
            }else{
                $.post('/upload/deleteBaseImageFile/',{
                    folder:parent_model.get('sub_folder'),
                    file_name:name
                }, function(response){
                    $('#blackShadow').remove();
                    if(response.error){
                        current_model.showAlert(response.error);
                    }else{
                        parent_model.get_files();
                    }
                });
            }
        });
        but_cont.append(yes).append(no);
        if(type == 'dir') var type_show = 'папку';
        else if(type == 'template') var type_show = 'шаблон';
        else var type_show = 'файл';

        var content = $('<div></div>').css({'width':'100%','padding-top':'25px'}).html('Вы действительно хотите удалить <br>'+type_show+' '+'"'+name+'"?').append(but_cont);
        current_model.showAlert(content);
    }
});