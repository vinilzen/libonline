/**
 *  Отображение редактора книги, кнопки выбора книги на стартовой станице
 */
var Book = Backbone.Model.extend({
  defaults: {
    name: "Book Name",
    author: "Book Author",
    widgets: [],
    content: ""
  },
  initialize: function(){
    this.search = new Search();
    this.parsed_elements = new Backbone.Collection;
    this.elements = new Elements();
    this.html_elements = new HtmlElements();
    book_view = new BookView({model:this});

    /*this.on('fetch_templates',function(){
      this.set_curent_style();
    }, this);*/

    /* Смена книги */
    this.on('change:selected',function(a,selected,c){
      if (selected){
        this.showTemplates();
      } else {
        this.hideTemplates();
      }
    }, this);

    /* Изменения массива элементов в книге */
    this.on('change:elements', function(model, response){
      if (response){
        book.elements.reset(response.elements);
        book.parseContent();
      } else {
          alert('Ошибка получения контента книги');
      }
    }, this);

    this.on('change:rerender_after', this.resetPrintedItems, this);

    /*this.on('change:curent_style', function(book_model, style, changes){

      selected_theme = this.get('curent_style');

      if (typeof(selected_theme)!='undefined'){
        this.view.style=selected_theme;

        if (typeof(style.dropcap)!='undefined'){
          this.view.getDropcapConfig();
        }
      } else {
        console.log('fatal error');
        alert('fatal error');
      }
    }, this);*/
  },

  /**
   * Сбросим все напичатанные элементы после указанной страницы
   * @return void
   */
  resetPrintedItems:function(){
    var page = this.get('rerender_after');
    this.html_elements.each(function(m){
      if (m.has('container') && m.get('container').model.id >= page && m.get('printed')!=0){
        // если это первый параграф на странице с которой начинаем 
        // перестраивать, и у него уже первая часть напечатана
        if (
          m.get('container').model.id == page && 
          m.get('container').model.get('inside_collection').at(0).id == m.id && 
          m.get('cut')>0) 
        {
          m.set({'printed':2}, {silent:true});
        } else {
          m.set({'printed':0, 'cut':0}, {silent:true});
        }
      }
    });
  },
  
  set_curent_style:function(){
    this.selected_theme = book.get('selected_theme');

    if (typeof(this.selected_theme) != 'undefined'){
      var copy_attributes = {};
      this.set({'selected_theme':this.selected_theme},{silent:true});

      _.each(this.selected_theme.attributes, function(value,name){

        //copy only style
        if (_.find(available_elements_type.models, function(model){
          return name == model.get('themeContainer');
        })){
          switch(name){
            case 'title_style':
              if (typeof(value.h1)!='undefined'){
                _.each(value, function(style, lvl){
                  copy_attributes['title_'+lvl] = _.clone(style);
                });
              } else {
                 copy_attributes[name] = _.clone(value);
              }
              break;
            case 'illustration':
            case 'video':
            case 'audio':
              copy_attributes[name] = [];
              _.each(value,function(v){
                v_attr = _.clone(v);
                if (typeof(v.placeholders)!='undefined' && _.size(v.placeholders) > 0){
                  v_attr['placeholders'] = new Placeholders(_.toArray(v.placeholders));
                } else {
                  if (typeof(copy_attributes[name]['placeholders'])!='undefined'){
                    delete v_attr['placeholders'];
                  }
                }
                copy_attributes[name].push(v_attr);
              });
              break;
            case 'inset':
            case 'latex':
              copy_attributes[name] = _.clone(value);
              if (typeof(value.placeholders)!='undefined' && _.size(value.placeholders) > 0){
                copy_attributes[name]['placeholders'] = new Placeholders(_.toArray(value.placeholders));
              } else {
                if (typeof(copy_attributes[name]['placeholders'])!='undefined'){
                  delete copy_attributes[name]['placeholders'];
                }
              }
              break
            default:
              if (value){
                copy_attributes[name] = _.clone(value);
                if (typeof(value.placeholders)!='undefined' && _.size(value.placeholders) > 0){
                  copy_attributes[name]['placeholders'] = new Placeholders(_.toArray(value.placeholders));
                } else {
                  if (typeof(copy_attributes[name]['placeholders'])!='undefined'){
                    delete copy_attributes[name]['placeholders'];
                  }
                }
              } // else { console.log(name) }
          }
        } else {// OR colon_title colon_number
          var available_other_option = ['colon_number', 'colon_title', 'dropcap', 'divscript', 'scripts', 'scripts_data','special_page'];
          if (available_other_option.indexOf(name)>-1){

            if (value && value!={}){
              copy_attributes[name] = _.clone(value);

              if (typeof(value.placeholders)!='undefined' && _.size(value.placeholders) > 0){
                copy_attributes[name]['placeholders'] = new Placeholders(_.toArray(value.placeholders));
              } else {
                if (typeof(copy_attributes[name]['placeholders'])!='undefined'){
                  delete copy_attributes[name]['placeholders'];
                }
              }

            } else {
              delete copy_attributes[name]
            }

          }
        }
      });

      /* Мержим изменения если они есть */
      if (0 && typeof(this.get('changes')) != 'undefined'){
        if (typeof(this.get('changes').get(this.selected_theme.id)) != 'undefined'){

          this.selected_theme_changes = this.get('changes').get(this.selected_theme.id);

          _.each(this.selected_theme_changes.get('changed_elements').models, function(model){
            type = _.find(available_elements_type.models, function(m){ return m.get('name') == model.get('type'); });

            if (typeof(type)!='undefined'){
              if (typeof(copy_attributes[type.get('themeContainer')])!='undefined'){
                _.extend( copy_attributes[type.get('themeContainer')], model.get('style') );
              } else {
                console.log('такого свойства нет', type.get('themeContainer'), copy_attributes);
                copy_attributes[type.get('themeContainer')] = model.get('style');
              }
            } else {
              console.log('Неизвестный тип')
            }
          });
        }
      }

      this.set('curent_style', new Backbone.Model( copy_attributes ));

      if (this.get('curent_style').has('dropcap')) {
        if (_.size(this.get('curent_style').get('dropcap'))) {
          this.view.getDropcapConfig();
        }
      }

    } else console.log('need selected theme');
  },
  /*
   *  Получаем текущую выбранную тему
   *  и создаем или добавляем новые изменения в параметр книги book.attributes.changes
   *  и сразу рендерим книгу с новыми параметрами
   */
  save_changes_local: function(change, type, style){

    if (!this.has('changes')){// если не было изменения создадим коллекцию с одним элементом

      var change = new Change({
        id : this.get('selected_theme').id,
        changed_elements : new Backbone.Collection([new ChangedElementStyle({
          type:type,
          style:_.clone(style),  //change:_.clone(change)
        })]),
        themeName : this.get('selected_theme').get('name')
      });

      this.set('changes', new Changes([change]));

    } else {// если было, проверим для какой темы

      if (typeof(this.get('changes').get(this.get('selected_theme').id)) == 'undefined'){ // если новая тема то добавим в коллекцию
        var change = new Change({
          id : this.get('selected_theme').id,
          changed_elements : new Backbone.Collection([new ChangedElementStyle({
            type:type,
            style:_.clone(style), //change:_.clone(change)
          })]),
          themeName : this.get('selected_theme').get('name')
        });

        this.get('changes').add(change);

      } else {// если изменения в этой теме уже были то обновим модель

        this.changed_theme = this.get('changes').get(this.get('selected_theme').id);

        if (typeof(this.changed_theme) != 'undefined') {

          this.changed_elements = this.changed_theme.get('changed_elements');
          this.changed_element = _.find(this.changed_elements.models, function(model){
            return model.get('type') == type;
          });

          if (typeof(this.changed_element) != 'undefined'){
            var old_change = this.changed_element.get('change');
            this.changed_element.set({
              style:_.clone(_.extend(style,change)),// change: _.extend(old_change, change)
            });
          } else {
            this.changed_elements.add(new ChangedElementStyle({
              type:type,
              style:_.clone(style), //change:_.clone(change)
            }));

          }
        } else {
          console.log('error',this.get('changes'));
        }
      }
    }
    this.view.render();
  },
  save_attr_local:function(model, value, html_element, attr){
    console.log(model, value, html_element, attr);
  },
  showTemplates:function(){
    if (typeof(this.get('user_templates')) == 'undefined' && typeof(this.get('base_templates')) == 'undefined'){
      this.btn_view.showPreloader();
      this.set('user_templates',new UserTemplates([],this)); // collection  this = current BOOK
      this.set('base_templates',new BaseTemplates([],this)); // collection
    } else {
      if (typeof(this.get('base_templates')) != 'undefined'){
        this.get('base_templates').view.$el.fadeIn(1000);
        this.get('base_templates').view_editor.$el.fadeIn(1000);
      }

      if (typeof(this.get('user_templates')) != 'undefined'){
        this.get('user_templates').view.$el.fadeIn(1000);
        this.get('user_templates').view_editor.$el.fadeIn(1000);
      }
    }

    $('#templates .preloader').remove();
    $('#goto_edit').show();
    this.search.form_view.$el.show();
  },
  hideTemplates:function(){
    if (typeof(this.get('base_templates')) != 'undefined'){
      this.get('base_templates').view.$el.hide();
      this.get('base_templates').view_editor.$el.hide();
    }

    if (typeof(this.get('user_templates')) != 'undefined'){
      this.get('user_templates').view.$el.hide();
      this.get('user_templates').view_editor.$el.hide();
    }
    this.search.form_view.$el.hide();
  },
  url: function() {
    return '/getbooks/getContent/?book_uid=' + this.id;
  },
  save_placeholders_local:function(html_element){
    var type = html_element.get('type'),
        type_theme = book.get('curent_style').get(type);
  },
  /*
  update_widgets:function(){

      var new_widget = [];

      _.each(this.widget_collection.models, function(widget){
        var a = {
          id: widget.id,
          pos: widget.get('pos'),
          src: widget.get('src'),
          type: widget.get('type'),
          style: widget.get('style'),
          'type-pos': widget.get('type-pos')
        };

        var widget_placeholders = [];
        _.each(widget.placeholders.models, function(placeholder){
          widget_placeholders.push(placeholder.attributes);
        });
        a['placeholders'] = widget_placeholders;
        new_widget.push(a);
      });

      var curent_style = this.get('curent_style');
      curent_style.illustration = new_widget;

      this.set('curent_style', curent_style);

      $('.popover').remove();
      media_lib_view.close();
      save_changes_local('illustration');
  },*/
  // неактуально video -> attributes.video
  /*
  update_video:function(){
    var page = book.edited_widget.get('pos');

    var curent_style = this.get('curent_style');
    var video_collection = new Backbone.Collection(curent_style.video);
    var video = _.find(video_collection.models, function(video){
      return (video.get('type-pos') == 'page' && video.get('pos') == page);
    });

    video.set('src', book.edited_widget.get('src'));

    var array = [];
    _.each(video_collection.models, function(a){
      array.push(a.toJSON());
    });

    curent_style.video = array;

    this.set('curent_style', curent_style);

    $('.popover').remove();
    media_lib_view.close();
    save_changes_local('video');
  },*/
  save_user_theme: function(){

    user_theme = _.find(user_themes.models, function(model){
      return this.get("theme_id") == model.id;
    }, this);

    if ( !user_theme ){ // Если еще не сохронялись или если переключили тему
      user_theme = new UserTheme({  theme_id: this.get("theme_id"),
                                    book_id: this.id
                                });

      user_themes.add(user_theme);
    } else {
      if (!user_theme.view) {
        user_theme.view = new UserThemeView({model:user_theme});
      }
    }

    user_theme.view.$el.removeClass('btn-success').addClass('btn-warning').html("Сохранить").fadeIn();

    /* TO DO переделать ???*/
    var style = get_style(this);
    user_theme.set(style,{silent: true});
  },
  /*
   * Заполняем коллекцию структурных элементов
   * (название книги, автор, параграфы, заголовки и ...)
   */
	parseContent:function() {
		var content = this.get('content');

		if (this.elements.length == 0){
			this.elements.reset(this.get('elements'));
		}
    this.footnotes = new Backbone.Collection();
		this.html_elements.reset();

		if (this.elements.length > 0){

			_.each(this.elements.models, function(e){

				if (!e.has('inline')){

					if (e.get('delete')) {
						book.elements.remove(e);
					} else {
						var text = test_pos(e.get('pos_start'), e.get('pos_end'));
						if (e.get('type') == 'paragraph'){
							var cut = _.find(book.elements.models, function(el){
							return  e.get('pos_start') <= el.get('pos_start') &&
                      e.get('pos_end') >= el.get('pos_end') &&
                      el.get('type') != 'paragraph' &&
                      !el.get('delete');
							});

							// перенос на новую страницу
							if (cut) {
								text = text.substr(0, cut.get('pos_start')-e.get('pos_start'))+text.substr(cut.get('pos_end')-e.get('pos_start')+1);
							}
						}

						var element_type = _.find(available_elements_type.models, function(model){
							return model.get('name') == e.get('type');
						});

						if (typeof(element_type)!='undefined'){
							var html_element = new HtmlElement({
								content: text,
								pos_start:e.get('pos_start'),
								pos_end:e.get('pos_end'),
								type: e.get('type'),
								typeLvl: e.get('typeLvl'),
								id: e.id,
								element_type: element_type
							});

              if (e.get('type') == 'inset') console.log(element_type);
							if (e.get('type') == 'table') html_element.set('table',e.get('content'));
							if (e.get('type') == 'epigraph'){
								epigraph_text = text.split('<br />').join('<br>').split('<br/>').join('<br>');
								html_element.set('content',epigraph_text);
							}

							book.html_elements.add(html_element);
						} else {
              if (e.has('footnotes_inline') || e.has('footnotes_style')){

                var element_type = _.find(available_elements_type.models, function(model){
                  return model.get('name') == 'footnote';
                });

                var footnote_type = e.has('footnotes_inline')?'footnotes_inline':'footnotes_style';

                _.each(e.get(footnote_type), function(model){

                  var footnote = new Backbone.Model({
                    id:model.id,
                    content: test_pos(model['pos_start'], model['pos_end']),
                    text: test_pos(model['pos_start'], model['pos_end']),
                    pos_start:model['pos_start'],
                    pos_end:model['pos_end'],
                    pos_in_text:model['pos_in_text'],
                    symbol_position:model['pos_in_text'],
                    type: 'footnote',
                    element_type: element_type
                  });

                  book.footnotes.add(footnote);
                });
              } else if (e.has('inset')){
                var element_type = _.find(available_elements_type.models, function(model){
                  return model.get('name') == 'inset';
                });

                _.each(e.get('inset'), function(model){
                  var html_element = new CoverPicModel({
                    content: test_pos(model['pos_start'], model['pos_end']),
                    pos_start:model['pos_start'],
                    pos_end:model['pos_end'],
                    type: 'inset',
                    element_type: element_type
                  });

                  book.html_elements.add(html_element);
                });
              } else if (e.has('tables')){
                var element_type = _.find(available_elements_type.models, function(model){
                  return model.get('name') == 'table';
                });

                _.each(e.get('tables'), function(model){
                  var html_element = new HtmlElement({
                    type: 'table',
                    row:model['rows'],
                    element_type: element_type
                  });

                  book.html_elements.add(html_element);
                });
              } else {
                console.log('не найден тип элемента',e.attributes);
              }
						}
					}
				}
			});
		}

		return this.html_elements;
  }
});

/**
 *  Кнопки выбора книги на стартовой станице
 */
var BookBtnView = Backbone.View.extend({
  className:'btn',
  events:{
    "click": "select_book"
  },
  tagName:'button',
  initialize:function(){
    this.model.btn_view = this;
    this.render();
  },
  render: function(){
    this.$el.attr('data-book-uid',this.model.get('book_uid'));

    if (this.model.get('name') || this.model.get('author') ) {
      // после парса доступны имя автора и название
      this.$el.html(this.model.get('name')+' ('+this.model.get('author')+')');

    } else {
      // если загрузил и не распарсил, имя файла уже не доступно
      this.$el.html(this.model.get('book_uid'));
    }
  },
  select_book: function(){
    book = this.model;
    book.set('selected',true);

    $('.saver div').hide();

    if (typeof(book.get('changes')) != 'undefined'){
      _.map(book.get('changes').models,function(model){
        if (model.id!=selected_theme.id) {
          model.view.$el.fadeOut();
        } else {
          model.view.$el.fadeIn();
        }
      });
    }

    this.model.fetch({
      success: function(model, response){
        //book.parseContent();
      }, error:function(a,b,c){
        console.log('error fetch book', a,b,c)
      }
    });

    var book_id = this.model.id;
    _.map(library.models, function(model){
      if (model.id != book_id){
        model.set('selected',false);
      }
    });
  },
  getTemplates:function(){},
  showPreloader:function(){
    $('#templates').prepend('<img class="preloader" src="/img/preloader.gif" />').css({'text-align': 'center', 'border':'none'});
  },
  hidePreloader:function(){
    $('#templates .preloader').remove();
  }
});

var BookView = Backbone.View.extend({
  cover: {},
  el: '.page_container',
  initialize: function(){
    this.model.view = this;
    this.model.pages = new Pages();
    this.model.size_table = new SymbolCollection(); // for multiline, save width letter for current font-name and font-size
  },
  // Все ранее напечатаное стерли (при переключении тем/книг)
  reset_printed_items:function(){
      this.model.html_elements.each(function(model){ // this.model = book
        model.set({"printed":0});
      });
  },
  render:function(widget_collection){
    progressBar.show();
    var THIS = this;
    $.post('/savedata/getHtml/?book_uid=1385381143&theme_uid=1')
      .done(function(data) {
        if (data.length > 0 && data != '' && 0) {
          $('.page_container').html(data);
          $('.container-narrow').slideUp();
          $('.container-editor').slideDown();
          progressBar.hide();
        } else {
          THIS.render_book(widget_collection);
        }
      });

    return this;
  },
  render_book:function(widget_collection){
    this.filter_def = new FilterDef(this.model);
    this.model.pages.reset();
    this.model.size_table.reset();
    this.style = this.model.get('curent_style');
    this.markedDropcapParagraphs();

    if (this.style.has('divscript')) this.renderScripts();

    // var scale = 130/parseInt(style["page_style"]["width"]);  //!!! 130  custom width for preview
    this.model.set('prev_scale', 0.2);
    $('.page_previews').html('');
    this.$el.html('');
    this.page = 1;

    $('.container-editor').slideDown();

    if (this.model.html_elements.length > 0) {
      // соберем виджеты иллюстраций/видео в одну кучу book.widget_collection
      this.model.widget_collection = widget_collection || new WidgetCollection(null,this);
      this.reset_printed_items();
      this.model.page_template_collection = new TemplateCollection(null,this); // this.model = book
      this.renderPages();
    }
  },
  save_html_result:function(){
    var html = $('.page_container').html();
    $.post(
      '/savedata/updateData/?book_uid='+this.model.id+'&theme_uid='+this.model.selected_theme.id,
      {html: html}
    );
  },

  renderPages:function(){

    $('.container-narrow').slideUp();

    if (this.isSpecialPage()) {
      this.page_style = this.getPageStyle();
      this.createPage({special_page:true});
      this.fillSpecialPage('first');
      this.page_model.set({printed:1});
      this.page++;
    } else {
      this.createCover();
    }

    var makePage = $.proxy(this.makePage, this),
        doneCallback = $.proxy(function(){
          
          this.page_model.set({printed:1});
          this.page++;

          if (this.haveSomethingToPrint() //&& this.page < 4
          ) {
            makePage().done(doneCallback);
          } else {
            this.addLastSpecPage();
            progressBar.hide();
            this.save_html_result();
          }
        }, this);

    makePage().done(doneCallback);
  },

  makePage:function(){
    var deferred = $.Deferred();
    setTimeout($.proxy(function () {

      this.page_style = this.getPageStyle();

      this.createPage();
        
      if (this.isSpecialPage()) {
        this.fillSpecialPage('first'); // перед основными страницами
      } else {
        this.full_page_features = this.addFeatures(this.page_model.view);
        if (!this.full_page_features){// например картинка на всю страницу

          if(!this.page_model.has('restored') || this.page_model.get('restored') == 0){ // not printed form cache(localStorage)
            
            if ( this.page_style.column > 1) {// если многоколоночная верстка
              this.page_model.column_contents = [];
              for (var j=1; j<=this.page_style.column; j++) {
                this.page_model.view.$el.page('out', '.column'+j, this.page_model.view, j);
                // for preview
                var pm = this.page_model.column_contents[j] = new Backbone.Collection;
                $.each($('.column'+j+' p, .column'+j+' h1, .column'+j+' h2, .column'+j+' h3', this.page_model.view.$el), function(a,b){
                  pm.add({
                    tag:b.tagName,
                    height:$(b).height(),
                    width:$(b).width(),
                    text:$(b).text(),
                    color:$(b).css('color'),
                    'font-size':$(b).css('font-size')
                  });
                });
              }
            } else {// если одноколоночная верстка
              this.page_model.view.$el.page('out', '.content', this.page_model.view, 0);
            }
          } else book.widget_collection.getFlowWidgets(this.page_model.view);
        }
      }

      deferred.resolve();
    }, this), 50);

    return deferred.promise();
  },

  /**
   * Удаление из хранилища страниц для выбранной темы
   * @param  {Number} theme_id Id темы
   * @return {[type]}          [description]
   */
  removeSavedPage:function(theme_id) {
    theme_id = theme_id || book.selected_theme.id;
    for (i=0;i<100;i++){
      localStorage.removeItem(book.id+'_'+theme_id+'_page_'+i);
    }
  },

  addLastSpecPage:function(){

    if (  book.get('curent_style').has('special_page') &&
          book.get('curent_style').get('special_page') &&
          book.get('curent_style').get('special_page').last_page &&
          _.size(book.get('curent_style').get('special_page').last_page)>0
    ){
      _.each(book.get('curent_style').get('special_page').last_page, function(p,i){
        this.page_style = this.getLastPageStyle(i);

        this.createPage({special_page:true});
        this.fillSpecialPage(i);
        this.page++;
        this.page_model.set({printed:1});
      }, this)
    }
  },

  /**
   * Создание вьюхи страницы 
   * @param  {[type]} attr [description]
   * @return page_model
   */
  createPage:function(attr){
    var attr = attr || {};
    this.page_model = new OnePageContentModel({
      page:this.page,
      column:parseInt(this.page_style.column),
      special_page: attr.special_page || false
    });

    this.model.pages.add(this.page_model);
    this.page_model.view.$el.css(this.page_style).appendTo(this.$el);
    this.page_model.set('style', this.page_style);
    return this.page_model;
  },

  /**
   * Заполнение страницы специальными элементами и виджитами
   * @param  {String} whence 'first' || 'last' специальные 
   *                         страницы могуть быть в начале книги и в конце
   *                         
   * @return {[type]} [description]
   */
  fillSpecialPage:function(whence){

    if (whence == 'first'){
      this.page_model.features = book.get('curent_style').get('special_page').first_page['page'+this.page];
    } else {
      this.page_model.features = book.get('curent_style').get('special_page').last_page[whence];
    }

    if (this.page_model.features){
      _.each(this.page_model.features, function(f,k){
        var element = _.find(available_elements_type.models, function(element_type){
          return element_type.get('themeContainer') == k;
        })

        if (element && k != 'page_style'){
          switch(k){
            case 'contents':
              var contents = new Contents();
              this.page_model.view.content.html(contents.view.el);
              contents.findElements();
              break;

            case 'cover_author_style':
              var book_author_models = _.filter(book.html_elements.models, function(m){ return m.get('type') == 'author'; });
              if (book_author_models){
                var old_style = this.style.get('cover_author_style');
                if(old_style)
                  this.style.set('cover_author_style', _.extend(old_style, f));
                this.addBookAuthor(book_author_models);
              }
              break;

            case 'cover_name_style':
              var book_name_model = _.find(book.html_elements.models, function(m){ return m.get('type') == 'name'; });
              if (book_name_model){
                var old_style = this.style.get('cover_name_style');
                if (old_style)
                  this.style.set('cover_name_style', _.extend(old_style, f));
                this.addBookName(book_name_model);
              }
              break;

            case 'audio':
            case 'illustration':
            case 'video':
              var media_collection = new Backbone.Collection();

              _.each(f, function(attr){                
                var new_attr = _.clone(attr);
                new_attr['placeholders'] = new Placeholders(_.toArray(attr.placeholders));
                var model = new CoverPicModel(new_attr);
                media_collection.add(model);
              })
              this.pasteMediaContent(media_collection.models);
              break;

            case 'cover_pic':
              var media_collection = new Backbone.Collection(),
                  new_attr = _.clone(f);
              new_attr['placeholders'] = new Placeholders(_.toArray(f.placeholders));
              new_attr['type'] = 'cover_pic';

              var model = new CoverPicModel(new_attr);
              media_collection.add(model);
              this.pasteMediaContent(media_collection.models);
              break;

            default:
              if (element.get('position')=='static'){
                var model = _.find(book.html_elements.models, function(m){
                  return m.get('type')==k;
                });

                if (model){
                  model.createView();
                  if (f.style){
                    var old_style = model.get('style');
                    if (old_style)
                      model.set('style', _.extend(old_style, f.style));
                    else
                      model.set('style', f.style);
                  }
                  model.view.$el.css(model.get('style'));
                  this.page_model.view.content.append(model.view.el);
                  model.set('printed',1)
                }

              } else console.log(k, f, element);
          }
        }

      },this);
    }
  },

  /**
   * Печатаем страницы пока есть не напечатанные элементы
   * @return {Boolean} есть что-либо для печати на новой странице?
   */
  haveSomethingToPrint:function(){
    return _.filter(this.model.html_elements.models, function(model){
      return model.get("printed")!=1 && typeof(model.id)!='undefined' && typeof(this.model.html_elements.get(model.id))!='undefined';
    }, this).length>0 || this.isSpecialPage();
  },

  isSpecialPage:function(){
    if (  book.get('curent_style').has('special_page') && 
          book.get('curent_style').get('special_page').first_page && 
          _.size(book.get('curent_style').get('special_page').first_page)>0 &&
          typeof(book.get('curent_style').get('special_page').first_page['page'+this.page])!='undefined' &&
          this.page == book.get('curent_style').get('special_page').first_page['page'+this.page].page
    )
      return true;
    else
      return false;
  },
  
  getLastPageStyle:function(i){
    var global_page_style = _.clone(this.style.get('page_style'));
    if(!'column' in global_page_style){
      global_page_style.column = 1;
    }

    if (book.get('curent_style').has('special_page')){
      var special_page = book.get('curent_style').get('special_page');
      if (special_page && special_page.last_page && _.size(special_page.last_page)>0){
        if (typeof(special_page.last_page[i])!='undefined'){
          var special_page_param = special_page.last_page[i];
          if (typeof(special_page_param.page_style)!='undefined'){
            _.extend(global_page_style, special_page_param.page_style);
          }
        }
      }
    }
    return global_page_style;
  },

  getPageStyle:function(){
    var global_page_style = _.clone(this.style.get('page_style'));
    if(!'column' in global_page_style) global_page_style.column = 1;

    if (book.get('curent_style').has('special_page')){
      var special_page = book.get('curent_style').get('special_page');

      if (special_page) {
        if (
          special_page.first_page && _.size(special_page.first_page)>0 &&
          typeof(special_page.first_page['page'+this.page])!='undefined' &&
          this.page == special_page.first_page['page'+this.page].page
        ){
          var special_page_param = special_page.first_page['page'+this.page];
          if (typeof(special_page_param.page_style)!='undefined'){
            _.extend(global_page_style, special_page_param.page_style);
          }
        } else {
          if (this.specialMiddlePage(special_page) == 1){

            var tk = this.model.page_template_collection.template_key;
            _.each(special_page.page_templates[tk], function(v,k){
              if (k == 'page_style'){
                _.extend(global_page_style, special_page.page_templates[tk]['page_style']);
              }

              if (widget_element.indexOf(k)>-1){

                var page_widget = _.find(this.model.widget_collection.models, function(widget){
                  return widget.get('type')==k && widget.get('pos')==this.page && widget.get('type-pos')=='page';
                }, this);

                if (page_widget){
                  if (typeof(v.placeholders)!='undefined'){
                    page_widget.set('placeholders', new Placeholders(_.toArray(v.placeholders)));
                  }
                }
              }
            }, this)
          }
          /*else {
            global_page_style = this.page_model.checkTitle();
            console.log(this.page_model.id, global_page_style)
          }*/
        }
      }
    }
    return global_page_style;
  },

  specialMiddlePage:function(special_page){
    if (special_page.page_templates && _.size(special_page.page_templates)>0) {
      if (this.model.widget_collection.length>0){
        var inset_widget = _.find(book.html_elements.models, function(model){
          return model.get('type')=='inset' && model.get('printed') == 0 && !model.has('page');
        });

        special_page.widget = _.filter(this.model.widget_collection.models, function(widget){
         return  (widget.get('type-pos') == 'page' && widget.get('pos') == this.page) || (widget.get('type') == 'inset' && inset_widget);
        }, this);

        if (_.size(special_page.widget)>0){
          var type_array = [];

          _.each(special_page.widget, function(widget){
            if (widget.has('type')){
              type_array.push(widget.get('type'));
            } else {
              type_array.push(widget.get('element_type'));
            }
          });

          this.model.page_template_collection.checkMatches(type_array);
          return 1;
        }
      }
    }
  },

  getDropcapConfig:function(){
    if (book.get('curent_style').has('dropcap')){
      if (_.size(book.get('curent_style').get('dropcap'))!=0) {
        var src = this.model.get('curent_style').get('dropcap').src;
        if (src){
          var src_dropcap = src.split('/');
          if (src_dropcap){
            // console.log(src_dropcap, src);
            var dropcap_img = src_dropcap[2].split('.'), // get file name without .ext
                dropcap_config = new DropCapConfig();
                dropcap_config.set({
                  url:'/dropcaps/getBaseConfig/?config='+dropcap_img[0]+'.xml',
                  src:'/drop_caps/base_drop_caps/'+dropcap_img.join('.')
                });
          }
        } else {
          console.log('fatal error');
        }
      }
      /* if (src_dropcap[2] == 'base_drop_caps') {
        dropcap_config.set({
          url:'/dropcaps/getBaseConfig/?config='+dropcap_img[0]+'.xml',
          src:'/drop_caps/base_drop_caps/'+dropcap_img.join('.')
        })
      } else {
        dropcap_config.set({
          url:'/dropcaps/getCustomConfig/?config='+dropcap_img[0]+'.xml',
          src:'/drop_caps/users_drop_caps/%md5(user_uid)%/'+dropcap_img.join('.')
        });
      }*/
    }
  },

  markedDropcapParagraphs:function(){
    if (this.model.has('curent_style') && this.model.get('curent_style').has('dropcap')) {
      var dropcap_style = this.model.get('curent_style').get('dropcap');

      if ( _.size(dropcap_style)==0){
          _.map(book.html_elements.models, function(model){
            if (model.get('type') == 'text' || model.get('type') == 'paragraph'){
              model.set('dropcap',0);
            }
          });
      } else {

        if (typeof(dropcap_style) != 'undefined' && typeof(dropcap_style.type) != 'undefined' && dropcap_style.type == 'paragraph') {
          if (dropcap_style.only_first == 0) {
            _.map(book.html_elements.models, function(model){
              if (model.get('type') == 'text' || model.get('type') == 'paragraph'){
                model.set('dropcap',1);
              }
            });
          } else {
            var firstPmodel = _.find(book.html_elements.models, function(model){
              return model.get('type') == 'text' || model.get('type') == 'paragraph';
            });
            firstPmodel.set('dropcap',1);
            console.log('mark first P');
          }
        } else {

          if (typeof(dropcap_style) != 'undefined' && typeof(dropcap_style.type_lvl) != 'undefined'){

            var titles = _.filter(book.html_elements.models, function(model){
              if (dropcap_style.type_lvl > 0){
                return model.get('type') == 'title' && model.get('typeLvl') == parseInt(dropcap_style.type_lvl);
              } else {
                return model.get('type') == 'title';
              }
            });

            if (titles.length > 0){

              //console.log('mark all chapter with typeLvl=', parseInt(dropcap_style.type_lvl));
              for (var i=0; i<titles.length; i++){

                // find one next paragraph for marked how Dropcap
                marked_p = _.find(book.html_elements.models, function(model){
                  return (model.get('type') == 'text' || model.get('type') == 'paragraph') && model.id > titles[i].id;
                });

                if (marked_p){
                  marked_p.set('dropcap',1);
                }
              }
            }
          }
        }
      }
    }
  },

  renderScripts: function(){
    // Рисуем кастомные функции из базы
    var custom_scripts = '';
    _.each(book.get('curent_style').get('scripts'), function(script,index){
        var funDemoName = "demo_custom_js"+script.id;
        var funConfName = "conf_custom_js"+script.id;
        var clear_container = "\ncontainer.html('');\n";
        var saveButton = "\nvar saveButton = " +
            "$('<button></button>').attr({'class':'btn saveButton'})" +
            ".append($('<span></span>').attr('class','curent_prop').html('Save')); " +
            "\n$(container).append(saveButton);\n" +
            "saveButton.css('top', parseInt(saveButton.parent().css('height'))+5);\n" +
            "saveButton.click(function(){" +
            "$.post('/scripts/saveScripts/', {" +
            "book_uid:"+book.id+"," +
            "theme_uid:"+book.selected_theme.id+"," +
            "id:"+script.id+"," +
            "pos:pos," +
            "data_js:data" +
            "},function(){" +
            "changeScriptData("+script.id+", pos, data);\n" +
            "dispatcher_scripts('demo', "+script.id+", container, pos, data);\n" +
            "});\n" +
            "});\n";

        var execDemo = "\nfunction "+funDemoName+"(container, pos, data){\n"+clear_container+script.script_demo+"\n}\n";
        var execConf = "\nfunction "+funConfName+"(container, pos, data){\n"+clear_container+script.script_conf+"\n"+saveButton+"\n}\n";

        custom_scripts += execDemo + execConf;
        custom_scripts += "\nfunction dispatcher_scripts(name, script_id, container, pos, data){" +
            "\nswitch(script_id){\n";

        for(var i=1;i<book.get('curent_style').get('scripts').length+1;i++){
            custom_scripts += "case "+i+":\n" +
                "if(name == 'demo'){\n" +
                "if(!demo_custom_js"+i+"($(container), pos, data)){\n" +
                "changeScriptData(script_id, pos, data);\n" +
                "conf_custom_js"+i+"($(container), pos, data);\n" +
                "}\n" +
                "}else{\n" +
                "conf_custom_js"+i+"($(container), pos, data);\n" +
                "}\n" +
                "break;\n"
        }
        custom_scripts += "}\n}";
    });

    $('#customs_js').html(custom_scripts);
  },

  /**
   * add illustration, video, script, colon_title, colon_number
   * @param {Backbone.View} page_view вьюха страницы
   * @return {Bool} true если добавили фичу на всю страницу и дальнейший парс не трубуется
   */
  addFeatures:function(page_view){
    var style = this.model.get('curent_style'),
        page_model = page_view.model,
        divscripts_style = this.model.get('selected_theme').get('divscript');

    if (style.has('colon_number') && typeof(style.get('colon_number'))!='undefined') {
      page_view.$el.find(".colon_number").css(style.get('colon_number')).html(this.page);
    }

    var colon_title_style = style.get('colon_title');
    if (colon_title_style && colon_title_style.title_style){
        var colon_title = page_view.$el.find(".colon_title");
        colon_title.css(colon_title_style.title_style);

        var author = colon_title_style['set_author'] ? book.get("author") : '';
        var name = colon_title_style['set_name'] ? "<b>" + book.get("name") + "</b>" : '';

        if(author) {
          var delimiter = colon_title_style['delimiter'] ? colon_title_style['delimiter'] : '. ';
        } else {
          delimiter = '';
        }

        colon_title.html(author + delimiter + name);
    }

    var videos = _.filter(book.widget_collection.models, function(m){
      return  m.get('type-pos')=='page' &&
              m.get('pos') == page_model.id &&
              m.get('fullpage') &&
              m.get('element_type') == 'video';
    });

    if (this.pasteMediaContent(videos, page_model) == 1) return 1;

    var illustrations = _.filter(book.widget_collection.models, function(m){
      return  m.get('type-pos')=='page' &&
              m.get('pos') == page_model.id &&
              m.get('fullpage') &&
              m.get('element_type') == 'illustration';
    });

    if (this.pasteMediaContent(illustrations, page_model) == 1) return 1;

    if (typeof(divscripts_style) != 'undefined' && divscripts_style.length) {
      divscript = '';
      _.each(divscripts_style, function(element){
          if (parseInt(element["pos"]) == page_model.get('page')) {
              divscript = element;
          }
      });
    }

    if (typeof(divscript)=='object' && parseInt(divscript.pos) == page_model.get('page')){
      var divscript_model = new HtmlElement({
        id         :parseInt(divscript.id),
        pos        :parseInt(divscript.pos),
        type       :'divscript',
        style      :divscript.style,
        container  :page_view,
        src        :divscript.src
      });


      var widget = this.model.widget_collection.get(divscript.id);
      if (widget){
        widget.set('type','divscript');
        widget.view = new DivscriptView({ model:divscript_model });
        $('.content', page_view.$el).html(widget.view.el);
        var inside_collection = new Backbone.Collection;
        inside_collection.add(divscript_model);
        page_model.set('inside_collection', inside_collection);
        return 1;
      }
    }

  },

  /**
   * [pasteMediaContent description]
   * @param  {[type]} media_content [description]
   * @return {[type]}               [description]
   */
  pasteMediaContent:function(media_content){
    if (typeof(media_content)!='undefined' && media_content.length>0){
      _.each(media_content, function(m){

        this.new_attributes = _.clone(m.attributes);
        this.new_attributes['container'] = this.page_model.view;
        this.new_attributes['element_type'] = _.find(available_elements_type.models, function(m){
          return m.get('name') == this.new_attributes['type'];
        },this);

        this.html_model = new CoverPicModel(this.new_attributes);
        this.html_model.createView();


        this.page_model.view.content.append(this.html_model.view.render().el);

        this.html_model.set('printed',1);
        book.html_elements.add(this.html_model);

        if (this.page_model.has('inside_collection')){
          this.inside_collection = this.page_model.get('inside_collection')
        } else {
          this.inside_collection = new Backbone.Collection;
        }
        this.inside_collection.add(m);
        this.page_model.set('inside_collection', this.inside_collection);
      },this);
      return 1;
    } else {
      return 0;
    }
  },

  /**
   * Создание обложки
   */
  createCover:function(){

    this.page_style = this.getPageStyle();
    this.createPage();

    var book_name_model = _.find(book.html_elements.models, function(m){ return m.get('type') == 'name'; }),
        book_author_models = _.filter(book.html_elements.models, function(m){ return m.get('type') == 'author'; }),
        book_genre_model = _.find(book.html_elements.models, function(m){ return m.get('type') == 'genre'; });

    if(book.get('curent_style').has('cover_pic'))
      this.addCoverPic(this.page_model);

    if (typeof(book_name_model) != 'undefined')
      this.addBookName(book_name_model);

    if (typeof(book_author_models) != 'undefined')
      this.addBookAuthor(book_author_models);

    if (typeof(book_genre_model) != 'undefined')
      this.addBookGenre(book_genre_model)

    this.page_model.set({
      'inside_collection':this.page_model.inside_collection,
      'printed':1
    });
  },

  addBookName:function(book_name_model){
      book_name_model.createView();

      book_name_model.set({
        style:this.style.get('cover_name_style'),
        container:this.page_model.view,
        printed:1
      });

      this.page_model.inside_collection.add(book_name_model);
      this.page_model.view.content.append(book_name_model.view.el);
  },

  addBookAuthor:function(book_author_models){
      var author_model = book_author_models[0];
      if (author_model){
        author_model.authors = book_author_models;
        author_model.set('authors', []);

        book_author_models.forEach(function(one_author_model,i){
          if (i>0) one_author_model.set('merged',1);
          author_model.get('authors').push(one_author_model.get('content'));
          one_author_model.set({ container:this.page_model.view })
        }, this);

        author_model.set({
          style: this.style.get('cover_author_style'),
          container:this.page_model.view
        });

        author_model.view = new CoverAuthorView({ model:author_model })
        author_model.view.render();

        book_author_models.forEach(function(one_author_model){
          one_author_model.set({printed: 1});
        });

        this.page_model.inside_collection.add(author_model);

        this.page_model.view.content.append(author_model.view.el);
      }
  },

  addBookGenre:function(book_genre_model){
      book_genre_model.createView()
      book_genre_model.set({
        container:this.page_model.view,
        style:this.style.get('genre')
      });

      this.page_model.inside_collection.add(book_genre_model);
      this.page_model.view.content.append(book_genre_model.view.el);

      book_genre_model.set({
        printed:1
      });
  },

  addCoverPic:function(page_model){

    this.cover_pic_model = _.find(this.model.html_elements.models, function(m){ return m.get('type')=='cover_pic'; })

    if (typeof(this.cover_pic_model)!='undefined'){
      this.model.html_elements.remove(this.cover_pic_model);
    }

    this.cover_pic_model_attr = book.get('curent_style').get('cover_pic');
    this.cover_pic_model_attr.element_type = _.find(available_elements_type.models, function(m){
      return m.get('name') == 'cover_pic';
    });
    this.cover_pic_model_attr.container = page_model.view;
    this.cover_pic_model_attr.type = 'cover_pic';

    this.cover_pic_model = new CoverPicModel( this.cover_pic_model_attr );

    this.model.html_elements.add(this.cover_pic_model);
    this.page_model.inside_collection.add(this.cover_pic_model);
    if (typeof(this.cover_pic_model.view)!='undefined') {
      $('.content', page_model.view.$el).append(this.cover_pic_model.view.el);
      this.cover_pic_model.set('printed',1);
    }
  }
});

var BookPrevView = BookView.extend({
  el: '.page_previews'
});

var Library = Backbone.Collection.extend({
  model: Book,
  url: '/books/',
  initialize:function(){
    this.fetch({error:function(){
        console.log('error fetch library');
    }});

    // Если загрузили еще книгу отобразим её в списке доступных
    this.on("reset add", function(msg) {
        $('.my_books').html('');
        $('#goto_edit').hide();

        if (library.length>0) {

            $('.libconteiner').show();

            library.each(function(model){
                model.set('id', model.get('book_uid'));
                var book_btn = new BookBtnView({model:model});
                $('.my_books').append(book_btn.el);
            });
        }
    })
  }
});

/*
 * get text fragment from book content between START and END
 */
function test_pos(start, end){
  if (typeof(book)!='undefined'){
    if (book.has('content')){
      var content = book.get('content');
      return content.substr(parseInt(start-1),parseInt(end-start+1));
    } else {
      console.log('book don\'t have content');
    }
  } else {
    console.log('book undefined')
  }
}

/*
 * get html element/elements by position
 */
function test_el(pos){
  if (typeof(book)!='undefined'){
    if ('html_elements' in book){
      var result = _.filter(book.html_elements.models, function(model){
        return model.get('pos_start') < pos && model.get('pos_end') > pos;
      });

      if (result.length == 1){
        return JSON.stringify(result[0].toJSON(), null, '\t' );
      } else {
        return result;
      }

    } else {
      console.log('book don\'t have html_elements');
    }
  } else {
    console.log('book undefined')
  }
}

/*
 * Get ComputedStyle
 */
function getStyle(el,styleProp) {
  var camelize = function (str) {
    return str.replace(/\-(\w)/g, function(str, letter){
      return letter.toUpperCase();
    });
  };

  if (el.currentStyle) {
    return el.currentStyle[camelize(styleProp)];
  } else if (document.defaultView && document.defaultView.getComputedStyle) {
    return document.defaultView.getComputedStyle(el,null)
                               .getPropertyValue(styleProp);
  } else {
    return el.style[camelize(styleProp)];
  }
}