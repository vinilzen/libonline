//change in theme
var Change = Backbone.Model.extend({
	initialize:function(){
		this.view = new ChangeView({model:this});
	}
});

// changed element
var ChangedElementStyle = Backbone.Model.extend({
	initialize:function(a,b){  // new Model([attributes], [options])
//		console.log(a,b);
	}
});

// list changed(modified)  themes
var Changes = Backbone.Collection.extend({
	model:Change
});

// btn save
var ChangeView = Backbone.View.extend({
	className:'btn btn-mini btn-warning',
	events:{
		'click':'save_changes',
	},
  	initialize:function(){
    	this.render();
   	},
   	render:function(){
   		
   		this.$el
   				.html('Сохранить')
   				.attr('data-themeName',this.model.get('themeName'));

   		$('.saver').append(this.el);
   		return this;
   	},
	save_changes:function(){
		var el = this.$el;
		var t = this;

		el.addClass('disabled');

 		if (typeof(book.get('changes')) != 'undefined'){

			var hash = _.clone(book.get('changes').get(this.model.id).get('changed_elements').at(0).attributes);
			delete hash.change;

			var save_this = {};
			save_this[hash.type] = hash.style;

			$.ajax({
				url: '/templates/updateTemplate/?book_uid='+book.id+'&theme_uid='+this.model.id,
				type: 'POST',
				data: {data : JSON.stringify(save_this)},
				dataType: "json"
			}).error(function( jqXHR, textStatus, errorThrown) {

				el.removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
				console.log( jqXHR.responseText);

			}).success(function(data){

				el.removeClass('btn-warning')
					.removeClass('btn-danger')
					.removeClass('disabled')
					.addClass('btn-success')
					.fadeOut(1000, function(){
						t.model.collection.remove([t.model], {silent:true});
					});

			});
		} else {
			console.log('кнопка показана и по ней видимо кликнули а изменений нет. как так ?');
		}
	}
});