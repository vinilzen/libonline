/*
 *  Сожержание - Оглавление
 *	Если level = 0 то все заголовки
 *		var c = new Contents();
 *		book.pages.models[2].view.content.append(c.view.el)
 *		c.add({title:"Заголовок 1", page:6})
 */

var TitleContentView = Backbone.View.extend({
	tagName:'li',
	className:'',
	template: _.template('<span><%= title %> - <em class="pull-right"><%= page %></em></span>'),
	initialize:function(){
		this.model.on('change:page', this.render, this);
		this.render();
	},
	render:function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}
});

var TitleContent = Backbone.Model.extend({
	defaults:{
		title:'No name title',
		page:'-'
	},
	initialize:function(){
		this.view = new TitleContentView({model:this});
	},
});

var ContentsView = Backbone.View.extend({
	tagName:'ul',
	className:'contents_view unstyled',
	initialize:function(){
		this.render();
	},
	render:function(){
		return this;	
	}
});

var Contents = Backbone.Collection.extend({
	model:TitleContent,
	initialize:function(){
		this.view = new ContentsView();
		this.view.collection = this;
		this.on('all',this.render,this);
	},
	render:function(){
		if (this.length>0){
			this.each(function(model){
				this.view.$el.append(model.view.el);
			}, this);
		}
	},
	findElements:function(){
		this.addTitle();
		/* OR add Other Items */
	},
	addTitle:function(){
		book.html_elements.each(function(html_element){
			if (html_element.get('type') == 'title'){
				var title_content = new TitleContent({
					title:html_element.get('content')
				})
				this.add(title_content);
				html_element.contents = title_content;
			}
		}, this);
	},
	comparator:function(model) {
		return model.get("page");
	}
});
