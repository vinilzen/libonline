/*
 * DropCap буквицы (состоят из картинки и 
 * файла конфига в котором находится список букв и её координаты на картинке)
 * DropCapsBaseCollection - готовые буквицы доступные всем пользователям
 * DropCapsCustomCollection - буквицы загруженные пользователем и доступные только ему
 */

var DropCapView = Backbone.View.extend({
    tagName:'li',
    initialize:function(){
        this.render();
    },
    render:function(){
        if(this.model.collection.container!='base'){
            this.$el.html('<a href="#">'+this.model.get('name')+'&nbsp;<button class="close delete">&times;</button></a>')
        } else {

            this.$el.append('<strong style="display:block;">'+this.model.get('name')+'</strong>');

            // получение списка букв из конфига и отрисовка
            var param = book.get('curent_style').get('dropcap');
            if (param){
                var max_h = this.get_max_height(param);
                _.each(param, function(val, key){
                    if (key.length==1){
                        dropcap_img_param = val.split(';');
                        size = dropcap_img_param[0].split('x');
                        shift_position = dropcap_img_param[1].split('x');

                        var dc = new DropCapLetterLib({
                          'shift_position_top':shift_position[1],
                          'shift_position_left':shift_position[0],
                          'width':size[0],
                          'height':size[1],
                          'letter':key,
                        });

                        var dcc = $('<div class="thumbnail">').css({
                            'float':'left',
                            'height':parseInt(max_h)+40
                        });

                        dcc .append(dc.el)
                            .append('<p><small>Размер:<strong>'+dropcap_img_param[0]+'</strong>,<br>'+
                                    ' Смещение:<strong>'+dropcap_img_param[1]+'</strong></small></p>');
                        this.$el.append(dcc);
                    }
                }, this);
            }
        }
        return this;
    },
    get_max_height:function(param){
        var max_h = 0;
        _.each(param, function(val, key){
            if (key.length==1){
                var size = val.split(';')[0].split('x');
                if (parseInt(size[1])>max_h)
                    max_h = parseInt(size[1]);
            }
        });
        return max_h;
    },
    events:{
        'click .delete':'delete'
    },
    delete:function(){
        if (confirm("Вы действительно хотите удалить эту буквицу?")) {
            this.model.destroy({ wait: true });
        }
    }
});

var DropCap = Backbone.Model.extend({
    initialize:function(){
        this.id = this.get('name');
        this.view = new DropCapView({model:this});
    },
    sync:function(method, model, options){

        var THIS = this;

        this.show_preloader();

        if (method == 'delete'){
            model.url = '/upload/deleteDropCapFile/?file='+model.id;
            options.error = function(a,b,c){
                THIS.hide_preloader();
                console.log('error',a,b,c);
            }
            options.success = function(a,b,c){

                THIS.hide_preloader();
                if (a.success){
                    THIS.show_message('Буквица успешно удалена', 'success');
                    customDropCaps.fetch();
                    $('.my_alert .alert').fadeOut(null, function() {$(this).html('')});
                } else {
                    if (a.error){
                        THIS.show_message(a.error, 'error');
                    } else {
                        THIS.show_message('Неизвестная ошибка', 'error');
                    }
                }
            }
        }

        Backbone.sync.call(this, method, model, options);
    },
    show_preloader:function(){
        $('.preloader .progress').fadeIn();
    },
    hide_preloader:function(){
        $('.preloader .progress').fadeOut();
    },
    show_message:function(text, type){
        var my_alert = $('<div/>', {
            class: 'alert alert-'+type,
            text: text
        });
        my_alert.append('<button type="button" class="close" data-dismiss="alert">&times;</button>');
        $('.my_alert').html(my_alert);
    }
});

var DropCapsBaseCollection = Backbone.Collection.extend({
  model: DropCap,
  container:'base',
  url:'/upload/getDropCapsBase/',
  initialize:function(){

    var container = this.container;

    this.on('reset', function(collection,b){
        $('.'+container+'_drop_case').html('');
        if (collection.length > 0){
            collection.each(function(model) {
                $('.'+container+'_drop_case').append(model.view.el);
            });
        }
    })
  }
});

var DropCapsCustomCollection = DropCapsBaseCollection.extend({
  url:'/upload/getDropCapsCustom/',
  container:'custom',
});

var DropCapConfig = Backbone.Model.extend({
    initialize:function(){
        this.on('change:url', function(a,b,c){
            this.url = this.get('url');
            this.fetch();
        }, this);

        this.on('change', function(a,b,c){
            if (typeof(b.changes.url) == 'undefined'){
                var dropcap_config = book.get('selected_theme').get('dropcap');
                if (dropcap_config){
                    book.get('curent_style').set('dropcap',dropcap_config);
                    _.extend(dropcap_config, this.attributes)
                } else {
                    console.log('fatal error')
                }
            }
        }, this);
    }
});

var DropCapLetter = Backbone.View.extend({
    className:'dropcap_letter',
    template: _.template('<div class="dropcap_img_container"><img border="0"></div>'),
    events:{'click':'show_menu'},
    initialize:function(){
        this.config = book.get('curent_style').get('dropcap');
        this.render();
    },
    render:function(){

        this.$el
         .html(this.template())
         .css({
            top: this.options.top ? this.options.top+'px' : 0,
            left: this.options.left ? this.options.left+'px' : 0,
            width:this.options.width+'px',
            height:this.options.height+'px',
        });

        this.paste_img();

        return this;
    },
    paste_img:function(){
        if (this.options.original == 1){

            this.$el
                .html('<img alt="'+this.options.letter+'" width="'+this.options.width+'" height="'+this.options.height+'" />')
                .css({
                    'font-size':parseInt(this.options.height*0.8)+'px',
                    'border':0
                });

        } else {

            var myImage = new Image(),
                THIS = this;

            myImage.src = this.config['src'];
            myImage.onload = function() {

                $('.dropcap_img_container', THIS.$el).css({
                    width:myImage.width+'px',
                    height:myImage.height+'px',
                    top:'-'+THIS.options.shift_position_top+'px',
                    left:'-'+THIS.options.shift_position_left+'px',
                });

                $('img', THIS.$el).css({
                    width:myImage.width+'px',
                    height:myImage.height+'px',
                    'font-size':parseInt(THIS.options.width*0.8)+'px'
                }).attr({
                    src:myImage.src,
                    width:myImage.width,
                    height:myImage.height,
                    alt:THIS.options.letter
                });
            }
        }
    },
    show_menu:function(){
        if (typeof(this.menu)!='undefined'){
            this.menu.remove();
            delete this.menu;
        } else {
            var position = this.$el.position();
            var container = this.options.container.closest(".page");

            this.menu = new DropCapMenuView();

            container.append(this.menu.el);
           
            this.menu.$el.css({
                position:'absolute',
                top:position.top-50+parseInt(this.options.container.css('padding-top')),
                left:parseInt(position.left)+parseInt(this.$el.width())+parseInt(container.css('padding-left')),
                width:'300px',
            }).fadeIn();
        }
    }
});

var DropCapLetterLib = DropCapLetter.extend({
    render:function(){
        this.$el
         .html(this.template())
         .css({
            'top': this.options.top ? this.options.top+'px' : 0,
            'left': this.options.left ? this.options.left+'px' : 0,
            'width': this.options.width+'px',
            'height': this.options.height+'px',
            'position': 'relative',
            'background': '#C9C9C9'
        });
        this.paste_img();
        return this;
    },
    show_menu:function(){}
});

var DropCapMenuView = Backbone.View.extend({
    className:'popover right',
    events:{
        'click .type_drc':'type_sh',
        'click .save_dropcap':'save',
    },
    template:_.template('<div class="arrow"></div>'+
                        '<h3 class="popover-title">Настройки буквицы</h3>'+
                        '<div class="popover-content"><form class="dropcap_form">'+
                            '<div class="controls"><label class="checkbox"><input type="checkbox" name="replace">Заменять первую букву</label></div>'+
                            '<div class="controls"><label class="checkbox"><input type="checkbox" name="only_first">В начале каждого абзаца</label></div>'+
                            '<div class="controls"><label class="checkbox"><input type="checkbox" name="float">Обтекать текстом</label></div>'+
                            '<div class="controls">В каких элементах рисовать буквицу<br>'+
                                '<label class="radio inline type_drc"><input type="radio" name="type" value="paragraph">Параграф</label>'+
                                '<label class="radio inline type_drc"><input type="radio" name="type" value="chapter">Глава</label>'+
                            '</div>'+
                            '<div class="controls brd" id="chapter_lvl">Уровень главы:<br>'+
                                '<label class="radio inline"><input type="radio" name="chapter_lvl" value="0">Все</label>'+
                                '<label class="radio inline"><input type="radio" name="chapter_lvl" value="1">1</label>'+
                                '<label class="radio inline"><input type="radio" name="chapter_lvl" value="2">2</label>'+
                                '<label class="radio inline"><input type="radio" name="chapter_lvl" value="3">3</label>'+
                                '<label class="radio inline"><input type="radio" name="chapter_lvl" value="4">4</label>'+
                                '<label class="radio inline"><input type="radio" name="chapter_lvl" value="5">5</label>'+
                            '</div>'+
                            '<div class="controls brd">Выравнивание по вертикали:<br>'+
                                '<label class="radio inline"><input type="radio" name="vertical_align" value="top">top</label>'+
                                '<label class="radio inline"><input type="radio" name="vertical_align" value="bottom">bottom</label>'+
                                '<label class="radio inline"><input type="radio" name="vertical_align" value="middle">middle</label>'+
                            '</div>'+
                            '<div class="controls" style="padding-top:5px;">'+
                                '<button type="submit" class="btn save_dropcap">Сохранить</button>'+
                            '</div>'+
                        '</form></div>'),
    initialize:function(){
        this.param = book.get('curent_style').get('dropcap');
        this.render();
    },
    render:function(){
        this.$el.html(this.template());
        this.set_curent_val();
        
        if (this.param.type == 'paragraph'){
            $('#chapter_lvl', this.$el).hide();
        } else {
            $('#chapter_lvl', this.$el).show();
        }

        return this;
    },
    set_curent_val:function(){
        if (this.param){
            $('[name="replace"]', this.$el).prop('checked', parseInt(this.param.replace));
            $('[name="only_first"]', this.$el).prop('checked', parseInt(this.param.only_first));
            $('[name="float"]', this.$el).prop('checked', parseInt(this.param.float));
            $('[name="type"][value="'+this.param.type+'"]', this.$el).prop('checked', true);
            $('[name="chapter_lvl"][value="'+this.param.type_lvl+'"]', this.$el).prop('checked', true);
            $('[name="vertical_align"][value="'+this.param['vertical-align']+'"]', this.$el).prop('checked', true);
        }
    },
    type_sh:function(){
        if ($('.type_drc input:checked', this.$el).val() == 'paragraph')
            $('#chapter_lvl', this.$el).hide();
        else
            $('#chapter_lvl', this.$el).show();
    },
    save:function(){
        book.get('curent_style').set(
            'dropcap', _.extend(this.param,{
                'replace'       : $('[name="replace"]', this.$el).is(':checked') ? 1 : 0,
                'only_first'    : $('[name="only_first"]', this.$el).is(':checked') ? 1 : 0,
                'float'         : $('[name="float"]', this.$el).is(':checked') ? 1 : 0,
                'type'          : $('[name="type"]:checked', this.$el).val(),
                'type_lvl'      : $('[name="chapter_lvl"]:checked', this.$el).val(),
                'vertical-align': $('[name="vertical_align"]:checked', this.$el).val(),
            })
        );
        book.view.render();
        return false;
    }
});

var baseDropCaps = new DropCapsBaseCollection();
var customDropCaps = new DropCapsCustomCollection();