/*
 * Список допустимых элементов в книге, 
 *	некоторые должны обязательно иметь собственные 
 *  вьюхи, некоторые в случае отсутствия собственных 
 *  могут наследоть родительские (простого параграфа)
 *
 * wrap_type:
 * 1. Абсолютно спозиционированные элементы "absolute" должны иметь хотя 
 *  бы один пласехолдер и располагаются в выбранном, если нет помеченного то в первом
 *
 * 2. Элементы типа "title" занимают всю ширину страницы и ничего не обтекают
 *
 * 3. Элементы типа "text" обтекают пласехолдеры элментов absolute
 */
var ElementsType = Backbone.Collection.extend({});

var available_elements_type = new ElementsType([
	{	name:'page', view:'CoverNameView', cover:0, position:'page',properties_group:'block',
		remmapingTo: [],
		title:'Страница Книги',
		themeContainer:'page_style'
	},
	{	name:'name', title:'Название', view:'CoverNameView', cover:1, position:'absolute',properties_group:'text',
		remmapingTo: [
			'author','genre','paragraph','epigraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','volume','title'
		],
		themeContainer:'cover_name_style'
	},
	{	name:'author', title:'Автор', view:'CoverAuthorView', cover:1, position:'absolute',properties_group:'text',
		remmapingTo: [
			'name','genre','paragraph','epigraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','volume','title'
		],
		themeContainer:'cover_author_style'
	},
	{	name:'genre', title:'Жанр', view:'CoverGenreView', cover:1, position:'absolute',properties_group:'text',
		remmapingTo: [
			'name','author','paragraph','epigraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content'
		],
		themeContainer:'genre'
	},
	{	name:'paragraph', title:'Параграф', view:'PView', position:'static',properties_group:'text',
		remmapingTo: [
			'name','author','genre','epigraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','title','volume'
		],
		themeContainer:'text_style'
	},
	{	name:'epigraph', title:'Епиграф', view:'EpigraphView', position:'static',properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','volume','title'
		],
		themeContainer:'epigraph'
	},
	{	name:'serial', title:'Название серии', view:'SerialView', position:'static', properties_group:'text',
		remmapingTo:[
			'name','author','genre','paragraph','epigraph','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','volume','title'
		],
		themeContainer:'serial'
	},
	{	name:'introduction', title:'Заголовок "Введения"', view:'IntroductionView', position:'static', properties_group:'text',
		remmapingTo:[
			'name','author','genre','paragraph','epigraph','serial','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','volume','title'
		],
		themeContainer:'introduction'
	},
	{	name:'introduction_content', title:'Содержимое "Введения"', view:'IntroductionContentView', position:'static', properties_group:'text',
		remmapingTo:[
			'name','author','genre','paragraph','epigraph','serial','introduction','volume','title',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content'
		],
		themeContainer:'introduction_content'
	},
	{	name:'volume', title:'Название тома', view:'VolumeView', position:'static', properties_group:'text',
		remmapingTo:[	
			'name','author','genre','paragraph','epigraph','serial','introduction','introduction_content',
			'title','date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content'
		],
		themeContainer:'volume'
	},
	{	name:'title', title:'Заголовок', view:'TitleView', position:'title', properties_group:'text',
		remmapingTo:[
			'name','author','paragraph','epigraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','volume','genre'
		],
		themeContainer:'title_style'
	},
	{	name:'table', view:'TableView', position:'title',properties_group:'block',
		remmapingTo: [],
		title:'Таблица',
		themeContainer:'table'
	},
	{	name:'date_create', title:'Дата создания', view:'DateCreateView', position:'static', properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','epigraph','introduction','introduction_content',
			'volume','title','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','serial'
		],
		themeContainer:'date_create'
	},
	{	name:'annotation', title:'Заголовок "Аннотации"', view:'AnnotationView', position:'title', properties_group:'text',
		remmapingTo: [
			'name','author','genre','epigraph','serial','introduction','introduction_content',
			'volume','title','date_create','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','paragraph'
		],		
		themeContainer:'annotation'
	},
	{	name:'footnote', title:'Сноска', view:'FootnotesView', position:'title', properties_group:'text',
		remmapingTo: [
			'name','author','genre','epigraph','serial','introduction','introduction_content',
			'volume','title','date_create','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','from_author_content','paragraph'
		],		
		themeContainer:'footnote'
	},
	{	name:'annotation_content', title:'Содержимое "Аннотации"', view:'AnnotationContentView', position:'static', properties_group:'text',
		remmapingTo: [
			'name','author','paragraph','epigraph','serial','introduction','dedication','volume',
			'introduction_content','title','date_create','annotation','about_author','genre',
			'about_author_content','dedication_content','from_author','from_author_content'
		],
		themeContainer:'annotation_content'
	},
	{	name:'about_author', title:'Заголовок "Об авторе"', view:'AboutAuthorView', position:'title', properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','epigraph','serial','introduction','introduction_content',
			'volume','title','date_create','annotation','annotation_content','about_author_content',
			'dedication','dedication_content','from_author','from_author_content'
		],
		themeContainer:'about_author'
	},
	{	name:'about_author_content', title:'Содержимое "Об авторе"', view:'AboutAuthorContentView', position:'static', properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','epigraph','serial','introduction','introduction_content',
			'date_create','annotation','annotation_content','about_author','volume','title',
			'dedication','dedication_content','from_author','from_author_content'
		],
		themeContainer:'about_author_content'
	},
	{	name:'dedication', title:'Заголовок "Посвящения"', view:'DedicationView', position:'title', properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','epigraph','serial','introduction','volume','title',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication_content','from_author','from_author_content','introduction_content'
		],
		themeContainer:'dedication'
	},
	{	name:'dedication_content', title:'Содержимое "Посвящения"', view:'DedicationContentView', position:'static', properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','epigraph','serial','introduction','volume','title',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','from_author','from_author_content','introduction_content'
		],
		themeContainer:'dedication_content'
	},
	{	name:'from_author', title:'Заголовок "От автора"', view:'FromAuthorView', position:'static', properties_group:'text',
		remmapingTo: [
			'name','author','genre','paragraph','epigraph','serial','introduction','volume',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author_content','introduction_content','title'
		],
		themeContainer:'from_author'
	},
	{	name:'from_author_content', title:'Содержимое "От автора"', view:'FromAuthorContentView', position:'static', properties_group:'text',
		remmapingTo: [
			'name','author','paragraph','epigraph','serial','introduction','volume','title',
			'date_create','annotation','annotation_content','about_author','about_author_content',
			'dedication','dedication_content','from_author','introduction_content','genre'
		],
		themeContainer:'from_author_content'
	},
	{	name:'speech', title:'Прямая речь', view:'SpeechView', internal:1, position:'static', properties_group:'text',
		remmapingTo: ['paragraph'],
		themeContainer:'speech'
	},
	{	name:'inset', title:'Вынос', view:'InsetView', internal:1, position:'absolute', properties_group:'text',
		remmapingTo: ['paragraph'],
		themeContainer:'inset'
	},
	{	name:'video', title:'Видео', view:'VideoView', position:'absolute', properties_group:'media',
		remmapingTo:[''],
		themeContainer:'video'
	},
	{	name:'audio', title:'Аудио', view:'AudioView', position:'absolute', properties_group:'media',
		remmapingTo:[''],
		themeContainer:'audio'
	},
	{	name:'illustration', title:'Иллюстрация', view:'IllustrationView', position:'absolute', properties_group:'media',
		remmapingTo:[''],
		themeContainer:'illustration'
	},
	{	name:'latex', title:'Формула', view:'LatexView', position:'absolute', properties_group:'media',
		remmapingTo:[''],
		themeContainer:'latex'
	},
	{	name:'cover_pic', title:'Иллюстрация на обложке', view:'CoverPicView', position:'absolute', properties_group:'media',
		remmapingTo:[''],
		themeContainer:'cover_pic'
	},
	{	name:'contents', title:'Оглавление', view:'ContentsView', position:'absolute', properties_group:'block',
		remmapingTo:[''],
		themeContainer:'contents'
	},
]);


var SerialView = PView.extend();
var IntroductionView = PView.extend();
var IntroductionContentView = PView.extend();
var VolumeView = PView.extend();
var DateCreateView = PView.extend();
var AnnotationView = PView.extend();
var AnnotationContentView = PView.extend();
var AboutAuthorView = PView.extend();
var AboutAuthorContentView = PView.extend();
var DedicationView = PView.extend();
var DedicationContentView = PView.extend();
var FromAuthorView = PView.extend();
var FromAuthorContentView = PView.extend();
var SerialView = PView.extend();
var EpigraphView = PView.extend();
var TableView = CoverPicView.extend({
	render: function() {
		if(this.options.preview){
			this.$el.css({
				height:parseInt(this.model.view.$el.height())*book.get('prev_scale')+'px',
				overflow: 'hidden'
			});
		}

		if (this.model.has('placeholders')){

			var placeholder = _.find(this.model.get('placeholders').models, function(placeholder){
				return placeholder.get('selected');
			});

			if(typeof(placeholder)!='undefined'){
				var style = _.clone(placeholder.attributes);
				if (typeof(style.selected)!='undefined'){
					delete style.selected;
				}

				var tbl = $('<table>')
							.addClass('table-bordered table padding-zero');
							
				if (this.model.has('row')){
					_.each(this.model.get('row'), function(row){
						var tr = $('<tr>');
						_.each(row.cells, function(cell){
							var td = $('<td>');
							if (typeof(cell.content)!='undefined'){
								_.each(cell.content, function(el){
									td.append($('<p>').html(test_pos(el[0]['pos_start'],el[0]['pos_end'])));
								});
							}

							if (typeof(cell.attr)!='undefined'){
								if('colspan' in cell.attr){
									td.attr('colspan',cell.attr['colspan'])
								}
								if('rowspan' in cell.attr){
									td.attr('rowspan',cell.attr['rowspan'])
								}
							}
							tr.append(td);
						})
						tbl.append(tr);
					})
				}

				if (this.options.preview){
					tbl.addClass('preview_table');
				}
				this.$el.html(tbl)
						.css(style)
						.css('position','absolute');
			} else {
				console.log('need placeholder')
			}
		} else {
			console.log('need placeholder')
		}
		return this;
	},
});
var InsetView = CoverPicView.extend({
	render: function(){

		this.$el
				.html(this.model.get('content'))
				.css({
					position:'absolute',
					overflow:'hidden',
					background:'rgba(221, 221, 221, 0.5)'
				});

		this.model.set({
			'curent_placeholder_size':[this.$el.width(),this.$el.height()],
			'printed':1
		},{silent:true});

		return this;
	},
});