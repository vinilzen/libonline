/*
 * Добавить фильтры в окно предпросмотра,
 * отсеюивающие превью-страницы по следующимкритериям:
 *	- наличие настранице дефолтного контента из шаблона
 */
var FilterDef = Backbone.View.extend({
	el:$('#filter_def'),
	events:{
		'click':'filter'
	},
	initialize:function(options){
		this.book = options;
		this.render();
	},
	render:function(){
		return this;
	},
	filter:function(){
		if(this.el, this.$el.is(':checked')){
			this.enable_filter();
		} else {
			this.disable_filter();
		}
	},
	enable_filter:function(){
		
		this.book.defaults = _.filter(book.html_elements.models, function(m){
			return ['video','illustration','img','cover_pic', 'audio'].indexOf(m.get('type'))>-1;
		});

		this.book.pages.list = [];

		if (typeof(this.book.defaults)!='undefined' && this.book.defaults.length > 0){
			_.each(this.book.defaults, function(m){
				if (m.has('container') && !m.get('approve') &&
					typeof(m.get('container'))!='undefined' && 
					typeof(m.get('container').model)!='undefined' &&
					typeof(m.get('container').model.id)!='undefined'
				){
					this.book.pages.list.push(m.get('container').model.id)
				}
			},this)
			this.book.pages.hide_preview();
		} else {
			console.log('empty');
		}

		if (book.widget_collection && book.widget_collection.length>0){
			_.each(book.widget_collection.models, function(widget){
				if (typeof(widget)!='undefined' && typeof(widget.get('page'))!='undefined' && widget.get('type')!='inset') {
					if (	typeof(widget.html_model)!='undefined' && 
							(	!widget.html_model.has('approve') || 
								(widget.html_model.has('approve') && widget.html_model.get('approve')==0)
							)
						) {
						this.book.pages.list.push(widget.get('page'));
					}
				}
			})
		}

		this.book.pages.hide_preview();
	},
	disable_filter:function(){
		this.book.pages.show_all_preview();
		this.book.defaults = this.book.pages.list = [];
	}
}); 