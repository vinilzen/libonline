/*
	Footnotes - Сноски
	Сноска назначается на конкретный символ  в тексте, выводится внизу страницы, на
	которой в текущей момент размещен этот символ  и автоматически нумеруется при
	наличии более одной сноски на странице. Пользователь может редактировать текст
	сноски. В информации о сноске так же присутствует значение длины в символах, чтобы в
	тексте сноски привести поясняемое слово/фразу целиком.

	var footnotes = new Footnotes([], {page_view:book.pages.get(2).view});
	var fn = new Footnotes([], {page_view:book.pages.get(2).view});
	
	fn.reset([
		{symbol_position:1500,text:'Petya'},
		{symbol_position:1300,text:'Vasya'},
		{symbol_position:1200,text:'Ivan'}
	]);

	fn.add({symbol_position:1500, text:'Petya'});
	fn.add({symbol_position:1300, text:'Vasya'});
	fn.add({symbol_position:1200, text:'Ivan'});
*/
var FootnotesView = PView.extend({
	initialize:function(){
		this.symbol_position = this.model.get('pos_in_text');
		this.page = this.get_page();

		if (book.pages.get(this.page)){
			this.page_view = book.pages.get(this.page).view;
		}

		if (typeof(this.page_view)!='undefined'){
			if (typeof(this.page_view.footnotes)!='undefined'){

				if (this.page_view.footnotes.get(this.model.id)){
					console.log(this.page_view.footnotes.get(this.model.id))
				} else {
					this.page_view.footnotes.add({
						symbol_position:this.symbol_position,
						text:this.model.get('content'),
						id:this.model.id,
						page:this.page
					});
				}

			} else {
				this.page_view.footnotes = new Footnotes([], {page_view:this.page_view});
				this.page_view.footnotes.add({
					symbol_position:this.symbol_position,
					text:this.model.get('content'),
					id:this.model.id,
					page:this.page
				});
			}
		} else {
			console.log('Не удалось найти страницу для размещения сноски')
		}
		this.model.set('printed',1);
	},
	get_page:function(){
		this.html_element = _.find(book.html_elements.models, function(html_element){
			return html_element.get('pos_start')<=this.symbol_position && html_element.get('pos_end')>=this.symbol_position;
		}, this);

		if (this.html_element){
			return this.html_element.get('container').model.id;
		} else {
			console.log('Не найдено место к которому относится эта сноски', this);
			return 0;
		}
	},
	render:function(){}
});

var FootnotesListView = Backbone.View.extend({
	className:'footnotes',
	events:{
		'mouseover':'show',
		'mouseleave':'timerhide'
	},
	animated:0,
	showed:0,
	initialize:function(){
		_.bindAll(this, 'render');
		this.render();
	},
	render:function(){
		return this;
	},
	show:function(){
		if (!this.animated && !this.showed){
			var THIS = this;
			this.animated == 1;
			var h = $('.olul', this.$el).height();
			this.$el.animate({height:h}, function(){
				THIS.showed = 1;
				THIS.animated = 0;
			});
		} else {
			clearTimeout(this.timer);
		}
	},
	timerhide:function(){
		var THIS = this;
		clearTimeout(this.timer);
		if (this.showed && !this.animated){
			this.timer = setTimeout(function(){
				THIS.my_hide(THIS)
			}, 1500);
		}
	},
	my_hide:function(THIS){
		this.animated = 1;
		this.$el.animate({height:'5px'}, function(){
			THIS.animated = 0;
			THIS.showed = 0;
		});	
	}
});

var FootnoteView = Backbone.View.extend({
	className:'footnote',
	tagName:'li',
	template:_.template('<span class="edit"><%= text %></span><input class="input" type="text" value="<%= text %>" />'),
	events: {
		'click .edit':'edit',
		'blur .input':'close',
		'keypress .input':'updateOnEnter'
	},
	initialize:function(){
		_.bindAll(this, 'render');
	},
	render:function(){
		this.$el.html( this.template(this.model.toJSON()) );
		this.input = this.$('input');
		this.span = this.$('span');
		this.input.hide();
		return this;
	},
	edit:function(){
		this.input
			.val(this.model.get('text'))
			.css({
				'height':this.span.height(),
				'line-height':this.span.height()+'px',
				'padding':0,
				'margin':0
			})
			.show()
			.focus();

		this.span.hide();
	},
	updateOnEnter:function(e){
		if (e.keyCode == 13) {
			var value = this.input.val();
			this.model.set({'text':value});
		}
	},
	close:function(){
		this.render();
	}
});


var MarkedLetterView = Backbone.View.extend({
	className:'hav_footnote',
	tagName:'span',
	initialize:function(){
		_.bindAll(this, 'render');
		this.render();
	},
	render:function(){
		this.$el.html(this.model.get('symbol'));
		return this;
	}
});


var FootnoteAnchor = Backbone.View.extend({
	className:'superscript',
	tagName:'span',
	events:{
		'mouseover':'show_list',
		'mouseleave':'hide_list'
	},
	initialize:function(){
		_.bindAll(this, 'render');
		/*this.model.on('change:counter', function(model,v){
			if(model.footnotes.length>1){
				this.$el.html(v);
			} else {
				this.$el.html('*');
			}
		}, this)
		this.render();*/
	},
	render:function(){
		var THIS = this,
			number_scale = 0.5;

		this.$el.css({
			top:'-'+parseInt(THIS.model.get('height')*number_scale)+'px',
			left:THIS.model.get('width')+'px',
			width:parseInt(THIS.model.get('width')*number_scale)+'px',
			height:parseInt(THIS.model.get('height')*number_scale)+'px',
			'font-size':parseInt(THIS.model.get('height')*number_scale)+'px'
		});
		return this;
	},
	show_list:function(){
		this.model.collection.view.show()
		this.model.view.$el.css({'text-decoration':'underline'});
	},
	hide_list:function(){
		this.model.collection.view.timerhide();
		this.model.view.$el.css({'text-decoration':'none'});
	}
});

var Footnote = Backbone.Model.extend({
	initialize:function(){
		this.view = new FootnoteView({model:this});
		this.anchor = new FootnoteAnchor({model:this});

		this.on('change',function(){ this.view.render(); });

		// paste anchor
		this.on('change:top', function(){
			this.letter_view.$el.append(this.anchor.render().el);
		}, this);

		// create footnote collection in each page (if hav)
		this.on('change:page', function(model,value,changes){
			var page_model = book.pages.get(value);
			if (typeof(page_model)!='undefined'){
				
				if ('footnotes' in page_model == false){
					page_model.footnotes = new Footnotes([],{page_view:page_model.view});
				}

				page_model.footnotes.add(this);
				this.footnotes = page_model.footnotes;

			} else {
				console.log('fatal error')
			}
		}, this);

		var symbol_position = this.get('symbol_position');

		// найдем параграф с помеченным символом
		var p = _.find(book.html_elements.models, function(model){
			return model.get('pos_start')<= symbol_position && model.get('pos_end')>= symbol_position;
		});

		if (typeof(p)!='undefined' && typeof(p.lines)!='undefined'){

			// найдем строку с помеченным символом
			var line = _.find(p.lines.models, function(model){
				return model.get('pos') <= symbol_position && model.get('end') >= symbol_position;
			});

			if (typeof(line)!= 'undefined'){

				var shift = symbol_position - line.get('pos');
				
				this.set('symbol',line.get('text').substr(shift, 1));
				
				this.letter_view = new MarkedLetterView({model:this});
				
				line.view.$el
					.html(line.get('text').substr(0,shift))
					.append(this.letter_view.el)
					.append(line.get('text').substr(shift+1));

				this.set({
					'top':this.letter_view.$el.position().top,
					'left':this.letter_view.$el.position().left,
					'width':this.letter_view.$el.width(),
					'height':this.letter_view.$el.height()
				});

			} else {
				if (p.lines.length == 1){
					
					var line = _.find(p.lines.models, function(model){
						return 1;
					});

					var shift = line.get('text').length;
				
					this.set('symbol',line.get('text').substr(shift, 1));
					
					this.letter_view = new MarkedLetterView({model:this});
					
					line.view.$el
						.html(line.get('text').substr(0,shift))
						.append(this.letter_view.el)
						.append(line.get('text').substr(shift+1));

					this.set({
						'top':this.letter_view.$el.position().top,
						'left':this.letter_view.$el.position().left,
						'width':this.letter_view.$el.width(),
						'height':this.letter_view.$el.height()
					});
				}
			}
        }
	}
});

var AllFootnotes = Backbone.Collection.extend({
	model:Footnote,
	initialize:function(models,o){
		//this.on(change)
	}
});
/*
var all_footnotes = new AllFootnotes();
all_footnotes.url = 'test_footnotes.json';
all_footnotes.fetch();
*/

var Footnotes = Backbone.Collection.extend({
	model:Footnote,
	initialize:function(models,options){
		this.view = new FootnotesListView();
		this.on('add reset', this.rerender, this);
		$('.content', options.page_view.$el).append(this.view.el);
		return this;
	},
	comparator:function(footnote) {
		return footnote.get("symbol_position");
	},
	add_one:function(footnote, counter){
		footnote.set({'counter':parseInt(counter+1)});
		footnote.anchor.render();

		if (typeof(footnote.view)!='undefined'){
			footnote.view.render();
			if (typeof(footnote.view.el)!='undefined'){
				$('.olul', this.view.$el).append(footnote.view.el);
			}
			footnote.view.delegateEvents();
		}
	},
	rerender:function(){
		var THIS = this;
		this.view.$el.html(THIS.length>1?'<ol class="olul"></ol>':'<ul class="olul"></ul>');
		this.each(this.add_one, this);
		
		if (this.length == 1){
			_.each(this.models, function(model){
				model.anchor.$el.html('*');
			});
		}

		if (this.length > 1){
			var i = 1;
			_.each(this.models, function(model){
				model.anchor.$el.html(i++);
			});
		}
	}
});