var InsetMenuView = Backbone.View.extend({
    className:'popover right',
    column_menu_showed:0,
    events:{
        'click .sel':'showSeletArea',
        'click .del':'del',
        'click .ok':'save',
        'click .add_widget':'add_widget',
        'click .add_widget_link':'add_widget_link',
        'click .add_link':'add_link',
        'click .add_ill':'add_ill',
        'click .set_column':'showColumn',
        'click .count_column':'setColumn',
        'click .column_gap':'setColumnGap'
    },
    template:_.template('<div class="arrow"></div>'+
        '<h3 class="popover-title">'+
        '<a href="#" class="sel btn btn-mini">Выделить текст для выноса/виджета</a>'+
        '<a href="#" class="set_column btn btn-mini">Управление колоночностью</a>'+
        '</h3>'+
        '<div class="popover-content"><p></p>'+
        '<a href="#" class="ok"><i class="icon-ok"></i>Вынос</a><br>'+
        '<a href="#" class="add_widget"><i class="icon-plus"></i>Вставить виджет</a><br>'+
        '<a href="#" class="add_widget_link"><i class="icon-share"></i>Ссылка на виджет</a><br>'+
        '<a href="#" class="del"><i class="icon-remove"></i>Отменить</a>'+
        '</div>'),
    initialize:function(){
        this.render();
    },
    render:function(){
        this.$el.html(this.template());
        this.p = $('p', this.$el);
        var position = this.model.view.$el.position();
        if (this.model.has('container')){
            this.model.get('container').$el.append(this.el);
        } else {
            console.log('need container');
        }

        this.$el.css({
            position:'absolute',
            top:position.top+parseInt(this.model.get('container').$el.css('padding-top')),
            left:parseInt(position.left)+parseInt(this.model.view.$el.width())+parseInt(this.model.get('container').$el.css('padding-left')),
            width:'160px',
        }).fadeIn();

        return this;
    },
    setColumn:function(ev){
        var val = parseInt($(ev.target).attr('data-column'));
        this.model.set('column',val);
        $('a.count_column', this.p).css({'font-weight':'normal'}).removeClass('selected');
        $('a[data-column='+val+']', this.p).css({'font-weight':'bold'}).addClass('selected');
        this.model.makeColumn(val);
    },
    setColumnGap:function(ev){
        var val = parseInt($(ev.target).attr('data-gap'));
        this.model.column_gap = val;
        $('a.column_gap', this.p).css({'font-weight':'normal'})
        $('a[data-gap='+val+']', this.p).css({'font-weight':'bold'});

        //var count_column = parseInt($('a.count_column.selected', this.p).attr('data-column'));
        if (typeof(this.model.count_column)!='undefined'){
            count_column = this.model.count_column;
            if (count_column>1) this.model.makeColumn(count_column);
        }
    },
    showColumn:function(){
        if (this.column_menu_showed==0) {
            this.p.html(	'<a href="#" class="count_column" data-column="1">1</a>, '+
                '<a href="#" class="count_column" data-column="2">2</a>, '+
                '<a href="#" class="count_column" data-column="3">3</a> колонки<br>'+
                'отступ между колонками:'+
                '<a href="#" class="column_gap" data-gap="5" >5 </a>, '+
                '<a href="#" class="column_gap" data-gap="10">10</a>, '+
                '<a href="#" class="column_gap" data-gap="15">15</a>, '+
                '<a href="#" class="column_gap" data-gap="20">20</a>px');

            if (parseInt(this.model.get('column'))<2 || parseInt(this.model.get('column'))>4) {
                $('a[data-column=1]', this.p).css({'font-weight':'bold'})
            } else {
                $('a[data-column='+this.model.get('column')+']', this.p).css({'font-weight':'bold'})
            }

            if (typeof(this.model.column_gap)!='undefined'){
                $('a.column_gap', this.p).css({'font-weight':'normal'})
                $('a[data-gap='+this.model.column_gap+']', this.p).css({'font-weight':'bold'});
            } else {
                $('a.column_gap', this.p).css({'font-weight':'normal'})
                $('a[data-gap=5]', this.p).css({'font-weight':'bold'});
            }

            this.column_menu_showed=1;
        } else {
            this.p.html('');
            this.column_menu_showed=0;
        }
    },
    showSeletArea:function(){
        if (this.selet_area){
            this.selet_area.remove();
            delete this.selet_area;
        } else {
            this.selet_area = new SeletAreaView({model:this.model});
        }
        return false;
    },
    del:function(){
        if (this.selet_area){
            this.selet_area.remove();
            delete this.selet_area;
        }
        this.remove();
    },
    add_widget:function(){
        this.page = this.model.get('container').model.id
        // если параграф разбивался на две страницы, проверим в какой части начинается наше выделение
        if (this.model.get('cut')>0){
            var first_part_cut = this.model.get('content').split(' ').length - this.model.get('cut');
            var first_part = this.model.get('content').split(' ').slice(0,first_part_cut).join(' ');
            if (first_part.length > this.model.get('start_select')){
                this.page--;
            }
        }
        if (typeof(this.model.get('start_select'))!='undefined'){
            this.paste_position = this.model.get('start_select') + this.model.get('pos_start');
            this.paste_widget();
        } else {
            alert('Нужно выделить хотябы один символ');
        }
    },
    add_widget_link:function(){
        if(this.model.get('end_select')-this.model.get('start_select')>0&&this.model.get('end_select')-this.model.get('start_select')!=NaN){

        widget_lib_collection.view.show(this.widget_selected,this)
        }else{
            alert('Нужно выделить хотябы один символ');
        }
    },
    widget_selected:function(widget,tgMenu){
            tgMenu.model.set_link_to_widget(widget.cid,tgMenu.model.get('start_select'),tgMenu.model.get('end_select'))

    },
    add_ill:function(){
        book.collection.btnsLib.show_pasted_files('image', 0, 'select_file', this.page, 'illustration');
        media_lib_collection.html_element = this.model;
        media_lib_collection.paste_position = this.paste_position;
        media_lib_collection.selected_placeholder = this.get_first_placeholder();
    },
    add_link:function(){

    },
    get_first_placeholder:function(){
        if (book.selected_theme.has('custom_widgets')){
            var cw = book.selected_theme.get('custom_widgets'); // book.selected_theme.get('custom_widgets')
            if (_.size(cw)>0){
                if (typeof(cw['illustration'])!='undefined') {
                    return cw['illustration'][0];
                }
            }
        }
    },
    paste_select_widget:function(){
        var html = ""
        book.widget_collection.each(function(widget){if(widget.get('type-pos')=='page'){
            html+='<a href="#" class="add_link">'+"Виджет test"+'</a>'
        }})
        $('.popover-content, p', this.$el).html(html);
    },
    paste_widget:function(){
        $('.popover-content, p', this.$el).html(
            '<a href="#" class="add_ill">иллюстрация</a>, '+
            '<a href="#" class="add_ill">видео</a>, '+
            '<a href="#" class="add_ill">аудио</a>'
        );
    },
    save:function(){
        this.page = this.model.get('container').model.id
        // если параграф разбивался на две страницы, проверим в какой части начинается наше выделение
        if (this.model.get('cut')>0){
            var first_part_cut = this.model.get('content').split(' ').length - this.model.get('cut');
            var first_part = this.model.get('content').split(' ').slice(0,first_part_cut).join(' ');
            if (first_part.length > this.model.get('start_select')){
                this.page--;
            }
        }

        var old_html_element_inset = _.find(book.html_elements.models, function(model){
            return model.get('type') == 'inset' && model.get('page') == this.page;
        });

        if (!old_html_element_inset){ // спец выноса для этой страницеы еще не было
            var element_type = _.find(available_elements_type.models, function(model){
                return model.get('name') == 'inset';
            });

            var attr = {
                content: this.model.get('selected'),
                element_type: element_type,
                printed: 0,
                type: "inset",
                page:this.page
            }
            book.html_elements.add(attr);
        } else { // обновим в нем текст
            old_html_element_inset.set({
                content: this.model.get('selected')
            });
        }
        book.view.render();
    }
});

var SeletAreaView = Backbone.View.extend({
    events:{
        'mouseup':'getSelectedText'
    },
    initialize:function(){
        this.render();
    },
    render:function(){
        this.model.get('container').$el.append(this.el);
        var position = this.model.view.$el.position();
        this.$el
            .html(this.model.get('content'))
            .css({
                top:position.top+parseInt(this.model.get('container').$el.css('padding-top')),
                left:position.left+parseInt(this.model.get('container').$el.css('padding-left')),
                width:this.model.view.$el.width(),
                height:this.model.view.$el.height(),
                position:'absolute',
                background:'#ccc',
                color:'#000',
                overflow: 'auto',
                'z-index': 20
            })
        return this;
    },
    getSelectedText:function(){
        if (window.getSelection) {
            var range = window.getSelection().getRangeAt(0);
            if (range){
                this.start = range.startOffset,
                    this.end = range.endOffset;
                this.content = this.model.get('content').substr(this.start, this.end-this.start)
                this.model.set({
                    'selected':this.content,
                    'start_select':this.start,
                    'end_select':this.end
                });
                if (this.model.view.inset_menu && this.model.view.inset_menu.p)
                    this.model.view.inset_menu.p.html(this.content);
            } else console.log('not range');
        } else console.log('not window getSelection');
    }
});