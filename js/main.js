/*
1.получаем книги (список)
2.выбираем одну книгу
3.меняем тему (по умолчанию первая)
4.переходим к редактору (загружаем контент книги и свойста темы)
5.парсим постранично
*/
Backbone.emulateJSON = true;
Backbone.emulateHTTP = true;
var debug = 0, library, t_id, b_id, user_themes, book_view, book, style={};
var progressBar = new MyProgressModel();

$(function(){

// отключаем правую кнопку мыши
//    document.oncontextmenu = function() {return false;};
    
    $('#dropzone').click(function(){
        $('#fileupload').click();
    });

    var w_h = $(window).height();
    $('.container-editor').height(w_h + 'px');
    $('#preview_span').height((w_h-120) + 'px');
    $('#editor_span').height((w_h-85) + 'px');

    // upload new book
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {

            if (data.result && data.result.error){
                my_alert(data.result.error);
                return false;
            } else if (data.result) {
                var book_model = new Book({
                    book_uid:data.result.book_uid,
                    author:data.result.author,
                    name:data.result.name,
                    id:data.result.book_uid
                });
                library.add(book_model);
            }

            $('#progress').fadeOut();
            $('#dropzone').html('Перетащите файл сюда или выберите с диска');

            // доБавляем загуженную книгу в список наших книг
        },
        progressall: function (e, data) {
            $('#progress').show();
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css('width',progress + '%');
        },
        dropZone: $('#dropzone'),
        drop: function(e, data){
            if (data.files.length > 1) {
                my_alert('Вы можете добавлять файлы только по одному');
                return false;
            }

            $.each(data.files, function (index, file) {
                if ( /^.*\.(docx)$/.test(file.name) ) {
                    $('#dropzone').html(file.name);
                } else {
                    my_alert('Недопустимый формат');
                    return false;
                }
            });
        },
        change: function(e, data){
            $.each(data.files, function (index, file) {
                if ( /^.*\.(docx)$/.test(file.name) ) {
                    $('#dropzone').html(file.name);
                } else {
                    my_alert('Недопустимый формат');
                    return false;
                }
            }); 
        }
    }).error(function (jqXHR, textStatus, errorThrown) {
        if (errorThrown === 'abort') {
            my_alert('File Upload has been canceled');
        }
    });

/*  Deprecated
    if ($.browser.opera){
        $('#fileupload').removeClass('hidden').attr('placeholder','Выберите файл на диске');
        $('#dropzone').hide();
    }
*/

    $('#upload_alert .close').click(function(){
        $('#upload_alert').hide();
        $('#upload_alert h4, #upload_alert p').remove();
    });


    // список книг которые я уже загрузил, отобразятся на стартовой
    library = new Library();
    library.btnsLib = new BtnsLib();

    library.btnsLib.reset([
      { id:1, type:'image', title:'Библиотека изображений'},
      { id:2, type:'image', title:'Клипарты изображений', clipart:1 },
      { id:3, type:'video', title:'Библиотека видео' },
      { id:4, type:'video', title:'Клипарты видео', clipart:1},
      { id:5, type:'audio', title:'Библиотека аудио' },
      { id:6, type:'audio', title:'Клипарты аудио', clipart:1},
      { id:7, type:'dropcap', title:'Буквицы'}
    ]);


    var goEdit = Backbone.View.extend({
        events:{ click:'go_edit' },
        go_edit:function(){
            // console.log(book.id, book.selected_theme.id);
            window.location.replace('/index_.html#book'+book.id+'/theme'+book.selected_theme.id+'/edit');
            // return false;
        }
    });

    var search_view = new goEdit({ el: $("#goto_edit") });

    $('.refresh').click(function(){
        book.view.removeSavedPage();
        console.log('clear localStorag (removeSavedPage)');
        return false;
    })

    $('.brand').click(function(){
        $('.container-editor').slideUp();
        $('.container-narrow').slideDown('slow');
        return false;
    });

    $(window).resize(function() {
        var w_h = $(window).height();
        $('.container-editor').height(w_h + 'px');
        $('#preview_span').height((w_h-120) + 'px');
        $('#editor_span').height((w_h-85) + 'px');
    });
});


/*
 *  Стили в таком же виде у нас хранятся в book, user_theme, available_theme
 */
function get_style(model, id){
    var style = {       "text_style":  model.get("text_style"),
                        "title_style": model.get("title_style"),
                        "page_style":  model.get("page_style"),
                        "cover_author_style": model.get("cover_author_style"),
                        "cover_name_style": model.get("cover_name_style"),
                        "cover_pic_style": model.get("cover_pic_style"),
                        "illustration": model.get("illustration")?model.get("illustration"):0,
                        "video": model.get("video")?model.get("video"):0,
                        "placeholders": model.get("placeholders"),
                        "colon_number": model.get("colon_number"),
                        "dropcap": model.get("dropcap"),
                        "colon_title": model.get("colon_title")
            };

    if (id){
        style["theme_id"] = id;
    }

    return style;
}

function changeScriptData(script_id, pos, data_js){
    var current_style = book.get('curent_style');
    var new_data = new Array();
    var coincidence_flag = false;
    if (current_style.scripts_data && current_style.scripts_data.length){
        _.each(current_style.scripts_data, function(data,name){
            if (data.pos == pos){
                coincidence_flag = true;
                new_data.push({
                    'pos':pos,
                    'id':script_id,
                    'data':data_js
                });
            }else{
                new_data.push({
                    'pos':data.pos,
                    'id':data.id,
                    'data':data.data
                });
            }
        });
        if(!coincidence_flag){
            new_data.push({
                'pos':pos,
                'id':script_id,
                'data':data_js
            });
        }
    } else {
        new_data.push({
            'pos':pos,
            'id':script_id,
            'data':data_js
        });
    }

    current_style.scripts_data = new_data;
    
    console.log('don\'t work');
    // !?    book.set('curent_style',current_style);
}

function my_alert(message){
    $('#upload_alert h4, #upload_alert p').remove();
    var alert = '<h4>Warning!</h4><p>'+message+'</p>';
    $('#upload_alert').append(alert).show();
}

$(document).bind('dragover', function (e) {
    var dropZone = $('#dropzone'),
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    } else {
        clearTimeout(timeout);
    }
    if (e.target === dropZone[0]) {
        dropZone.addClass('hover');
    } else {
        dropZone.removeClass('hover');
    }
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});

/*$('body').keydown(function(e) {
  if(e.keyCode == 65){//a
    library.at(0).btn_view.select_book();
  }
  if(e.keyCode == 83){//s
    $('#myProgress').modal('show');
    book.view.render();
  }
});*/