/*
Библиотека медиа контента (видео, илюстрации)

2.4 Библиотека 
Добавить в редактор библиотеку контента (на втором этапе только картинки) и 
предоставить пользователю возможность выбирать иллюстрацию из библиотеки. 
Контент разбит или фильтруется по тематикам, например, детектив, фантастика и т.п.

2.4.1 Семпловый контент
Раздел библиотеки контента, сожержащий подборку для данного шаблона  бесплатных 
(в будущем монетизируемо) изображений для вставки в качестве иллюстраций.

2.4.2 Пользовательский контент
Раздел библиотеки содержащий картинки, загруженные пользователем.

теги для поиска:

MediaLibItemModel.tags = TagCollection() - for view
MediaLibItemModel.attributes.tags = ["big","dark"] - for db
MediaLibItemModel.tags.tag.all_tags = &AllTagCollection()
MediaLibCollection.all_tags = AllTagCollection()
*/

var video_server = 'http://lovidsrv.pandra.ru',
	upload_point = '/upload/',
	media_conf = {
		upload_video:			video_server + upload_point +'uploadVideo/',
		get_video:				video_server + upload_point +'getVideo/',
		get_video_clipart:		video_server + upload_point +'getVideoClipArts/',
		upload_video_clipart:	video_server + upload_point +'uploadVideoClipArt/',
		create_video_folder:	video_server + upload_point +'createVideoFolder/',
		delete_video_folder:	video_server + upload_point +'deleteVideoFolder/',
		delete_video_clipart:	video_server + upload_point +'deleteVideoClipArtFile/',
		delete_video:			video_server + upload_point +'deleteVideoFile/',
		upload_image: 			upload_point+'uploadImage/',
		delete_image:			upload_point+'deleteImageFile/',
		delete_image_clipart:	upload_point+'deleteImageClipArtFile/',
		upload_image_clipart:	upload_point+'uploadImageClipArt/',
		get_image_clipart: 		upload_point+'getClipArts/',
		create_image_folder:	upload_point+'createImageFolder/',
		delete_image_folder:	upload_point+'deleteImageFolder/',
		get_image: 				upload_point+'getImage/',
		upload_audio:			upload_point+'uploadMultimedia/',
		upload_audio_clipart:	upload_point+'uploadMultimediaClipArt/',
		delete_audio:			upload_point+'deleteMultimediaFile/',
		delete_audio_clipart:	upload_point+'deleteMultimediaClipArtFile/',
		get_audio:				upload_point+'getMultimedia/',
		get_audio_clipart:		upload_point+'getMultimediaClipArts/',
		create_audio_folder:	upload_point+'createMultimediaFolder/',
		delete_audio_folder:	upload_point+'deleteMultimediaFolder/',
		set_tag:				upload_point+'setFileTags/'
	},
	curent_folder = '',
	property_value_view_widget = 0;

function getCookie(name){
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

var MediaLibItemModel = Backbone.Model.extend({
	defaults: {
		full_path:'',
		name_img:'',
		tags:[]
	},
	initialize:function(){
		this.on('change:tags',function(model){
			model.save();
		});

		this.view = new MediaLibItemView({model:this});

		if (['image','video','multimedia', 'audio'].indexOf(this.get('type')) > -1){
			this.tags = new TagCollection([],this);
			if (this.has('tags') && this.get('tags').length > 0){
				var collection = [];
				_.each(this.get('tags'), function(tag){
					if (tag)
						collection.push({tag:tag})
				});
				this.tags.reset(collection);
			}
		}/* else {
			console.log('undefined type', this.get('type'));
		}*/
	},
	sync:function(method, model, options){

		console.log(method, model, options);

		if (method == 'update') {
			options.url = media_conf.set_tag
		}
		
		return Backbone.sync.call(this, method, model, options);
	},
	parse:function(response){
		if (response && response.type && ['image','video','multimedia', 'audio'].indexOf(response.type) > -1){
			
			if (response.tags && response.tags.length>0){
				var collection = [];
				_.each(response.tags, function(tag){
					if (tag)
						collection.push({tag:tag})
				});
			} else {
				response.tags = [];
			}
		}
		return response;
	}
});


var MediaLibView = Backbone.View.extend({
	id:'myLib',
	className:'modal hide fade',// ="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	events: {
		'click .add_dir button':'add_dir',
		'click .close':'close',
		'click #new_img_drop':'click_emulation'
	},
	template: _.template(
		'<div class="modal-header">'+
			'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
			'<h3>Библиотека</h3>'+
		'</div>'+
		'<div class="modal-body dn" id="dropcap-lib">'+
			'<div class="row-fluid">'+
				'<div class="row-fluid">'+
					'<h4>Базовые:</h4><ul class="base_drop_case"></ul>'+
				'</div>'+
				'<div class="row-fluid">'+
					'<h4>Пользовательские:</h4><ul class="custom_drop_case"></ul>'+
				'</div>'+
				'<div class="row-fluid preloader">'+
					'<div class="progress progress-striped active hide"><div class="bar" style="width: 100%;"></div></div>'+
					'<div class="my_alert"></div>'+
				'</div>'+
			'</div>'+
			'<div class="accordion" id="accordion2">'+
				'<div class="accordion-group">'+
					'<div class="accordion-heading">'+
						'<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Загрузить свою Буквицу</a>'+
					'</div>'+
					'<div id="collapseOne" class="accordion-body collapse">'+
						'<div class="accordion-inner">'+
							'<form action="/upload/uploadDropCap/" class="form-horizontal" method="post" id="upload_dropcap_form" enctype="multipart/form-data">'+
								'<div class="row-fluid">'+
									'<span class="btn btn-file">'+
										'<i class="icon-plus"> </i><span>Выберите изображение <strong></strong></span>'+
										'<input type="file" name="image_drop_cap" id="image_drop_cap" />'+
									'</span>'+
								'</div>'+
								'<div class="row-fluid">'+
									'<span class="btn btn-file">'+
									'<i class="icon-plus"> </i><span>Выберите конфиг <strong></strong></span>'+
									'<input type="file" name="config_drop_cap" id="config_drop_cap" />'+
									'</span>'+
								'</div>'+
								'<div class="row-fluid">'+
									'<input type="submit" class="btn" value="Загрузить"><br>'+
									'<div id="status"></div>'+
								'</div>'+
							'</form>'+
							'<div class="progress progress-striped upload_initial"><div class="bar" style="width: 0%;"></div></div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="modal-body" id="media-lib">'+
			'<div class="add_dir">'+
				'<div class="input-append">'+
					'<input type="text" name="dir_name" />'+
					'<button class="btn" type="button">Создать папку</button>'+
				'</div>'+
			'</div>'+
			'<div class="tag_filter"></div>'+
			'<table class="table" id="tbl-list"><thead><tr>'+
				'<th>Preview</th>'+
				'<th>Name</th>'+
				'<th style="width: 36px;"></th>'+
				'</tr>'+
			'</thead><tbody class="item-list"></tbody></table>'+
			'<div class="msg hide"></div>'+	      						
			'<div id="upload_img">'+
				'<button class="btn btn-large btn-link btn-block drop" id="new_img_drop">'+
					'Перетащите файл сюда или выберите с диска'+
				'</button>'+
					'<input name="file_name" class="hidden" id="uploadMultimedia" type="file" />'+
			'</div>'+
			'<div id="upload_audio" style="display:none;" class="accordion-inner">'+
				'<form action="'+media_conf.upload_audio+'" class="form-horizontal" method="post" id="upload_audio_form" enctype="multipart/form-data">'+
					'<div class="row-fluid">'+
						'<span class="btn btn-file">'+
							'<i class="icon-plus"> </i><span>Выберите аудио файл<strong></strong></span>'+
							'<input type="file" name="file_name" id="audio_file">'+
						'</span>'+
					'</div>'+
					'<div class="row-fluid">'+
						'<span class="btn">'+
							'<span>Тайминг</span>'+
						'</span>'+
						'<textarea name="data" id="config_audio"></textarea>'+
						'<input type="hidden" name="folder" class="curent_folder" value="">'+
					'</div>'+
					'<div class="row-fluid">'+
						'<input type="submit" class="btn" value="Загрузить">'+
						'<br><div id="status"></div>'+
					'</div>'+
				'</form>'+
				'<div class="progress progress-striped upload_initial"><div class="bar" style="width: 0%;"></div></div>'+
			'</div>'+
			'<div id="upload_video" style="display:none;" class="accordion-inner">'+
				'<form action="'+media_conf.upload_video+'" class="form-horizontal" method="post" id="upload_video_form" enctype="multipart/form-data">'+
					'<div class="row-fluid">'+
						'<span class="btn btn-file">'+
							'<i class="icon-plus"> </i><span>Выберите видео файл<strong class="video_file_path"></strong></span>'+
							'<input type="file" name="file_name" id="video_file">'+
						'</span>'+
					'</div>'+
					'<div class="row-fluid">'+
						'<span class="btn btn-file">'+
							'<i class="icon-plus"> </i><span>Выберите постер для видео<strong class="poster_file_path"></strong></span>'+
							'<input type="file" name="file_name_img" id="video_poster">'+
						'</span>'+
					'</div>'+
					'<div class="row-fluid">'+
						'<input type="hidden" name="folder" class="curent_folder" value="'+curent_folder+'">'+
						'<input type="hidden" name="user_uid" class="user_uid" value="'+getCookie('lib_user_uid')+'">'+
						'<input type="submit" class="btn" value="Загрузить">'+
						'<br><div id="status"></div>'+
					'</div>'+
				'</form>'+
				'<div class="progress progress-striped upload_initial"><div class="bar" style="width: 0%;"></div></div>'+
			'</div>'+
			'</div>'+
		'<div class="modal-footer"></div>'
	),
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el
			.html(this.template())
			.attr({'tabindex':-1,'role':'dialog','aria-hidden':true}); // modal ettribute
		$('body').append(this.el);
		this.media = $('#media-lib', this.$el);
		this.dropcap = $('#dropcap-lib', this.$el);
		this.upload_form();
		return this;
	},
	upload_form:function(){
		$('#uploadMultimedia',this.$el).fileupload({
			type:'POST',
			dataType: 'json',
			formData:{
				'user_uid':getCookie('lib_user_uid')
			},
			done: function (e, data) {
				if (data.result && data.result.error){
					media_message(data.result.error, 'alert-error');
					return false;
				} else if (data.result && data.result.success) {
					media_lib_collection.fetch({data:{folder:curent_folder}});
				} else {
					media_message('Пустой ответ сервера', 'alert-error');
				}
				$('#new_img_drop').html('Перетащите файл сюда или выберите с диска');
			},
			dropZone: $('#new_img_drop'),
		});
	},
	close:function(){
		$('.modal-backdrop').hide();
		this.$el.fadeOut().removeClass('fade').removeClass('in');
	},
	add_dir:function(){
		if ($('.add_dir input').val() != ''){
			switch (this.collection.type){
				case 'video':
					var url = media_conf.create_video_folder;
					break;
				case 'audio':
					var url = media_conf.create_audio_folder;
					break;
				case 'image':
				default:
					var url = media_conf.create_image_folder;
			}

			data = {
				folder:$('.add_dir input').val(),
				user_uid:getCookie('lib_user_uid')
			};

			$.ajax({
				type: "POST",
				url: url, // folders only in pictures
				data: data,
				dataType: "json"
			}).done(function( data ) {
				if (data){
					if (data.error){
						media_message(data.error, 'alert-error');
					} else {
						media_lib_collection.fetch();
						$('.add_dir input').val('');
					}
				} else {
					media_message('Пустой ответ сервера', 'alert-error');
				}
			});
		}
	},
	click_emulation:function(){
		$('#uploadMultimedia',this.$el).click();
	},
	show:function(){
		$('.modal-backdrop').show();
		this.$el.addClass('fade in').show();
	},
	hide:function(){
		$('.modal-backdrop').hide();
		this.$el.removeClass('fade in').hide();
	}
});

var MediaLibCollection = Backbone.Collection.extend({
	model: MediaLibItemModel,
	url: function(){ // for get files
		switch (this.type){
			case 'video':
				return this.clipart ? media_conf.get_video_clipart : media_conf.get_video;
				break;
			case 'audio':
				return this.clipart ? media_conf.get_audio_clipart : media_conf.get_audio;
				break;
			case 'image':
			default:
				return this.clipart ? media_conf.get_image_clipart : media_conf.get_image;
				break;
		}
	},
	sync:function(method, collection, options){
		options || (options = {});
		if (collection.type == 'video'){
			options.crossDomain == true;
            // options.headers = {"Origin":"lo.il"};
			if (getCookie('lib_user_uid')){
				options.data || (options.data = {});
				_.extend( options.data, {
					"user_uid": getCookie('lib_user_uid')
				});
			} else {
				console.log('error, need user_uid')
			}
		}
		Backbone.sync.call(this, method, collection, options);
	},
	initialize:function(){
		this.all_tags = new AllTagCollection([], this);
		this.on('reset', function(){
			
			$('.item-list').html('');
			$('.msg').fadeOut();
			
			this.check_all_tags();

			if (curent_folder != ''){
				this.add({name:curent_folder,type:'a_root'},{silent:true});
			}

			if (this.length>0){
				$('#tbl-list').fadeIn();
				this.each(function(model){
					$('.item-list').append(model.view.el);
				});
			} else {
				$('#tbl-list').fadeOut();
				var t = (media_lib_collection.error_message) ? ' ('+ media_lib_collection.error_message +')' : '';
				media_message('У вас еще нет файлов'+t,'');
			}			
		}, this);

		this.view = new MediaLibView({collection:this});
	},
	check_all_tags:function(){
		this.all_tags.reset();
		this.each(function(model){
			if (typeof(model.tags)!='undefined'){
				model.tags.check_all();
			}
		});
	},
	comparator:function(model){
		return model.get("type");
	},
	hide_all:function(){
		this.each(function(model){
			model.view.$el.hide();
		})
	},
	filter:function(ids){
		this.showed = _.filter(this.models, function(model){
			return _.indexOf(ids, model.id) != -1;
		});

		_.each(this.showed, function(model){
			model.view.$el.show();
		})
	},
	show_all:function(){
		this.each(function(model){
			model.view.$el.show();
		})
	}
});

var media_lib_collection = new MediaLibCollection();

var MediaLibItemView = Backbone.View.extend({
	tagName:'tr',
	template_img: _.template(
		'<td><a href="<%= full_path %><%= name %>" title="Открыть в новой вкладке" target="_blank">'+
			'<img class="media-object" src="<%= full_path %><%= name %>" />'+
		'</a></td>'+
		'<td>'+
			'<%= name %>'+
			'<div class="form-inline"><input class="tag" type="text"><a class="btn btn-mini add_tag">Добавить Тег</a></div>'+
			'<div class="row"></div>'+
		'</td>'+
		'<td class="controls">'+
			'<span class="del<% if (media_lib_collection.clipart) print(" hide"); %>" title="удалить"><i class="icon-remove"></i></span>'+
		'</td>'
	),
	template_multimedia: _.template(
		'<td><a href="<%= full_path %><%= name %>" title="Открыть в новой вкладке" target="_blank">'+
			'<b class="icon-camera-2" title="Мультимедиа"></b>'+
		'</a></td>'+
		'<td>'+
			'<%= name %>'+
			'<div class="form-inline"><input class="tag" type="text"><a class="btn btn-mini add_tag">Добавить Тег</a></div>'+
			'<div class="row"></div>'+
		'</td>'+
		'<td class="controls">'+
			'<span class="del<% if (media_lib_collection.clipart) print(" hide"); %>" title="удалить"><i class="icon-remove"></i></span>'+
		'</td>'
	),
	template_video: _.template(
		'<td><a href="'+video_server+'<%= full_path %><%= name %>" title="Открыть в новой вкладке" target="_blank">'+
			'<b class="icon-camera-2" title="Мультимедиа"></b></a>'+
		'<a href="'+video_server+'/video_img/'+getCookie('lib_user_uid')+'/<%= name_img %>" title="Открыть в новой вкладке" target="_blank">'+
			'<img width="50" height="50" src="'+video_server+'/video_img/'+getCookie('lib_user_uid')+'/<%= name_img %>" />'+
		'</a></td>'+
			'<td>'+
				'<%= name %>'+
				'<div class="form-inline"><input class="tag" type="text"><a class="btn btn-mini add_tag">Добавить Тег</a></div>'+
				'<div class="row"></div>'+
			'</td>'+
			'<td class="controls">'+
				'<span class="del<% if (media_lib_collection.clipart) print(" hide"); %>" title="удалить"><i class="icon-remove"></i></span>'+
			'</td>'
	),
	template_dir: _.template(
		'<td><b class="icon-folder-close" title="Содержимое папки"></b></td>'+
		'<td><%= name %></td>'+
		'<td><span class="del<% if (media_lib_collection.clipart) print(" hide"); %>" title="Удалить"><i class="icon-remove"></i></span></td>'
	),
	template_root: _.template(
		'<td><b class="icon-folder-open" title="Наверх"></b></td>'+
		'<td><%= name %></td>'+
		'<td></td>'
	),
	// url:'/save_tags',
	events:{
		'click .del':'del',
		'click .add':'add',
		'click .add_tag':'add_tag',
		'keypress .tag':'add_tag_enter',
		'click .icon-folder-close':'go_to_folder',
		'click .icon-folder-open':'go_to_up'
	},
	initialize:function(){
		_.bindAll(this);
		this.model.view = this;
		this.render();
	},
	render:function(){
		var t = this.model.get("type");

		if (t=='image' || t == 'clipart'){
			var content = this.template_img(this.model.toJSON());
		} else if (t=='multimedia' || t=='multimedia_clipart') {
			var content = this.template_multimedia(this.model.toJSON());
		} else if (t=='dir') {
			var content = this.template_dir(this.model.toJSON());
		} else if (t=='video' || t=='video_clipart') {
			var content = this.template_video(this.model.toJSON());
		} else if (t=='a_root') {
			var content = this.template_root(this.model.toJSON());
		}

		this.$el.html(content);

		if (t == 'clipart' || t == 'video_clipart' || t == 'multimedia_clipart'){
			$('.form-inline', this.$el).hide();
		} else {
			$('.form-inline', this.$el).show();
		}

		if (typeof(this.model.collection.mode)!='undefined' && this.model.collection.mode == 'select_file'){
			$('.controls', this.$el).append('<span class="add" title="Встроить"><i class="icon-plus"></i></span>');
		}

		return this;
	},
	add_tag_enter:function(e){
		if (e.keyCode == 13){
			this.add_tag()
		}
	},
	add_tag:function(){

		this.value = $('.tag', this.$el).val();
		if (this.value != '' && this.value.length > 2){
			if (_.indexOf(this.model.get('tags'), this.value) > -1){
				alert('Такой тег уже существует');
			} else {
				if (typeof(this.model.tags)!='undefined'){
					var tags = this.model.get('tags');
					if (tags && typeof(tags)!='undefined'){
						tags.push(this.value);
						this.model.set('tags',tags);
						this.model.save();
						this.model.tags.add({tag:this.value});
						$('.tag', this.$el).val('');
					} else {
						console.log('this.model.get(tags) == undefined');
					}
				} else {
					console.log('this.model.tags == undefined');
				}
			}
		} else {
			alert('Тег должен содержать как минимум 3 символа');
		}
	},
	add:function(){
		if (typeof(media_lib_collection.html_element) != 'undefined'){
			
			if (typeof(media_lib_collection.paste_position) == 'number'){
				var new_path = this.model.get('full_path')+this.model.get('name'),
					placeholder = media_lib_collection.selected_placeholder,
					page = media_lib_collection.html_element.get('container').model.id;

				placeholder['selected'] = 1;

				var new_ill = {
					description: "",
					element_type: "illustration", // media_lib_collection.edited_prop
					placeholders: new Placeholders([placeholder]), // placeholders: new Placeholders([placeholder]),
					pos: parseInt(page),  // media_lib_collection.paste_position
					src: new_path,
					illustration_id:media_lib_collection.paste_position,
					title: "",
					type: "illustration", //media_lib_collection.edited_prop
					'type-pos': "page"
				};	

				if (book.get('curent_style').has('illustration'))
					book.get('curent_style').get('illustration').push(new_ill);
				else
					book.get('curent_style').set('illustration', [new_ill]);

				media_lib_collection.html_element.additional_widget = 1;

				book.set({
					'rerender_after':page,
					'save_widgets':1
				});

                delete book.edited;

                book.widget_collection.addAndSave(new_ill);

				book.view.render();

			} else {

				if (typeof(media_lib_collection.html_element) == 'number'){
					var new_path = this.model.get('full_path')+this.model.get('name'),
						placeholder = media_lib_collection.selected_placeholder.options.style;
					placeholder['selected'] = 1;

					var new_ill = {
						description: "",
						element_type: "illustration", // media_lib_collection.edited_prop
						placeholders: new Placeholders([placeholder]), // placeholders: new Placeholders([placeholder]),
						pos: media_lib_collection.html_element,
						src: new_path,
						illustration_id:21,
						style: {},
						title: "",
						type: "illustration", //media_lib_collection.edited_prop
						'type-pos': "page"
					};

					if (book.get('curent_style').has('illustration')){
						book.get('curent_style').get('illustration').push(new_ill);
						book.view.render();
					} else  console.log('need illustration')
				} else {
					if (media_lib_collection.html_element.get('type')=='video' && media_lib_collection.edited_prop != 'poster'){
						var new_path = video_server+this.model.get('full_path')+this.model.get('name');
						if (this.model.has('full_path_img') && this.model.has('name_img')){
							var new_poster_path = video_server+this.model.get('full_path_img')+this.model.get('name_img');
							media_lib_collection.html_element.set('poster', new_poster_path);
							$('video', media_lib_collection.html_element.view.$el).attr('poster',new_poster_path);
						}
					} else var new_path = this.model.get('full_path')+this.model.get('name');

					media_lib_collection.html_element.set(media_lib_collection.edited_prop, new_path);
				}
			}
			media_lib_collection.view.close();

		} else console.log('need html_element', media_lib_collection);	
	},
	del:function(){
		if ( confirm ("Вы действительно хотите удалить элемент?") ) {
			var n = this.model.get("name");
			var t = this.model.get("type");
			if (t == 'dir'){
				var url = media_conf.delete_image_folder+'?folder='+n;
			} else {
				switch(media_lib_collection.type){
					case 'video':
						var url = media_conf.delete_video;
						break;
					case 'audio':
						var url = media_conf.delete_audio;
						break;
					case 'image':
					default:
						var url = media_conf.delete_image;
						break;
				}
				var data = (curent_folder!='')?{folder:curent_folder, file:n}:{file:n};
			}

			data['user_uid'] = getCookie('lib_user_uid');

			$.ajax({
				type: "POST",
				url: url,
				data: data,
				dataType: "json"
			}).done(function( data ) {
				if (data){
					if (data.error){
						media_message(data.error, 'alert-error');
					} else {
						if (t != 'dir' && curent_folder != '') {
							media_lib_collection.fetch({data:{folder:curent_folder}});
						} else {
							media_lib_collection.fetch();
						}
					}
				} else {
					media_message('Пустой ответ сервера', 'alert-error');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				media_message(jqXHR.responseText, 'alert-error');
			});
		}
	},
	go_to_folder:function(){
		$('.add_dir').fadeOut();
		curent_folder = this.model.get("name");
		media_lib_collection.fetch({data:{folder:curent_folder}});
		$('#uploadMultimedia')
			.attr('data-url', media_conf.upload_image+'?folder='+curent_folder)
			.fileupload('option',{url: media_conf.upload_image+'?folder='+curent_folder});
	},
	go_to_up:function(){
		if (media_lib_collection.section != 'ClipArts') $('.add_dir').fadeIn();
		curent_folder = '';
		media_lib_collection.fetch();

		switch (media_lib_collection.type){
			case 'video':
				var url = media_lib_collection.clipart ? media_conf.upload_video_clipart : media_conf.upload_video;
				break;
			case 'audio':
				var url = media_lib_collection.clipart ? media_conf.upload_audio_clipart : media_conf.upload_audio;
				break;
			case 'image':
			default:
				var url = media_lib_collection.clipart ? media_conf.upload_image_clipart : media_conf.upload_image;
				break;
		}
		$('#uploadMultimedia')
			.attr('data-url',url)
			.fileupload('option',{url: url});
	}
});

var MediaLibBtnView = Backbone.View.extend({
	tagName:'li',
	events:{
		'click':'show'
	},
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el
			.attr({'data-target':'#myLib'})
			.html('<a href="#">'+this.model.get('title')+'</a>');

		return this;
	},
	show:function(mode){
		if (this.model.get('type') == 'dropcap') {

			baseDropCaps.fetch();
			customDropCaps.fetch();

			media_lib_collection.view.media.hide();
			media_lib_collection.view.dropcap.show();
			this.show_dropcap_lib();

		} else {

			media_lib_collection.view.dropcap.hide();
			media_lib_collection.view.media.show();

			if (mode == 'select_file'){
				media_lib_collection.mode = mode;
			}
			if (typeof(this.model.html_element)!='undefined'){
				media_lib_collection.html_element = this.model.html_element;
			}

			curent_folder = '';
			media_lib_collection.clipart = (this.model.get('clipart') == 1)?1:0;
			media_lib_collection.type = this.model.get('type')

			if (this.model.get('clipart') == 1){
				$('#upload_img, .add_dir').hide();
			} else {
				$('#upload_img, .add_dir').show();
			}

			if (this.model.get('type') == 'video'){
				$('#upload_img').hide();
				$('#upload_audio').hide();
				$('#upload_video').show();
				this.create_upload_video_form();
			} else if (this.model.get('type') == 'audio'){
				$('#upload_img').hide();
				$('#upload_video').hide();
				$('#upload_audio').show();
				this.create_upload_form();
			} else {
				$('#upload_audio').hide();
				$('#upload_video').hide();
				$('#upload_img').show();
			}

			switch (this.model.get('type')){
				case 'video':
					var upload_url = this.model.get('clipart')?media_conf.upload_video_clipart:media_conf.upload_video;
					break;
				case 'audio':
					var upload_url = this.model.get('clipart')?media_conf.upload_audio_clipart:media_conf.upload_audio;
					break;
				case 'image':
				default:
					var upload_url = this.model.get('clipart')?media_conf.upload_image_clipart:media_conf.upload_image;
			} 

			$('#uploadMultimedia')
				.attr('data-url',upload_url)
				.fileupload('option',{url:upload_url});

			media_lib_collection.fetch({
				success:function(collection, response){
					if (response){
						if (response.error){
							media_lib_collection.error_message = response.error;
							collection.reset();
						}
					} else {
						media_lib_collection.error_message = 'Пустой ответ сервера';
						collection.reset();
					}
				},
				error:function(){
					media_lib_collection.error_message = 'Некорректный ответ сервера';
				}
			});
		}
		media_lib_collection.view.show();
		return false;
	},
	show_dropcap_lib:function(){
		$('#upload_dropcap_form .btn-file').each(function (){
			var self = this;
			$('input[type=file]', this).change(function (){
				$('strong', self).html('');
				var value = $(this).val();
				var fileName = value.substring(value.lastIndexOf('/')+1); // var fileExt = fileName.split('.').pop().toLowerCase();
				$('strong', self).html('(' + fileName + ')');
			});
		});

		$('#upload_dropcap_form').ajaxForm({
			beforeSend: function() {
				$('#status').empty();            
				$('.upload_initial.progress .bar').css('width','0%');
			},
			uploadProgress: function(event, position, total, percentComplete) {
				$('.upload_initial.progress .bar').css('width',percentVal + '%');
			},
			success: function() {
				$('.upload_initial.progress .bar').css('width','100%').fadeOut();
			},
			complete: function(xhr) {
				$('#upload_dropcap_form strong').html('');
				customDropCaps.fetch();
			}
		}); 
	},
	create_upload_form:function(){

		$('#add_dir').hide();

		$('#upload_audio form').attr('url',media_conf.upload_audio);

	    $('#upload_audio_form .btn-file').each(function (){
	        var self = this;
	        $('input[type=file]').change(function(){
				$('#upload_audio .curent_folder').val(curent_folder);
	            $('strong', self).html('(' + fileName + ')');
	        });
	    });

	    $('#status').html('');
	       
	    $('#upload_audio_form').ajaxForm({
	    	dataType:'json',
	        beforeSend: function(){         
	            $('.upload_initial.progress .bar').css('width','0%');
	            $('#upload_audio .curent_folder').val(curent_folder);
	        },
	        uploadProgress: function(event, position, total, percentComplete) {
	            var percentVal = percentComplete + '%';
	            $('.upload_initial.progress .bar').css('width',percentVal + '%');
	        },
	        success: function() {
	            $('.upload_initial.progress .bar').css('width','100%').fadeOut();

	        },
	        complete: function(xhr) {
	        	if (xhr){
		        	var response = JSON.parse(xhr.responseText);

		        	if (response){
		        		if (response.success){
		        			$('#upload_audio #status').html('<span class="alert alert-success">Файл успешно загружен</span>');
		        			if (curent_folder!=''){
		        				media_lib_collection.fetch({data:{folder:curent_folder}});
		        			} else {
			        			media_lib_collection.fetch();
			        		}
		        		} else {
		        			if (response.error){
		        				$('#upload_audio #status').html('<span class="alert alert-error">'+response.error+'</span>');
		        			} else {
		        				$('#upload_audio #status').html('<span class="alert alert-error">Неизвестная ошибка</span>');
		        			}
		        		}
		        	} else {
		        		$('#upload_audio #status').html('<span class="alert alert-error">Неизвестная ошибка</span>');
		        	}
		            $('#upload_audio_form strong').html('');
		            $('#config_audio').val('');
		        } else {
		        		$('#upload_audio #status').html('<span class="alert alert-error">Неизвестная ошибка</span>');
		        }
	        }
	    }); 
	},
	create_upload_video_form:function(){
		$('#add_dir').hide();
		$('#upload_video form').attr('url',media_conf.upload_audio);

		$('#upload_video #video_file').change(function(){
			var value = $(this).val(),
				fileName = value.substring(value.lastIndexOf('/')+1);
			$('.video_file_path').html('(' + fileName + ')');
		});

		$('#upload_video #video_poster').change(function(){
			var value = $(this).val(),
				fileName = value.substring(value.lastIndexOf('/')+1);
			$('.poster_file_path').html('(' + fileName + ')');
		});

	    $('#status').html('');
	       
	    $('#upload_video_form').ajaxForm({
	    	dataType:'json',
	        beforeSend: function(){         
	            $('#upload_video .upload_initial.progress .bar').css('width','0%').show();
	            $('#upload_video .curent_folder').val(curent_folder);
	        },
	        uploadProgress: function(event, position, total, percentComplete) {
	            var percentVal = percentComplete + '%';
	            $('#upload_video .upload_initial.progress .bar').css('width',percentVal + '%');
	        },
	        success: function() {
	            $('#upload_video .upload_initial.progress .bar').css('width','100%').fadeOut();
	        },
	        complete: function(xhr){
	        	if (xhr){
					var response = JSON.parse(xhr.responseText);
					if (response){
						if (response.success){
							$('#upload_video #status').html('<span class="alert alert-success">Файл успешно загружен</span>');
							if (curent_folder!=''){
								media_lib_collection.fetch({data:{folder:curent_folder}});
							} else {
								media_lib_collection.fetch();
							}
						} else {
							if (response.error){
								$('#upload_video #status').html('<span class="alert alert-error">'+response.error+'</span>');
							} else {
								$('#upload_video #status').html('<span class="alert alert-error">Неизвестная ошибка</span>');
							}
						}
					} else {
						$('#upload_video #status').html('<span class="alert alert-error">Неизвестная ошибка</span>');
					}
					$('#upload_video_form strong').html('');
				} else {
					$('#upload_video #status').html('<span class="alert alert-error">Неизвестная ошибка</span>');
				}
				$('#upload_video_form strong').html('');
	        }
	    }); 
	}
});

var BtnLib = Backbone.Model.extend({
	initialize:function(){
		this.view = new MediaLibBtnView({model:this});
	}
});

var BtnsLib = Backbone.Collection.extend({
	model:BtnLib,
	initialize:function(){
		this.on("reset", function(){
			if (this.length > 0) {
				_.each(this.models, function(model){
					$('.lib_buttons .dropdown-menu').append(model.view.el);
				},this);
			}
		},this);
	},
	show_files:function(type, clipart, mode, html_element, prop){
		media_lib_collection.edited_prop = prop;
		switch(type){
			case 'media':
			case 'video':
				type = 'video';
				break;
			case 'audio':
				type = 'audio';
				break;
			case 'illustration':
			case 'background':
			case 'cover_pic':
			case 'img':
				type = 'image';
		}

		this.necessary = _.find(this.models, function(m){
			if (clipart){
				return m.get('type') == type && m.has('clipart') && m.get('clipart')==1;
			} else {
				return m.get('type') == type;
			}
		});

		if (typeof(this.necessary)!='undefined'){
			if (typeof(html_element)!='undefined'){
				this.necessary.html_element = html_element;
			} else console.log('necessary html_element');
			this.necessary.view.show(mode);
		} else console.log('Библиотеки с файлами такого типа не найдено');
	},
	show_pasted_files:function(type, clipart, mode, html_element, prop){
		media_lib_collection.edited_prop = prop;
		switch(type){
			case 'media':
			case 'video':
				type = 'video';
				break;
			case 'audio':
				type = 'audio';
				break;
			case 'illustration':
			case 'background':
			case 'cover_pic':
			case 'img':
				type = 'image';
		}

		this.necessary = _.find(this.models, function(m){
			if (clipart) return m.get('type') == type && m.has('clipart') && m.get('clipart')==1;
			else return m.get('type') == type;
		});

		if (typeof(this.necessary)!='undefined'){
			if (typeof(html_element)!='undefined'){
				this.necessary.html_element = html_element;
			} else console.log('necessary html_element');
			this.necessary.view.show(mode);
		} else console.log('Библиотеки с файлами такого типа не найдено');
	}
});

// type =  alert-error|alert-success|alert-info|'' (last empty)
function media_message(message, type) {

	if (type=="alert-error") {
		
		strong_message = 'Oh snap!';

	} else if (type=="alert-success") {
		
		strong_message = 'Well done!';
	
	} else if (type=="alert-info") {
		
		strong_message = 'Heads up!';
	
	} else {
		
		strong_message = 'Warning!'; 
	
	}

	$('.msg').html(	'<div class="alert '+type+'"><button type="button" class="close" data-dismiss="alert">&times;</button>'+
						'<strong>'+strong_message+'</strong> '+ message +
					'</div>').fadeIn();
}



var WidgetLibView = Backbone.View.extend({
    id:'widgetLib',
    selectedWidgetId:'',
    thumbsAdded:'false',
    callback:null,
    menu:null,
    className:'modal hide fade',// ="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    events: {
        'click .close':'close',
        'click .widget-thumb':'onClick',
        'click .accept-button':'onAccept'
    },
    template: _.template(
		'<div class="modal-header">'+
			'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
			'<h3>Список виджетов</h3>'+
		'</div>'+
		'<div class="modal-body" id="media-lib">'+
			'<div class="tag_filter"></div>'+
			'<div class="widget-preview-list" id="widget-preview-list"></div>'+
			'<div class="msg hide"></div>'+
			'</div>'+
		'<div class="modal-footer-widget"><button class="accept-button">Принять</button></div>'
    ),
    thumb_template:'<div class="widget-preview"><a class="widget-thumb" href="#"><img class="widget_thumb_image" id="thumb_image" widget_id="id_placeholder" src="pic_placeholder"/></a></div>',
    initialize:function(){
        this.render();
    },
    render:function(){
        this.$el
            .html(this.template())
            .attr({'tabindex':-1,'role':'dialog','aria-hidden':true}); // modal ettribute

        $('body').append(this.el);

        return this;
    },
    close:function(){
        $('.modal-backdrop').hide();
        this.$el.fadeOut().removeClass('fade').removeClass('in');
    },
    onClick:function(e){
        this.$el.find("#widget-preview-list").find(".widget-preview").removeClass("widget-preview-selected")
        $(e.target).parent().parent().addClass("widget-preview-selected")
        this.selectedWidgetId=$(e.target).attr("widget_id")
    },
    onAccept:function(e){
        if(this.selectedWidgetId!=""){
            this.callback(this.getWidgetById(this.selectedWidgetId),this.menu)
            this.close()
        }else{
            alert("Выберите виджет")
        }
    },
    getWidgetById:function(id){
        var arr =this.collection.widget_array
        for(var i = 0;i<arr.length;i++){
            if(arr[i].id==id){
                return arr[i].widget
            }
        }
    },
    show:function(callbackFn,tgMenu){
        this.menu = tgMenu
        this.callback = callbackFn
        if(this.thumbsAdded=='false'){
            this.$el.addClass("widget_popups")
        this.addThumbs()
        }
        $('.modal-backdrop').show();
        this.$el.addClass('fade in').show();
    },
    hide:function(){
        $('.modal-backdrop').hide();
        this.$el.removeClass('fade in').hide();
    },
    addThumbs:function(){
        var list = this.$el.find("#widget-preview-list")
        var tt = this.thumb_template
        list.html("")
        var widget_array = []
        var i = 0
        book.widget_collection.each(function(widget){if(widget.get('type-pos')=='page'){
            if(widget.get('type-pos')=='page'){
                var type = widget.get('element_type')
                var pic=""
                switch (type) {
                    case "illustration":
                        pic=widget.get("src")
                        break
                    case "divscript":
                        pic=widget.get("src")
                        break
                    case "video":
                        pic="/img/video_icon.png"
                        break
                    case "audio":
                        pic="/img/multimedia_icon.png"
                        break
                    case "latex":
                        pic="/img/template_icon.png"
                        break
                    case "inset":
                        pic="/img/template_icon.png"
                        break
                    default:
                        pic="http://cs424530.vk.me/v424530337/2646/YsX6IJx-2u0.jpg"
                        break
                }
                var arrayObj = {}
                arrayObj.widget = widget
                arrayObj.id =i
                widget_array.push(arrayObj)
                var template_string = tt.replace("pic_placeholder",pic)
                template_string = template_string.replace("id_placeholder",i)
                list.append(template_string)
                i++
            }
        }})
      // this.thumbsAdded='true'
       this.collection.widget_array = widget_array
    }
});

var WidgetLibCollection = Backbone.Collection.extend({
    initialize:function(){
        this.view = new WidgetLibView({collection:this});
    },
    hide_all:function(){
        this.each(function(model){
            model.view.$el.hide();
        })
    },
    show_all:function(){
        this.each(function(model){
            model.view.$el.show();
        })
    }
});

var widget_lib_collection = new WidgetLibCollection();


var WidgetPopupView = Backbone.View.extend({
    id:'WidgetPopup',
    selectedWidgetId:'',
    thumbsAdded:'false',
    callback:null,
    menu:null,
    className:'modal hide fade',// ="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    events: {
        'click .close':'close',
        'click .close_button':'close',
        'click .widget-thumb':'onClick',
        'click .accept-button':'onAccept'
    },
    template: _.template(
        '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
            '<h3>Просмотр виджета</h3>'+
            '</div>'+
            '<div class="modal-body" id="media-lib">'+
            '<div class="tag_filter"></div>'+
            '<div id="widget-container"><img class="widget_preview_image" id="thumb_image" src="pic_placeholder"/></div>'+
            '<div class="msg hide"></div>'+
            '</div>'+
            '<div class="modal-footer-widget"><button class="close_button">Скрыть</button></div>'
    ),
    initialize:function(){
        this.render();
    },
    render:function(){
        this.$el
            .html(this.template())
            .attr({'tabindex':-1,'role':'dialog','aria-hidden':true}); // modal ettribute
        this.$el.addClass("widget_popups")
        $('body').append(this.el);

        return this;
    },
    close:function(){
        $('.modal-backdrop').hide();
        this.$el.fadeOut().removeClass('fade').removeClass('in');
    },
    onClick:function(e){
        this.$el.find("#widget-preview-list").find(".widget-preview").removeClass("widget-preview-selected")
        $(e.target).parent().parent().addClass("widget-preview-selected")
        this.selectedWidgetId=$(e.target).attr("widget_id")
    },
    onAccept:function(e){
        if(this.selectedWidgetId!=""){
            this.callback(this.getWidgetById(this.selectedWidgetId),this.menu)
            this.close()
        }else{
            alert("Выберите виджет")
        }
    },
    getWidgetById:function(id){
        var arr =this.collection.widget_array
        for(var i = 0;i<arr.length;i++){
            if(arr[i].id==id){
                return arr[i].widget
            }
        }
    },
    show:function(preview){

        this.$el.addClass("widget_popups")
        this.$el.find("#widget-container").html(preview)
        $('.modal-backdrop').show();
        this.$el.addClass('fade in').show();
    },
    hide:function(){
        $('.modal-backdrop').hide();
        this.$el.removeClass('fade in').hide();
    },
    addThumbs:function(){
        var container = this.$el.find("#widget-preview-list")
        var tt = this.thumb_template
        var widget_array = []
        this.thumbsAdded='true'
        this.collection.widget_array = widget_array
    }
});

var WidgetPopupCollection = Backbone.Collection.extend({
    initialize:function(){
        this.view = new WidgetPopupView({collection:this});
    },
    hide_all:function(){
        this.each(function(model){
            model.view.$el.hide();
        })
    },
    show_all:function(){
        this.each(function(model){
            model.view.$el.show();
        })
    }
});

var widget_popup_collection = new WidgetPopupCollection();
