
/*
 * Jquery  break  Paragraph to multi line
 * EXAMPLE $("p").multiLine();
 */
(function($) {
  $.fn.multiLine = function(widget_collection, available_height, p_model) {

	var elem = this[ 0 ] || {},
		i = 0,
		l = this.length,
		pos = p_model.get('pos_start');

	if (typeof(p_model.lines) == 'undefined' || p_model.get('cut')==0){ // если параграф разбиляс на несколько страниц
		p_model.lines = new Backbone.Collection();
	}

	if (l > 1 ){
		try {
			console.log ('Error! You can enable multiline parse for one element')
		} catch( e ) {}
	} else {
		if ( l==1 ) {
			var tagName = $(elem).prop('tagName'),
				width = $(elem).width(),
				text = $(elem).text(),
				chars = _.uniq(text.split('')),
				indent = parseInt( $(elem).css('text-indent') );

			if (text.length > 0){

				if ( p_model.get('printed_dropcap') == 1){
					var style = book.get('curent_style'),
						dropcap_style = style.get('dropcap');  // config for DropCap

					if (dropcap_style && dropcap_style.replace == 1) text = text.substr(1);
				}

				// померяем размеры всех букв
				for (var i = 0; i<chars.length; i++){
					var model = _.find(book.size_table.models, function(s){
						return s.get('symbol') == chars[i] && s.get('type')==p_model.get('type');
					});

					if (typeof(model) == 'undefined') {

						var param = get_letter_param(chars[i], elem, indent);
						var symbol_model = new SymbolModel({
							symbol:chars[i],
							w:param[0],
							h:param[1],
							type:p_model.get('type')
						});

						if (book.size_table.length == 0) {
							book.size_table.line_height = param[1];
						}

						book.size_table.add(symbol_model);
					}
				}

				$(elem).html('').width(width+'px').css('text-indent',0);

				if (p_model.get('type') == 'epigraph'){
					var epigraph_html = p_model.get('content'),
						epigraph_line = epigraph_html.split('<br>'),
						epigraph_line_size = [],
						epigraph_text = epigraph_html.split('<br>').join(' ');

					// for break line
					for (var i=0; i<epigraph_line.length; i++){
						epigraph_line_size.push(epigraph_line[i].split(' ').length);
					}
					words = epigraph_text.split(' ');
				} else {
					words = text.split(' ');	
				}

				var span = '';

				var test_line = new SpanModel({ text:'O' });
				$(elem).append(test_line.view.el);
				var position = $(elem).position(),
					line_height = test_line.view.$el.height();

				test_line.view.remove();

				// подбираем сколько слов влазит в строку и вставляем span-сроку
				j = 0;
				var num_line = 0,
					long_word = 0,
					places = [],
					matrix = [],
					paragraph_height = line_height;

				while (words.length > 0 && paragraph_height <= available_height){

					var word = words.shift(),
						side = {},
						line,
						span_words = [word],
						width =	$(elem).parent().width(),
						paragraph_height = (num_line+2)*line_height;


					matrix[num_line] = [];

					if (widget_collection.length > 0) { // !!! after create line need clear places
						places = check_place(line_height, num_line, width, position.top, indent, widget_collection);
					} else {
						places = [[0, width,(num_line == 0)?indent:0]];
					}

					if (places.length > 0 ){

						for ( var i=0; i<places.length; i++ ){

							if (places[i].length == 3){

								if($(elem).html() == '')
	 								w = places[i][1] - places[i][0]-indent;
								else
 									w = places[i][1] - places[i][0];
								
								var padding_top = 0;
								// если есть буквицы
								if ( i == 0 && typeof(p_model.dropcap_letter)!='undefined') {
									if (typeof(p_model.dropcap_letter.options['vertical-align'])!='undefined'){
										
										if (p_model.dropcap_letter.options['vertical-align'] == 'bottom' && num_line == 0){
											padding_top = parseInt(p_model.dropcap_letter.options.height - line_height);
											if (places.length == 1){
												w = places[i][1]-p_model.dropcap_letter.options.width;
												places[i][2] = p_model.dropcap_letter.options.width;
											} else {
												if (i==0){
													if (places[i][2]>=p_model.dropcap_letter.options.width){
														places[i][0]=places[i][1];
														places[i][1]=p_model.dropcap_letter.options.width;
														places[i][2]=p_model.dropcap_letter.options.width;
													} else {
														w = places[i][1]-p_model.dropcap_letter.options.width;
														places[i][0] = p_model.dropcap_letter.options.width;
														places[i][2] = p_model.dropcap_letter.options.width;
													}
												}
											}
										}

										if (p_model.dropcap_letter.options['vertical-align'] == 'middle'){
											
											padding_top = parseInt((p_model.dropcap_letter.options.height - line_height)/2);
											
											if (p_model.dropcap_letter.options.height > ((num_line+1)*line_height+padding_top)){
												if (places.length == 1){
													w = places[i][1]-p_model.dropcap_letter.options.width;
													places[i][2] = p_model.dropcap_letter.options.width;
												} else {
													if (i==0){
														if (places[i][2]>=p_model.dropcap_letter.options.width){
															places[i][0]=places[i][1];
															places[i][1]=p_model.dropcap_letter.options.width;
															places[i][2]=p_model.dropcap_letter.options.width;
														} else {
															w = places[i][1]-p_model.dropcap_letter.options.width;
															places[i][0] = p_model.dropcap_letter.options.width;
															places[i][2] = p_model.dropcap_letter.options.width;
														}
													}
												}
											}
											if (num_line != 0){
												padding_top = 0;
											}
										}

										if (p_model.dropcap_letter.options['vertical-align'] == 'top'){
																						
											if (p_model.dropcap_letter.options.height > ((num_line+1)*line_height)){
												if (places.length == 1){
													w = places[i][1]-p_model.dropcap_letter.options.width;
													places[i][2] = p_model.dropcap_letter.options.width;
												} else {
													if (i==0){
														if (places[i][2]>=p_model.dropcap_letter.options.width){
															places[i][0]=places[i][1];
															places[i][1]=p_model.dropcap_letter.options.width;
															places[i][2]=p_model.dropcap_letter.options.width;
														} else {
															w = places[i][1]-p_model.dropcap_letter.options.width;
															places[i][0] = p_model.dropcap_letter.options.width;
															places[i][2] = p_model.dropcap_letter.options.width;
														}
													}
												}
											}
											if (num_line != 0){
												padding_top = 0;
											}
										}

										if (num_line == 0 && padding_top > 0){
											available_height = available_height - padding_top;
											p_model.view.$el.css('padding-top',padding_top);
										}
									}

									if (num_line > 0 && p_model.dropcap_letter.options['float'] == 0) {
										if (places.length == 1){
											w = places[i][1]-p_model.dropcap_letter.options.width;
											places[i][2] = p_model.dropcap_letter.options.width;
										}
									}
								}

								if (p_model.get('type') == 'epigraph'){ // for brake line
									while(get_width(span_words.join(' '), p_model.get('type')) < w && span_words.length < epigraph_line_size[num_line] ){
										word = words.shift(),
										span_words.push(word);
									}
								} else {
									while(get_width(span_words.join(' '), p_model.get('type')) < w && typeof(word)!='undefined' ){
										word = words.shift(),
										span_words.push(word);
									}
								}
								

								// если одно очень длинное слово не влезло в строку, то печатаем его влюбом случае
								if (places.length == 1 && 
									span_words.length == 1 && 
									get_width(span_words.join(' '), p_model.get('type')) > width
								){
									console.log(w, width, span_words);
									long_word = 1;
								} else {
									if (words.length != 0 && get_width(span_words.join(' '), p_model.get('type')) > w)	{
										span_words.pop(); // удалим последний элемент т.к. он не поместился
										words.unshift(word); // вернем его в массив всех слов
									} else {
										if (get_width(span_words.join(' '), p_model.get('type')) > w && typeof(word) != 'undefined') {
											span_words.pop(); // удалим последний элемент т.к. он не поместился
											words.unshift(word); // вернем его в массив всех слов
										}
									}
								}

								var text = span_words.join(' ').trim(),
									len = (words.length == 0)?text.length:text.length+1;


								line_model = new SpanModel({
									text:text,
									pos:pos,
									end:pos+len,
									p_id:p_model.id,
									len:len,
									indent:places[i][2],
									width:w,
									height:line_height,
									long_word:long_word,
									num_line:num_line,
									dropcap: p_model.get('dropcap'),
									'padding-top':padding_top,
									page:p_model.get('container').model.id
								});
							
								if (typeof(p_model.lines.add)!='undefined') {
									p_model.lines.add(line_model);

									long_word = 0;
									pos = pos + len;

									if (matrix[num_line].length == 0) line_model.view.$el.css('clear','both');

									matrix[num_line].push(span);
									$(elem).append(line_model.view.el);
									span_words = [];
								} else console.log('error')

							} else console.log('не все параметры', i, places[i]);
						}
						num_line++;

					} else {
						
						line_model = new SpanModel({
							text:'',
							indent:0,
							pos:pos,
							width:width,
							height:line_height,
							page:p_model.get('container').model.id
						});

						p_model.lines.add(line_model);

						$(elem).append(line_model.view.el);

						num_line++;
					}
				}

				if (words.length > 0){
					return words.length;
				}
			}
		} else {
			console.log('Не найден элемент');
			return this;
		}
	}

  	return this;
  }
})(jQuery);

/*
  line_height - line height
  line_number - the current line number
  width - possible line width
  return array:
   arr[0] start coordinates span
   arr[1] final coordinates span, (arr[1]-arr[0]) = width widget
   arr[2] width widget = left padding for span
*/
function check_place(line_height, line_number, width, start_height, indent, widget_collection){
	var places = [],
		place_start = 0,
		last_widget_point = 0,
		last_place_point = 0,
		prev_widget_width = 0,
		left_padding = 0,
		start = line_height*line_number+start_height,
		stop = line_height*(line_number+1)+start_height;

	// поиск виджетов в строке (по высоте) между  height_start и height_stop
	if (typeof(widget_collection) != 'undefined'){
		var w = widget_collection;
		widget_in_line = _.find(widget_collection, function(widget){
			return 	( start > widget.get('height_start') && start < widget.get('height_stop') )
					|| 
					( start <= widget.get('height_start') && stop >= widget.get('height_start') );
		});
	}


	if (typeof(widget_in_line) != 'undefined'){

		// по пикселям проверяем есть ли в текущей координате по горизонтали виджет
		for ( var i=0; i<width; i++ ){
			var point = _.find(widget_collection, function(widget){
				return i >= widget.get('width_start') && i <= widget.get('width_stop') && 
				( 
					( start > widget.get('height_start') && start < widget.get('height_stop') )
					|| 
					( start <= widget.get('height_start') && stop >= widget.get('height_start') ) 
				);
			});

			if (typeof(point) != 'undefined'){

				if (place_start>0){
					places.push([place_start-1, i, (line_number==0 && places.length == 0)?prev_widget_width+indent:prev_widget_width]);
					last_place_point = i;
					place_start = -1;
				}

				last_widget_point = i;
				if (places.length == 0){
					prev_widget_width = point.get('w')+point.get('left');
				} else {
					prev_widget_width = point.get('w');
				}
			} else {
				if (place_start < 1){
					place_start = i;
					left_padding = i-last_place_point-1;
				}

				if (last_widget_point != 0 && i == width-1 ){

					if (left_padding!=0){
						places.push([last_widget_point, i+1, (line_number==0 && places.length == 0)?left_padding+indent:left_padding]);
					} else{
						places.push([last_widget_point, i+1, (line_number==0 && places.length == 0)?prev_widget_width+indent:prev_widget_width]);
					}
				}
			}
		}


	} else {
		places.push([0, width,  line_number==0?indent:0]);
	}

	return places;
}

var SpanView = Backbone.View.extend({
	tagName: 'span',
	className: 'line',
	events:{
		'mousedown':'mousedown',
		'mouseup':'mouseup',
		'contextmenu':'popover'
	},
	initialize:function(){
		this.render();
	},
	render:function(){

		this.$el.html(this.model.get('text'));

		if ( this.model.has('indent') ){
			this.$el.css('padding-left',this.model.get('indent')+'px').width(this.model.get('width'));
		} else this.$el.width(this.model.get('width'));

		if ( this.model.has('height') ) this.$el.css('height',this.model.get('height'));

		if ( this.model.has('padding-top') ) this.$el.css('padding-top',this.options['padding-top']);

		if ( this.model.has('long_word') ) this.$el.css('overflow','hidden');//.attr('title',this.model.get('text'));

		if (this.model.get('num_line') == 0 && this.model.get('dropcap') == 1) this.$el.css('overflow','hidden');

		return this;
	},
	mousedown:function(e){
		if (e.button != 2){
			book.set('selected_from',this.cid);
		}
	},
	mouseup:function(e){
		if (e.button != 2){
			if (book.get('selected_from') == this.cid) {
				book.set('selected_to',this.cid);
			} else {
				book.set({	'selected_from':null,
							'selected_to':null	});
			}
		}
	},

	popover:function(e){
		
		$('.page_container').popover('destroy')
		
		if (typeof(book.remapping_menu) != 'undefined'){
			book.remapping_menu.el.remove();
		}

		if (e.button == 2){

			if (this.cid == book.get('selected_from') && this.cid == book.get('selected_to')){

				var text = this.getSelectedText(e);

				if (text.text != ''){

					if (typeof(book.get('selected_from')) != 'undefined'){

						$('.page_container').popover({
							'container': 'body',
							'placement':'bottom',
							'html':true,
							'trigger':'manual',
							'content':'Преобразовать в:',
							'title':'Переназначить текст "'+text.text+'"'
						}).popover('toggle');

						book.remapping_menu = new RemappingMenu({	text:text.text,
																	pos:this.model.get('pos'),
																	start:text.start,
																	end:text.end,
																	p_id:this.model.get('p_id'),
																	from:'text', 
																	to: 'name|название,author|автор,title|заголовок' });

						$('.popover-content').append(book.remapping_menu.el);

						var x,y;
						if (e.pageX || e.pageY) { 
							x = e.pageX;
							y = e.pageY;
						} else { 
							x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
							y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
						}

						$('.popover').css({top:y+6, left:x-117}); // корректировка положения поповера

					}
				}
			} else {

				$('.page_container').popover('destroy');
				book.set('selected_from',null);
			}
		}
	},

	getSelectedText:function(e){
		var text = '';
		if (window.getSelection) {
			var range = window.getSelection().getRangeAt(0);
			var start = range.startOffset;
			var end = range.endOffset;

			var container = document.createElement("div");
			container.appendChild(range.cloneContents());

			text =  container.innerText;
		} else if (document.selection) {
			text = document.selection.createRange().text;
			console.log('document.selection:',document.selection.createRange().text, document.selection.createRange());
		}

		return {text:text, start:start, end:end};
	}
});

var SpanModel = Backbone.Model.extend({
	initialize:function(){
		this.view = new SpanView({model:this});
	}
});

function get_letter_param(l, container, indent){
	
	if (l == ' '){
		l = '&nbsp;'
	}

	var width = parseInt($(container).html(l)
									.css({'display':'inline-block','width':'auto'})
									.width()) - indent;

	var height = $(container).html(l)
							.css({'display':'inline-block'})
							.height()

	return [width, height];
}

function get_width(str, type){
	var chars = str.split(''),
		size_table = book.size_table,
		sum = 0;

	for (var i = 0; i<chars.length; i++){

		var model = _.find(size_table.models, function(s){
			return s.get('symbol') == chars[i] && s.get('type')==type;
		});

		if (typeof(model) != 'undefined') {
			sum = sum + parseInt(model.get('w'));
		} else {
			// подставим первый символ
			model = size_table.shift();
			sum = sum + parseInt(model.get('w'));
			size_table.unshift(model);
		}
	}
	return sum;
}

/*Symbols Debug*/
var SymbolView = Backbone.View.extend({
	tagName:'li',
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el.html(this.model.get('symbol')+'='+this.model.get('w')+'x'+this.model.get('h'));
		return this;
	}
});

var SymbolModel = Backbone.Model.extend({
	initialize:function(){
		this.view = new SymbolView({model:this});	
	}
});

var SymbolCollection = Backbone.Collection.extend({
	model:SymbolModel,
	initialize:function(){
		var collection = this;
		this.on('add',function(model){
			$('#symbols').append(model.view.el);
		});
	}
});

/* Return array models */
function get_widget_places(container, line_height){
	var rezult = [];
	var i=1;

	var id = $(container).attr('id');

	$('.widget', container).each(function(index){

		if (id == 'c2'){
			console.log($(this));
		}

		var p = $(this).position();
		var widget = new Widget({
			id:i++,
			w:$(this).width(),
			h:$(this).height(),
			left:p.left,
			top:p.top,

			width_start: p.left,
			width_stop:  p.left+$(this).width(),

			height_start: p.top,
			height_stop:  p.top + $(this).height()
		});
		rezult.push(widget);
	});


	if (id == 'c2'){
		console.log( rezult );
	}

	return rezult;
}