/*
 * Past part in Jquery Object and cat it with check height, 
 * EXAMPLE $('.page').page('out', Backbone.Collection, class container, Style, Backbone.View); 
 * class_container  = .content OR column1, column2 ...
 * method "out" paste visible content and return this
 */
(function( $ ){
  var methods = {
    out : function( class_container, page_view, column) {

      var page = page_view.model.get('page'), // номер страницы
          style = book.get('curent_style'),
          container = $(class_container, this).html(''),
          height = container.height(), // высота области для текста (для одноколоночной верстки .content)
          width = container.width(), // ширина области для текста (для одноколоночной верстки .content)
          childs_sum_height = 0, // childs_sum_height = сумма высот заголовков и параграфов (для измерения оставшейся части/метса на странице)
          text, chars, indent = 0, line_height=0, position,
          THIS = this;

      // вставляем виджеты
      if (typeof(book.widget_collection) != 'undefined' && typeof(book.widget_collection.length) != 'undefined'){
        var flow_widget_collection = book.widget_collection.getFlowWidgets(page_view);
      }

      var inside_collection = new InsiedCollection(),
          y = 0;

      // вставляем по элементу проверяя когда пересатнем помещаться в странице
      print_one:
      while (
              height > childs_sum_height && // пока не вылезем из страницы
              _.filter(book.html_elements.models, function(model){
                return model.get("printed")!=1 && typeof(model.id)!='undefined' && typeof(book.html_elements.get(model.id))!='undefined';
              }).length>0 // и не напечатаем всё
            )
      {

        var not_printed_first_item = _.filter(book.html_elements.models, function(model){
          return model.get("printed")!=1 && typeof(model.id)!='undefined' && typeof(book.html_elements.get(model.id))!='undefined';
        }).shift();

        if (not_printed_first_item){

          if (0 && (page_view.model.id == 13 || page_view.model.id == 14)){
            console.log(not_printed_first_item.get('printed'), not_printed_first_item.get('cut'), not_printed_first_item);
          }

          var model = book.html_elements.get(not_printed_first_item.id),
              cover_exist=0,
              view = model.createView(), // find function for view element
              s = model.get('style');

          if (model.has('column'))
            model.set({ container:page_view });
          else
            model.set({ container:page_view, column:column });

          // если разрыв перед элементом
          if (
              typeof(s)!='undefined' && 
              s['pagebreak-before'] && 
              s['pagebreak-before'] == 1 && !model.get('braked')
            )
          {
            model.set('braked', true);
            break print_one;
          }

          var available_height = height - childs_sum_height, paste_dropcap = 0;
          
          if (model.get('dropcap') && _.size(style.get('dropcap'))>0) {

            var inner_text = model.get('content'), 
                letter = inner_text.substr(0,1),
                dropcap_style = style.get('dropcap');
                dropcap_img_param = dropcap_style[letter.toLowerCase()];

            if (typeof(dropcap_img_param) != 'undefined'){
              dropcap_img_param = dropcap_img_param.split(';');
              size = dropcap_img_param[0].split('x');
              shift_position = dropcap_img_param[1].split('x');
            } else {
              size = ['100','100'];
              shift_position = [0,0];
              paste_dropcap = 2;
            }

            if (available_height > size[1] || typeof(available_height) == 'undefined'){
              paste_dropcap = paste_dropcap==2?2:1;
              model.set({
                'dropcap':0,
                'printed_dropcap':1
              });

            } else {
              model.set('break',true);
              break print_one;
            }
          }

          container.append(view.el);

          view.position = view.$el.position();
          view.position['width'] = view.$el.width();
          view.position['height'] = view.$el.height();
          view.line_height = $('span:first-child', view.$el).height();

          // DropCap after paste PView, because need position
          if (paste_dropcap == 1 || paste_dropcap == 2){

              var position = view.$el.position();

              model.dropcap_letter = new DropCapLetter({
                  'container':container,
                  'top':position.top,
                  'left':position.left,
                  'shift_position_top':shift_position[1],
                  'shift_position_left':shift_position[0],
                  'width':size[0],
                  'height':size[1],
                  'original':paste_dropcap == 1?0:1,
                  'letter':letter,
                  'float':dropcap_style['float'],
                  'vertical-align':dropcap_style['vertical-align']
              });
              container.append(model.dropcap_letter.el);
              view.$el.css('min-height', size[1]+'px');
          } else {
            delete model.dropcap_letter;
          }

          // если разрыв после элемента
          if (typeof(s)!='undefined' && 'pagebreak-before' in s && s['pagebreak-after'] && s['pagebreak-after'] == 1) {
            model.set({"printed":1, "container":page_view});
            inside_collection.add(model);
            break print_one;
          }

          if (model.get("cut")>0){
            var part_content = model.get("content").trim().split(' ');
            if (part_content.length != model.get("cut")) {
              view.$el
                    .html( part_content.slice(part_content.length-model.get("cut")).join(' ') )
                    .css('text-indent',0);
            }
          }

          // check hight
          switch (model.get("type")){
            case 'title':
              childs_sum_height += parseInt(view.$el.height()) + parseInt(view.$el.css('padding-bottom')) + parseInt(view.$el.css('padding-top'));

              if (childs_sum_height > height) {
                childs_sum_height = childs_sum_height - (parseInt(view.$el.height()) + parseInt(view.$el.css('padding-bottom')) + parseInt(view.$el.css('padding-top')));
                view.$el.remove();
                break print_one;
              } else {
                            
                if (typeof(s)!='undefined' && 'pagebreak-before' in s && s['pagebreak-before'] && s['pagebreak-before'] == 1) {

                  if (inside_collection.length > 0){
                    childs_sum_height = childs_sum_height - (parseInt(view.$el.height()) + parseInt(view.$el.css('padding-bottom')) + parseInt(view.$el.css('padding-top')));
                    view.$el.remove();
                    break print_one;
                  } else {
                    model.set({"printed":1, "container":page_view});
                    inside_collection.add(model);
                  }
                } else {
                  model.set({"printed":1, "container":page_view});
                  inside_collection.add(model);
                }
              }
            break;

            case 'volume':
              
              var curent_height = parseInt(view.$el.height()) + parseInt(view.$el.css('padding-bottom')) + parseInt(view.$el.css('padding-top'));

              childs_sum_height += curent_height;
              
              model.set({"printed":1, "container":page_view});
              inside_collection.add(model);
            break;

            case 'table':
              var curent_height = parseInt(view.$el.height()) + parseInt(view.$el.css('padding-bottom')) + parseInt(view.$el.css('padding-top'));
              childs_sum_height += curent_height;
              if (childs_sum_height > height) {
                childs_sum_height = childs_sum_height - (parseInt(view.$el.height()) + parseInt(view.$el.css('padding-bottom')) + parseInt(view.$el.css('padding-top')));
                view.$el.remove();
                break print_one;
              } else {
                model.set({"printed":1, "container":page_view});
                inside_collection.add(model);
              }             
            break;

            case 'text':
            default:
              var count_out_word = view.$el
                                        .width(width)
                                        .multiLine(flow_widget_collection.models, available_height, model);

              if (count_out_word > 0){
                var word = model.get("content").split(' '),
                    count_word = word.length;

                if (count_word == count_out_word){
                  model.set({"cut":0});
                  view.remove();
                } else {
                  model.set({"printed":2, "cut":count_out_word});
                }

              } else {
                model.set({"printed":1, "container":page_view});
              }

              model.first_part_view = view.$el;

              if (model.get('printed')!=0) inside_collection.add(model);
              
              childs_sum_height +=  parseInt(view.$el.height()) + 
                                    parseInt(view.$el.css('padding-bottom')) + 
                                    parseInt(view.$el.css('padding-top'));

            break;
          }
        } else console.log('not_printed_first_item');
      }

      page_view.model.set('inside_collection', inside_collection);
      
      return this;
    }
  };

  $.fn.page = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Метод ' +  method + ' не существует в jQuery.page' );
    }
  };

  /*
   * Jquery outerHTML 
   * EXAMPLE $("#my").outerHTML();
   */
  $.fn.outerHTML = function() {
    return $(this).clone().wrap('<div></div>').parent().html();
  }

  /*
   * Jquery сумма высот дочерних элементов
   * EXAMPLE $("#my").outerHTML();
   */
  $.fn.childsHeight = function() {
    var h = 0;
    this.each(function(a,b){
      h = h + $(b).height();
    })
    return h;
  }
})(jQuery);

var InsiedCollection = Backbone.Collection.extend({
  initialize:function(){
    this.on('add', function(a,b,c){
      book.parsed_elements.add(a);
    });
  }
})