/**
 * Страницы книги, вьюхи и модели
 */

var PagePrevView = Backbone.View.extend({
    className: "page_prev_container",
    events: {
      "click": "scrollTo"
    },
    initialize: function() {
      this.$el.append('<div class="page_prev"><div class="colon_number"></div><div class="colon_title"></div></div>');
      this.render();
      $('.page_previews').append( this.el );
    },
    scale: function(styles, object, css_style){
        object.css(styles);
        _.each(styles, function(value, name){
            if(name != 'opacity' && name != 'background'){
                n_value = (value.indexOf("\%")+1) ? value : Math.floor(parseInt(value)*scale);
                object.css(name, n_value);
            }
        });

        var colon_font_size = styles['font-size'] ? styles['font-size'] : css_style;
        object.css('font-size', Math.floor(parseInt(colon_font_size)*scale)+'px');
    },
    render: function(){
      if (this.model) {
        p = this.model.get("page");
        scale = book.get('prev_scale');
        page_style = book.get('curent_style').get('page_style');

        if(!this.model.get('cover')){
            colon_number_style = book.get('curent_style').get('colon_number');
            colon_title_style = book.get('curent_style').get('colon_title');

            if(colon_number_style){
                var colon_number = $('.colon_number',this.$el);
                this.scale(colon_number_style, colon_number, $('.colon_number').css('font-size'));
                colon_number.html(p);
            }

            if (colon_title_style && colon_title_style.title_style){
                var colon_title = $('.colon_title',this.$el);

                this.scale(colon_title_style.title_style, $('.colon_title',this.$el), $('.colon_title').css('font-size'));
                var author = colon_title_style['set_author'] ? book.get("author") : '';
                var name = colon_title_style['set_name'] ? "<b>" + book.get("name") + "</b>" : '';

                if(author) var delimiter = colon_title_style['delimiter'] ? colon_title_style['delimiter'] : '. ';
                else delimiter = '';

                colon_title.html(author + delimiter + name);
            }

            if(typeof(page_style.column) != 'undefined'){
              switch (parseInt(page_style.column)){
                case 2:
                $('.page_prev', this.$el).append( '<div class="span6 column1 content"></div>'+
                                                  '<div class="span6 column2 content"></div>');   
                break

                case 3:
                $('.page_prev', this.$el).append( '<div class="span4 column1 content"></div>'+
                                                  '<div class="span4 column2 content"></div>'+
                                                  '<div class="span4 column3 content"></div>');   
                break

                default:
                $('.page_prev', this.$el).append('<div class="content"></div>');
              }
            } else {
              $('.page_prev', this.$el).append('<div class="content"></div>');   
            }

        } else {
          $('.page_prev', this.$el).append('<div class="content"></div>'); 
        }

        this.content = $('.content', this.$el);

        $('.page_prev',this.$el).attr('data-page', p).css({
          width:Math.floor(parseInt(page_style.width)*scale)+'px',
          height:Math.floor(parseInt(page_style.height)*scale)+'px',
          padding:Math.floor(parseInt(page_style.padding)*scale)+'px',
          background:page_style.background
        });
       
        var widgets = this.model.get('widget');
        if (typeof(widgets) != 'undefined' && widgets.length > 0){
          widgets.each(function(widget){
            if (widget.get('type') == 'img' && widget.has('placeholders') && widget.get('placeholders').length > 0){
              var el = $( '<img src="'+widget.get('src')+'"/>').css({
                position:'absolute',
                top: parseInt(parseInt(widget.get('top'))*scale)+'px',
                left:parseInt(parseInt(widget.get('left'))*scale)+'px',
                width: parseInt(parseInt(widget.get('w'))*scale)+'px',
                height: parseInt(parseInt(widget.get('h'))*scale)+'px',
                'z-index': 1
              });

              $('.page_prev', jo).append(el);
            }
          });
        }

        this.$el.append('<span class="num_page">'+p+'</span>');

        this.model.preview = this;
      }
      return this;
    },

    scrollTo: function(){
      $('.editor_main').scrollTo('.page[data-page='+this.model.get("page")+']', 700);
    }
});

var properties;

var PageView = Backbone.View.extend({ 
    className: "page",
    initialize: function() {
      this.model.view = this;
      this.render();
    },
    events: {
      "click .icon-edit": "show_menu"
    },
    menu_showed:0,
    /*
     * Сейчас доступно только для 1,2,3 колоночной верстки. 
     * Больше можно добавить в switch-case (spanN из грида twiter bootstrap)
     */
    render: function(){
      if(typeof(this.options.column) != 'undefined'){
        switch (parseInt(this.options.column)){           
          case 2:
          this.$el.append('<div class="span6 column1 content"></div>'+
                          '<div class="span6 column2 content"></div>');   
          break

          case 3:
          this.$el.append('<div class="span4 column1 content"></div>'+
                          '<div class="span4 column2 content"></div>'+
                          '<div class="span4 column3 content"></div>');   
          break

          default:
          this.$el.append('<div class="content"></div>');
        }
      } else {
        this.$el.append('<div class="content"></div>');   
      }

      this.content = $('.content', this.$el);

      this.$el.append('<div class="colon_number"></div><div class="colon_title"></div>');   

      this.$el.attr('data-page', this.model.get('page'));

      if (this.model.has("content")) {
        $('.content',this.$el).html(this.model.get("content"));
      }

      this.add_edit_menu();

      return this;
    },
    add_edit_menu:function(){
      this.edit_icon = $('<i class="icon-edit page_edit_ico"></i>');
      this.$el.append(this.edit_icon);
    },
    show_menu:function(){
      if (this.menu_showed == 1){
        this.menu_hide();
      } else {
        this.menu_show();
      }
    },
    menu_show:function(){
      this.menu = new PageMenuView();
      this.menu.page = this;
      this.$el.append(this.menu.render().el);
      this.menu_showed = 1;
    },
    menu_hide:function(){
      if (_.size(this.menu.custom_widgets)>0){ // remove old placeolders
        _.each(this.menu.custom_widgets, function(cw){
          cw.hidePlaceholders();
        });
      }
      this.menu.remove();
      this.menu_showed = 0;
    }
});

var PageMenuView = Backbone.View.extend({
  custom_widgets:[],
  className:'well well-small page-menu',
  initialize:function(){},
  render:function(){
    this.$el.html('');
    this.add_custom_widget();
    return this;
  },
  add_custom_widget:function(){
    if (book.selected_theme.has('custom_widgets')){
      var cw = book.selected_theme.get('custom_widgets');
      if (_.size(cw)>0){
        _.each(cw, function(w, k){
          var cw_item = new WidgetPlaceholderModel({
            placeholders:_.toArray(w),
            name:k
          });
          this.$el.append(cw_item.label.el).append('&nbsp;');
          cw_item.page = this.page;
          cw_item.custom_widgets = this.custom_widgets;
          this.custom_widgets.push(cw_item);
        }, this);
      }
    }
  }
});

var OnePageContentModel = Backbone.Model.extend({
  defaults: {
    "cover":  0,
    "column": 1,
    "marked":0
  },
  initialize:function(options){
    this.options = options;
    _.bindAll(this);
    this.view = new PageView({
      model: this,
      column: this.get('column')
    });

    this.preview = new PagePrevView({
      model: this,
      column: this.get('column')
    });

    this.inside_collection = new Backbone.Collection();
    this.set('inside_collection',this.inside_collection);
    this.id = this.get('page');

    if (book.widget_collection && book.widget_collection.length > 0){
      this.curent_widget_collection = _.filter(book.widget_collection.models, function(m){
        return m.get('type-pos')=="page" && m.get('pos') == this.id || m.get('type')=='inset';
      }, this);
      this.set('curent_widget_collection', this.curent_widget_collection)
    }

    if (this.isSaved() && this.id != 1 && (!book.has('rerender_after') || book.get('rerender_after')>this.id)) this.restoreFromCache();

    this.on('change:printed', this.afterPrint, this);
  },

  /**
   * После печати страницы можем её сохранить и добавить сноски
   * @return {[type]} [description]
   */
  afterPrint: function() {
    progressBar.view.add('Render page ' + this.get('page') + ' finish<br>');
    this.checkTitle();
    this.addFootnote();
    this.checkMultiColumn();
    if (
      (!this.has('restored') || this.get('restored') == 0) && 
      !this.get('special_page') &&
      this.id != 1
    ) this.savePage();
  },

  /**
   * Если есть элеменитя с мультиколоночностью то сделаем их мультиколоночными
   * @param  {[type]} argument [description]
   * @return {[type]}          [description]
   */
  checkMultiColumn:function() {
    if (this.has('inside_collection')){
      var multi_column_el = this.get('inside_collection').find(function(model){
        return model.count_column > 1;
      });

      if (multi_column_el) multi_column_el.makeColumn();
    }
  },

  /**
   * Проверка сохраняли ли мы эту страницу в localStorage
   * @return {Boolean}
   */
  isSaved:function() {
    return !!localStorage.getItem(book.id+'_'+book.selected_theme.id+'_page_'+this.id);
  },

  /**
   * Подготавливает и сохраняет страницу
   * style: стиль страницы 
   */
  savePage:function(){
    console.log('savePage'+this.id);
    var elements = this.getElements(),
        style = this.getStyle();

    var page_data = {
      elements:elements,
      style:style
    };

    localStorage.setItem(book.id+'_'+book.selected_theme.id+'_page_'+this.id, JSON.stringify(page_data));
    this.set('saved',1)
  },

  /**
   * Может отличаться от общего стиля страницы из-за присутсвия какого либо виджета
   * 
   * @return {Object} css стили страницы
   */
  getStyle:function(){
    return this.get('style');
  },

  /**
   * Возвращает основную информацию о виджетах на странице
   * @return {Array}  Список виджетов c параметрами и одним плейсхолдером
   */
  getWidgets: function() {
    var widgets = [];

    if (this.curent_widget_collection && _.size(this.curent_widget_collection) > 0) {

      _.each(this.curent_widget_collection, function(w) {

        // врезка без контента липовая
        if (w.get('type') != 'inset' || (w.get('type') == 'inset' && w.has('content'))) {
          
          var widget = {};

          _.each(w.attributes, function(param, key) {
            if (key != 'page_view'){
              if (key != 'placeholders') {
                widget[key] = param;
              } else {
                if (placeholder = _.find(param.models, function(p) {
                  return p.get('selected');
                })) {
                  widget.placeholder = placeholder;
                }
              }
            }
          });

          if ('placeholder' in widget) widgets.push(widget);
        }
      }, this);
    }

    return widgets;
  },

  /**
   * Возвращает основную информацию о элементах на странице, в каждом элементе
   *  должна быть информация о строках, позиции, типе. 
   *  lines: Строки это набор элементов, с ограниченной высотой, шириной,
   *  отступами для обтекания и содержимым текстом.
   *  
   * @return {Object} Список элементов
   */
  getElements:function(){
    var elements = {},
        inside_collection = this.has('inside_collection')?this.get('inside_collection'):this.inside_collection || false;

    // if (this.id == 1) console.log(inside_collection);

    if (inside_collection){
      inside_collection.each(function(e){
        if (e.get('type')!='divscript'){
          
          if (e.has('first_part_lines_length') && e.get('first_part_lines_length')){
            var first_part_lines_length = e.get('first_part_lines_length');
          }

          var lines = [];
          if (e.lines) e.lines.each(function(line){
            if (line.get('text')!='') lines.push(line.attributes);
          });
          
          // для параграфов которые бьются на две страницы 1 часть
          if (e.get('printed') == 2 && first_part_lines_length ) {
            lines = lines.slice(0,first_part_lines_length);
          }

          // для параграфов которые бьются на две страницы 2 часть
          if (e.get('printed') == 1 && first_part_lines_length ) {
            lines = lines.slice(first_part_lines_length);
          }
          
          elements[e.id] = {
            content:e.get('content'),
            view_name:e.view_name,
            cut:e.get('cut'),
            lines:lines,
            style:e.get('style'),
            position:e.view.$el.position(),
            width:e.view.$el.width(),
            height:e.view.$el.height()
          }
        }
      }, this);
    }
    return elements;
  },

  /**
   * Вотстанавливаем страницу из сохраненных элементов
   * создаваем новые вьюхи
   * @return {[type]} [description]
   */
  restoreFromCache: function() {
    try {
      var page_param = JSON.parse(localStorage.getItem(book.id+'_'+book.selected_theme.id+'_page_'+this.id));
    } catch (e) {
      console.log(e.name, index)
    }

    if (page_param){
      this.restore_elements(page_param.elements);

      // если это специальная страница то мы уже все вставили в BookView.makePage
      /*if (!this.options.special_page) {
        if (!this.get('full_page_features')){
          console.log(' без full_page_features', this.id)
          this.restore_widgets(page_param.widgets);
        }
      }*/

      this.set({'restored':1});
    }
  },

  /**
   * Создадим новые вьюхи для элементов из сохраненной копии
   * @return {[type]} [description]
   */
  restore_elements: function(elements) {
    _.each(elements, function(el, id) {
      if (el.content != '') {
        var model = book.html_elements.get(id);
        if (model) {

          var view = model.createView();
          model.pasteLineFromCache(el.lines);

          this.view.content.append(view.el);

          this.get('inside_collection').add(model);

          // если параграф разбит на две страицы то сохраним другой контейнер
          if (el.cut > 0 && !model.get('printed')) model.set({'container_first_part':this.view},{silent:true});
          // if (this.id == 12){ console.log(el); console.log(model); console.log(el.cut, model.get('printed')); }

          model.set({
            'printed': (el.cut>0 && !model.get('printed'))?2:1,
            'style': model.get('style') || {},
            'container': this.view,
            'cut': el.cut
          }, {
            silent: true
          });

        }
      }
    }, this);
  },

  /**
   * Создадим новые вьюхи для виджетов из сохраненной копии
   * @return {[type]} [description]
   */
  restore_widgets:function(widgets) {
    book.view.addFeatures(this.view);
    book.widget_collection.getFlowWidgets(this.view);
  },
  checkTitle:function(){
    if (this.has('inside_collection') && this.get('inside_collection').length > 0){
      var has_title = _.find(this.get('inside_collection').models, function(m){
        return m.get('type') == 'title';
      })
      if (typeof(has_title)!='undefined'){
        var title_template = _.find(book.page_template_collection.models, function(t){
          return t.has('items') && t.get('items').indexOf('title_style')>-1;
        });
        if (title_template){
          var template_style = book.get('curent_style').get('special_page').page_templates[title_template.get('key')];
          if (template_style.page_style){
            book.pages.get(this.id).view.$el.css(template_style.page_style);
          }
          if (template_style.text_style){
            _.each(this.get('inside_collection').models, function(m){
              if (m.get('type') == 'paragraph' || m.get('type') == 'text'){
                _.each(m.lines.models, function(line){
                  if (line.get('page') == this.id) line.view.$el.css(template_style.text_style)
                }, this);
              }
            }, this);
          }
        }
      }
    }
  },
  addFootnote:function(){
    
    var page = this.get('page');
        
    _.each(book.footnotes.models, function(fn){
      
      var html_element = _.find(book.html_elements.models, function(model){
        return  model.has('container') && model.get('container').model.id==page && 
                model.get('pos_start')<=fn.get('pos_in_text') && 
                model.get('pos_end')>=fn.get('pos_in_text');
      }, fn);

      if (html_element){
        if (html_element.get('cut')!=0){
          var word_array = html_element.get('content').split(' ');
          
          if (html_element.get('printed')==2) {
            var new_end = html_element.get('pos_start') + word_array.slice(0,word_array.length-html_element.get('cut')).join(' ').length;
            
            if (new_end >= fn.get('pos_in_text')){

              if (typeof(this.view.footnotes)=='undefined'){
                this.view.footnotes = new Footnotes([], {page_view:this.view});
              }
              fn.set({ page:page });
              fn.view = new FootnotesView({model:fn});
              this.view.footnotes.add(fn)
            }
          } else {
            var new_start = html_element.get('pos_start') + word_array.slice(0,word_array.length-html_element.get('cut')).join(' ').length;

            if (new_start <= fn.get('pos_in_text')){
              if (typeof(this.view.footnotes)=='undefined'){
                this.view.footnotes = new Footnotes([], {page_view:this.view});
              }
              fn.set({ page:page });
              fn.view = new FootnotesView({model:fn});
              this.view.footnotes.add(fn)
            }
          }
        } else {
          if (typeof(this.view.footnotes)=='undefined'){
            this.view.footnotes = new Footnotes([], {page_view:this.view});
          }
          fn.set({ page:page });
          fn.view = new FootnotesView({model:fn});
          this.view.footnotes.add(fn)
        }
        past_html_element_id = html_element.id;
      }
    },this);
  },
  mark:function(){
    this.set('marked',1);
    this.view.$el.css('background','pink');
    this.preview.$el.css('background','pink');
    this.collection.show_marked_preview()
  },
  de_mark:function(){
    this.set('marked',0);
    this.view.$el.css('background','inherit');
    this.preview.$el.css('background','inherit');
  },
  gotopage:function(){
    this.preview.scrollTo();
  }
});

var Pages = Backbone.Collection.extend({
    model: OnePageContentModel,
    initialize:function(){   },
    show_marked_preview:function(){
      this.each(function(model){ 
        if (model.get('marked') == 0){
          model.preview.$el.hide();
        } else {
          model.preview.$el.show();
        }
      });
    },
    hide_preview:function(){
      if (typeof(this.list)!='undefined'){
        this.each(function(model){ 
          if (this.list.indexOf(model.id)>-1){
            model.preview.$el.show();
          } else {
            model.preview.$el.hide();
          }
        }, this);
      }
    },
    show_all_preview:function(){
      this.each(function(model){ 
         model.preview.$el.show();
      });
    }
});

var MyProgressModel = Backbone.Model.extend({
    defaults:{
        'showed':false,
        'hidden':true
    },
    initialize:function(){
        this.view = new MyProgressView({model:this});
        this.on('change:showed', function(model,value){
           if(value == 1)
               this.view.show();
           else
               this.view.hide();
        }, this)
    },
    hide:function(){
        this.set('showed',0);
    },
    show:function(){
      this.view.clear();
      this.set('showed',1);
    }
})

var MyProgressView = Backbone.View.extend({
    className:'modal progress-bar',
    id:'myProgress',
    initialize:function(){
        this.$el.html('<div class="modal-body"><p>Start render<br></p></div>').hide();
        this.p = $('p', this.$el);
        $('body').append(this.el);
    },
    hide:function(){
        this.$el.hide();
        this.model.set('hidden',true);
    },
    show:function(){
        var model = this.model;
        this.$el.show(50, function(){
            model.set('hidden',false);
        });
    },
    add:function(str){
        this.p.append(str);
    },
    clear:function(){
      this.p.html('');
    }
})