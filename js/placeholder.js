/*
 *	Обозначенные места на странице куда можно поместить какой либо виджет
 */
var Placeholder = Backbone.Model.extend({
	initialize:function(){
		this.view = new PlaceholderView({model:this});

		this.on('change:selected', function(model){
			_.each(model.collection.models, function(m){
				if (model.cid != m.cid)	m.set({selected:0},{silent:true});
			});

			model.html_element.set('curent_placeholder_size',[parseInt(model.get('width')), parseInt(model.get('height'))]);
			model.html_element.view.fill();
			if (model.html_element.preview){
				model.html_element.preview.remove();
			}
			model.html_element.view.create_preview();
			model.html_element.save_curent_position();
		});
	}
});

var Placeholders = Backbone.Collection.extend({
	model:Placeholder,
});

/*
 *  DropZone for widgets
 */
var DropView = Backbone.View.extend({
	className:'drop',
	initialize:function(){
		this.render();
	},
	render:function(){
		var m = this.model;
		this.$el.css({
			top: m.get('top'),
			left: m.get('left'),
			width: m.get('width'),
			height: m.get('height')
		});
		return this;
	},
	drop:function(widget){
		var THIS = this;
		this.$el.droppable({
			drop: function( event, ui ) {
				_.each(widget.placeholders.models, function(placeholder){
					placeholder.set('selected',0);
				});
				THIS.model.set('selected',1);			
				book.view.render();
			}
		});
	}
});

var PlaceholderView = Backbone.View.extend({
	className:'drop',
	initialize:function(){
		this.style = _.clone(this.model.attributes);
		delete this.style.selected
	},
	render:function(){
		this.$el.css(this.style);
		return this;
	},
	droppable:function(my_page){
		var model = this.model;
		$('.content', my_page.$el).append(this.render().el);
		
		this.$el.droppable({
			accept:'.ui-draggable-dragging',
			activeClass: "ui-state-hover",
			hoverClass: "ui-state-active",
			drop: function( event, ui ) {					
				model.set('selected',1);
				book.view.render()
			},
			tolerance: 'pointer' // в каком случае считается что попали в пласехолдер
		});
	},
	remove_droppable:function(){
		this.$el.droppable();
		this.$el.droppable('destroy');
		this.remove();
	}
});


/*
 * Plaseholder каждой страницы для вставки своих виджетов
 */

var WidgetPlaceholderModel = Backbone.Model.extend({
	placeholders_array:{},
	initialize:function(){
		this.label = new WidgetPlaceholderLabelView({model:this});
	},
	showPlaceholders:function(){
		if (this.has('placeholders')) {
			this.pl = this.get('placeholders');
			this.placeholders_array[this.get('name')] = [];
			_.each(this.pl, function(p){
				var placeholder_view = new WidgetPlaceholderView({model:this, style:p});
				this.placeholders_array[this.get('name')].push(placeholder_view);
				placeholder_view.$el.css(p);
				this.page.content.append(placeholder_view.el)
			},this);
		}		
	},
	hidePlaceholders:function(){
		if (typeof(this.placeholders_array[this.get('name')])!='undefined'){
			_.each(this.placeholders_array[this.get('name')], function(plview){
				plview.remove();
			});
			this.placeholders_array[this.get('name')] = [];
		}
	}
});

var WidgetPlaceholderLabelView = Backbone.View.extend({
	className:'wplaceholder-label label label-inverse',
	tagName:'span',
	events:{
		'click':'showPlaceholders'
	},
	initialize:function(){
		this.render();	
	},
	render:function(){
		this.$el.html(this.model.get('name'));
		return this;
	},
	showPlaceholders:function(){
		_.each(this.model.custom_widgets, function(cw){
			cw.hidePlaceholders();
		});	
		this.model.showPlaceholders();
	}
});

var WidgetPlaceholderView = Backbone.View.extend({
	className:'wplaceholder pa',
	events:{
		'click':'pasteWidget',
		'hover':'mouseover',
	},
	template:_.template('+<%= name %>'),
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},
	pasteWidget:function(){
		book.collection.btnsLib.show_pasted_files('image',0,'select_file', this.model.page.model.id, this.model.get('name'));
		media_lib_collection.selected_placeholder = this;
	},
	mouseover: function(event) {
		$(event.currentTarget).toggleClass('hover');
	}
});