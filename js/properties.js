var defaults_properties = [
	{	name: "text-align", 
		description: "Выравнивание текста", 
		values: "right,center,left,justify", 
		type:'text'
	},
	{	name: "font-weight", 
		description: "Жирный шрифт", 
		values: "bold,normal", 
		type:'text'
	},
	{	name: "font-size",
		description: "Размер шрифта",
		values: "10px,12px,14px,16px,18px,20px,22px,24px,26px,28px,32px",
		type:'text'
	},
	{	name: "font-style",
		description: "Начертание шрифта",
		values: "normal,italic,inherit",
		type:'text'
	},
	{	name: "color",
		description: "Цвет текста",
		values: "#fff,#ddd,#ccc,#000,pink,green,orange",
		type:'text'
	},
	{	name: "font-family",
		description: "Шрифт",
		values: "Tahoma,Times New Roman,Arial,Georgia,Verdana,Trebuchet MS,Comic Sans MS,Helvetica",
		type:'text'
	},
	{	name: "text-indent", 
		description: "Абзац", 
		values: "1em,1.5em,2em",
		type:"text"
	},
	{	name: "text-transform",
		description: "Преобразованием в заглавные или прописные",
		value: "capitalize,lowercase,uppercase,none,inherit",
		type:"text"
	},,
	{	name: "border",
		description: "Граница блока",
		value: "1px solid #000,1px solid red",
		type:"text"
	},
	{	name:'path',
		description: "Путь к файлу",
		type:"media"
	},
	{	name:'poster',
		description: "Путь к файлу Постеру",
		type:"media"
	},
	{	name:'approve',
		description: "Утвердить",
		type:"media"
	},
	{	name: "float",
		description: "Обтекание",
		values: "left,right,none",
		type:"block"
	},
	{	name: "background",
		description: "Фон",
		values: "#000,#fff,#66ff00,#3333cc",
		type:"block"
	},
	{	name: "width",
		description: "Ширина",
		values: "50px,100px,200px,400px",
		type:"block"
	},
	{	name: "height",
		description: "Высота",
		values: "50px,100px,200px,400px",
		type:"block"
	},
];


var PropertiesView = Backbone.View.extend({
	className: "properties",
	template: _.template(	'<span class="show_hide_tool_bar"><i class="btn" /><i class="btn" /><i class="btn" /></span>'+
							'<div class="btn-toolbar"></div>'),
	events: {
	  "click .show_hide_tool_bar": "show_hide_tool_bar"
	},
	initialize: function() {
		_.bindAll(this);
		this.render();
	},
	render: function() {
		this.$el.html(this.template());
		this.collection.each(function(model){
			$('.btn-toolbar', this.$el).append(model.view.el);
		},this);

		if (typeof(available_element) != 'undefined'){
			$('.btn-toolbar', this.$el).prepend('<strong class="btn btn-mini disabled">'+available_element.get('title')+'</strong>');
		}
		return this;
	},
	show_hide_tool_bar: function(){
		if (this.$el.height() < 32) {
			this.$el.animate({height: '32px'}, 'fast', function(){
				$('.btn-toolbar', this.$el).show();
			});
		} else {
			$('.btn-toolbar', this.$el).hide();
			this.$el.animate({height: '6px'}, 'fast');
		}
	}
});

var PropertyView = Backbone.View.extend({
	className:"btn-group",
	template:_.template(
		'<button class="btn btn-mini dropdown-toggle" data-toggle="dropdown">' +
			'<i class="hide"></i><span class="curent_prop"><%= name %>:<%= curent_value%></span>' +
			'<span class="caret"></span>' +
		'</button>' +
		'<ul class="dropdown-menu"></ul>'
	),
	events:{
		"click button":"open"
	},
	initialize:function(){
		_.bindAll(this);
		this.model.on('change:curent_value', this.save_change, this);
		this.render();
	},
	save_change:function(){
		this.html_element_style = this.model.html_element.get('style');
		this.html_element_style[this.model.get('name')] = this.model.get('curent_value');
		this.model.html_element.update_style();
	},
    open:function () {
        if ($('button', this.el).is('.open')) {
            $('button', this.el).removeClass('open');
        } else {
            $('button', this.el).addClass('open');
        }
    },
	render:function () {
		this.$el.html(this.template(this.model.toJSON()));
        switch (this.model.get("name")) {
			case 'poster':			
				var fromLib = {
					active:0,
					name:'Выбрать из библиотеки постер',
					value:'Выбрать из библиотеки постер',
					type:'poster',
					html_element:this.model.html_element,
					media_id:this.model.get("media_id")
				};

				var property_value_model = new PropertyValueModel(fromLib);
				$('.dropdown-menu', this.$el).append(property_value_model.view.el);
        		break;
			case 'path':			
				var fromLib = {
					active:0,
					name:'Выбрать из библиотеки',
					value:'Выбрать из библиотеки',
					type:this.model.get("type"),
					html_element:this.model.html_element,
					media_id:this.model.get("media_id")
				};

				var property_value_model = new PropertyValueModel(fromLib);
				$('.dropdown-menu', this.$el).append(property_value_model.view.el);
        		break;

        	case 'approve':
				var ApproveModel = {
					active:0,
					name:'Утвердить дефолтный контент',
					value:this.model.get('curent_value'),
					type:'approve',
					html_element:this.model.html_element,
					media_id:this.model.get("media_id")
				};	

				var property_value_model = new PropertyValueModel(ApproveModel);
				$('.dropdown-menu', this.$el).append(property_value_model.view.el);
        		break;

        	// Paste all variant
        	default:
				if (this.model.has('values') && this.model.get('values').length > 0){
					_.each(this.model.get('values'),function (v){
						var Model = {
							active:(v == this.model.get("curent_value"))?1:0,
							value:v,
							html_element:this.model.html_element,
							property:this.model
						};
						var property_value_model = new PropertyValueModel(Model);
						$('.dropdown-menu', this.$el).append(property_value_model.view.el);
					}, this)
				}
		}


        this.$el.attr({
            'data-name':this.model.get("name"),
            'data-value':this.model.get("value"),
            'title':this.model.get("name") + ':' + this.model.get("curent_value")
        });
        return this;
    },
});

var Property = Backbone.Model.extend({
	defaults: {	curent_value:null },
	initialize:function(attr, html_element){
		this.html_element = html_element || null;
		if (html_element){
			switch(html_element.get('type')){
				case 'illustration':
					var media_id = html_element.get('illustration_id');
					break;
				case 'video':
					var media_id = html_element.get('video_id');
					break;
				case 'audio':
					var media_id = html_element.get('audio_id');
					break;
				case 'inset':
					var media_id = html_element.get('inset_id');
					break;
				default:
					var media_id = null;
					break;
			}
			this.set('media_id',media_id)
			this.view = new PropertyView({model:this});
		} else {
			this.view = new PropertyView({model:this});
			console.log('fatal error, need html_element');
		}
	}
});

var Properties = Backbone.Collection.extend({
	model: Property,
	initialize:function(models,options){ // options.text_model = html_element Model
		if (this.validate(options)){
			book.properties_view?book.properties_view.remove():1;

			// 1. получаем список свойств нашей группы для элемента options.text_model
			if (typeof(options)!='undefined' && typeof(options.text_model)!='undefined'){
				this.text_model = options.text_model;
				this.get_properties();
				
				// 2. пробегаемся по доступным для редактирования свойствам и раставляем текущее значение
				this.set_property_value();
				book.properties_view = new PropertiesView({collection:this});
				$('.dropdown-toggle').dropdown();
				$('.page_container').prepend(book.properties_view.$el);
				book.properties_view.$el.css({height: '32px'});
				
			} else console.log('need options.text_model');
		} else console.log('ошибка определение коллекции Properties')
	},
	validate:function(options){
		if (typeof(options.text_model)!='undefined' && typeof(options.style)!='undefined')
			return true;
		else
			return false;
	},
	set_property_value:function(){
		_.each(this.my_properties, function(prop){
			switch (prop.name){
				case 'poster':
					if (this.text_model.has('poster') && typeof(this.text_model.get('poster'))!='undefined'){
						prop['curent_value'] = this.text_model.get('poster');
					}	
					break;
				case 'path':
					if (this.text_model.has('src') && typeof(this.text_model.get('src'))!='undefined'){
						prop['curent_value'] = this.text_model.get('src');
					}
					break;
				case 'approve':
					if (this.text_model.has('approve') && typeof(this.text_model.get('approve'))!='undefined'){
						prop['curent_value'] = this.text_model.get('approve');
					}
					break;
				default:
					if (this.text_model.has('style') && typeof(this.text_model.get('style'))!='undefined'){
						var text_model_style = this.text_model.get('style');
						if (typeof(text_model_style[prop.name])!='undefined') {
							prop['curent_value'] = text_model_style[prop.name];
						}
					} else console.log('html_element don\'t hav value');
			}

			if (typeof(prop.values)!='undefined' && prop.values.length > 0){
				prop.values = prop.values.split(',');
			}
			
			if (prop.name=='poster'){
				if (this.text_model.get('type')=='video') {
					var property_value_model = new Property(prop, this.text_model);
					this.add(property_value_model);
				}
			} else {
				var property_value_model = new Property(prop, this.text_model);
				this.add(property_value_model);
			}

		},this)
	},
	get_properties:function(){
		this.element_type = this.text_model.get('element_type'),
		this.my_properties = [];
		if (typeof(this.element_type)!='undefined'){
			_.each(defaults_properties, function(property){
				if (property.type == this.element_type.get('properties_group')) {
					this.my_properties.push(_.clone(property));
				}
			}, this);
		} else console.log('I need prop');
	},
	hide:function(){
		if (typeof(book.properties_view)!='undefined')	book.properties_view.remove();
	}
});

var PropertyValueModel = Backbone.Model.extend({
	defaults:{ active:0 },
	initialize:function(){
		switch (this.get('type')) {
			case 'media':
				this.view = new PropertyLibView({model:this})
				break;
			case 'poster':
				this.view = new PropertyPosterView({model:this})
				break;
			case 'approve':
				this.view = new PropertyApproveView({model:this})
				break;
			default:
				this.view = new PropertyValueView({model:this})
		}
	}
});

var PropertyValueView = Backbone.View.extend({
	className: "property-value",
	tagName: "li",
	template: _.template('<a data-value="<%= value %>"><%= value %></a>'),
	template_prev_img: _.template('<a data-value="<%= value %>"><img src="<%= value %>" width="32" />&nbsp;<%= value %></a>'),
 	events: {
 		"click": "set_new_property"
 	},
	initialize: function() {
		this.render();
	},
	render: function(){
	  	this.$el.html(this.template(this.model.toJSON()));
	  	if (this.model.get("active")) 
	  		this.$el.addClass("active");
	  	return this;
	},
	set_new_property:function(){
		this.model.get('property').set({
			curent_value: this.model.get('value')
		});
	}
});

var PropertyLibView = PropertyValueView.extend({
 	events: {
 		"click": "show_lib"
    },
	initialize: function() {
		this.render();
	},
	render: function(){
		var content = this.template(this.model.toJSON());
	  	this.$el.html(content);
	  	if (this.model.get("active")) 
	  		this.$el.addClass("active");

	  	return this;
	},
	show_lib: function(){
		book.collection.btnsLib.show_files(this.model.get('html_element').get('type'),0,'select_file',this.model.get('html_element'),'src');
	}
});

var PropertyPosterView = PropertyLibView.extend({
	show_lib: function(){
		book.collection.btnsLib.show_files('image',0,'select_file',this.model.get('html_element'),'poster');
	}
})

var PropertyApproveView = PropertyValueView.extend({
	template: _.template('<a><label class="checkbox"><input <%=checked %> class="approve" type="checkbox"> Утвердить</label></a>'),
	initialize: function() {
		this.model.set('checked',(this.model.get('value') == 1)?'checked':'');
		this.render();
	},
	set_new_property:function(){
		this.input = $('input',this.$el);
		if (this.model.has('html_element') && typeof(this.model.get('html_element'))!='undefined'){
			this.html_element = this.model.get('html_element');
			if (this.input.is(':checked')){
				this.html_element.set('approve',1);
			} else {
				this.html_element.set('approve',0);
			}
		} else {
			console.log('don\'t find html_element');
		}
	}
});

var PropertyDivscriptView = Backbone.View.extend({
    className:"btn-divscript-group",
    template:_.template('<button class="btn dropdown-toggle <%= script_name %>_<%= container_id %>" data-toggle="dropdown">' +
        '<i class="hide"></i><span class="curent_prop"><%= name %></span>' +
        '</button>'),
    events:{
        "click button":function(){
            if (this.model.get('script_name') == 'change'){
                var changeScript = new ChangeScriptView({model:this.model});
            }else{
                dispatcher_scripts(
                	this.model.get('script_name'), 
                	this.model.get('script_id'), 
                	this.model.get('container'), 
                	this.model.get('pos'), 
                	this.model.get('data')
                );
            }
        }
    },
    render:function () {
        var content = this.template(this.model.toJSON());
        this.$el.html(content);
        this.$el.addClass('divscript_button_'+this.model.get("script_name"));
        return this;
    }
});

var PropertiesDivscriptView = PropertiesView.extend({
    renderAll: function() {
        var jqo = this.$el;
        this.collection.each(function(model){
            var divscript_view = new PropertyDivscriptView({model:model});
            $('.btn-toolbar', jqo).append(divscript_view.render().el);
        });
    }
});