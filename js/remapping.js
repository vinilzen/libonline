// контейнер для кнопок переназначения
var RemappingMenu = Backbone.View.extend({
  className: 'btn-group',
  render:function(){

    this.$el.html('<button class="btn btn-mini">Преобразовать в:</button>'+
    '<button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>'+
    '<ul class="dropdown-menu"></ul>');

    available_element_type = _.find(available_elements_type.models, function(model){
      return model.get('name') == this.model.get('from');
    }, this);

    var remapping_menu_model = this.model
    _.each(available_element_type.get('remmapingTo'), function(to){
      
      available_element_type = _.find(available_elements_type.models, function(model){
        return model.get('name') == to;
      });

      var attributes = remapping_menu_model.toJSON();
      attributes['to'] = to;

      if (typeof(available_element_type) != 'undefined' && available_element_type.has('title')){
        attributes['title'] = available_element_type.get('title');
      } else {
        console.log(available_element_type, to);
      }

      var remapping_to = new RemappingTo(attributes);
      $('.dropdown-menu', this.$el).append(remapping_to.el);
    },this);
    return this;
  }
});

var RemappingMenuModel = Backbone.Model.extend({
  initialize:function(){
    this.view = new RemappingMenu({model:this});
    this.view.render();
  }
});

// кнопка для клика-переназначения в новый тип
var RemappingTo = Backbone.View.extend({
  tagName: 'li',
  events: {
    'click a':'remapping'
  },
  initialize:function(options){
    this.options = options;
    this.render();
  },
  render: function(){
    this.$el.html('<a href="#">'+this.options.title+' ('+this.options.to+')'+'</a>');
    return this;
  },
  remapping: function(){
    //console.log(this.options);
    this.savebook();
    /*
    switch (this.options.from){
      case 'name':
      case 'author':
        this.remapping_author();
        break;
      case 'text':
        this.remapping_text();
        break;
      case 'title':
        this.remapping_title();
        break
    }*/
  },

  // ONE method for author and name !!!
  remapping_author: function(){
    var from = this.options.from; // string  name|author
        to = this.options.to;  // string  name|author|text

    switch(to){
      case 'text':

        // получим автора|название (для координат)
        element = _.find(book.elements.models, function(model){
          return model.get('type') == from;
        });

        if (element){

          // проверим не находится ли наш автор или название внутри параграфа
          var inside = _.find(book.elements.models, function(model){
            return model.get('type') == 'paragraph' && model.get('pos_start') <= element.get('pos_start') &&  model.get('pos_end') >= element.get('pos_start');
          });

          // удалим из коллекции элементов (текст отобразится внутри параграфа)
          if (typeof(inside) != 'undefined') {

            element.set('delete', true);

          } else {
            _.each(book.elements.models, function(element){
              if (element.get('type') == from){
                element.set('type', 'paragraph');
              }
            });
          }
        } else {
          console.log(element, this);
          alert('Fatal error');
        }
        break;

      case 'name':
      case 'author':
        // поменяем местами автора и название
        var element_from = _.find(book.elements.models, function(element){
          return element.get('type') == from;
        });
        var element_to = _.find(book.elements.models, function(element){
          return element.get('type') == to;
        });
        if (element_from){
          element_from.set('type', to);
        }
        if (element_to){
          element_to.set('type', from);
        }
        break;
    }

    this.savebook(this.model.get('text'), to, from);
  },

  remapping_title: function(){

    var from = this.model.get('from'),
        to = this.model.get('to'),
        element_id = this.model.get('p_id'),
        pos_start = this.model.get('start'),
        pos_end = this.model.get('end');


    switch(this.model.get('to')){
      case 'text':
        // элемент текущего параграфа
        old_element = _.find(book.elements.models, function(element){
          return element.id == element_id;
        });

        //старое название/автора сделаем простым текстом
        if (old_element){
          // проверим не находится ли наш заголовок внутри параграфа
          var inside = _.find(book.elements.models, function(model){
            return  model.get('type') == 'paragraph' && 
                    model.get('pos_start') <= old_element.get('pos_start') &&
                    model.get('pos_end') >= old_element.get('pos_start');
          });

          // удалим из коллекции элементов (текст отобразится внутри параграфа)
          if (typeof(inside) != 'undefined') {
            old_element.set('delete', true);
          } else {
            old_element.set('type', 'paragraph');
          }
        } else {
          console.log('no element', to);
        }

      break;
      case 'name':
      case 'author':

        //старое название/автора сделаем простым текстом
        var old_element = _.find(book.elements.models, function(element){
          return element.get('type') == to;
        });

        if (old_element){
          // проверим не находится ли наш автор или название внутри параграфа
          var inside = _.find(book.elements.models, function(model){
            return  model.get('type') == 'paragraph' && 
                    model.get('pos_start') <= old_element.get('pos_start') &&
                    model.get('pos_end') >= old_element.get('pos_start');
          });

          // удалим из коллекции элементов (текст отобразится внутри параграфа)
          if (typeof(inside) != 'undefined') {
            old_element.set('delete', true);
          } else {
            old_element.set('type', to);
          }
        } else {
          console.log('no element', to);
        }

        data[this.model.get('to')] = this.model.get('text');
        book.set(this.model.get('to'), this.model.get('text'));
      break;
      default:
        alert('Не корректный тип');
    }

    this.savebook(this.model.get('text'), this.model.get('to'), this.model.get('from'));
  },

  remapping_text: function(){
    if (this.model.get('text') != '') {

      var from = this.model.get('from'),
          to = this.model.get('to'),
          element_id = this.model.get('p_id'),
          pos_start = this.model.get('start'),
          pos_end = this.model.get('end');
     
      // элемент текущего параграфа
      element = _.find(book.elements.models, function(element){
        return element.id == element_id;
      });

      //console.log(this.model.attributes, pos_start, pos_end, element);

      switch(to){
        case 'title':

          if (pos_start == 0 && pos_end == this.model.get('text').length){
            element.set('type', to);
          } else {
            var new_element = new Backbone.Model({
              type:to,
              pos_start:pos_start+this.model.get('pos')+1,
              pos_end:pos_end+this.model.get('pos'),
              typeLvl:1
            });
            console.log('set new title', new_element);
            book.elements.add(new_element);
          }
        break;
        case 'name':
        case 'author':
          
          //старое название/автора сделаем простым текстом
          var old_element = _.find(book.elements.models, function(element){
            return element.get('type') == to;
          });

          if (old_element){
            // проверим не находится ли наш автор или название внутри параграфа
            var inside = _.find(book.elements.models, function(model){
              return  model.get('type') == 'paragraph' && 
                      model.get('pos_start') <= old_element.get('pos_start') &&
                      model.get('pos_end') >= old_element.get('pos_start');
            });

            // удалим из коллекции элементов (текст отобразится внутри параграфа)
            if (typeof(inside) != 'undefined') {
              old_element.set('delete', true);
            } else {
              _.each(book.elements.models, function(element){
                if (element.get('type') == to){
                  element.set('type', 'paragraph');
                }
              });
            }
          } else {
            //console.log('no element', to);
          }

          //установим нового автора/название
          book.set(to, this.model.get('text'));

          if (pos_start == 0 && pos_end == this.model.get('text').length){
            element.set('type', to);
          } else {
            var new_element = new Backbone.Model({
              type:to,
              pos_start:pos_start+this.model.get('pos')+1,
              pos_end:pos_end+this.model.get('pos')
            });
            console.log('set new author|name', new_element);
            book.elements.add(new_element);
          }
        break;
        default:
          alert('Не корректный тип');
      }

      this.savebook(this.model.get('text'), to, from);

    } else {
      console.log('ничего не выделено?');
    }
  },

  savebook:function(){
    var THIS = this;

    var save_element = {
      id:this.options.html_element_id,
      pos_start:this.options.pos_start,
      pos_end:this.options.pos_end,
      type:this.options.to
    },
    data = {
      elements:JSON.stringify(save_element)
    };

    console.log(save_element, data);
/*
    $.post(
      "/books/saveBook/?book_uid="+book.id,
      {elements:JSON.stringify(book.elements.models), name:book.name, author:book.author},
      function(response) {
        $('.page_container').popover('destroy');
        $('.popover').remove();

        if (response.success && response.success == 1){
          THIS.reassign(text, to, from);
          book.fetch({success:function(model,response){

          }});
        } else {
          $('#myProgress').modal('hide');
          alert('error update BOOK');
        }
      }, 'json')
    .error(function(msg){
      console.log(msg);
      $('.page_container').popover('destroy');
      $('.popover').remove();
      alert('Ошибка на сервере');
    });
    */
  },

  // сохраним для статистики
  reassign:function(text, to, from){
    if (to == 'title' || to == 'author' || to == 'name'){
        $.ajax({
          type: "POST",
          url: "/reassign/",
          data:{reassign:'[{"phrase":"'+text+'", "type":"'+to+'"}]'},
          dataType: "json"
        });
    }

    if (from == 'title' || from == 'author' || from == 'name'){
        $.ajax({
          type: "POST",
          url: "/reassign/",
          data: {reassign:'[{"phrase":"'+text+'", "type":"'+from+'"}]'},
          dataType: "json"
        });
    }
  },

  // array with two halves
  new_content:function(start, end){
    var html_text_elements = _.filter(book.html_elements.models, function(model){
      return model.get('type') == 'text'; 
    }); // только параграфы

    var before = new Backbone.Collection(
      _.filter(html_text_elements, function(model){
        return model.id < start;
      })
    );
    var before_content = before.pluck("content").join('#');

    var after = new Backbone.Collection(
      _.filter(html_text_elements, function(model){
        return model.id > end; 
      })
    );
    var after_content = after.pluck("content").join('#');

    return {before:before_content, after:after_content};
  }
});


/*
 * Подвинем все виджеты на step
 * начиная с start (с позиции в тексте), если не указать то подвинет все виджеты
 */
function shift_widget(step, start){
  var widgets = book.get('widgets');
  
  if (widgets.length > 0) {
    for (var i=0; i< widgets.length; i++){
      if (typeof(start) != 'undefined'){
        if (widgets[i].pos > start){
          widgets[i].pos = parseInt(widgets[i].pos)+step;
        }
      } else {
        widgets[i].pos = parseInt(widgets[i].pos)+step;
      }
    }
  }
}