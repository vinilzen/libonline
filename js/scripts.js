var Scripts = Backbone.Model.extend({
    url: function() {
        return "/scripts/getScriptsPublic/";
    },

    initialize: function(){
        this.first_load = true;
        this.fetch();
        this.on('change', function(){
            this.show_names();
        })
    },

    show_names: function(){
        var scripts = {
            'parent':this,
            'title':'Available Scripts',
            'names':this.attributes
        }
        new ScriptsView({model:scripts});
    }
});

var ScriptsView = Backbone.View.extend({
    tagName: "div",
    className: "admin_container",

    initialize: function(){
        this.message = new ShowFiles();
        this.render();
    },

    render: function(){
        $('#admin_scripts').html('');

        this.scripts_table = $('<table></table>').attr({
            'id':'scripts_table',
            'class':'admin_table'
        });
        this.countId = 1;
        var title = this.model.title;
        var this_model = this;
        var last_id = 0;

        _.each(this.model.names, function(value){
            this_model.render_new_row_scripts(
                value.id,
                value.script_name,
                this_model.render_ta_scripts('demo', value.id, 'Demo Script', value.script_demo),
                this_model.render_ta_scripts('conf', value.id, 'Configuration Script', value.script_conf)
            );
            last_id = value.id;
            this_model.countId++;
        });

        this.render_new_row_scripts(
            'new',
            '+Add New Scripts',
            this_model.render_ta_scripts('demo', 'new', 'Demo Script', ''),
            this_model.render_ta_scripts('conf', 'new', 'Configuration Script', '')
        );

        $('#admin_scripts').append(this.el);
        this.$el.append($('<h3></h3>').html(title));
        this.$el.append(this.scripts_table);
        this.$el.append($('<div></div>').css({
            'width': '100%',
            'height': '25px'
        }));

        if(!this.model.parent.first_load){
            $('#scripts_code_row_'+last_id).show();
            this.message.showAlert('Save complete');
        }
        this.model.parent.first_load = false;
    },

    save_script: function(id, type_script, script){
        var this_model = this;
        if(id == 'new'){
            var script_name = $('#val_name_new').val() ? $('#val_name_new').val() : $('#val_name_new').attr('placeholder');
            var demo_script_new = $('#demo_script_new').val();
            var conf_script_new = $('#conf_script_new').val();

            $.post('/scripts/insertScripts/',{
                'script_name':script_name,
                'script_demo':demo_script_new,
                'script_conf':conf_script_new
            }, function(data){
                if(data.success) this_model.model.parent.fetch();
                else if(data.error) this_model.message.showAlert(data.error);
            });
        }else{
            $.post('/scripts/saveCurrentScript/',{'script_field':type_script, 'value':script, 'id':id}, function(data){
                if(data.success) this_model.message.showAlert('Save complete');
                else if(data.error) this_model.message.showAlert(data.error);
            });
        }
    },

    render_new_row_scripts: function(id, script_name, demo_script, conf_script){
        var this_model = this;
        var admin_id = $('<td></td>').attr('class','admin_id');
        var admin_cell = $('<td></td>').attr('class','admin_text');
        var admin_aux = $('<td></td>').attr('class','script_del_cell');
        var row_name = $('<tr></tr>').attr({
            'id':'scripts_name_row_'+id,
            'class':'list'
        });

        var call_back = function(e){
            var id = e.data.id;
            var container = $('#'+id);

            $('.scripts_code_row').hide('fast');
            if(container.css('display') == 'none') container.show('fast');
        }

        admin_cell.bind('click', {id:'scripts_code_row_'+id}, call_back);
        this.scripts_table.append(row_name.append(admin_id).append(admin_cell).append(admin_aux));

        var row_scripts = $('<tr></tr>').attr({
            'id':'scripts_code_row_'+id,
            'class':'scripts_code_row'
        }).css('display','none');

        var admin_scripts_cell = $('<td></td>').attr('colspan', 3);
        var name_of_script = $('<div></div>').attr('class','script_name');
        var save_button_name = $('<button></button>').bind('click', {'id':id}, function(e){
            var input = $('#val_name_' + e.data.id);
            if(e.data.id != 'new') admin_cell.html(input.val());
            this_model.save_script(e.data.id, 'script_name', input.val());
        }).attr({
            'id':'save_name_'+id,
            'class':'script_button'
        }).html('Save');

        var input_name = $('<input>').attr('id','val_name_'+id);
        name_of_script.append(input_name).append(save_button_name);

        this.scripts_table.append(
            row_scripts.append(
                admin_scripts_cell
                    .append($('<h4></h4>').html('Name of Script'))
                    .append(name_of_script)
                    .append(demo_script)
                    .append(conf_script)
            )
        );

        if(id != 'new'){
            admin_id.html(this.countId);
            admin_cell.html(script_name);
            input_name.val(script_name);
            var delete_button = $('<button></button>').bind('click', {'id':id}, function(e){
                this_model.confirmDelete(e.data.id, script_name);
            }).css('margin','0px').attr({
                'id':'delete_'+id,
                'class':'script_button'
            }).html('Delete');
            admin_aux.append(delete_button);
        }else{
            row_name.css({
                'background':'#D2CCC2'
            });
            admin_aux.css({
                'font-size':'13px',
                'font-weight':'bold',
                'color':'gray'
            });
            admin_aux.html(script_name);
            admin_aux.bind('click', {id:'scripts_code_row_'+id}, call_back);
            input_name.attr('placeholder','Unknown Script');
        }
    },

    render_ta_scripts: function(type, id, title, script){
        var this_model = this;
        var save_button = $('<button></button>').attr({
            'id':'save_'+type+'_'+id,
            'class':'script_button'
        }).html('Save');

        save_button.bind('click', {id:id}, function(e){
            var id = e.data.id;
            var script = $('#'+type+'_script_'+id).val();
            this_model.save_script(id, 'script_demo', script);
        });

        var current_script = $('<div></div>').append($('<h4></h4>').html(title)).append($('<textarea></textarea>').attr({
            'id':type+'_script_'+id,
            'class':'script_container'
        }).html(script)).append(save_button);

        return current_script;
    },

    confirmDelete: function(id, name){
        var this_model = this;
        var but_cont = $('<div></div>').attr('id','subButtonsCont');
        var yes = $('<span></span>').attr('class','subButton').html('[Да]');
        var no = $('<span></span>').attr('class','subButton').html('[Нет]');

        no.click(function(){$('#blackShadow').remove();});
        yes.click(function(){
            $.post('/scripts/deleteScripts/',{id: id}, function(data){
                if(data.success){
                    $('#blackShadow').remove();
                    $('#scripts_name_row_'+id).remove();
                    $('#scripts_code_row_'+id).remove();
                    var id_cells = $('.list .admin_id');
                    id_cells.each(function(key){
                        if(key != id_cells.length - 1) $(this).html(key+1);
                    });
                }
                else if(data.error) this_model.message.showAlert(data.error);
            });
        });
        but_cont.append(yes).append(no);
        var content = $('<div></div>').css({'width':'100%','padding-top':'25px'}).html('Вы действительно хотите удалить скрипты<br> '+'"'+name+'"?').append(but_cont);
        this.message.showAlert(content);
    }
});

var ChangeScriptView = Backbone.View.extend({
    tagName:"button",
    className: "btn saveButton",

    initialize: function(){
        this.render();
    },

    render: function(){
        var my_model = this;
        var pos = this.model.get('pos');
        var table_scripts = $('<table></table>').css('width','inherit').attr('class','tableScripts');
        var container = this.model.get('container');
//        var main_container = $(container)[1];
        var base_class = container.replace('.','');
        _.each(book.get('curent_style').get('scripts'), function(script,count){
            var row = $('<tr></tr>').css('border','2px solid white').attr('class',base_class+'_'+count);
            var cells_id = $('<td></td>').css({'text-align':'center'}).attr('class','id_script').html(script.id);
            var cell_name = $('<td></td>').html(script.script_name);
            var cell_check = $('<td></td>').css({'text-align':'center','font-style':'italic','font-weight':'normal'}).attr('class','check_cell');

            if(my_model.model.get('script_id') == script.id){
                row.css('background-color','red');
                cell_check.html('active');
            }
            row.append(cells_id);
            row.append(cell_name);
            row.append(cell_check);
            table_scripts.append(row);
        });
        $(container).html('');
        $(container).append(table_scripts);

        var btn = this.$el.append(
            $('<span></span>').attr('class','curent_prop').html('Save')
        );

        $(container).append(btn);
        btn.css('top', parseInt(btn.parent().css('height'))+5);
        btn.hide();

        var new_script_id;
        $(container).find('tr').click(function(){
            $(container).find('tr').css('background-color','');
            $(container).find('.check_cell').html('');

            var current_class = $(this).attr('class');
            $('.'+current_class).css('background-color','red');
            $('.'+current_class+' .check_cell').html('active');

            var selected_id = parseInt($(this).find('.id_script').html());

            if(selected_id != my_model.model.get('script_id')){
                new_script_id = selected_id;
                btn.show('fast');
            }
            else btn.hide('fast');
        });

        btn.click(function(){
            changeScriptData(new_script_id, pos, {});
            dispatcher_scripts('demo', new_script_id, container, pos, {});

            $.post('/scripts/saveScripts/',{
                book_uid: book.id,
                theme_uid: book.selected_theme.id,
                id: new_script_id,
                pos: pos
            });
        });
    }
});