/*
 * Search in text with select element type
 */
var SearchResultView = Backbone.View.extend({
  tagName:'p',
  events:{
    'click .gotopage':'gotopage'
  },
  className:'one_result',
  template:_.template('<span><%= c %><span> - <em><%= type %></em> (на странице <strong class="gotopage"><%= page %></strong>)'),
  initialize:function(){
    _.bindAll(this);
    this.render();
  },
  render:function(){

    this.page = book.html_elements._byId[this.model.id].get('container');
    this.q = this.model.collection.view.form.q;
    
    this.page.model.mark();

    var c = this.model.get('content').split(this.q).join('<strong>'+this.q+'</strong>');
    var content = this.template({
      c:c,
      type:this.model.get('type'),
      page:this.page.model.id
    });

    this.$el.html(content);
    return this;
  },
  gotopage:function(){
    this.page.model.gotopage();
  }
});

var SearchResultModel = Backbone.Model.extend({
/*  initialize:function(){
    this.view = new SearchResultView({model:this});
  }*/
});

var SearchResultsCollection = Backbone.Collection.extend({
  model:SearchResultModel,
  initialize:function(){
    _.bindAll(this);
  },
  marked_result:function(){

    // hide all page preview

    book.pages.each(function(page){
      page.preview.$el.hide();
    });

    this.each(function(m){
      var html_element = book.html_elements._byId[m.id];
      if (html_element){
        html_element.mark_search_result(m);
        html_element.get('container').model.preview.$el.show();
      }
    },this);
  },
  demarked:function(){

    // show all page preview
    book.pages.each(function(page){
      page.preview.$el.show();
    });

    this.each(function(m){
      var html_element = book.html_elements._byId[m.id];
      if (html_element){
        html_element.demark_search_result(m);
      }
    },this);
  }
});


var SearchResultsView = Backbone.View.extend({
  className:'search_result',
  template:_.template('<em class="close_results">Закрыть</em>'+
                      '<em class="collaps_result"></em>'+
                      '<div class="list"></div>'),
  events:{
    'click .close_results':'close',
    'click .collaps_result':'collaps'
  },
  initialize:function(options){
    this.form = options.form;
    this.positions = options.positions;
    this.result = new SearchResultsCollection();
    this.check_html_element();
/*  this.collapse = 0;
    this.result.view = this;
    this.render();
    $('body').append(this.el);*/
  },
  render:function(){
    this.$el.html(this.template());
    return this;
  },
  check_html_element:function(){
    var type_filter = this.type_filter = $('select',this.form.$el).val()=='paragraph'?'text':$('select',this.form.$el).val();
    
    _.each(this.positions, function(position){
      var r = _.find(book.html_elements.models, function(m){
        if (type_filter != '0' && type_filter != 0){
          return m.get('pos_start')<=position && m.get('pos_end')>=position && m.get('type') == type_filter;
        } else {
          return m.get('pos_start')<=position && m.get('pos_end')>=position;
        }
      });

      if (typeof(r)!='undefined'){
        this.result.add({
          content:r.get('content'),
          type:r.get('type'),
          id:r.id,
          q:this.form.q
        });
      } // else {         console.log('position ',position,' empty');       }
    }, this);

    if (this.result.length>0){
      this.result.marked_result();
    }
  },
  close:function(){
    this.result.each(function(m){
      if (m.view && m.view.page && m.view.page.model){
        m.view.page.model.de_mark();
        m.view.page.model.collection.show_all_preview();
      }
    });
    $('input', this.form.$el).val('');
    this.remove();
  },
  collaps:function(){
    if (this.collapse == 0){
      this.$el.animate({width: "10px"});
      this.collapse = 1;
    } else {
      this.$el.animate({width: "50%"});
    }
  }
});

var SearchForm = Backbone.View.extend({
  tagName:'form',
  className:'navbar-form pull-right search',
  template:_.template('<input name="q" class="span5" type="text" placeholder="Текст для поиска" >'+
                      '<select class="span5"></select>'+
                      '<span type="submit" class="btn btn-mini btn-primary"><i class="icon-search icon-white"></i></span>'+
                      '<strong type="submit" class="btn btn-mini btn-primary"><i class="icon-remove icon-white"></i></strong>'
  ),
  events:{
    'click span':'start_search',
    'click strong':'reset_result',
    'keypress input':'start_search_enter'
  },
  initialize:function(){
    this.book = book;
    this.render();
    $('.lib_navbar').append(this.el);
    this.select = $('select', this.$el);
    this.select.append('<option value="0">искать по всем типам</option>')
    available_elements_type.each(function(element_type){
      if (element_type.get('name')!='page'){
        this.select.append('<option value="'+element_type.get('name')+'">'+element_type.get('title')+'</option>')
      }
    }, this)
  },
  render:function(){
    this.$el.html(this.template());
    this.$el.submit(function(){ return false; })
    return this;
  },
  start_search_enter:function(e){
    if (e.keyCode == 13){
      this.start_search()
    }
  },
  start_search:function(e){

    if ($('input', this.$el).val()!='' && $('input', this.$el).val().length > 2){
      this.q = $('input', this.$el).val();

      if (typeof(this.result_view)!='undefined'){
        this.result_view.close();
      }

      this.search();

      if (this.positions.length>0){
        /*if (typeof(this.result_view)!='undefined'){
          this.result_view.remove()
        }*/

        // remove old marked phrase
        if (typeof(this.result_view)!='undefined' && typeof(this.result_view.result)!='undefined'){
          this.result_view.result.demarked();
        }
        this.result_view = new SearchResultsView({
          positions:this.positions,
          form:this
        });
      }
    } else {
      alert('Строка для поиска должна содержать как минимум 3 символа');
    }
    return false;
  },
  search:function(){
    if(book.get('content').indexOf(this.q)>-1){
      this.positions=[book.get('content').indexOf(this.q)];
      this.count = book.get('content').split(this.q).length - 1;
      if (this.count>1){
        this.search_next();
      }
    } else {
      alert('Ничего не найдено');
      this.positions = [];
      this.count = 0;
    }
  },
  search_next:function(){
    var r = book.get('content').indexOf(this.q,  _.last(this.positions)+1);
    if (r >-1){
      this.positions.push(r);
      if (this.positions.length<this.count){
        this.search_next();
      }
    }
  },
  reset_result:function(){
    $('input', this.$el).val('');
    if (typeof(this.result_view)!='undefined' && typeof(this.result_view.result)!='undefined'){
      this.result_view.result.demarked();
    }    
  }
});

var Search = Backbone.Model.extend({
  initialize:function(){
    this.form_view =  new SearchForm()
  }
});
