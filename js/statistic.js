var Statistic = Backbone.Model.extend({
    url: function() {
        return "/books/getReassignStatistic/";
    },

    initialize: function(){
        this.fetch();
        this.on('change', function(){
            this.stat();
        })
    },

    stat: function(){
        var stat = {
            'title':'Reassignment Phrases',
            'title_stat':'phrases',
            'stat':this.attributes
        }
        new StatView({model:stat});
    }
});

var StatView = Backbone.View.extend({
    tagName: "div",
    className: "admin_container",

    initialize: function(){
        this.render();
    },

    render: function(){
        var stat_model = this.model.stat;
        var title_stat = this.model.title_stat;
        var title = this.model.title;

        var phrases_table = $('<table></table>').attr({
            'id':'stat_'+title_stat,
            'class':'admin_table'
        });

        var countId = 1;
        _.each(stat_model, function(value){
            var stat_id = $('<td></td>').attr('class','admin_id').html(countId);
            var stat_cell = $('<td></td>').attr('class','admin').html('«'+value.phrase+'»');
            var row = $('<tr></tr>').attr({
                'id':'phrase_row_'+value.id,
                'class':'list'
            });

            row.bind('click', {id:'stat_score_'+title_stat+'_'+value.id}, function(e){
                var id = e.data.id;
                var container = $('#'+id);

                $('.stat_score').hide('fast');
                if(container.css('display') == 'none') container.show('fast');
            });

            phrases_table.append(row.append(stat_id).append(stat_cell));

            var stat_row = {
                row_obj: stat_cell,
                title_stat: title_stat,
                score: value
            }
            new StatTableView({model:stat_row});
            countId++;
        });

        $('#admin_stat').append(this.el);
        this.$el.append($('<h3></h3>').html(title));
        this.$el.append(phrases_table);
    }
});

var StatTableView = Backbone.View.extend({
    initialize: function(){
        this.stat_score = $('<div></div>').attr({
            'id':'stat_score_'+this.model.title_stat+'_'+this.model.score.id,
            'class':'stat_score'
        }).css('display','none');

        this.render();
    },

    render: function(){
        var closeBtn = $('<div></div>').attr('id','closeButton').html('[X]').css({
            'font-style':'normal',
            'font-weight':'bold',
            'margin-left':'280px',
            'margin-top':'0px'
        });
        closeBtn.click(function(){$('.stat_score').hide('fast');});
        this.stat_score.append(closeBtn);

        this.stat_score.append($('<div></div>').attr('class','title_score').html('Turn to:'));
        this.renderBars(this.model.score.turn_to, 'score_bar_turn');

        this.stat_score.append($('<div></div>').attr('class','title_score').html('Was:'));
        this.renderBars(this.model.score.was_it, 'score_bar_was');

        this.stat_score.append($('<div></div>').css({
            'width': '100%',
            'height': '25px'
        }));
        this.model.row_obj.append(this.stat_score);
    },

    renderBars: function(score_obj, bar_class){
        var stat_score = this.stat_score;
        var max_val = 0;
        var max_width = 190;

        _.each(score_obj, function(turn){
            if(turn.score > max_width && turn.score > max_val) max_val = turn.score;
        });

        _.each(score_obj, function(turn){
            var title = turn.turn ? turn.turn : turn.was;
            stat_score.append($('<div></div>').attr('class','title_turn').html(title));

            var score_container_bar = $('<div></div>').attr('class','score_container');
            var score_width = max_val ? (turn.score * max_width)/max_val : turn.score;

            score_container_bar.append($('<div></div>').attr('class',bar_class).css('width', score_width));
            score_container_bar.append($('<span></span>').attr('class','score_bar_number').html(turn.score));

            stat_score.append(score_container_bar);
            stat_score.append($('<div></div>').attr('class','score_bar_positions').html(
                '<b>Position start:</b> '+turn.start_pos+'; <b>Position end:</b> '+turn.end_pos
            ));
        });
    }
});

var StatisticBooks = BaseModel.extend({
    url: function() {
        return "/books/getBooksStatistic/";
    },

    initialize: function(){
        this.fog();
        this.fetch();
        this.on('change', function(){
            this.stat();
        })
    },

    stat: function(){
        var stat = {
            'title':'Books Statistics',
            'title_stat':'books',
            'stat':this.attributes
        }
        new StatBooksView({model:stat});
    }
});


var StatBooksView = BaseView.extend({
    tagName: "div",
    className: "admin_container",

    initialize: function(){
        this.render();
    },

    render: function(){
        var stat_model = this.model.stat;
        var title_stat = this.model.title_stat;
        var title = this.model.title;

        var stat_table = $('<table></table>').attr({
            'id':'stat_'+title_stat,
            'class':'admin_table'
        });

        var title_stat_id = $('<td></td>').attr('class','stat_id').html('Book UID');
        var title_stat_name = $('<td></td>').attr('class','admin').html('Book Name');
        var title_stat_author = $('<td></td>').attr('class','admin').html('Book Author');
        var title_row = $('<tr></tr>').attr('class','title_list');
        stat_table.append(title_row.append(title_stat_id).append(title_stat_name).append(title_stat_author));

        _.each(stat_model, function(value){
            var stat_id = $('<td></td>').attr('class','stat_id').html(value.book_uid);
            var stat_name = $('<td></td>').attr('class','admin').html(value.name);
            var stat_author = $('<td></td>').attr('class','admin').html(value.author);

            var row = $('<tr></tr>').attr({
                'id':'phrase_row_'+value.id,
                'class':'list'
            });

            row.bind('click', {id:'stat_score_'+title_stat+'_'+value.book_uid}, function(e){
                var id = e.data.id;
                var container = $('#'+id);

                if(container.css('display') == 'none'){
                    $('.stat_score').hide('fast');
                    container.show('fast');
                }
            });

            stat_table.append(row.append(stat_id).append(stat_name).append(stat_author));

            var stat_row = {
                id:value.book_uid,
                row_obj: stat_name,
                title_stat: title_stat,
                statistic: value.statistic
            }
            new StatBookTableView({model:stat_row});
        });

        $('#admin_books_stat').html('');
        $('#admin_books_stat').append(this.el);
        this.$el.append($('<h3></h3>').html(title));
        this.$el.append(stat_table);

        this.removeFog();

        var currentModel = this;
        _.each(this.model.stat, function(book){
            currentModel.showPage(1, book.book_uid);
        });
    }
});

var StatBookTableView = BaseView.extend({
    initialize: function(){
        stat_score = $('<div></div>').attr({
            'id':'stat_score_'+this.model.title_stat+'_'+this.model.id,
            'class':'stat_score'
        }).css('display','none');

        this.render();
    },

    render: function(){
        var closeBtn = $('<div></div>').attr('id','closeButton').html('[X]').css({
            'font-style':'normal',
            'font-weight':'bold',
            'margin-left':'280px',
            'margin-top':'0px'
        });
        closeBtn.click(function(){$('.stat_score').hide('fast');});
        stat_score.append(closeBtn);
        stat_score.append($('<div></div>').attr('class','title_score').html('Elements'));

        var max_val = 0;
        var max_width = 190;
        var max_sum_width = 40;
        _.each(this.model.statistic, function(statistic){
            var phrase_length = statistic.end_pos - statistic.start_pos;
            if(phrase_length > max_width && phrase_length > max_val) max_val = phrase_length;
        });

        var count = 0;
        var totalPages = 1;
        var current_model = this;
        var score_page = this.addPage(totalPages, current_model.model.id);

        _.each(this.model.statistic, function(statistic,key){
            if(count > 5){
                stat_score.append(score_page);
                totalPages++;
                score_page = current_model.addPage(totalPages, current_model.model.id);
                count = 0;
            }
            count++;
            var score_container_bar = $('<div></div>').attr('class','score_container');

            var phrase_length = statistic.end_pos - statistic.start_pos;
            var width = max_val ? Math.round((phrase_length * max_width)/max_val-10) : phrase_length;
            var sum_width = phrase_length + width;
            var content = statistic.content;
            var full_cont_id = 'full_content_'+(key+1);

            var score_bar = $('<div></div>').attr('class','score_bar_turn').css('width', width);
            var score_type = $('<span></span>').attr('class','score_bar_number').html('<b>Type: </b>' + statistic.type);
            var full_content = $('<div></div>').attr({
                'id':full_cont_id,
                'class':'stat_full_content'
            }).append($('<div></div>').html(content));

            full_content.hide();

            score_container_bar.append($('<div></div>').attr('class','divide'));
            score_container_bar.append(score_bar);
            score_container_bar.append(full_content);

            score_container_bar.bind('mouseover', {id:full_cont_id}, function(e){
                $('.stat_full_content').hide();
                $('#'+e.data.id).show();
            });

            score_container_bar.bind('mouseout', {id:full_cont_id}, function(e){
                $('#'+e.data.id).hide();
            });

            if(statistic.type != 'table' && statistic.type != 'epigraph'){
                if (statistic.content.length < max_width){
                    if(sum_width > max_sum_width){
                        var cut_pos = Math.round((max_width-width)/6.5);
                        content = content.substr(0,cut_pos) + '...';
                    }
                    var score_phrase = $('<span></span>').attr('class','score_bar_number').html(content);
                    score_container_bar.append(score_phrase);
                }else{
                    if(sum_width > max_sum_width){
                        cut_pos = phrase_length >= width ? Math.round(width/6) : phrase_length;
                        content = content.substr(0,cut_pos) + '...';
                    }
                    score_phrase = $('<span></span>').attr('class','score_bar_number').html(content);
                    score_bar.append(score_phrase);
                }
            }
            score_container_bar.append(score_type);
            score_page.append(score_container_bar);

            score_page.append($('<div></div>').attr('class','score_bar_positions').html(
                '<b>Position start:</b> '+statistic.start_pos+'; <b>Position end:</b> '+statistic.end_pos
            ));
        });

        stat_score.append(score_page);
        var pages = $('<div></div>').css({
            'width': '100%',
            'height': '25px',
            'padding-top': '10px',
            'text-align': 'center'
        });

        current_model.showPageNumbers(pages, totalPages, current_model.model.id);

        stat_score.append(pages);
        this.model.row_obj.append(stat_score);
    }
});