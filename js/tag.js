/*
 *  Теги для фильтрации в библиотеке 
 */
var FilterTagModel = Backbone.Model.extend({
	defaults:{
		ids:[],
		name:'FilterTagModel'
	}
});

var ResetFilter = Backbone.View.extend({
	tagName:'span',
	className:'btn btn-inverse btn-mini',
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el.html('CLEAR');
		return this;
	},
	events:{
		'click':'reset'
	},
	reset:function(){
		this.items.show_all()
	}
});

var AllTagCollection = Backbone.Collection.extend({
	model:FilterTagModel,
	initialize:function(collection, items){
		this.items = items;
		this.on('add remove reset', this.render, this);

		if (this.length > 0){
			this.render();
		}
	},
	render:function(){
		var container = $('.tag_filter').html('');
		this.each(function(tag){	  
		  tag.view = new FilterTagView({model:tag})
		  container.append(tag.view.el).append('&nbsp;');
		});
		
		if (this.length>0){
			var resetview = new ResetFilter();
			resetview.items = this.items;
			container.append(resetview.el);
		}
	}
});


var TagModel = Backbone.Model.extend({
	defaults:{
		name:'TagModel'
	}
});

var TagView = Backbone.View.extend({
	tagName:'span',
	className:'btn btn-inverse btn-mini',
	template: _.template('<%= tag %><button title="Удалить тег" class="delete">×</button>'),
	events:{
		'click .delete':'delete'
	},
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},
	delete:function(){
		this.removedFromModel();
		this.removedFromCollection();
		this.checkRemovedAllTags();
	}, 
	removedFromModel:function(){
		this.model.parent.set(
			'tags',_.without(this.model.parent.get('tags'), this.model.get('tag'))
		);
	},
	removedFromCollection:function(){
		var removed_tag = _.find(this.model.parent.tags.models, function(tag){
			return tag.get('tag') == this.model.get('tag');
		},this);
		if (typeof(removed_tag)!='undefined'){
			this.model.parent.tags.remove(removed_tag);
			removed_tag.destroy();
		}
	},
	checkRemovedAllTags:function(){

		var removed_tag = _.find(this.model.parent.collection.all_tags.models, function(tag){
			return tag.get('tag') == this.model.get('tag');
		}, this);
		if (typeof(removed_tag)!='undefined'){
			var ids = _.without(removed_tag.get('ids'), this.model.parent.id)
			if (ids.length==0){
				this.model.parent.collection.all_tags.remove(removed_tag);
				removed_tag.destroy();
			} else {
				removed_tag.set('ids',ids)
			}
		}
	}
});

var FilterTagView = TagView.extend({
	events:{
		'click':'filter'
	},
	template: _.template('<%= tag %>'),
	close:function(){
		console.log(this);
	},
	filter:function(){
		this.model.collection.items.hide_all();
		this.model.collection.items.filter(this.model.get('ids'));
	}
})

var TagCollection = Backbone.Collection.extend({
	model:TagModel,
	initialize:function(collection, parent){
		this.parent = parent;
		this.all_tags = parent.collection.all_tags;

		this.on('add reset remove', function(){
			this.check_all();
			this.render();
		}, this);
	},
	check_all:function(){
		this.each(function(model){

			var check = _.find(this.parent.collection.all_tags.models, function(tag){
				return tag.get('tag') == model.get('tag');
			});
			
			if (typeof(check)=='undefined'){
				var filter_tag_model = new FilterTagModel({
					tag:model.get('tag'),
					ids:[this.parent.id]
				});
				this.parent.collection.all_tags.add(filter_tag_model);	
			} else {
				var ids = check.get('ids');
				ids.push(this.parent.id);
				check.set(ids,_.uniq(ids));
			}

		}, this)
	},
	render:function(){
		
		var container = $('.row', this.parent.view.$el).html('');

		this.each(function(tag) {
		  tag.view = new TagView({model:tag})
		  tag.parent = this.parent;
		  container.append(tag.view.el).append('&nbsp;');
		}, this);
	}
})

var AddTagForm = Backbone.View.extend({
	className:'tag_form',
	events:{
		'click .add_tag':'add_tag'
	},
	template: _.template(	'<div class="form-inline">'+
								'<input type="text" class="input-small" placeholder="tag">'+
								'<button type="submit" class="btn btn-mini add_tag"><i class="icon-plus"></i></button>'+
								'<div class="row"></div>'+
							'</div>'),
	initialize:function(options){
		this.html_model = options.parent;
		this.render();
	},
	render:function(){
		this.$el
			.html(this.template());

		this.inp = $('input', this.$el);
		return this;
	},
	add_tag:function(){
		var value = $('input', this.$el).val();
		if (value && value!='' && value.length>2){
			if (!this.check_uniq(value)) {
				this.html_model.get('tags').add({
					tag:value
				});
				$('input', this.$el).val('')
			} else {
				alert('Такой тег уже существует');
			}
		} else {
			alert('Тег должен содержать как минимум 3 символа');
		}
	},
	check_uniq:function(value){
		return _.find(this.html_model.get('tags').models, function(m){
			return m.get('tag') == value;
		});
	}
});

var VideoTagView = Backbone.View.extend({
	className:'btn btn-mini',
	events:{
		'click .delete':'del_tag'
	},
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el.html(this.model.get('tag')+'<button title="Удалить тег" class="delete">×</button>');
		return this;
	},
	del_tag:function(){
		var view = this;
		this.model.destroy({
			success:function(){
				view.remove();
				console.log('success');
			},
			error:function(){
				console.log('error')
			}
		})
	}
});

var VideoTagModel = Backbone.Model.extend({
	initialize:function(){
		this.view = new VideoTagView({model:this});
	}
});;

var VideoTagCollection = Backbone.Collection.extend({
	model:VideoTagModel,
	initialize:function(){
		this.on('all', this.render, this);
	},
	render:function(){
		// TODO send to server


		var row = $('.row',this.tag_form.$el).html('');
		this.each(function(tag){
			row.append(tag.view.el);
			tag.view.delegateEvents();
		});
	}
});