/*
 *	Вьюхи элементов на странице, такие как заголовки, сам текст, название книги, автор...
 */
var TextView = Backbone.View.extend({
    events:{
        'click': 'show_edit_menu',
        'mousedown':'mousedown',
        'mouseup':'mouseup'
    },
    options:{
        preview:false,
        inner:false
    },
    initialize: function(model, options){
        _.bindAll(this);

        if (!this.options.preview){
            this.rows = new Backbone.Collection();

            //save page part for cut html_element (view_second_part - view for second page)
            this.model.set(this.model.get('cut')>0?'view_second_part':'view',this);

            this.model.view = this;
            this.model.on('change:style', this.render, this);
            this.model.on('change:poster', function(){
                $('video',this.$el).attr('poster',this.model.get('poster'))
            }, this);

            _.bindAll(this, 'detect_scroll');
            // bind to window
            $('.editor_main').scroll( this.detect_scroll );
        } else {
            this.scale_css();
            this.render();
        }
    },
    scale_css:function(){
        this.style = this.model.get('style');
        this.scaled_style = '';

        if (typeof(this.style)!='undefined' && !_.include(this.style, 'font-size')){
            if (window.getComputedStyle(this.model.view.el).fontSize){
                this.style['font-size'] = window.getComputedStyle(this.model.view.el).fontSize;
            }
        }

        _.each(this.style, function(value,prop){
            if (prop == 'font-size' || prop == 'top' || prop == 'left' ||
                prop == 'width' || prop == 'height'|| prop == 'right' ||
                prop == 'bottom' || prop == 'padding'
                ){
                if (value.indexOf('%')>-1){
                    this.scaled_style += prop+':'+value+'; ';
                } else {
                    this.scaled_style += prop+':'+Math.floor(parseInt(value)*book.get('prev_scale')*10)/10 +'px; ';
                }
            } else {
                this.scaled_style += prop+':'+value+'; ';
            }
        }, this);

        return this.scaled_style;
    },
    mouseup:function(e){
        if (
            e.button != 2 &&
                !this.preview &&
                this.model.get('type') == 'text' &&
                typeof(book.get('selected_from')) != 'undefined'
            ){
            try {
                console.log('selected')
                var selected = this.getSelectedText(e);
                if (selected.text.length > 0){

                    if ( this.model.id < book.get('selected_from') ){
                        var from = book.get('selected_from');
                        book.set('selected_from',this.model.id);
                        this.model.id = from;
                    }

                } else {
                    book.set({'selected_from':-1,'selected_to':-1,'selected_text':''});
                }
            } catch (err) {
                txt="There was an error on this page.\n\nError description: " + err.message;
                console.log(txt)
            }
        }
    },
    getSelectedText: function(e){
        var text = '';
        if (window.getSelection) {
            var range = window.getSelection().getRangeAt(0);
            var start = range.startOffset;
            var end = range.endOffset;

            var container = document.createElement("div");
            container.appendChild(range.cloneContents());

            text =	container.innerText;

        } else if (document.selection) {
            text = document.selection.createRange().text;
            console.log('document.selection:',document.selection.createRange().text, document.selection.createRange());
        }
        return {text:text, start:start, end:end};
    },
    remove_edit_menu:function(){
        if (this.inset_menu) this.inset_menu.del();

        this.model.set('edited',0);

        this.de_marked_page(this.model.get("container"));

        this.de_marked();

        if (typeof(this.properties)!= 'undefined') this.properties.hide();

        this.de_draggable(this.model.get("container"));
    },
    create_edit_menu:function(){
        this.model.set('edited',1);
        this.properties = new Properties(
            [],  // models
            {style:this.model.get("style"), text_model:this.model} // options
        );
        // ?
        if (this.$el.hasClass('popover-active')){
            this.$el.popover('destroy').removeClass('popover-active');
        }

        this.marked();

        if (typeof(this.model.get("container"))!='undefined'){
            this.draggable(this.model.get("container"), this.model); // делаем элемент подвижным
            this.marked_page(this.model.get("container"));
        } else  console.log('need cantainer');
    },
    mousedown:function(){},
    draggable:function(){},
    de_draggable:function(){},
    marked_page: function(my_page){
        this.de_marked_page();

        my_page.content .css({top:'-1px',	left:'-1px'	})
            .addClass('marked_page');
    },
    de_marked_page: function(){
        $('.marked_page')
            .css({top:0,left:0	})
            .removeClass('marked_page');
    },
    marked: function() {
        this.de_marked();
        this.$el.addClass('marked').css({
            'background': 'rgba(190, 11, 111, 0.15);',
        });
    },
    marked_green: function() {
        this.$el.addClass('marked_green')
            .css({
                'margin-top': '-1px',
                'margin-left': '-1px'
            });
    },
    de_marked_green: function() {
        $('.marked_green')
            .css({
                'cursor': 'auto',
                'margin-top': 0,
                'margin-left': 0
            })
            .removeClass('marked_green');
    },
    de_marked: function() {
        book.edited_items = [];
        this.de_marked_green();
        $('.marked').css({'cursor': 'auto', 'background': 'rgba(0, 0, 0, 0);'}).removeClass('marked');
    },
    detect_scroll:function(e){
        if (this.$el.hasClass('popover-active')){
            this.$el.popover('destroy').removeClass('popover-active');
        }
    },
    create_preview: function(after_break){

        if (	typeof(this.model.get('element_type'))!='undefined' &&
            typeof(this.model.get('element_type').get('view'))!='undefined' &&
            typeof(window[this.model.get('element_type').get('view')])!='undefined'
            ){
            var FuncView = window[this.model.get('element_type').get('view')];
            this.model.preview = new FuncView({model:this.model, preview:1, after_break:after_break});
        } else {
            this.model.preview = new PView({model:this.model, preview:1, after_break:after_break});
        }

        if (typeof(this.model.get('container'))!='undefined' && typeof(this.model.get('container').model)!='undefined'){
            var my_container = this.model.get('container').model.preview.content;
            if (this.model.get('column')>0){
                my_container = $('.column'+this.model.get('column'), this.model.get('container').model.preview.$el);
            }
            my_container.append(this.model.preview.el);
        }
    },
    render: function(){
        this.$el.html(this.model.get("content"))
            .attr({'data-type':this.model.get('type')});

        if (this.model.get("style") && _.size(this.model.get("style")) > 0) {
            if (this.options.preview){
                this.$el.attr( 'style', this.scale_css() );
            } else {
                var style_str = '';
                _.each(this.model.get("style"), function(value,prop){
                    style_str += prop+':'+value+'; ';
                });

                this.$el.attr( 'style', style_str );
            }
        }
        this.add_widget_links()
        return this;
    },
    scale:function(style, position){

        var position = position || 0,
            style = style || this.model.get('style'),
            style_str = '';

        _.each(style, function(value,prop){
            if (position && prop == 'font-size' || !position){
                if (prop == 'font-size') {
                    style_str += prop+':'+Math.floor(parseInt(value)*book.get('prev_scale')*10)/10 +'px; ';
                } else {
                    style_str += prop+':'+value+'; ';
                }
            }
        });

        this.$el.attr( 'style', style_str );
    },
    show_edit_menu: function(e){
        if (!this.options.preview){

            if (typeof(book.edited)!='undefined'){ // edited html_element model in book.edited
                if (book.edited.cid == this.model.cid){ // toogle
                    this.remove_edit_menu();
                    delete book.edited;
                } else {
                    // выделяем абзацы для установки им мультиколоночности
                    if (e.ctrlKey && this.model.get("type") == book.edited.get("type")){
                        this.marked_green();

                        if (typeof(book.edited.edited_items) == 'undefined'){
                            book.edited.edited_items = [this.model];
                        } else {
                            var check = _.find(book.edited.edited_items, function(model){
                                return model.id == this.model.id;
                            }, this);

                            if (!check)	book.edited.edited_items.push(this.model);
                        }

                        return false;
                    } else {
                        if (book.edited && book.edited.view){
                            book.edited.view.remove_edit_menu(); // remove old edit menu
                        }
                        book.prev_marked_model = book.edited = this.model;
                        this.create_edit_menu();
                        this.show_inset_menu();
                    }
                }
            } else {
                book.prev_marked_model = book.edited = this.model;
                this.create_edit_menu();
                this.show_inset_menu();
            }
        } else return this;
    },
    show_inset_menu:function(){
        this.inset_menu = new InsetMenuView({model:this.model});
    },
    add_widget_links:function(){
        //alert("Длина = "+this.model.links_to_widgets.length)
        if(this.model.links_to_widgets!=null&&this.model.links_to_widgets!=undefined&&this.model.links_to_widgets!=""){
            for(var i=0;i<this.model.links_to_widgets.length;i++){
                if(this.model.id == this.model.links_to_widgets[i].paragraphId){
                    var start = this.model.links_to_widgets[i].start
                    var end = this.model.links_to_widgets[i].end
                    var id = this.model.links_to_widgets[i].cid
                    if(this.model.lines&&this.model.lines.models.length>0){
                        var currentSymbol = 0
                        for(var a =0;a<this.model.lines.models.length;a++){
                            var startSymbol = null
                            var endSymbol = null
                            var line = this.model.lines.models[a]
                            var lineLength=line.view.$el.html().length
                            if(currentSymbol<=start&&currentSymbol+lineLength>=start){
                                startSymbol = start
                                if(currentSymbol<=end&&currentSymbol+lineLength>=end){
                                    endSymbol = end
                                }else if(currentSymbol+lineLength<end){
                                    endSymbol = currentSymbol+lineLength
                                }
                            }else if(currentSymbol>start&&currentSymbol<end){
                                startSymbol=0
                                if(currentSymbol+lineLength>=end){
                                    endSymbol = end
                                }else if(currentSymbol+lineLength<end){
                                    endSymbol = lineLength
                                }
                            }
                            if(startSymbol!=null&&endSymbol!=null){
                                var lineText = line.get("text")
                                var prepStr = this.sliceString(lineText,endSymbol,"</a>")
                                prepStr = this.sliceString(prepStr,startSymbol,"<a class='widget_link' cid='"+id+"'>")
                                line.view.$el.html(prepStr)
                                // var a = new Backbone.View({tagName:'a'});
                                //  a.$el.html(lineText.substr(startSymbol,endSymbol))
                                // alert(lineText.substr(startSymbol,endSymbol))
                                // line.view.$el.html(lineText.substr(0,startSymbol)).append(a.el).append(lineText.substr(endSymbol,lineText.length))
                            }
                            currentSymbol+=lineLength
                        }
                    }
                }
            }
        }
    },
    get_widget_by_id:function(id){
        var toReturn = null
        book.widget_collection.each(function(widget){if(widget.get('type-pos')=='page'){
                if(widget.cid == id){
                    toReturn= widget
                }
            }
            }
        )
        return toReturn
    },
    sliceString:function(txt, symbol,text){
        return txt.slice(0, symbol) + text + txt.slice(symbol)
    },
});

var DraggableView = TextView.extend({
    tagName:'div',
    show_placeholders:function(my_page){
        _.each(this.model.get('placeholders').models, function(model){
            model.view.droppable(my_page);
            model.html_element = this.model;
        }, this);
    },
    hide_placeholders:function(){
        _.each(this.model.get('placeholders').models, function(model){
            model.view.remove_droppable();
        }, this);
    },
    draggable: function(my_page, text_model){
        if ( typeof(this.model.get('placeholders'))!='undefined' && this.model.get('placeholders').length>1){
            this.show_placeholders(my_page);

            this.$el.draggable({
                revert: 'invalid',
                scroll: false
            }).draggable("enable").css({
                    'cursor':'move',
                });
        }
    },
    de_draggable:function(my_page){
        if (typeof(this.model.get('placeholders'))!='undefined' && this.model.get('placeholders').length>1){
            this.hide_placeholders(my_page);
            this.$el.draggable();
            this.$el.draggable('disable').css({
                'cursor':'auto',
            });
        }
    }

})

var PView = TextView.extend({
    tagName: 'p',
    events:{
        'contextmenu':'popover',
        'click': 'show_edit_menu',
        'click .widget_link':'widget_link_clicked',
        'mousedown':'mousedown',
        'mouseup':'mouseup',
    },
    detect_scroll:function(e){
        if (this.$el.hasClass('popover-active')){
            this.$el.popover('destroy').removeClass('popover-active');
        }
    },
    popover:function(e){
        if (this.$el.hasClass('popover-active')){
            this.$el.popover('destroy').removeClass('popover-active');
        } else {
            $('.popover-active').popover('destroy').removeClass('popover-active');

            var type = (this.model.type == 'text')?'paragraph':this.model.type;

            from_type = _.find(available_elements_type.models, function(model){
                return model.get('name') == type;
            }, this);

            if(typeof(from_type) != 'undefined' && from_type.get('remmapingTo').length > 0){

                var content = this.model.get('content').split(' ').slice(0,6).join(' ')+'...';

                this.$el.popover({
                    'container': 'body',
                    'placement':'bottom',
                    'html':true,
                    'trigger':'manual',
                    'content':'',
                    'title':'Переназначить '+type+' "'+content+'"'
                }).popover('toggle').addClass('popover-active');

                var remapping_menu = new RemappingMenuModel({
                    text:this.model.get('content'),
                    html_element_id: this.model.id,
                    from:type,
                    pos_end:this.model.get('pos_end'),
                    pos_start:this.model.get('pos_start'),
                    typeLvl:this.model.get('typeLvl')
                });

                $('.popover-content').html(remapping_menu.view.el);
            } else {
                console.log('Не переназначаемый элемент '+this.model.type);
            }
        }
    },
    widget_link_clicked:function(e){
        var widget = this.get_widget_by_id($(e.target).attr("cid"))
        if(e.shiftKey==true){
            widget_popup_collection.view.show(widget.view.$el.html())
        }else{
            $('.editor_main').scrollTo(book.pages.get(widget.get("pos")).view.$el, 700);
        }
        return false
    }
});

var TitleView = TextView.extend({
    tagName:'h1',
    events:{
        'contextmenu':'popover',
        'click': 'show_edit_menu',
        'mousedown':'mousedown',
        'mouseup':'mouseup'
    },
    popover:function(e){
        $('.popover-active').popover('destroy').removeClass('popover-active');

        this.$el.popover({
            'container': 'body',
            'placement':'bottom',
            'html':true,
            'trigger':'manual',
            'content':'',
            'title':'Переназначить текст заголовка "'+this.model.get('content')+'"'
        }).popover('toggle').addClass('popover-active');

        var remapping_menu = new RemappingMenuModel({
            text:this.model.get('content'),
            html_element_id: this.model.id,
            from:'title',
            pos_end:this.model.get('pos_end'),
            pos_start:this.model.get('pos_start'),
            typeLvl:this.model.get('typeLvl')
        });

        $('.popover-content').html(remapping_menu.view.el);
    }
});

var CoverNameView = PView.extend({
    tagName: 'h1',
    events:{
        'click': 'show_edit_menu',
        'contextmenu':'popover'
    }
});

var CoverAuthorView = PView.extend({
    tagName: 'h2',
    events:{
        'click': 'show_edit_menu',
        'contextmenu':'popover'
    },
    render: function() {
        if (this.model.has("authors")) {
            this.$el.html(this.model.get("authors").join('<br>'));

            if (this.model.get("style") && _.size(this.model.get("style")) > 0) {
                if (this.options.preview){
                    this.$el.attr( 'style', this.scale_css() );
                } else {
                    var style_str = '';
                    _.each(this.model.get("style"), function(value,prop){
                        style_str += prop+':'+value+'; ';
                    });
                    this.$el.attr( 'style', style_str );
                }
            }
        }
        return this;
    }
});

var CoverGenreView = PView.extend({
    tagName: 'h3',
    events:{
        'click': 'show_edit_menu',
        'contextmenu':'popover'
    }
});

var CoverPicView = DraggableView.extend({
    render: function(){
        if (this.model.has('style') && this.model.get('style')){
            this.$el.css(this.model.get('style'));
        }
        this.$el.css({
            position:'absolute',
            overflow:'hidden',
            background:'rgba(221, 221, 221, 0.5)'
        });


        this.model.set({'curent_placeholder_size':[this.$el.width(),this.$el.height()]},{silent:true});

        var thisJq = this.$el.html('');

        if (!this.options.preview){
            this.img = new Image();
            this.img.parent = this;
            this.img.onload = function(){
                this.parent.model.set('original_img_size',[this.width,this.height]);
                $(this).appendTo(thisJq);
                this.parent.fill();
            }
            this.img.src = this.model.get('src');
            this.img.title = this.model.get('title');
        } else {
            console.log('prev');
        }

        return this;
    },
    create_preview:function(){
        if (typeof(this.model.preview)!='undefined'){
            this.model.preview.remove();
        }
        this.model.preview = new Backbone.View();

        this.prev_img = new Image();
        this.prev_img.src = this.model.get('src');

        this.model.preview.$el
            .attr('style',this.scale_css())
            .css({
                position:'absolute',
                overflow:'hidden'
            }).html(this.prev_img);

        this.model.get('container') // вьюха страницы которая содержит эту модель
            .model.preview.content.append(this.model.preview.el);
    },
    fill:function(){
        var css = this.model.get('style'),
            original_img_size = this.model.get('original_img_size'),
            curent_placeholder_size = this.model.get('curent_placeholder_size');

        if (typeof(original_img_size)!='undefined'){

            if (typeof(this.preview)!= 'undefined'){

                _.each(css, function(a,b){
                    css[b] = parseInt(a)*book.get('prev_scale');
                });

                curent_placeholder_size = [
                    parseInt(curent_placeholder_size[0]*book.get('prev_scale')),
                    parseInt(curent_placeholder_size[1]*book.get('prev_scale'))
                ];
            }

            this.$el.css(css);
            var curent_img_size = [$(this.img).width(), $(this.img).height()];

            if (original_img_size[0]/original_img_size[1]>curent_placeholder_size[0]/curent_placeholder_size[1]){
                var ml = parseInt((curent_placeholder_size[0]-curent_img_size[0])/2);

                $(this.img)
                    .css({'height':'100%','width':'auto'})
                    .css({'margin-left':ml, 'margin-top':0});

                $(this.prev_img)
                    .css({'height':'100%','width':'auto'})
                    .css({'margin-left':parseInt(ml*book.get('prev_scale')), 'margin-top':0});
            } else {

                var mt = parseInt((curent_placeholder_size[1]-curent_img_size[1])/2);

                $(this.img)
                    .css({'width':'100%','height':'auto'})
                    .css({'margin-top':mt, 'margin-left':0});

                $(this.prev_img)
                    .css({'width':'100%','height':'auto'})
                    .css({'margin-top':parseInt(mt*book.get('prev_scale')), 'margin-left':0});
            }
        } // else { console.log(this); }
    },
    show_inset_menu:function(){},
});

var IllustrationView = CoverPicView.extend({
    tagName:"p",
    className: "illustration",
    template: _.template('<img src="<%= src %>" />'),
    render: function() {
        var content = this.template(this.model.toJSON());

        this.$el.html(content);

        if (this.model.get("style") && _.size(this.model.get("style")) > 0) {

            var style_str = '',src,w,h;

            _.each(this.model.get("style"), function(value,prop){
                switch(prop){
                    case 'height':
                        if (value == 0 || value == '0' || value == ''){
                            style_str += prop+':auto; ';
                            h = 'auto';
                        } else {
                            style_str += prop+':'+value+'; ';
                            h = value;
                        }
                        break;
                    case 'width':
                        if (value == 0 || value == '0' || value == ''){
                            style_str += prop+':auto; ';
                            w = 'auto';
                        } else {
                            if (value == 100){
                                value = '100%';
                                w = '100%'
                            } else {
                                w = value;
                            }
                            style_str += prop+':'+value+'; ';
                        }
                        break;
                    default:
                        style_str += prop+':'+value+'; ';
                }
            });

            this.$el.attr( {'style': style_str, 'src': this.model.get("src") });
        }

        if (this.model.has('description')){
            var img_descr = new ImgDescriptionModel({
                text:this.model.get('description'),
                parent_model:this.model
            });
            this.$el.append(img_descr.view.el);
        }

        this.on('change:description',function(){
            console.log(this.attributes)
        }, this);

        return this;
    },
    create_preview:function(){
        if (typeof(this.model.preview)!='undefined'){
            this.model.preview.remove();
        }
        this.model.preview = new Backbone.View();

        this.prev_img = new Image();
        this.prev_img.src = this.model.get('src');

        this.model.preview.$el
            .attr('style',this.scale_css())
            .css({
                position:'absolute',
                overflow:'hidden'
            }).html(this.prev_img);

        this.model.get('container') // вьюха страницы которая содержит эту модель
            .model.preview.content.append(this.model.preview.el);
    },
});

var VideoView = IllustrationView.extend({
    className: 'video',
    events:{
        'click': 'show_edit_menu',
        'contextmenu':'popover',
        'mousedown':'mousedown',
        'mouseup':'mouseup'
    },
    popover:function(e){
        if (e.button == 2){
            $('.popover').remove();

            this.$el.popover({
                'container': 'body',
                'placement':'bottom',
                'html':true,
                'trigger':'manual',
                'content':'Заменить видео:<br>',
                'title':'Редактирование виджета видео'
            }).popover('toggle');


            this.model.btn_video = new WidgetEditImgBtn({
                model:this.model,
                lib_id:3,
                type:'video',
                title:'Выбрать из библиотеки'
            });

            this.model.btn_clipart_video = new WidgetEditImgBtn({
                model:this.model,
                lib_id:4,
                type:'video_clipart',
                title:'Выбрать из клипартов'
            });

            $('.popover-content')
                .append(this.model.btn_video.el)
                .append(this.model.btn_clipart_video.el);

            return false;
        }
    },
    template: _.template('<video controls poster="<%= poster %>">'+
        '<source src="<%= src %>">'+
        '<p>Ваш браузер не поддерживает html5 видео</p>'+
        '</video>'),
    render: function() {
        if (!this.model.has('poster')){
            this.model.set('poster','')
        }
        $(this.el)
            .html(this.template(this.model.toJSON()))
            .attr('id','video_'+this.model.get('video_id'));

        if (this.model.get("style") && _.size(this.model.get("style")) > 0) {
            var style_str = '', src,w,h;
            _.each(this.model.get("style"), function(value,prop){
                switch(prop){
                    case 'height':
                        if (value == 0 || value == '0' || value == ''){
                            style_str += prop+':auto; ';
                            h = 'auto';
                        } else {
                            style_str += prop+':'+value+'; ';
                            h = value;
                        }
                        break;
                    case 'width':
                        if (value == 0 || value == '0' || value == ''){
                            style_str += prop+':auto; ';
                            w = 'auto';
                        } else {
                            if (value == 100){
                                value = '100%';
                                w = '100%'
                            } else {
                                w = value;
                            }
                            style_str += prop+':'+value+'; ';
                        }
                        break;
                    default:
                        style_str += prop+':'+value+'; ';
                }
            });

            this.$el
                .attr( {'style': style_str, 'src': src })
                .css('position','absolute');

            $('video',this.$el).css({'width':w, 'height':h});
        }
        return this;
    },
    show_inset_menu:function(){},
    create_preview:function(){
        this.model.preview = new Backbone.View({
            tagName:'p',
            className:'video preview_video',
        });

        this.prev_video = document.createElement('video');
        $(this.prev_video).attr('src', this.model.get('src')).css({
            width:'100%'
        });

        this.model.preview.$el
            .attr('style',this.scale_css())
            .css({
                position:'absolute',
                overflow:'hidden'
            }).html(this.prev_video);

        this.model.get('container') // вьюха страницы которая содержит эту модель
            .model.preview.content.append(this.model.preview.el);
    },
    marked: function(){
        this.de_marked();
        this.$el.addClass('marked')
            .css({	'margin-top':'-1px',
                'margin-left':'-1px'});
        this.tag_menu();
    },
    de_marked: function(){
        $('.marked').css({	'cursor':'auto',
            'margin-top':0,
            'margin-left':0}).removeClass('marked');
        this.hide_tags();
    },
    hide_tags:function(){
        if (typeof(this.tag_form)!='undefined'){
            this.tag_form.remove();
        }
    },
    tag_menu:function(){
        if (!this.model.has('tags')){
            var tags = new VideoTagCollection();
            this.model.set('tags',tags)
        }
        this.add_tag_form();
        this.show_tags();
    },
    show_tags:function(){
        if (this.model.get('tags').length>0){
            this.model.get('tags').render();
        }
    },
    add_tag_form:function(){

        this.tag_form = new AddTagForm({
            parent:this.model
        });

        this.model.get('tags').tag_form = this.tag_form;

        var position = this.$el.position();

        this.$el.closest('.content').append(this.tag_form.el);
        this.tag_form.$el.css({
            top: position.top + this.$el.height(),
            left: position.left,
            width:this.$el.width()
        });
    }
});

var AudioView = CoverPicView.extend({
    className: 'audio',
    template: _.template('<audio src="" controls></audio>'),
    render: function() {

        $(this.el)
            .html(this.template(this.model.toJSON()))
            .attr('id','audio_'+this.model.id);

        $('audio',this.$el).attr({'src': this.model.get('src') });

        if (this.model.get("style") && _.size(this.model.get("style")) > 0){
            var style_str = '', src,w,h;
            _.each(this.model.get("style"), function(value,prop){
                style_str += prop+':'+value+'; ';
            });

            this.$el
                .attr( {'style': style_str })
                .css({'top':'10px','position':'relative'});
        }
        return this;
    },
    create_preview:function(){
        this.model.preview = new Backbone.View({
            tagName:'p',
            className:'audio preview_audio',
        });

        this.prev_audio = document.createElement('audio');
        $(this.prev_audio).attr('src', this.model.get('src')).css({
            width:'100%'
        });

        this.model.preview.$el
            .attr('style',this.scale_css())
            .css({
                position:'absolute',
                overflow:'hidden'
            }).html(this.prev_audio);

        this.model.get('container') // вьюха страницы которая содержит эту модель
            .model.preview.content.append(this.model.preview.el);
    },
});

var DivscriptView = TextView.extend({
    tagName:"div",
    initialize: function() {
        this.render();
    },
    render: function() {
        var divscript_class = this.model.get("type")+'_'+this.model.id;
        $(this.el).attr({'class': divscript_class});
        $(this.el).addClass('divscript');
        $(this.el).append($('<img>').attr('src',this.model.get("src")));

        if (this.model.get("style") && _.size(this.model.get("style")) > 0) {

            var style_str = '',src,w,h;

            _.each(this.model.get("style"), function(value,prop){
                switch(prop){
                    case 'height':
                        if (value == 0 || value == '0' || value == ''){
                            style_str += prop+':auto; ';
                            h = 'auto';
                        } else {
                            style_str += prop+':'+value+'; ';
                            h = value;
                        }
                        break;
                    case 'width':
                        if (value == 0 || value == '0' || value == ''){
                            style_str += prop+':auto; ';
                            w = 'auto';
                        } else {
                            if (value == 100){
                                value = '100%';
                                w = '100%'
                            } else {
                                w = value;
                            }
                            style_str += prop+':'+value+'; ';
                        }
                        break;
                    default:
                        style_str += prop+':'+value+'; ';
                }
            });

            this.$el.attr( {'style': style_str, 'src': this.model.get("src") });

            var container = $(this.el);
            container.append($('<div></div>').attr('class','divscript_play').css({
                'width':parseInt(container.css('width'))/3,
                'height':parseInt(container.css('width'))/3,
                'margin-left':parseInt(container.css('width'))/2 - (parseInt(container.css('width'))/3)/2,
                'margin-top':parseInt(container.css('height'))/2 - (parseInt(container.css('width'))/3)/2
            }).append($('<img>').attr('src','/img/play.png')));
        }
        return this;
    },
    show_edit_menu: function(){
        if (!this.preview){
            var properties = new Properties(
                [],  // models
                {style:this.model.get("style"), text_model:this.model} // options
            );
            var text_model = this.model,
                div_container = "."+text_model.get('type')+'_'+text_model.get('id'),
                scripts_data = book.get('curent_style').get('scripts_data');

            if(scripts_data && scripts_data.length){
                for(var i=0;i<scripts_data.length;i++){
                    if(scripts_data[i].pos == text_model.get("pos")){
                        var script_id = parseInt(scripts_data[i].id);
                        var data = scripts_data[i].data;
                    }
                }
            }


            if(script_id){
                if ($(div_container).find('.divscript_play').length != 0){
                    dispatcher_scripts('demo', script_id, div_container, text_model.get("pos"), data);
                }

                properties.add(new Property({
                    "name":"Play",
                    "script_name":"demo",
                    "script_id":script_id,
                    "container_id":text_model.get('id'),
                    "pos":text_model.get("pos"),
                    "container":div_container,
                    "data":data
                }));

                properties.add(new Property({
                    "name":"Edit",
                    "script_name":"conf",
                    "script_id":script_id,
                    "container_id":text_model.get('id'),
                    "pos":text_model.get("pos"),
                    "container":div_container,
                    "data":data
                }));
            }

            var change_script_property = new Property({
                "name":"Change Script",
                "script_name":"change",
                "script_id":script_id,
                "container_id":text_model.get('id'),
                "pos":text_model.get("pos"),
                "container":div_container
            });
            properties.add(change_script_property);

            if(!script_id){
                if ($(div_container).find('.divscript_play').length != 0){
                    var changeScript = new ChangeScriptView({model:change_script_property});
                }
            }

            $('.properties').remove();

            properties_view = new PropertiesDivscriptView({collection: properties});
            properties_view.render();

            $('.page_container').prepend(properties_view.$el);
            properties_view.$el.css({height: '32px'});

        } else {
            return this;
        }
    },
});

var H1View = TitleView.extend({
    tagName: 'h1',
    draggable: function(){ },
});

var H2View = TitleView.extend({
    tagName: 'h2',
    draggable: function(){ },
});

var H3View = TitleView.extend({
    tagName: 'h3',
    draggable: function(){ },
});

var H4View = TitleView.extend({
    tagName: 'h4',
    draggable: function(){ },
});

var H5View = TitleView.extend({
    tagName: 'h5',
    draggable: function(){},
});

var LatexView = CoverPicView.extend({
    template: _.template('<img src="<%= src %>" />'),
    initialize: function(model, options){
        _.bindAll(this);
        this.model.view = this;
    },
    render: function(){
        this.$el.html(this.template({
            'src': 'http://chart.apis.google.com/chart?cht=tx&chl='+encodeURIComponent(this.model.get("formula"))
        }));

        this.$el.css(this.model.get('style'))
            .css({
                position:'absolute',
                overflow:'hidden',
                background:'rgba(221, 221, 221, 0.5)'
            });


        this.model.set({'curent_placeholder_size':[this.$el.width(),this.$el.height()]},{silent:true});

        var thisJq = this.$el;

        if (!this.options.preview){
            this.img = new Image();
            this.img.parent = this;
            this.img.onload = function(){
                this.parent.model.set('original_img_size',[this.width,this.height]);
                $(this).appendTo(thisJq);
                this.parent.fill();
            }
            this.img.src = this.model.get('src');
            this.img.title = this.model.get('title');
        } else {
            console.log('prev');
        }

        return this;
    },
    create_edit_menu:function(){
        this.model.set('edited',1);
        this.edit_formula = new EditLatexView({model:this.model});
        this.$el.append(this.edit_formula.el);

        this.properties = new Properties(
            [],  // models
            {style:this.model.get("style"), text_model:this.model} // options
        );

        $('[data-name="path"]').hide();

        if (this.$el.hasClass('popover-active')){
            this.$el.popover('destroy').removeClass('popover-active');
        }

        this.marked();

        if (typeof(this.model.get("container"))!='undefined'){
            this.draggable(this.model.get("container"), this.model); // делаем элемент подвижным
            this.marked_page(this.model.get("container"));
        } else {
            console.log('need cantainer');
        }
    },
    remove_edit_menu:function(){
        this.model.set('edited',0);
        this.de_marked_page(this.model.get("container"));
        this.de_marked();
        if (typeof(this.properties)!= 'undefined'){
            this.properties.hide();
        }
        if (typeof(this.edit_formula)!= 'undefined'){
            this.edit_formula.remove();
        }
        this.de_draggable(this.model.get("container"));
    },
});

/*
 * HTml представления элементов книги
 */
var HtmlElement = Backbone.Model.extend({
    defaults: {
        content:"",
        printed:0,
        cut:0,
        dropcap:0
    },
    links_to_widgets:[],
    initialize: function(model, options) {
        this.options = options;
        this.make_column = 1;
        this.on('change:printed', this.afterPrint, this);

        this.on('change:approve', function(model,value){
            book.save_attr_local(model, value, this, 'approve');
        },this);

        this.on('change:src', function(a,b,c){
            book.save_attr_local(model, value, this, 'src');
            this.view.render();
            this.view.create_preview();
        },this)

        this.on('change:style', this.save_changes, this);

        this.on('change:cut', this.checkForRerender, this);

        this.on("error", function(model, error) {
            console.log(error, model);
        });
    },

    afterPrint:function(model, value, c){

        if (value === 1 || value === 2) {

            if (value === 2) {
                model.set({
                    'first_part_lines_length':model.lines?model.lines.length:0,
                    'container_first_part':model.get('container')
                });
            }

            if (
                typeof(model['additional_widget']) != 'undefined' &&
                    model['additional_widget'] == 1 &&
                    this.get('container').model.get('page') &&
                    value === 1
                ){

                var widget = _.find(book.widget_collection.models, function(w_model){
                    return	w_model.get('type-pos')=='intext' &&
                        w_model.get('pos')>=model.get('pos_start') &&
                        w_model.get('pos')<=model.get('pos_end');
                });

                if (widget) {
                    var n_page = this.get('container').model.id;
                    if (model.has('cut') && model.get('cut') > 0) {
                        var cut_from_start = _.size(model.get('content').split(' ')) - model.get('cut'),
                            word_array = model.get('content').split(' ', cut_from_start),
                            first_part_length = word_array.join(' ').length;

                        var cut_position = model.get('pos_start') + first_part_length;

                        if (parseInt(widget.get('pos'))<cut_position) {
                            n_page=n_page-1;
                        }
                    }

                    widget.set({ 'type-pos':'page', 'pos':n_page });
                    book.view.render(book.widget_collection);
                } //else console.log('err');
            }

            if (typeof(model.view)!='undefined' && !model.has('merged') || (model.has('merged') && model.get('merged')==0)){
                model.view.create_preview(value);
            }
        }

        this.setPage();
    },

    setPage:function(){
        if (typeof(this.contents)!='undefined' && this.get('container').model.get('page'))
            this.contents.set('page', this.get('container').model.get('page'));
    },

    /**
     * Нужно ли перепечатывать последующие элементы
     * @return {[type]} [description]
     */
    checkForRerender:function(){
        if (this.get('printed') == 1){
            this.page = this.get('container');
            if (book.pages.get(this.page.model.id+1)) {
                /*

                 1. check first html_element
                 2. if the same -> change
                 2.1. old_2part.rerender();
                 3. else create new вторую половину
                 3.1 page.view.$el.prepend(new_2part.el)
                 4. запуск ререндера всех послед страниц

                 */
            }
            // console.log(this, this.attributes, this.get('printed'));
        }
    },
    save_first_view:function(){
        this.page.saved_view = {};
        this.page.model.get('inside_collection').each(function(m){
            this.page.saved_view[m.cid] = m.view.$el.html();
        }, this)
    },
    restore_page:function(){
        if (this.page.saved_view){
            this.page.model.get('inside_collection').each(function(m){
                if (this.page.saved_view[m.cid]){
                    m.view.marked();
                    m.view.de_marked();
                    m.view.$el.html(this.page.saved_view[m.cid]);
                }
            }, this)
        }
    },

    /**
     * Проверим на странице след элемент с такой же колоночностью и типом
     * 	что бы объеденить их в один блок, если они стоят рядом
     * 	@param {Number} next_id id след элемент для слияния
     * @return void
     */
    check_page:function(next_id){

        // console.log(next_id);

        var page_view = this.get('container'),
            inside_collection  = page_view.model.get('inside_collection');

        if (inside_collection)
            return inside_collection.find(function(html_element){
                return next_id == html_element.id &&
                    this.get('type') == html_element.get('type') &&
                    this.count_column == html_element.count_column;
            }, this);
    },

    /**
     * Найдем все элементы одного типа на странице и с одинаковой
     * 	колоночностью, и в первом запустим makeColumn
     * @param  {Number} count_column кол-во колонок
     * @return void
     */
    group_columns:function(count_column){

        this.edited_items.push(this);
        this.edited_items = _.sortBy(this.edited_items, function(m){ return m.id; });

        _.each(this.edited_items, function(model){
            model.count_column = count_column;
            if (model.make_column != 0){
                model.make_column = 2;

                var next = _.find(this.edited_items, function(m){
                    return m.id == model.id+1 && m.make_column != 0 && m.get('type') == model.get('type');
                });

                while(next){
                    next.make_column = 0;

                    next.view.position = next.view.$el.position();
                    next.view.position['width'] = next.view.$el.width();
                    next.view.position['height'] = next.view.$el.height();
                    next.view.line_height = $('span:first-child', next.view.$el).height();

                    if (typeof(model.merged)!='undefined')
                        model.merged.push(next);
                    else
                        model.merged = [next];

                    var next_id = next.id+1;

                    next = _.find(this.edited_items, function(new_m){
                        return new_m.id == next_id && new_m.make_column != 0 && new_m.get('type') == next.get('type');
                    });

                    if (!next){
                        next = this.check_page(next_id);
                    }
                }
            }
        }, this);



        _.each(this.edited_items, function(m){
            if (m.make_column == 2)	{
                m.merged = _.uniq(m.merged);
                m.makeColumn(m.count_column);
            }
        });

        _.each(this.edited_items, function(m){ m.make_column = 1; });
    },

    /**
     * Создаем колонки
     * @param  {Number} count_column количество колонок
     * @return {[type]}              [description]
     */
    makeColumn:function(count_column){ //1 | 2 | 3 | 4

        var count_column = count_column || this.count_column || 1;

        if (this.edited_items && _.size(this.edited_items)>0 && this.make_column != 2){

            this.group_columns(count_column);

        } else {

            this.count_column = count_column;
            this.page = this.get('container');
            this.$v = this.view.$el;
            this.available_height = this.page.content.height() - this.$v.position().top;

            if (this.count_column > 1 && typeof(this.merged)!='undefined'){
                _.each(this.merged, function(m){
                    m.count_column = count_column;
                });
            }

            if (this.count_column > 1 && typeof(this.merged)=='undefined') {
                this.searchConsecutiveMultiColumn_2();
            } else {
                if (this.count_column == 1) { // reset view
                    this.view.render();
                    if (typeof(this.merged)!='undefined'){
                        _.each(this.merged, function(m){
                            m.count_column = 1;
                            m.make_column = 1;
                            m.view.render();
                            m.view.$el.show();
                        });
                        this.merged = [];
                    }
                }
            }

            if (this.count_column == 1) {
                this.restore_page();
            } else {
                if (this.make_column == 1 || this.make_column == 2){ // this columns draw in the prev paragraph

                    this.all_width = this.page.$el.width();
                    if (typeof(this.page.saved_view)=='undefined')	this.save_first_view();

                    if (typeof(this.column_gap)=='undefined') this.column_gap = 5;
                    this.column_width = parseInt((this.all_width-(this.column_gap*(count_column-1)))/count_column*10)/10;
                    this.view.p = this.$v.position();
                    this.$v.html('');
                    this.columns = {};
                    for (var i=1; i <= count_column; i++) {
                        var gap = (i==count_column) ? 0 : this.column_gap;
                        this.columns[i] = $('<column data-column="'+i+'"></column>').css({
                            'width':parseInt(this.column_width)-1,
                            'display':'block',
                            'float':'left',
                            'margin-right': gap + 'px',
                            'min-height':'10px',
                        }).appendTo(this.$v);
                    }

                    this.fill_column();
                }
            }
        }
    },

    find_first:function(){
        this.checked_id = this.id-1;
        while(this.check_model()){
            this.checked_id--;
        }
        return book.html_elements.get(this.checked_id+1);
    },
    find_next:function(){
        this.checked_id = this.id+1;
        if (typeof(book.edited_items)!='undefined' && _.size(book.edited_items)>0){
            console.log(book.edited_items);
        } else {
            while(this.check_model()){
                this.merged.push(book.html_elements.get(this.checked_id));
                this.checked_id++;
            }
        }
    },
    check_model:function(){
        var model = book.html_elements.get(this.checked_id);
        if (typeof(model)=='undefined')
            return false;
        else
        if (this.get('type') == model.get('type') && this.count_column == model.get('column')) return true;
        else return false;
    },
    searchConsecutiveMultiColumn_2:function(){
        var page = this.get('container').model;
        if (page){
            this.edited_items = _.filter(page.get('inside_collection').models, function(m){
                return m.count_column && m.count_column == this.count_column;
            }, this)
        }

        this.group_columns(this.count_column);
    },
    searchConsecutiveMultiColumn:function(){
        var first = this.find_first();

        if(typeof(first)!='undefined'){
            first.merged = [];
            first.find_next();
            this.view.inset_menu.remove();
            first.make_column = 1;

            if(first.id!=this.id){
                this.make_column = 0;
                this.view.$el.hide();
                first.makeColumn(this.count_column);
            }
        }
    },
    get_lines:function(){
        this.lines = [];
        this.content = this.get('content');
        this.words_array = this.get('content').split(' ');

        var line_height = $('span:first-child', this.view.$el).height(),
            crossing_widget = this.get_widget();

        while(this.words_array.length > 0){

            var word =  this.words_array.shift(),
                lines_word = [];

            if (typeof(word)!='undefined'){
                lines_word.push(word);
                var line_width = this.column_width;
                if (_.size(crossing_widget)>0) {
                    console.log('accros -> as');
                    line_width = this.get_line_width(crossing_widget, _.size(this.lines), line_height);
                }

                while( get_width(lines_word.join(' '), this.get('type')) < line_width && typeof(word)!='undefined' ){
                    word = this.words_array.shift();
                    lines_word.push(word);
                }

                if (get_width(lines_word.join(' '), this.get('type')) > line_width && lines_word.length>1){
                    lines_word.pop();
                    this.words_array.unshift(word);
                }
                this.lines.push(lines_word.join(' '));
            }
        }

        if (typeof(this.merged)!='undefined' && _.size(this.merged)>0){
            _.each(this.merged, function(m){
                m.column_width = this.column_width;
                m.lines = m.get_lines();
                this.lines.push('&nbsp;');// добавим пустую строку для разрыва между параграфами
                this.lines = this.lines.concat(m.lines);
            }, this);
        }

        return this.lines;
    },
    get_empty_columns: function(total_columns) {
        var columns = {};
        for (j = 1; j <= total_columns; j++) {
            columns[j] = [];
        }
        return columns;
    },

    get_words: function() {
        var words = this.get('content').split(' ');

        if (this.get('cut')>0){
            words = words.slice(-this.get('cut'));
        }

        if (typeof(this.merged) != 'undefined' && _.size(this.merged) > 0) {
            _.each(this.merged, function(m) {
                m.view.$el.hide();
                m.column_width = this.column_width;
                var add_words = m.get('content').split(' ');
                add_words.unshift('&nbsp;');
                words = words.concat(add_words);
            }, this);
        }
        return words;
    },

    /**
     * Заполнение колонок текстом
     * @return {[type]} [description]
     */
    fill_column:function(){
        var total_columns = this.count_column,
            columns = this.get_empty_columns(total_columns),
            line_in_column = 1,
            column = 1,
            words = this.get_words(),
            i = 0,
            crossing_widget = this.crossing_widget = this.get_widget();

        while( _.size(words)>0){
            var line = [],
                word = words.shift(),
                span = new Backbone.View({ tagName:'span' });


            line.push(word);

            if (word != ' '){
                this.columns[column].append(span.render().el);
                span.$el.html(line.join(' '));
                this.span_height = span.height = span.$el.height();
                span.$el.css({
                    'height':span.height,
                    'display': 'inline-block',
                    'white-space': 'nowrap'
                });

                while(
                    span.$el.width()<this.get_width(columns, column, span) &&
                        _.size(words)!=0 &&
                        word != '&nbsp;'
                    ){
                    var word = words.shift();
                    line.push(word);
                    span.$el.html(line.join(' ')).css('text-indent',0);
                }


                if (span.$el.width()>this.get_width(columns, column, span)){
                    // удалим последний элемент из строки и вернем его в массив всех слов
                    words.unshift(line.pop());
                    span.$el.html(line.join(' ')).width('100%');

                    if (_.size(line)==0 && _.size(words)>0){
                        span.$el.html('&nbsp;').width('100%');
                    }
                }

                columns[column].push(line);
                i++;

                if (word == '&nbsp;'){ // разрыв строки для объединенных параграфов
                    var new_span = new Backbone.View({tagName:'span'});
                    this.columns[column].append(new_span.render().el);
                    new_span.$el.html('&nbsp;').css({
                        'height':span.height,
                        'display': 'inline-block',
                        'white-space': 'nowrap',
                        'width':'100%'
                    });
                    if (_.size(columns[column]) != line_in_column) columns[column].push([' ']);
                }

                // увеличим количетво строк колонке и после этого перестроим все колонки
                if (_.size(columns[column]) == line_in_column){
                    column++;
                    if (column > total_columns) {
                        column = 1;
                        if (_.size(words)>0){

                            if (span.height * (line_in_column+1) > this.available_height) {
                                this.set('cut', _.size(words));
                                words = [];
                            } else {
                                words = this.get_words();
                                columns = this.get_empty_columns(total_columns);
                                for (x=1;x<=total_columns;x++){ this.columns[x].html(''); }
                                line_in_column++;
                            }
                        }
                    }
                }
            }
        }
    },

    get_width: function(columns, column, span) {
        var crossing_widget = this.crossing_widget;

        if (_.size(crossing_widget)>0){

            var p = this.columns[column].position(),
                num_line = _.size(columns[column]) + 1,
                column_pos = {
                    h_start: p.left,
                    h_end: p.left + this.column_width
                },
                line_pos = {
                    v_start: (num_line - 1) * this.span_height + this.view.p.top,
                    v_end: (num_line - 1) * this.span_height + this.span_height + this.view.p.top
                },
                img_pos = {
                    v: { start: crossing_widget[0].top, end: crossing_widget[0].top + crossing_widget[0].height },
                    h: { start: crossing_widget[0].left, end: crossing_widget[0].left + crossing_widget[0].width }
                };

            if (
                (img_pos.v.start >= line_pos.v_start && img_pos.v.start <= line_pos.v_end) ||
                    (img_pos.v.start <= line_pos.v_start && img_pos.v.end >= line_pos.v_end) ||
                    (img_pos.v.end <= line_pos.v_end && img_pos.v.end >= line_pos.v_start)
                ) {
                if (column_pos.h_start <= img_pos.h.start && column_pos.h_end >= img_pos.h.start) {
                    // console.log('HaccrosL-'+num_line, column);
                    return img_pos.h.start - column_pos.h_start;
                } else if (column_pos.h_start >= img_pos.h.start && column_pos.h_end <= img_pos.h.end) {
                    // console.log('HaccrosAll-'+num_line, column);
                    return 0;
                } else if (
                    column_pos.h_start >= img_pos.h.start &&
                        column_pos.h_start <= img_pos.h.end && !column_pos.h_end <= img_pos.h.end
                    ) {
                    // console.log('HaccrosR-'+num_line, column);
                    var new_width = column_pos.h_end - img_pos.h.end;
                    span.$el.css('text-indent', this.column_width - new_width);
                    return new_width;
                } else return this.column_width;
            } else return this.column_width;
        } else return this.column_width;
    },
    get_widget:function(){
        var widgets = this.get('container').model['curent_widget_collection'],
            page = this.get('container').model.id,
            crossing_widget = [];

        this.view.position = this.view.$el.position();
        this.view.position['width'] = this.view.$el.width();
        this.view.position['height'] = this.view.$el.height();
        this.view.line_height = $('span:first-child', this.view.$el).height();

        if (_.size(widgets)>0){
            _.each(widgets, function(widget){
                if (widget.get('element_type') == 'inset'){

                    var widget_element = _.find(book.html_elements.models, function(model) {
                        return model.get('type') == 'inset' && model.get('container').model.id == page;
                    });

                    if (widget_element) {
                        var placeholders = widget.get('placeholders');
                        if (placeholders) {
                            var placeholder = placeholders.find(function(p) {
                                return p.get('selected') == 1;
                            });


                            if (placeholder) {
                                var start_placeholder = parseInt(placeholder.get('top')),
                                    end_placeholder = parseInt(placeholder.get('top')) + parseInt(placeholder.get('height')),
                                    bottom = (this.view.position['top'] + this.view.position['height']);

                                if (this.merged && _.size(this.merged) > 0) {
                                    bottom_view = this.merged[_.size(this.merged) - 1].view;
                                    bottom = (bottom_view.position['top'] + bottom_view.position['height']);
                                }


                                if (end_placeholder > this.view.position['top'] && start_placeholder < bottom) {
                                    var placeholder_position = {};
                                    _.each(placeholder.attributes, function(val, key) {
                                        if (key != 'selected') placeholder_position[key] = parseInt(val);
                                    });

                                    crossing_widget.push(placeholder_position);
                                }
                            }
                        }
                    }

                } else {

                    var placeholders = widget.get('placeholders');
                    if (placeholders) {
                        var placeholder = placeholders.find(function(p){ return p.get('selected') == 1; });
                        if (placeholder){
                            var start_placeholder = parseInt(placeholder.get('top')),
                                end_placeholder = parseInt(placeholder.get('top')) + parseInt(placeholder.get('height')),
                                bottom = (this.view.position['top'] + this.view.position['height']);

                            if (this.merged && _.size(this.merged)>0){
                                bottom_view = this.merged[_.size(this.merged)-1].view;
                                bottom = (bottom_view.position['top'] + bottom_view.position['height']);
                            }

                            if (end_placeholder>this.view.position['top'] && start_placeholder<bottom) {
                                var placeholder_position = {};
                                _.each(placeholder.attributes, function(val, key){
                                    if (key != 'selected') placeholder_position[key] = parseInt(val);
                                });

                                crossing_widget.push(placeholder_position);
                            }
                        }
                    }

                }
            }, this);
        }
        return crossing_widget;
    },

    /**
     * Создаем вьюху нужного типа/вида
     * @return {[type]} [description]
     */
    createView:function(){
        this.type = this.get('type');

        this.available_element_type = _.find(available_elements_type.models, function(model){
            return model.get('name') == this.type;
        }, this);

        this.set('element_type',this.available_element_type);
        this.set_content();

        if (typeof(this.available_element_type)!='undefined'){
            var view_name = this.view_name = this.available_element_type.get('view');

            if (typeof(window[this.view_name]) == 'function'){
                var FuncView;
                if (this.view_name == 'title'){
                    if(this.has('typeLvl')){
                        var typeLvl = parseInt(this.get('typeLvl'));
                        FuncView = window['H'+typeLvl+'View'];
                    } else FuncView = window['TitleView'];
                } else FuncView = window[this.view_name];

                var view = new FuncView({model:this}, this.options),
                    style = book.get('curent_style');

                switch (this.type){
                    case 'title':
                        if (this.has('typeLvl')){
                            var typeLvl = parseInt(this.get('typeLvl'));
                            if (typeof(style.get('title_h'+typeLvl))!='undefined'){
                                element_style = style.get('title_h'+typeLvl);
                            }
                        } else element_style = style.get('title_style');
                        break;

                    default:
                        if	(
                            typeof(this.get('element_type')) != 'undefined' &&
                                typeof(this.get('element_type').get('themeContainer'))!='undefined'
                            )
                        {

                            if (['illustration','video','audio','inset','latex'].indexOf(this.type) >-1){
                                // was at that time in attributes "style"
                                delete element_style;
                            } else {
                                element_style = style.get(this.get('element_type').get('themeContainer'));
                                if (typeof(element_style) != 'undefined' && 'placeholders' in element_style){
                                    view.placeholders = element_style.placeholders;
                                    delete element_style.placeholders;
                                }
                            }
                        }
                }

                if (typeof(element_style) != 'undefined'){
                    if (_.isEqual(this.get('style'), element_style)){	// если устанавливается тот же стиль, то не срабатывает тригер
                        view.render();
                    } else {
                        this.set('style',element_style); // рендер сработает тригер на change:style
                    }
                } else view.render();
            }
        } else {
            var view = new PView({model:this}),
                style = book.get('curent_style'),
                element_style = style.get('text_style');

            this.set('style',_.clone(element_style));
        }

        if (view){
            view.$el.attr('data-type',this.type);
            return view;
        } else console.log('ERROR (need VIEW)!');
    },

    /**
     * Вставка строк из кеша
     * @param {Array} lines строки уже подготовленные для обтекания
     * @return this
     */
    pasteLineFromCache:function(lines) {
        var style = model.get('style') || {};
        if (lines && _.size(lines)){
            this.lines = new Backbone.Collection();
            this.view.$el.html('');
            _.each(lines, function(line) {
                var span = new SpanModel(line);
                this.view.$el.append(span.view.el).css(style);
                this.lines.add(span);
            }, this)
        }
    },

    set_content:function(){
        var content = test_pos(this.get('pos_start'), this.get('pos_end'));

        // приведем перевод строки к одному виду
        if (this.type == 'epigraph') content = content.split('<br />').join('<br>').split('<br/>').join('<br>');

        this.set({content: content});
    },
    validate: function(attrs) {
        if (attrs.pos_end < attrs.pos_start) {
            return "Конечная координата не может быть больше начальной ("+attrs.pos_start+"-"+attrs.pos_end+")";
        }

        if (typeof(attrs.element_type)=='undefined'){
            return "Неопдределен тип элемента";
        }

        if (!attrs.element_type.has('title') || !attrs.element_type.has('view') || !attrs.element_type.has('name')) {
            console.log(attrs.element_type)
            return "Неправильный тип элемента";
        }
    },
    update_style:function(){
        this.first_style.set(this.get('style'));
        this.setFontStyle();
        this.save_changes();
        if (typeof(this.get('style'))!= 'undefined' &&
            typeof(this.get('style')['position'])!='undefined' &&
            this.get('style')['position'] == 'absolute'
            ){
            console.log('new propierties without render page');
        } else {
            book.view.render();
        }
    },
    setFontStyle:function(){
        _.each(this.get('style'), function(value,name){
            if (name != 'top' && name != 'left' && name != 'position'){
                this.view.$el.css(name, value);
            }
        }, this);
    },
    save_changes:function(){
        if (typeof(this.first_style) == 'undefined'){
            this.first_style = new Backbone.Model(this.get('style'));
            this.first_style.on('change',function(style){
                //TO DO add changes to  CHANGES element_type.style
                book.save_changes_local(
                    style.changedAttributes(),
                    this.get('element_type').get('themeContainer'),
                    this.get('style')
                );
            }, this);
        }
    },
    save_curent_position:function(){
        book.save_placeholders_local(this)
    },
    select_placeholder:function(){
        if(this.has('placeholders') && this.get('placeholders').length > 0){
            this.placeholder = (this.get('placeholders').length > 1)
                ? _.find(this.get('placeholders').models, function(model){ return model.get('selected') == 1; }) // find selected
                : this.get('placeholders').at(0) // or find first

            if (typeof(this.placeholder)!= 'undefined'){

                this.style = _.clone(this.placeholder.attributes);
                delete this.style.selected;

                this.set({
                    style:this.style,
                    height_start:parseInt(this.style.top),
                    height_stop:parseInt(this.style.top) + parseInt(this.style.height),
                    width_start:parseInt(this.style.left),
                    width_stop:parseInt(this.style.left) + parseInt(this.style.width)
                });

                switch(this.get('type')){
                    case 'inset':
                        this.view = new InsetView({model:this});
                        break
                    case 'latex':
                        this.view = new LatexView({model:this});
                        break
                    case 'illustration':
                    default:
                        this.view = new CoverPicView({model:this});
                }

                this.view.render();
            } // else console.log('this.placeholder undefined, ', this.get('placeholders'));
        } // else console.log('Для элемента нет места (пласехолдеров)');
    },
    mark_search_result:function(m){
        this.mark_preview_search_result(m);
        this.mark_view_search_result(m);
    },
    demark_search_result:function(m){
        this.demark_view_search_result(m);
        this.demark_preview_search_result(m);
    },
    demark_preview_search_result:function(m){
        var preview_content = this.preview.$el.html();
        var new_preview_content = preview_content.split('<span class="yellow">'+m.get('q')+'</span>').join(m.get('q'));
        this.preview.$el.html(new_preview_content);
    },
    mark_preview_search_result:function(m){
        var preview_content = this.preview.$el.html();
        var new_preview_content = preview_content.split(m.get('q')).join('<span class="yellow">'+m.get('q')+'</span>');
        this.preview.$el.html(new_preview_content);
    },
    mark_view_search_result:function(m){
        var q = m.get('q');
        if (typeof(this.lines)!='undefined' && this.lines.length>0){
            this.lines.each(function(line_model){
                var text = line_model.get('text');
                if (text.indexOf(q)>-1){
                    line_model.view.$el.html(text.split(q).join('<span class="yellow">'+q+'</span>'))
                }
            });
        } // else console.log('dont lines', this);
    },
    demark_view_search_result:function(m){
        var q = m.get('q');
        if (typeof(this.lines)!='undefined' && this.lines.length>0){
            this.lines.each(function(line_model){
                var text = line_model.get('text');
                if (text.indexOf(q)>-1){
                    line_model.view.$el.html(line_model.get('text'))
                }
            });
        }
    },
    set_link_to_widget:function(cid,start,end){
        var newLink = {}
        newLink.cid = cid
        newLink.start = start
        newLink.end = end
        newLink.paragraphId = this.id
        this.links_to_widgets.push(newLink)
        this.view.remove_edit_menu();
        delete book.edited;
        this.view.add_widget_links()
    },
});


var CoverPicModel = HtmlElement.extend({
    initialize:function(){
        _.bindAll(this);

        this.on('change:src', function(){
            this.view.render();
            this.view.create_preview();
        },this)

        this.on('change:printed', function(model,value){
            if (value === 1) model.view.create_preview();
        });

        this.on('change:curent_placeholder_size', this.set_style);

        this.select_placeholder();
    },
    set_style:function(){
        this.placeholder = (this.get('placeholders').length > 1)
            ? _.find(this.get('placeholders').models, function(model){ return model.get('selected') == 1; }) // find selected
            : _.find(this.get('placeholders').models, function(model){ return 1; }) // or find first

        this.style = _.clone(this.placeholder.attributes);
        delete this.style.selected;

        this.set({
            style:this.style,
            height_start:parseInt(this.style.top),
            height_stop:parseInt(this.style.top) + parseInt(this.style.height),
            width_start:parseInt(this.style.left),
            width_stop:parseInt(this.style.left) + parseInt(this.style.width)
        });
    }
});


/*
 * HTml представления элементов книги Коллекция
 */
var HtmlElements = Backbone.Collection.extend({
    model: HtmlElement,
    initialize: function(){
        this.comparator;
    },
    comparator: function(model){
        return model.get("pos_start");
    }
});

/*
 * Структурный элемент
 *(содержит координаты текста из общего потока и тип элемента,
 * для заголовком содержит еще уровень заголовка)
 * Модель
 */
var Element = Backbone.Model.extend({
    defaults: {
        pos_start:0
    }
});

/*
 * Структурные элементы
 * Коллекция
 */
var Elements = Backbone.Collection.extend({
    model: Element,
    initialize: function(){
        this.comparator;
    },
    comparator: function(model){
        return model.get("pos_start");
    }
});

function getMethods(obj) {
    var res = [];
    for(var m in obj) {
        if(typeof obj[m] == "function") {
            res.push(m)
        }
    }
    return res;
}

var ImgDescriptionView = Backbone.View.extend({
    className:'img_descr',
    template:_.template('<p><%= text %></p>'+
        '<input type="text" value="<%= text %>" />'),
    events:{
        'click p':'edit',
        'blur input':'save',
        'keypress input': 'updateOnEnter'
    },
    initialize:function(){
        _.bindAll(this);
        this.render()
    },
    render:function(){
        this.$el.html( this.template(this.model.toJSON()) );
        this.input = $('input', this.$el).hide();
        this.p = $('p', this.$el);
        return this;
    },
    edit:function(){
        this.p.hide();
        this.input.show();
        return false;
    },
    updateOnEnter:function(e){
        if (e.keyCode == 13) this.save();
    },
    save:function(){
        this.model.set('text',this.input.val());
        this.close();
    },
    close:function(){
        this.p.show();
        this.input.hide();
    }
});

var ImgDescriptionModel = Backbone.Model.extend({
    initialize:function(){
        _.bindAll(this);
        this.view = new ImgDescriptionView({model:this});
        this.on('change',this.rerender);
    },
    rerender:function(){
        this.get('parent_model').set('description',this.get('text'))
        this.view.render();
    }
});


var EditLatexView = Backbone.View.extend({
    className:'edit_latex',
    template:_.template('<input type="text" value="<%= formula %>" /><i class="icon-edit"></i>'),
    events:{
        'click .icon-edit':'show',
        'click input':'returnfalse',
        'blur input':'save',
        'keypress input': 'updateOnEnter'
    },
    initialize:function(){
        _.bindAll(this);
        this.render();
    },
    render:function(){
        this.$el.html( this.template(this.model.toJSON()) );
        this.ico = $('.icon-edit', this.$el);
        this.input = $('input', this.$el).css('cursor','pointer');
        return this;
    },
    show:function(){
        this.input.show();
        return false;
    },
    updateOnEnter:function(e){
        if (e.keyCode == 13) this.save();
    },
    save:function(){
        this.model.set('formula',this.input.val());
        this.model.view.render();
        console.log(this.model.attributes)
        this.close();
    },
    close:function(){
        console.log('close')
        this.input.hide();
    },
    returnfalse:function(){
        return false;
    }
});