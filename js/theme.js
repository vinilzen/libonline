/* BaseTemplates колекция базовых шаблонов
 * UserTemplates коллекция пользовательских шаблонов
 * 
 * Template модель шаблона
 */

var Template = Backbone.Model.extend({
  defaults: {
    name: null,
    url: null,
    icon: null,
    previews: null,
    selected: false,
    placeholders:{},
    scripts:{}
  },
  url: function(){
    return '/templates/?book_uid='+book.id+'&theme_uid='+this.id;
  },
  initialize:function(){
    // wait fetch template
    this.on('change', function(a,b){
      
      if(!_.isEqual(b.changes, {"selected":true})){ // disable change:selected
        if (this.collection.length>0){
          var l = _.filter(this.collection.models, function(model){
            return model.has('name');
          }).length;

          if (this.collection.length == l && _.find(this.collection.models, function(model){ return model.get('selected'); }) ){
            this.collection.book.trigger('fetch_templates');
          }
        }
      }
    }, this);

    // уберем selected с остальных моделей
    this.on('change:selected',function(a,b,c){
      var template_id = this.id;

      book.set({'selected_theme':this}); // в book сработает тригер
      book.set_curent_style();

      // подменим кнопку "сохранить"
      if (typeof(book.get('changes')) != 'undefined'){
        _.map(book.get('changes').models,function(model){
          if (model.id!=template_id) {
            model.view.$el.fadeOut();
          } else {
            model.view.$el.fadeIn();
          }
        });
      }

      // set selected theme and deselected
      _.map(book.get('base_templates').models,function(model){
        if (model.id!=template_id) {
          model.set({selected:false},{silent: true});
          
          if(typeof(model.view_tab) != 'undefined'){
            model.view_tab.$el.removeClass('active');
          }
        }
      });
      _.map(book.get('user_templates').models,function(model){
        if (model.id!=template_id) {
          model.set({selected:false},{silent: true});
          if(typeof(model.view_tab) != 'undefined'){
            model.view_tab.$el.removeClass('active');
          }
        }
      });

      $('a[href="#template_'+template_id+'"]').tab('show');
    });

    this.on('invalid', this.show_error(), this);
  },
  show:function(){
    var view_btn_prev = new ThemeBtnView({model:this});
    var view_prev = new ThemePreviewView({model:this});
    var view = new ThemeView({model:this});
 
    $(this.collection.view_editor.el).append(view_btn_prev.render().el);

    $('.tab-content', this.collection.view.el).append(view_prev.render().el);
    $('.themes', this.collection.view.el).append(view.render().el); // in <ul id="myTab"
    
    if (this.id == 1){
      this.set({selected:true});
    }
  },
  parse: function(response){
      var attr = {};
      _.each(response, function(value, name){
          switch (name) {
             case 'placeholders':
                var placehold = {};
                _.each(value, function(covers, cover_name){
                    var coverArr = new Array();
                    _.each(covers, function(cover){
                        coverArr.push(cover);
                    });
                    placehold[cover_name] = coverArr;
                });
                attr[name] = placehold;
                break
             case 'divscript':
                var divscrArr = [];
                _.each(value, function(divscript){
                  var placeholders = [];
                  if (typeof(divscript['placeholders'])!='undefined'){
                    _.each(divscript['placeholders'], function(placeholder){
                      placeholders.push(placeholder);
                    })
                    divscript['placeholders'] = placeholders;
                  }
                  divscrArr.push(divscript);
                });
                attr[name] = divscrArr;
                break
             case 'illustration':
                var illArr = new Array();
                _.each(value, function(illustraton){

                    if (typeof(illustraton)!='undefined'){
                      illustraton.illustration_id = illustraton.id;
                      delete illustraton.id;
                    }

                    var placeholders = [];
                    if( typeof(illustraton['placeholders']) != 'undefined' ){                      
                      _.each(illustraton['placeholders'], function(placeholder){
                          placeholders.push(placeholder);
                      });
                      illustraton['placeholders'] = placeholders;
                    }

                    illArr.push(illustraton);
                });
                attr[name] = illArr;
                break;
             case 'footnotes_inline':
             case 'footnotes_style':
                var footnoteArr = [];
                _.each(value, function(footnote){
                    delete footnote.id;
                    var placeholders = [];
                    if( typeof(footnote['placeholders']) != 'undefined' ){                      
                      _.each(footnote['placeholders'], function(placeholder){
                          placeholders.push(placeholder);
                      });
                      footnote['placeholders'] = placeholders;
                    }

                    footnoteArr.push(footnote);
                });
                attr['footnote'] = footnoteArr;
                break
             case 'video':
                var illArr = new Array();
                _.each(value, function(video){
                    if(video.id){
                      video.video_id = video.id;
                      delete video.id;
                    }
                    illArr.push(video);
                });
                attr[name] = illArr;
                break
             case 'audio':
                var illArr = new Array();
                _.each(value, function(audio){
                    illArr.push(audio);
                });
                attr[name] = illArr;
                break
             case 'previews':
                var previews = value.split(',');
                attr[name] = previews;
                break
             case 'widget':
                var illArr = new Array();
                _.each(value, function(illustraton){
                    illArr.push(illustraton);
                });
                attr[name] = illArr;
                break
             default:
                attr[name] = value;
                break
          }
      });
      return attr;
  },
  validate:function(attr){
      this.validate_errors = [];

      if (typeof(attr.error)!='undefined'){
        return this.validate_errors.push(attr.error);
      } else {
        if (this.validate_special_page(attr)){
          return this.validate_errors;
        } //else console.log(attr);
      }
  },
  show_error:function(){
    if (_.size(this.validate_errors)>0){
      var str = '';
      _.each(this.validate_errors, function(e){
        str = str + e + "\r\n";
      }, this);
      alert(str);
    }
  },
  validate_special_page:function(attr){
    if (typeof(attr.special_page)!='undefined'){
      if (typeof(attr.special_page)=='object' && _.size(attr.special_page)>0){
        _.each(attr.special_page, function(v,k){
          if (k == 'first_page' || k == 'last_page') {
            _.each(v,function(p,i){
              if (i.indexOf('page')!==0) // проверим индексы
                this.validate_errors.push('Некорректное описание первых или последних страниц в шаблоне '+attr.name+' (Индекс:'+i+')');

              _.each(p, function(e,j){  // проверим элементы в шаблоне
                if (j!='page' && j!='cover'){
                  
                  var element = _.find(available_elements_type.models, function(m){
                    return m.get('themeContainer') == j;
                  });

                  if (typeof(element)=='undefined')
                    this.validate_errors.push('Некорректное описание элемента в шаблоне '+attr.name+' (Элемент:'+j+')');
                }
              },this);
            },this); 
          }
          if (k == 'page_templates'){
            _.each(v,function(p,i){
              if (i.indexOf('template')!==0) // проверим индексы
                this.validate_errors.push('Некорректное описание шаблона '+attr.name+' (Индекс:'+i+')');

              _.each(p, function(e,j){  // проверим элементы в шаблоне
                var element = _.find(available_elements_type.models, function(m){
                  return m.get('themeContainer') == j;
                });

                if (typeof(element)=='undefined')
                  this.validate_errors.push('Некорректное описание элемента в шаблоне '+attr.name+' (Элемент:'+j+')');
              },this);
            }, this)
          }
        }, this);
        if (_.size(this.validate_errors)>0){
          return true;
        }
      } else return false;
    } else return false;
  }
});

var ThemePreview = Backbone.Model.extend({
  defaults: {
    image: null
  }
});

// контейнер для выбора шаблона на стартовой
var TemplatesView = Backbone.View.extend({
  className:'row-fluid hide',
  template: _.template( '<h3><%= title %></h3>'+
                        '<ul class="nav nav-tabs themes"></ul>'+
                        '<div class="tab-content"></div>'),
  template_upload: _.template( '<h3><%= title %></h3>'+
                        '<ul class="nav nav-tabs themes"></ul>'+
                        '<div class="tab-content"></div>'+
                        '<div class="row-fluid">'+
                          '<button class="btn btn-large btn-link btn-block drop" id="templates_dropzone">'+
                            'Перетащите файл сюда или выберите с диска'+
                          '</button>'+
                          '<input type="file" name="xml_file" class="hidden" id="template_upload" '+
                          'data-url="/upload/uploadTemplate/">'+
                        '</div>'),
  events:{
    'click #templates_dropzone':'select_file'
  },
  initialize:function(){
    this.render();
  },
  select_file:function(){
    $('#template_upload', this.el).click();
  },
  render:function(){
    
    if (this.options.name == 'user'){
      var content = this.template_upload(this.options);
    } else {
      var content = this.template(this.options);
    }

    this.$el.html(content).attr('id',this.options.name + '_template_'+this.options.book_id);

    if (this.options.name == 'user'){
      var el =  this.el;
      $('#template_upload', el).fileupload({// upload new template
          dataType: 'json',
          done: function (e, data) {

              if (data.result && data.result.success){

                   $('#templates_dropzone', el).append('<br><span class="success_msg green">Файл успешно загружен</span>');
                   $('.success_msg',el).fadeOut(3000, function(){
                      $('#templates_dropzone', el).html('Перетащите файл сюда или выберите с диска');
                   });

                   book.get('user_templates').fetch();

              } else if (data.result) {
                  if (typeof(data.result) == 'object'){
                    var error_message = '';
                    for (i=0; i< data.result.length; i++){
                      error_message += data.result[i].error+'<br>'
                    }
                    if (error_message != ''){
                      error_message = '<span class="red">'+error_message+'</span><br><span>Попробуйте еще раз</span>';
                      $('#templates_dropzone', el).html(error_message);
                    } else {
                      $('#templates_dropzone', el).html('Неизвестная ошибка');
                    }
                  }
                  return false;
              }
          },
          dropZone: $('#templates_dropzone', el),
          drop: function(e, data){
              if (data.files.length > 1) {
                  alert('Вы можете добавлять файлы только по одному');
                  return false;
              }

              $.each(data.files, function (index, file) {
                  if ( /^.*\.(xml)$/.test(file.name) ) {
                      $('#templates_dropzone',el).html(file.name);
                  } else {
                      alert('Недопустимый формат');
                      return false;
                  }
              });
          },
          change: function(e, data){
              $.each(data.files, function (index, file) {
                  if ( /^.*\.(xml)$/.test(file.name) ) {
                    $('#templates_dropzone',el).html(file.name);
                  } else {
                    alert('Недопустимый формат');
                    return false;
                  }
              }); 
          }
      }).error(function (jqXHR, textStatus, errorThrown) {
          if (errorThrown === 'abort') {
              my_alert('File Upload has been canceled');
          }
      });

    }
    return this;
  }
});

// контейнер для кнопопок выбора шаблона в редакторе
var TemplatesEditorView = Backbone.View.extend({
  className:'btn-group',
  initialize:function(){
    this.render();
  },
  render:function(){
    this.$el.html('');
    return this;
  }
});

var BaseTemplates = Backbone.Collection.extend({
  model: Template,
  url:'/templates/getBaseTemplates/',
  name:'base',
  title:'Базовые шаблоны',

  initialize:function(m,book){
    this.book = book;
    this.fetch();
    this.on('reset',function(){
      var book = this.book;
      if (typeof(book) != 'undefined') {

        if (typeof(this.view)=='undefined'){
          this.view = new TemplatesView({
            name:this.name,
            title:this.title,
            book_id: book.id
          });
          $('#templates').prepend(this.view.el);
        } else {
          this.view.render();
        }

        if (typeof(this.view_editor)=='undefined'){
          this.view_editor = new TemplatesEditorView({el:$('.theme_select .dropdown-menu')});
        } else {
          this.view_editor.render();
        }

        this.view.$el.fadeIn(1000);

        if (this.length > 0) {
          
          if (this.name=='base'){
            this.view_editor.$el.append('<li class="divider"></li><li class="disabled"><a href="#">Базовые темы</a></li>');
          } else {
            this.view_editor.$el.append('<li class="divider"></li><li class="disabled"><a href="#">Пользовательские темы</a></li>');
          }

          this.each(function(model){
            model.fetch({success:function(){
              model.show();
            },error:function(model, errors){
              model.show_error();
            }});
          });
        }
      }     
    },this);
  },
  comparator:function(model){
    return model.get('id');
  }
});

var UserTemplates = BaseTemplates.extend({
  url:'/templates/getUserTemplates/',
  name:'user',
  title:'Пользовательские шаблоны'
});

var ThemeView = Backbone.View.extend({
  className: "span3",
  tagName: "li",
  events: {
    "click": "select"
  },
  initialize: function() {
    var curent_model = this.model;
    _.bindAll(this);
    curent_model.view_tab = this;
  },
  render: function(){
    var content = '<a href="#template_'+this.model.id+'" data-toggle="tab">'+this.model.get("name")+'</a>';
    this.$el.html(content);
    return this;
  },
  select: function(){
    this.model.set({'selected': true});
  }
});

var ThemeBtnView = Backbone.View.extend({
  tagName: 'li',
  events: {
    "click": "select"
  },
  initialize: function() {
    model = this.model;
    _.bindAll(this);
    this.model.view_btn = this;
    this.model.bind("change:selected", function() {
      $('.theme_select .dropdown-menu li').removeClass('active');
      this.view_btn.$el.addClass('active');
    });
  },
  render: function(){
   
    this.$el.html('<a href="#" title="'+this.model.get("name")+'">'+this.model.get("name")+'</a>');
        
    if (this.model.get("selected")) {
      this.$el.addClass('btn-inverse');
    }

    return this;
  },
  select: function(){
    this.model.set('selected', true);
    book.view.render();
    //console.log(this.model);
  }
});

var ThemePreviewView = Backbone.View.extend({
  className: "tab-pane",
  
  initialize: function() {
    _.bindAll(this);
    this.model.view_prev = this;
  },

  render: function(){
    var content = '';
    _.each(this.model.get("previews"), function(item){
      content += '<div class="preview"><img src="'+item+'"></div>';
    });

    this.$el.attr('id', 'template_'+this.model.id);
    this.$el.html(content);
    return this;
  }
});

var UserThemeView = Backbone.View.extend({
  tagName: 'a',
  className: 'save btn btn-warning btn-mini',
  events: {
    'click': 'save'
  },
  initialize: function(){
    _.bindAll(this);
    this.model.view = this;

    this.render();
  },

  render: function(){

    _.each(user_themes.models, function(model){
      if( model.view ) model.view.$el.hide();
    })

    this.$el.hide();
    this.$el.html('Сохранить');
    this.$el.attr('id', this.model.get("theme_id"));
    $('.saver').append(this.el);
    //this.$el.fadeIn();
    return this;

  },

  save: function(){
    var $el = this.$el;
    
    this.model.save(
      this.attributes,
      {
        error: function(){
          $el.removeClass('btn-warning').addClass('btn-danger');
          console.log('error');
        },
        success: function(model, response){
          if (response && response['id'])
            model.id = response.id;
          
          $el.html("Cохранено")
          $el.removeClass('btn-warning').addClass('btn-success');
          $el.fadeOut("slow");
        }
      }
    );
  }
});

var UserTheme = Backbone.Model.extend({
  initialize: function(){
    user_theme_view = new UserThemeView({model:this});
    this.view = user_theme_view;
  },
  sync: function(method, model, options) {

      var userThemeOptions = options;
      
      if (method == 'read'){
        userThemeOptions.url = '/getdata/?book_uid='+book.id+'&theme_uid='+this.get("theme_id");
      }

      if (method == 'update'){
        userThemeOptions.url = '/savedata/updateData/?book_uid='+book.id+'&theme_uid='+this.get("theme_id");   
      }
      
      if (method == 'create'){
        userThemeOptions.url = '/savedata/insertTheme/?book_uid='+book.id+'&theme_uid='+this.get("theme_id");
      }
      
      if (method == 'update' || method == 'create'){
        userThemeOptions.data = {'data':JSON.stringify(model.toJSON())};
      }
      
      userThemeOptions.timeout = 20000;
      return Backbone.sync.call(this, method, model, userThemeOptions);  
  },
  parse: function(response, xhr){
    if (response && response.data){
      return jQuery.parseJSON(response.data);
    }
  }
});

var UserThemes = Backbone.Collection.extend({ model: UserTheme });


/*
 *  Специальные шаблоны страниц в середине (основной текстовой части) книги.
 */
var TemplateModel = Backbone.Model.extend();

var TemplateCollection = Backbone.Collection.extend({
  model:TemplateModel,
  initialize:function(models,book_view){
    var special_page = book.get('curent_style').get('special_page');
    if (special_page) {
      _.each(special_page.page_templates, function(template,i){
        var items = []
        _.each(template, function(v,k){ items.push(k); })
        var templ = new TemplateModel({
          items:items,
          key:i
        })
        this.add(templ);
      }, this);
    }
  },
  checkMatches:function(type_array){
    var template = 0;
    this.each(function(m){
      var intersection = _.intersection(m.get('items'), type_array);
      if (_.size(intersection)>0){
        if (typeof(sel_intersection)!='undefined'){
          if (_.size(intersection) > _.size(sel_intersection)){
            sel_intersection = intersection;
            template = m;
          }
        } else {
          var sel_intersection = intersection;
          template = m;
        }
      }
    })
    if (template != 0)
      this.template_key = template.get('key');
    else
      delete this.template_key;
  }
});