/*
 * Виджеты типо иллюстраций и видео, позиционируются абсолютно на странице, 
 * перемещаются только между пласехолдерами и должны принимать их размеры.
 * LaTeX - https://chart.googleapis.com/chart?cht=tx&chl=x%20=%20%5Cfrac%7B-b%20%5Cpm%20%5Csqrt%20%7Bb%5E2-4ac%7D%7D%7B2a%7D
 */

var widget_element = ['video','illustration','divscript','image','audio','inset','latex','table'];

var Widget = Backbone.Model.extend({
    htmlModel:null,
	createInsetWidget:function() {
		// вынос для указанной страницы
		var html_model = _.find(book.html_elements.models, function(model) {
			return model.get('type') == 'inset' && model.get('printed') == 0 && model.get('page') == this.get('page');
		}, this);

		// первый из списка виджетов без старницы
		if (!html_model) {
			html_model = _.find(book.html_elements.models, function(model) {
				return model.get('type') == 'inset' && model.get('printed') == 0 && !model.has('page');
			});
		}

		if (html_model) {

			html_model.set({
				container: this.get('page_view'),
				placeholders: this.get('placeholders'),
			})

			html_model.select_placeholder();

			this.get('page_view').content.append(html_model.view.render().el);

			this.view = html_model.view;

			html_model.view.$el
				.css(html_model.get('style'))
				.css(book.get('curent_style').get('inset').style);

			if (html_model.has('content')) this.set('content', html_model.get('content'));

			html_model.set({'printed':true});
			return html_model;
		} else return false;
	},
	createTableWidget:function() {
		var html_model = _.find(book.html_elements.models, function(model) {
			return model.get('type') == 'table' && model.get('printed') == 0;
		});

		if (html_model) {

			html_model.set({
				container: this.get('page_view'),
				placeholders: this.get('placeholders'),
			});

			html_model.select_placeholder();
			html_model.createView();
			flow_widgets.add(html_model);
			this.get('page_view').content.append(html_model.view.render().el);
			
			this.view = html_model.view;

			html_model.view.$el.css(html_model.get('style'));

			return html_model;
		} else return false;
	}
});

var WidgetCollection = Backbone.Collection.extend({
	model:Widget,
	initialize:function(models, book_view) {
		this.book_view = book_view;
		if (this.isSaved()){
			try {
				var widgets = JSON.parse(
					localStorage.getItem(book.id+'_'+book.selected_theme.id+'_widgets')
				);
			} catch (e) {
				console.log(e.name, index);
			}

			if (widgets){
				_.each(widgets, function(widget){
					if (widget.placeholders && _.size(widget.placeholders) > 0){
						widget.placeholders = new Placeholders(_.toArray(widget.placeholders));
					}
					this.add(new Widget(widget));
				}, this)
			} else this.getWidgetsFromStyle();
		} else this.getWidgetsFromStyle();
	},

	/**
	 * Если добавляем новый виджет к сохранненым
	 * @return void
	 */
	addAndSave:function(new_widget){
		if (this.isSaved()){
			try {
				var widgets = JSON.parse(
					localStorage.getItem(book.id+'_'+book.selected_theme.id+'_widgets')
				);
			} catch (e) {
				console.log(e.name, index);
			}

			if (widgets){
				_.each(widgets, function(widget){
					if (widget.placeholders && _.size(widget.placeholders) > 0){
						widget.placeholders = new Placeholders(_.toArray(widget.placeholders));
					}
					if (widget.pos) widget.pos = parseInt(widget.pos);
					this.add(new Widget(widget));
				}, this)
				this.add(new Widget(new_widget));



			} else this.getWidgetsFromStyle();
		} else this.getWidgetsFromStyle();
	},

	/**
	 * Соберем виджеты описаные в стилях
	 * @return void
	 */
	getWidgetsFromStyle:function(){
		var book_view = this.book_view
		_.each(book_view.style.attributes, function(value, name){
			if(name=='latex'){
				var widget = new Widget(value);
				widget.set('element_type','latex')
				this.add(widget);
			} else if(name=='inset'){
				var widget = new Widget(value);
				widget.set('element_type','inset')
				this.add(widget);
			} else if(name=='table'){
				var widget = new Widget(value);
				widget.set({
					'element_type':'table',
					'type':'table'
				});
				this.add(widget);
			} else {
				if (widget_element.indexOf(name)>-1){
					if (value.length>0){
						_.each(value, function(i){
							i.element_type = name;
							if (i.id) i.id = name+i.id;
							var widget = new Widget(i);
							this.add(widget);
						},this);
					}
				}
			}
		}, this);

		if (book.has('save_widgets') && book.get('save_widgets')) this.saveState();
	},

	/**	
	 * Проверяем сохроняли ли виджеты на локалстораж
	 * @return {Boolean} [description]
	 */
	isSaved:function(){
		return !!localStorage.getItem(book.id+'_'+book.selected_theme.id+'_widgets');
	},

	/**
	 * Save widget collection to localStorage
	 * @return {[type]} [description]
	 */
	saveState:function(){
		var widget_data = [];
		
		this.each(function(w){
			var widget = {};
			_.each(w.attributes, function(attr, i){
				if (i!='page_view')	widget[i] = attr;
			});
			widget_data.push(widget)
		});

		localStorage.setItem(book.id+'_'+book.selected_theme.id+'_widgets', JSON.stringify(widget_data));
	},

	/**
	 * Удаляем из локал стораж
	 * @return void
	 */
	clearSavedState:function(){
		localStorage.removeItem(book.id+'_'+book.selected_theme.id+'_widgets');
		console.log('remove saved Widgets');
	},

	/**
	 * Собираем виджеты для страницы page_view и вставляем их в страницу
	 * @param  {Backbone.View} page_view вьюха страницы
	 * @return {Backbone.Collection} коллекция виджетов на текущей странице
	 */
	getFlowWidgets: function(page_view) {
		var flow_widgets = new Backbone.Collection(),
			style = book.get('curent_style'),
			page = page_view.model.id;

		_.each(page_view.model.curent_widget_collection, function(widget) {
			widget.set({
				page:page,
				page_view:page_view
			});

			if (widget.get('type') == 'inset') {			
				var html_model = widget.createInsetWidget();
			} else if (widget.get('type') == 'table') {
				var html_model = widget.createTableWidget();
			} else {
				var new_attr = _.clone(widget.attributes);
				new_attr['container'] = page_view;
				new_attr['element_type'] = _.find(available_elements_type.models, function(m) {
					return m.get('name') == new_attr.type;
				});

				var html_model = new CoverPicModel(new_attr);
				widget.view = html_model.createView();
				page_view.content.append(html_model.view.render().el);
				html_model.view.$el.css('position', 'absolute');
			}

			if (html_model) {
                flow_widgets.add(html_model);
            }
		});

		return flow_widgets;
	}
});

var WidgetView = Backbone.View.extend({
	className:'widget',
	events:{
    	'contextmenu':'popover',
		'mousedown':'show_drop_zone',
		'mouseup':'hide_drop_zone'
	},
	initialize:function(){
		this.render();
	},
	render:function(){
		if (this.model.get('type') == 'img'){
			var widget_id=this.model.id,
				placeholders = this.model.get('placeholders'),
				style = {};

			if (placeholders)
				for (var i=0; i< placeholders.length; i++)
					if (placeholders[i].selected && placeholders[i].selected == 1) style = placeholders[i];

			this.$el
				.html('<div class="img_container_'+widget_id+'" style=\'background-image: url("..'+this.model.get('src')+'");background-repeat: no-repeat; background-position: 50% 50%;\' />')
				.css(style)
				.css({'z-index':1,'position':'absolute', 'background-color':'rgba(0, 0, 0, 0.1)'})
				.attr('data-widget-id', this.model.id);

			var img = new Image();
			img.onload = function(){
				var size = [this.width,this.height];
			
				new_height = parseInt(style.height);
				new_width = parseInt(this.width*parseInt(style.height)/this.height);
				
				if (new_width<parseInt(style.width)){
					new_width = parseInt(style.width);
					new_height = parseInt(this.height*parseInt(style.width)/this.width);
				}

				$('.img_container_'+widget_id, this.$el).css({'background-size': new_width+'px '+new_height+'px'});				
			}
			img.src = this.model.get('src');

			$('.img_container_'+widget_id, this.$el).css({ width:style.width, height:style.height })
		}
		return this;
	},
	show_drop_zone:function(e){
		if (e.button == 0){
			_.each(this.model.placeholders.models, function(placeholder){
				if (placeholder.get('selected') == 1){
					placeholder.view.$el.hide();
				} else {
					placeholder.view.$el.addClass('marked').show();
				}
			});
		}
	},
	hide_drop_zone:function(e){
		if (e.button == 0){
			_.each(this.model.placeholders.models, function(placeholder){
				placeholder.view.$el.removeClass('marked');
			});
		}
	},	
	get_position:function(){		
		var p = this.$el.position();
		var w = el.$el.width()+parseInt(el.$el.css('padding-left'))+parseInt(el.$el.css('padding-right'));
		var h = el.$el.height()+parseInt(el.$el.css('padding-top'))+parseInt(el.$el.css('padding-bottom'));
		console.log(p,w,h);
	},
	set_position:function(){
		var p = this.$el.position();
		var el = this;
		var w = el.$el.width()+parseInt(el.$el.css('padding-left'))+parseInt(el.$el.css('padding-right'));
		var h = el.$el.height()+parseInt(el.$el.css('padding-top'))+parseInt(el.$el.css('padding-bottom'));
		this.model.set({
			w:w,
			h:h,
			left:p.left,
			top:p.top,
			width_start: p.left,
			width_stop:  p.left + w,
			height_start: p.top,
			height_stop:  p.top + h
		},{silent: true});
	},
	popover:function(e){
		if (e.button == 2){
			$('.popover').remove();
			if (this.model.get('type') == 'img'){

				this.$el.popover({
					'container': 'body',
					'placement':'bottom',
					'html':true,
					'trigger':'manual',
					'content':'Изменить изображение:<br>',
					'title':'Редактирование виджета изображений'
				}).popover('toggle');
			}

			this.model.btn_image = new WidgetEditImgBtn({model:this.model, lib_id:1, type:'image', title:'Выбрать из библиотеки'});
			this.model.btn_clipart = new WidgetEditImgBtn({model:this.model, lib_id:2, type:'clipart', title:'Выбрать из клипартов'});

			$('.popover-content')
				.append(this.model.btn_image.el)
				.append(this.model.btn_clipart.el);

			return false;
		}
	},
	hide_popover:function(){
    	$('.popover').remove();
	},
	draggable:function(containment){
		this.$el.draggable({
	        containment: $('.content',containment),
	        revert: 'invalid'
	    });
	}
});

// only show btn for Open Lib
var WidgetEditImgBtn = Backbone.View.extend({
	className:'btn btn-mini',
	events:{
		'click':'show_lib'
	},
	initialize:function(){
		this.render();
	},
	render:function(){
		this.$el.html(this.options.title);
		return this;
	}, 
	show_lib:function(){
		book.edited_widget = this.model;//enable change image
		btnLib.get(this.options.lib_id).view.$el.click();
	}
});