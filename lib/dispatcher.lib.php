<?
	require_once('config/config.php');
	require_once('lib/exception.lib.php');
	
	class Dispatcher{
		private $url;
		private $method;
		
		private $hashCtl = array(
            'test' => 'TestCtl',

            'scripts' => 'ScriptsCtl',
            'admin' => 'AdminCtl',
            'upload' => 'UploadCtl',
            'parser' => 'ParserCtl',
            'login' => 'LoginCtl',
            'books' => 'BooksCtl',
            'savedata' => 'SavedataCtl',
            'index' => 'IndexCtl',
            'reassign' => 'ReassignCtl',
            'templates' => 'TemplatesCtl',
            'dropcaps' => 'DropCapsCtl'
//            'thesaurus' => 'ThesaurusCtl'
		);

        private $hashMethods = array(
            'ScriptsCtl' => array(
                'saveScripts',
                'getScriptsPublic',
                'saveCurrentScript',
                'insertScripts',
                'deleteScripts'
            ),
            'BooksCtl' => array(
                'saveBook',
                'getContent',
                'setReassignments',
                'getStatistic',
                'getBooksStatistic',
                'getReassignStatistic',
                'addThesaurusWord'
            ),
            'TemplatesCtl' => array(
                'getTypeTemplates',
                'getScriptData',
                'getBaseTemplates',
                'getUserTemplates',
                'updateTemplate'
            ),
            'DropCapsCtl' => array(
                'getBaseConfig',
                'getCustomConfig'
            )
        );
	
		public function getURL(){
			$request = $_SERVER['REQUEST_URI'];
			if ($request == '/' || !$request){
				$this->url = 'login';
				$this->method = DEFAULT_FUNCTION;
				return $this;
			}
			$matches = preg_match_all('/[a-zA-Z]+\//', $request, $partsUri);
			if($matches == 0) throw new ModelException('Invalid request');
			else{
				$urlMatch = preg_match('/'.implode('|', array_keys($this->hashCtl)).'/', $partsUri[0][0], $url);
				if($urlMatch == 0){
//					header('Location: '.self::getURI('underconst'));
					throw new ModelException('Invalid request class');
				}
				$this->url = strval($url[0]);
				if ($matches == 1) $this->method = DEFAULT_FUNCTION;
				else{
					preg_match('/\w+/', $partsUri[0][1], $method);
                    $allowMethods = $this->hashMethods[$this->hashCtl[$this->url]];
                    if(sizeof($allowMethods) && !in_array($method[0], $allowMethods)) throw new ModelException('Invalid request method');
					else $this->method = strval($method[0]);
				}
			return $this;
			}
		}
		
		public function redirect(){
			$fileCtl = 'ctl/'.$this->url.'.ctl.php';
			if(!file_exists($fileCtl)) throw new ModelException('File '.$fileCtl.' not exist');
			require_once($fileCtl);
            $class = $this->hashCtl[$this->url];
            $method = $this->method;
            $controller = new $class();
            $controller->$method();
		}
		
		public static function getParams(){
            $params = array();
            if($_GET){
                foreach ($_GET as $k => $value){
                    $params[$k] = $value;
                }
            }
            if($_POST){
                foreach ($_POST as $k => $value){
                    $params[$k] = $value;
                }
            }
            return $params;
		}
		
		public static function getURI($class, $method=NULL, $variables=NULL){
			if(!$class) return;
			$uri = '/'.$class.'/';
			if($method) $uri .= $method.'/';
			if($variables){
				if(!is_array($variables)) return $uri;
				$count = 0;
				foreach($variables as $variable => $value){
					if ($count == 0) $delimiter = '?';
					else $delimiter = '&';
					$uri .= $delimiter.$variable.'='.$value;
					$count++;
				}
			}
			return $uri;
		}
		
		public function factoryModel($table){
			require_once('models/'.$table.'.model.php');
			return ucfirst($table).'Model';
		}
		
		public static function checkLang(){
			$params = self::params();
			if($params['lang'] == 'en') return true;
			else return false;
		}
		
		public static function changeLang(){
			$params = self::params();
			$uri = str_replace('/', '', reset(explode("/?", $_SERVER['REQUEST_URI'])));
			if(!$uri) $uri = 'index';
			if($params['lang'] == 'en') $params['lang'] = 'ru';
			else $params['lang'] = 'en';
			return self::getURI($uri, '', $params);
		}
	}
?>