<?
class ModelException extends Exception{
	private $status = array(
		204 => 'No Content',
		400 => 'Bad Request',
		500 => 'Internal Server Error'
	);

	public function __construct($message, $code = 400) {
		parent::__construct($message, $code);
	}

	public function code() {
		return $this->getCode().' - '.$this->status[$this->getCode()];
	}

	public function message() {
		return $this->getMessage();
	}
}
?>