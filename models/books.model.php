<?
require_once('models/main.model.php');

class BooksModel extends MainModel{
	function __construct(){
		parent::__construct('books');
        $this->addField('id', new IntField('id', Field::PRIMARY_KEY));
        $this->addField('user_uid', new CharField('user_uid'));
		$this->addField('book_uid', new IntField('book_uid'));
        $this->addField('name', new CharField('name'));
        $this->addField('author', new CharField('author'));
        $this->addField('elements', new CharField('elements'));
        $this->addField('content', new CharField('content'));
        $this->addField('reassignments', new CharField('reassignments'));
        $this->addField('statistic', new CharField('statistic'));
        $this->addField('data', new CharField('data'));
	}
}
?>