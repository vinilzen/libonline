<?
require_once('models/main.model.php');

class FilesModel extends MainModel{
	function __construct(){
		parent::__construct('files');
        $this->addField('id', new IntField('id', Field::PRIMARY_KEY));
        $this->addField('user_uid', new CharField('user_uid'));
        $this->addField('filename', new CharField('filename'));
        $this->addField('path', new CharField('path'));
        $this->addField('type', new CharField('type'));
        $this->addField('screenshot', new CharField('screenshot'));
        $this->addField('tags', new CharField('tags'));
        $this->addField('data', new CharField('data'));
	}
}
?>