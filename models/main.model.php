<?	
	require_once('config/config.php');
	
	abstract class MainModel{
        private $triggerQuery;
        private $triggerInsert;

		protected $name;
		protected $table;
        protected $foreignTable;
        protected $useForeignTable;
		protected $fields = array();
		protected $sqlQuery = '';
		protected $stWrdFlt = ' WHERE ';
		protected $numRows;
        protected $lastId;
		protected $count;
        protected $fKeys = array();
		
		protected function __construct($table, $foreignModel=NULL){
			$this->table = $table;
            $this->foreignTable = $foreignModel;
		}
		
		public function getField($name){
			return $this->$name;
		}

        public function getFields(){
            return $this->fields;
        }
		
		public function getTable(){
			return $this->table;
		}

        protected function addField($name, Field $field){
			if ($this->$name) throw new ModelException('Already exist this field name');
			else $this->$name = $field;
			$this->fields[] = $name;
		}

//        public function customString(){
//            $resultStr = '';
//            $exceptWords = array(
//                'от',
//                'на',
//                'в',
//                'к',
//                'с'
//            );
//
//            $this->db_connect();
//            $result = mysql_query('SELECT word, count(word) AS count FROM thesaurus WHERE (text_pos > 227 AND text_pos < 815) GROUP BY word');
//
//            while($res = mysql_fetch_assoc($result)){
//                if(!in_array($res['word'], $exceptWords)){
//                    $resultStr .= "Word: ".$res['word']."\n";
//                    $resultStr .= "Count: ".$res['count']."\n";
//                    $resultStr .= "----------------------------------------------------------\n";
//                }
//            }
//
//            $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/thesaurus.txt', 'w+');
//            fwrite($fp, $resultStr);
//            fclose($fp);
//        }
		
		public function sqlSelect($fields=NULL, $useFTable=false, $orderBy=NULL, $distinct=false){
            $this->useForeignTable = $this->foreignTable ? $useFTable : false;

            for ($i=0;$i<count($this->fields);$i++){
                $this->getField($this->fields[$i])->value = array();
            }

			if(!$fields || $fields=='all' || $fields=='All' || $fields=='*') $selection = '*';
			elseif(is_array($fields)){
				for($i=0;$i<count($fields);$i++){
					$this->checkField($fields[$i], $useFTable);
				}
				$selection = implode(', ', $fields);
			}else{
				$this->checkField($fields, $useFTable);
				$selection = $fields;
			}
            $this->sqlQuery = $distinct ? 'SELECT DISTINCT '.$selection.' FROM '.$this->table : 'SELECT '.$selection.' FROM '.$this->table;
            if($this->useForeignTable) $this->sqlQuery .= ', '.$this->foreignTable->getTable();

            if($fields && $orderBy && (strtoupper($orderBy) == 'ASC' || strtoupper($orderBy) == 'DESC')){
                $order = is_array($fields) ? $fields[0] : $fields;
                $this->sqlQuery .= ' ORDER BY '.$order.' '.strtoupper($orderBy);
            }
			$this->triggerQuery = true;
			return $this;
		}
		
		public function sqlCount($fields=NULL, $as=''){
			if(!$fields){
				$selection = '*';
			}else if(is_array($fields)){
				for($i=0;$i<count($fields);$i++){
					$this->checkField($fields[$i]);
				}
				$selection = implode(', ', $fields);
			}else{
				$this->checkField($fields);
				$selection = $fields;
			}

            if($as && is_array($as)) $as = ' AS '.implode(', ', $as);
            elseif($as) $as = ' AS '.$as;

			$this->sqlQuery = 'SELECT '.$selection.', COUNT('.$selection.')'.$as.' FROM '.$this->table;
            $this->triggerQuery = true;
			return $this;
		}
		
		public function sqlLimit($offset, $rows=NULL){
			if (!$this->sqlQuery) throw new ModelException('Function "sqlLimit()" must be before function "sqlSelect()" or "sqlFilter()"');
			$this->sqlQuery .= ' LIMIT '.$offset;
            if($rows) $this->sqlQuery .= ','.$rows;
			return $this;
		}
		
		public function sqlUpdate(array $data){
			$this->sqlQuery = 'UPDATE '.$this->table.' SET ';
            foreach($data as $field => $value){
                if(!in_array($field, $this->fields)) throw new ModelException('The field "'.$field.'" is not found in model "'.$this->table.'"<br />');
                elseif($this->getField($field)->getFlag() != Field::PRIMARY_KEY){
                    $delim = $this->delimiter($field);
                    $this->sqlQuery .= $field.' = '.$delim.mysql_escape_string($this->getField($field)->value($value)).$delim.', ';
                }
            }
            $this->sqlQuery = substr($this->sqlQuery, 0, strlen($this->sqlQuery)-2);
            return $this;
		}
		
		public function sqlInsert(array $data){
            $this->sqlQuery = 'INSERT INTO '.$this->table.' SET ';
			$query = '';
			foreach($data as $field => $value){
				if (in_array($field, $this->fields)){
					$delim = $this->delimiter($field);
					$query .= $field.' = '.$delim.mysql_escape_string($this->getField($field)->value($value)).$delim.', ';
				}
			}
			$query = substr($query, 0, strlen($query)-2);
			if (!$query) throw new ModelException('empty query');
			$this->sqlQuery .= $query;
            unset($query);
            $this->triggerInsert = true;
			$this->triggerQuery = false;
            return $this;
		}

        public function multiplySql(array $data, $insertFlag = true){
            $command = $insertFlag ? 'INSERT' : 'REPLACE';
			$this->sqlQuery = $command.' INTO '.$this->table;
            $fieldsArray = array();

            $query = ' (';
            foreach($data[0] as $field => $value){
                if(in_array($field, $this->fields) && !($insertFlag && $this->getField($field)->getFlag() == Field::PRIMARY_KEY))
                    $query .= '`'.$field.'`, ';
            }
            $query = substr($query, 0, strlen($query)-2);
            $query .= ') VALUES ';
            foreach($data as $keyData => $itemsData){
                if(!is_array($itemsData)) throw new ModelException('All key-value in all data items must be equal. Error in number '.($keyData+1).' item');
                if($keyData > 0 && count($data[$keyData]) != count($data[$keyData-1])) throw new ModelException('All array items contains format key=value must be equal. Error in item number '.($keyData+1));
                $count = 0;
                $idFlag = false;
                $query .= '(';
                foreach($itemsData as $field => $value){
                    if(in_array($field, $this->fields)){
                        if(!$insertFlag && $this->getField($field)->getFlag() == Field::PRIMARY_KEY) $idFlag = true;
                        $fieldsArray[$keyData][$count] = $field;
                        $count++;
                        $delim = $this->delimiter($field);
                        if(!$insertFlag || !$this->getField($field)->getFlag() == Field::PRIMARY_KEY){
                            if($delim) $query .= "'".mysql_escape_string($this->getField($field)->value($value))."', ";
                            else $query .= mysql_escape_string($this->getField($field)->value($value)).", ";
                        }
                    }
                    if(!$insertFlag && !$idFlag) throw new ModelException('In all items must be exist "id" field. Error in number '.($keyData+1).' item.');
                }
                if($keyData > 0 && $fieldsArray[$keyData] !== $fieldsArray[$keyData-1]) throw new ModelException('All fields in item must be equal. Error in item number '.($keyData+1));
                $query = substr($query, 0, strlen($query)-2);
                $query .= '), ';
            }
            unset($fieldsArray);
            $query = substr($query, 0, strlen($query)-2);
			$this->sqlQuery .= $query;
            unset($query);
            $this->triggerInsert = true;
			$this->triggerQuery = false;
            return $this;
		}

		private function delimiter($field){
			if($this->getField($field)->getType() == 'int' || $this->getField($field)->getType() == 'bool') return '';
			else return '\'';
		}
		
		public function sqlFilter($field, $value=NULL, $sign=NULL, $useFTable=false){
			if (!$this->sqlQuery) throw new ModelException('Function "sqlFilter()": must be before function "sqlSelect()"');
			if (is_array($field)) throw new ModelException('Function "sqlFilter()": not support array variables on fields');
            if ($sign && $sign != '>' && $sign != '<') throw new ModelException('Function "sqlFilter()": sign must be ">" or "<"');

            $sign = $sign ? $sign : '=';
			$this->checkField($field, $useFTable);

            $table = $useFTable && $this->useForeignTable ? $this->foreignTable->getTable() : $this->table;
            $field = $this->useForeignTable ? $table.'.'.$field : $field;
			
			if (!$value) $this->sqlQuery .= $this->stWrdFlt.$field;
			elseif (is_array($value)){
				for($i=0;$i<count($value);$i++){
					if (is_int($value[$i]) || is_bool($value)) $this->sqlQuery .= $this->stWrdFlt.($i==0 ? '(' : '').$field.' '.$sign.' '.$value[$i];
					else $this->sqlQuery .= $this->stWrdFlt.$field.' = \''.$value[$i].'\'';
					$this->stWrdFlt = ' OR ';
				}
				$this->sqlQuery .= ')';
			}

			elseif (is_string($value)) $this->sqlQuery .= $this->stWrdFlt.$field.' '.$sign.' \''.$value.'\'';
			elseif (is_int($value) || is_bool($value))$this->sqlQuery .= $this->stWrdFlt.$field.' '.$sign.' '.$value;
//			$this->stWrdFlt = ' ';
//            $this->sqlQuery .= '; ';
			return $this;
		}
		
		public function sqlDelete($idValue){
			$this->sqlQuery = 'DELETE FROM '.$this->table;
            $fieldsArray = $this->getFields();
            for($i=0;$i<count($this->getFields());$i++){
                if($this->getField($fieldsArray[$i])->getFlag() == Field::PRIMARY_KEY){
                    $idName = $fieldsArray[$i];
                    break;
                }
            }
            if(!isset($idName)) throw new ModelException('In table "'.$this->table.'" not set PRIMARY_KEY');
            $this->sqlQuery .= $this->stWrdFlt.$idName;

            if(is_array($idValue)){
                $this->sqlQuery .= ' IN (';
                for($i=0;$i<count($idValue);$i++){
                    $this->sqlQuery .= $idValue[$i].', ';
                }
                $this->sqlQuery = substr($this->sqlQuery, 0, strlen($this->sqlQuery)-2);
                $this->sqlQuery .= ')';
            }else $this->sqlQuery .= ' = '.$idValue;

			$this->stWrdFlt = ' ';
            $this->triggerQuery = false;
			return $this;
		}
		
		public function sqlFilterAnd(){
            $this->stWrdFlt = ' ';
//			if($this->stWrdFlt == ' WHERE ') throw new ModelException('Function "sqlFilterAnd()" must be before function "sqlFilter()" or "sqlDelete()"');
			$this->sqlQuery .= ' AND';
			return $this;
		}

        public function sqlGroupBy($field){
            if(!$this->sqlQuery) throw new ModelException('Function "sqlGroupBy()": Empty SQL query');
            $this->checkField($field);
            $this->stWrdFlt = ' ';
            $this->sqlQuery .= ' GROUP BY '.$field;
            return $this;
        }
		
		public function exec(){
			if (!$this->sqlQuery) throw new ModelException('Function "exec()" must be run in final');
			$this->db_connect();
			if(@!$result = mysql_query($this->sqlQuery)) throw new ModelException('SQL error: ' . mysql_error());
			else{
                if ($this->triggerInsert) $this->lastId = mysql_insert_id();
				if (!$this->triggerQuery) return true;
				@$this->numRows = mysql_num_rows($result);
			}
            if(!is_bool($result)){
                if($this->useForeignTable){
                    foreach($this->foreignTable->getFields() as $field){
                        if(!in_array($field, $this->getFields())){
                            switch($this->foreignTable->getField($field)->getType()){
                                case 'char':
                                    $this->addField($field, new CharField($field));
                                    break;
                                case 'int':
                                    $this->addField($field, new IntField($field));
                                    break;
                                case 'bool':
                                    $this->addField($field, new BoolField($field));
                                    break;
                            }
                        }
                    }
                }

                while($res = mysql_fetch_assoc($result)){
                    foreach($res as $field => $val){
                        $this->$field->value[] = $val;
                    }
////                    for ($i=0;$i<count($this->fields);$i++){
////                        $this->getField($this->fields[$i])->value[] = $this->getField($this->fields[$i])->value($res[$this->fields[$i]]);
////                    }
                }
                $this->stWrdFlt = ' WHERE ';
            }
		}

		public function db_connect(){
			try{
				if(!$dbcnx = mysql_connect(DB_HOST.':'.DB_PORT, DB_USER, DB_PASS, DB_CLENT_INTERCTIVE)) throw new ModelException('Server is not accessible', 500);
				if(!mysql_select_db(DB_NAME)) throw new ModelException('Database is not accessible', 500);
			}catch(ModelException $e){
				print $e->code().'<br />'.$e->message().'<br />';
			}
		}
		
		public function getNumRows(){
			return $this->numRows;
		}

        public function getLastId(){
            return $this->lastId;
        }

		public function getCount(){
			return $this->count;
		}
		
		private function checkField($field, $useFTable=false){
			if(!$useFTable && !in_array($field, $this->fields)) throw new ModelException('The field "'.$field.'" is not found in model "'.$this->table.'"<br />');
            elseif($useFTable && !in_array($field, $this->foreignTable->getFields())) throw new ModelException('The field "'.$field.'" is not found in foreign table "'.$this->foreignTable->getTable().'"<br />');
		}
		
		public function showSqlQuery(){
			echo $this->sqlQuery.'<br />';
		}
	}
	
	abstract class Field{
		const PRIMARY_KEY = 0x0001;
	
		protected $type;
		protected $name;
		protected $flag;
		public $value = array();
		
		public function __construct($name, $flag=NULL) {
			$this->flag = $flag;
			$this->name = $name;
		}
		public function getType() {
			return $this->type;
		}
		public function getName() {
			return $this->name;
		}
		public function getFlag() {
			return $this->flag;
		}
        public function getTable(){
            return $this->name;
        }
		
	}
	
	class CharField extends Field{
		public function __construct($name, $flag=NULL){
			parent::__construct($name, $flag);
			$this->type = 'char';
		}
		
		public function value($string){
			return strval($string);
		}
	}
	
	class IntField extends Field{
		public function __construct($name, $flag=NULL){
			parent::__construct($name, $flag);
			$this->type = 'int';
		}
		public function value($num){
			return intval($num);
		}
	}
	
	class BoolField extends Field{
		public function __construct($name, $flag=NULL, $fkey=NULL){
			parent::__construct($name, $flag);
			$this->type = 'bool';
		}
		public function value($num){
			if ($num) return 1;
			else return 0;
		}
	}
	
?>