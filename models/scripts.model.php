<?
require_once('models/main.model.php');

class ScriptsModel extends MainModel{
	function __construct(){
		parent::__construct('scripts');
        $this->addField('id', new IntField('id', Field::PRIMARY_KEY));
        $this->addField('name', new CharField('name'));
        $this->addField('demo', new CharField('demo'));
		$this->addField('conf', new CharField('conf'));
	}
}
?>