<?
require_once('models/main.model.php');
require_once('models/themes_data.model.php');

class ThemesModel extends MainModel{
	function __construct(){
		parent::__construct('themes', new ThemesDataModel());
        $this->addField('theme_uid', new IntField('theme_uid', Field::PRIMARY_KEY));
        $this->addField('theme_name', new CharField('theme_name'));
        $this->addField('theme_type', new CharField('theme_type'));
		$this->addField('user_uid', new CharField('user_uid'));
	}
}
?>