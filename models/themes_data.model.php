<?
require_once('models/main.model.php');

class ThemesDataModel extends MainModel{
	function __construct(){
		parent::__construct('themes_data');
        $this->addField('id', new IntField('id', Field::PRIMARY_KEY));
		$this->addField('book_uid', new IntField('book_uid'));
        $this->addField('theme_uid', new IntField('theme_uid'));
        $this->addField('theme_data', new CharField('data'));
        $this->addField('script_data', new CharField('script_data'));
        $this->addField('html_data', new CharField('html_data'));
	}
}
?>