<?
require_once('models/main.model.php');

class ThesaurusModel extends MainModel{
	function __construct(){
		parent::__construct('thesaurus');
        $this->addField('id', new IntField('id', Field::PRIMARY_KEY));
        $this->addField('book_uid', new IntField('book_uid'));
        $this->addField('word', new CharField('word'));
        $this->addField('paragraph_pos', new IntField('paragraph_pos'));
        $this->addField('text_pos', new IntField('text_pos'));
	}
}
?>