<?
require_once('models/main.model.php');

class UidsModel extends MainModel{
	function __construct(){
		parent::__construct('uids');
		$this->addField('id', new IntField('id', Field::PRIMARY_KEY));
		$this->addField('uid', new CharField('uid'));
	}
}
?>