require.config({

    baseUrl: 'new_design/js/lib',

    paths: {
        app: '../app',
        tpl: '../tpl',
        jquery: './jquery-2.0.3.min',
        underscore: './underscore-min',
        backbone: './backbone-min',
        html2canvas: './html2canvas',
        popover: '../app/jquery.popover',
        modal: '../app/jquery.modal',
        scrollto: '../app/jquery.scrollTo.min',
    },
/*
    map: {
        '*': {
            'app/models/employee': 'app/models/memory/employee'
        }
    },
*/
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'popover'  : ["jquery"],
        'modal'  : ["jquery"],
    }
});

require(['jquery', 'backbone', 'app/router', 'html2canvas'], function ($, Backbone, Router) {
    var router = new Router();
    Backbone.history.start();

    function centration(){
        var window_w = $('#top').width(),
            left_ofset = (window_w-$('#left-side').width()-$('#right-side').width()-$('#main').width())/2;
/*
        $('#main').animate({'left': left_ofset+140}, 100);
        $('.tools').animate({'left': left_ofset+10}, 100);
*/
        $('#main').css({'left': left_ofset+140});
        $('.tools').css({'left': left_ofset+10});
    }

    centration();

    $(window).resize(function() {  centration(); })

});