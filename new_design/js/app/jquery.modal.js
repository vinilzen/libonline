/**
 * create popover for element
 * @param  {string} title
 * @param  {string} content
 * @param  {string} placement положение относительно элемента left|right|top|bottom
 * @param  {string} add_class дополнительный класс
 * @param  {bool} blue для голубого заголовка !TODO -> check arrow placement, for 'top' is OK
 * @return void
 */
(function( $ ){
    $.extend($.fn, {
        modal: function(title, content) {
            
            $(document.body).removeClass('modal_open');
            $('.modal, .modal-backdrop').remove();

            var template = _.template(
                '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
                  '<div class="modal-dialog">'+
                    '<div class="modal-content">'+
                      '<div class="modal-header">'+
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                        '<h4 class="modal-title"><%= title %></h4>'+
                      '</div>'+
                      '<div class="modal-body"><%= content %></div>'+
                      '<div class="modal-footer">'+
                        '<button type="button" class="btn btn-default close-modal">Close</button>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>');

            $(document.body).addClass('modal_open')
                .append('<div class="modal-backdrop fade in"></div>')
                .append(template({
                    title:title,
                    content:content
                }));

            $('.modal').addClass('in').fadeIn();

            var self = this;

            $('.modal .close, .modal .close-modal').click(function(){
                self.closeModal();
            });


            return this;
        },
        closeModal: function() {

            // check-remove existing modal
            $(document.body).removeClass('modal_open');
            $('.modal, .modal-backdrop').remove();
        }
    })
})( jQuery );