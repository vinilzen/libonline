define(function(require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        BookCoverView       = require('app/views/CoverBook'),
        BookView            = require('app/views/BookView'),
        HtmlElement         = require('app/models/HtmlElement'),
        CoverPicModel       = require('app/models/CoverPicModel'),
        HtmlElements        = require('app/models/HtmlElements'),
        ElementsType        = Backbone.Collection.extend({}),
        available_elements_type = new ElementsType([
            {   name:'page', view:'CoverNameView', cover:0, position:'page',properties_group:'block',
                remmapingTo: [],
                title:'Страница Книги',
                themeContainer:'page_style'
            },
            {   name:'name', title:'Название', view:'CoverNameView', cover:1, position:'absolute',properties_group:'text',
                remmapingTo: [
                    'author','genre','paragraph','epigraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','volume','title'
                ],
                themeContainer:'cover_name_style'
            },
            {   name:'author', title:'Автор', view:'CoverAuthorView', cover:1, position:'absolute',properties_group:'text',
                remmapingTo: [
                    'name','genre','paragraph','epigraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','volume','title'
                ],
                themeContainer:'cover_author_style'
            },
            {   name:'genre', title:'Жанр', view:'CoverGenreView', cover:1, position:'absolute',properties_group:'text',
                remmapingTo: [
                    'name','author','paragraph','epigraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content'
                ],
                themeContainer:'genre'
            },
            {   name:'paragraph', title:'Параграф', view:'PView', position:'static',properties_group:'text',
                remmapingTo: [
                    'name','author','genre','epigraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','title','volume'
                ],
                themeContainer:'text_style'
            },
            {   name:'epigraph', title:'Епиграф', view:'EpigraphView', position:'static',properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','volume','title'
                ],
                themeContainer:'epigraph'
            },
            {   name:'serial', title:'Название серии', view:'SerialView', position:'static', properties_group:'text',
                remmapingTo:[
                    'name','author','genre','paragraph','epigraph','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','volume','title'
                ],
                themeContainer:'serial'
            },
            {   name:'introduction', title:'Заголовок "Введения"', view:'IntroductionView', position:'static', properties_group:'text',
                remmapingTo:[
                    'name','author','genre','paragraph','epigraph','serial','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','volume','title'
                ],
                themeContainer:'introduction'
            },
            {   name:'introduction_content', title:'Содержимое "Введения"', view:'IntroductionContentView', position:'static', properties_group:'text',
                remmapingTo:[
                    'name','author','genre','paragraph','epigraph','serial','introduction','volume','title',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content'
                ],
                themeContainer:'introduction_content'
            },
            {   name:'volume', title:'Название тома', view:'VolumeView', position:'static', properties_group:'text',
                remmapingTo:[   
                    'name','author','genre','paragraph','epigraph','serial','introduction','introduction_content',
                    'title','date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content'
                ],
                themeContainer:'volume'
            },
            {   name:'title', title:'Заголовок', view:'TitleView', position:'title', properties_group:'text',
                remmapingTo:[
                    'name','author','paragraph','epigraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','volume','genre'
                ],
                themeContainer:'title_style'
            },
            {   name:'table', view:'TableView', position:'title',properties_group:'block',
                remmapingTo: [],
                title:'Таблица',
                themeContainer:'table'
            },
            {   name:'date_create', title:'Дата создания', view:'DateCreateView', position:'static', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','epigraph','introduction','introduction_content',
                    'volume','title','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','serial'
                ],
                themeContainer:'date_create'
            },
            {   name:'annotation', title:'Заголовок "Аннотации"', view:'AnnotationView', position:'title', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','epigraph','serial','introduction','introduction_content',
                    'volume','title','date_create','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','paragraph'
                ],      
                themeContainer:'annotation'
            },
            {   name:'footnote', title:'Сноска', view:'FootnotesView', position:'title', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','epigraph','serial','introduction','introduction_content',
                    'volume','title','date_create','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content','paragraph'
                ],      
                themeContainer:'footnote'
            },
            {   name:'annotation_content', title:'Содержимое "Аннотации"', view:'AnnotationContentView', position:'static', properties_group:'text',
                remmapingTo: [
                    'name','author','paragraph','epigraph','serial','introduction','dedication','volume',
                    'introduction_content','title','date_create','annotation','about_author','genre',
                    'about_author_content','dedication_content','from_author','from_author_content'
                ],
                themeContainer:'annotation_content'
            },
            {   name:'about_author', title:'Заголовок "Об авторе"', view:'AboutAuthorView', position:'title', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','epigraph','serial','introduction','introduction_content',
                    'volume','title','date_create','annotation','annotation_content','about_author_content',
                    'dedication','dedication_content','from_author','from_author_content'
                ],
                themeContainer:'about_author'
            },
            {   name:'about_author_content', title:'Содержимое "Об авторе"', view:'AboutAuthorContentView', position:'static', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','epigraph','serial','introduction','introduction_content',
                    'date_create','annotation','annotation_content','about_author','volume','title',
                    'dedication','dedication_content','from_author','from_author_content'
                ],
                themeContainer:'about_author_content'
            },
            {   name:'dedication', title:'Заголовок "Посвящения"', view:'DedicationView', position:'title', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','epigraph','serial','introduction','volume','title',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication_content','from_author','from_author_content','introduction_content'
                ],
                themeContainer:'dedication'
            },
            {   name:'dedication_content', title:'Содержимое "Посвящения"', view:'DedicationContentView', position:'static', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','epigraph','serial','introduction','volume','title',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','from_author','from_author_content','introduction_content'
                ],
                themeContainer:'dedication_content'
            },
            {   name:'from_author', title:'Заголовок "От автора"', view:'FromAuthorView', position:'static', properties_group:'text',
                remmapingTo: [
                    'name','author','genre','paragraph','epigraph','serial','introduction','volume',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author_content','introduction_content','title'
                ],
                themeContainer:'from_author'
            },
            {   name:'from_author_content', title:'Содержимое "От автора"', view:'FromAuthorContentView', position:'static', properties_group:'text',
                remmapingTo: [
                    'name','author','paragraph','epigraph','serial','introduction','volume','title',
                    'date_create','annotation','annotation_content','about_author','about_author_content',
                    'dedication','dedication_content','from_author','introduction_content','genre'
                ],
                themeContainer:'from_author_content'
            },
            {   name:'speech', title:'Прямая речь', view:'SpeechView', internal:1, position:'static', properties_group:'text',
                remmapingTo: ['paragraph'],
                themeContainer:'speech'
            },
            {   name:'inset', title:'Вынос', view:'InsetView', internal:1, position:'absolute', properties_group:'text',
                remmapingTo: ['paragraph'],
                themeContainer:'inset'
            },
            {   name:'video', title:'Видео', view:'VideoView', position:'absolute', properties_group:'media',
                remmapingTo:[''],
                themeContainer:'video'
            },
            {   name:'audio', title:'Аудио', view:'AudioView', position:'absolute', properties_group:'media',
                remmapingTo:[''],
                themeContainer:'audio'
            },
            {   name:'illustration', title:'Иллюстрация', view:'IllustrationView', position:'absolute', properties_group:'media',
                remmapingTo:[''],
                themeContainer:'illustration'
            },
            {   name:'latex', title:'Формула', view:'LatexView', position:'absolute', properties_group:'media',
                remmapingTo:[''],
                themeContainer:'latex'
            },
            {   name:'cover_pic', title:'Иллюстрация на обложке', view:'CoverPicView', position:'absolute', properties_group:'media',
                remmapingTo:[''],
                themeContainer:'cover_pic'
            },
            {   name:'contents', title:'Оглавление', view:'ContentsView', position:'absolute', properties_group:'block',
                remmapingTo:[''],
                themeContainer:'contents'
            },
        ]);

    /*
     * get text fragment from book content between START and END
     */
    function test_pos(start, end, book){
      if (typeof(book)!='undefined'){
        if (book.has('content')){
          var content = book.get('content');
          return content.substr(parseInt(start-1),parseInt(end-start+1));
        } else {
          console.log('book don\'t have content');
        }
      } else {
        console.log('book undefined')
      }
    }

    return Backbone.Model.extend({

        url: function() {
            return '/getbooks/getContent/?book_uid=' + this.id;
        },

        initialize: function(options) {
            this.id = this.get('book_uid');
            this.coverView = new BookCoverView({ model: this });
            this.html_elements = new HtmlElements();

            this.on('change:name change:author', this.coverView.render, this);
            this.on('change:selected', this.afterSelect, this);

            /* Изменения массива элементов в книге */
            this.on('change:elements', function(model, response) {
                if (response) {
                    this.parseContent();
                    
                    this.view = new BookView({model:this});
                    this.view.render();
                    this.view.createPages();

                } else console.log('Ошибка получения контента книги');
            }, this);
        },

        afterSelect: function(model, value) {
            if (value == 1) {
                this.coverView.$el.addClass('selected');

                this.collection.trigger('set_book', this);

                // unselect other model
                _.chain(this.collection.without(this)).invoke('unselect');

            } else {
                this.coverView.$el.removeClass('selected');
            }
        },

        unselect: function() {
            this.set({
                'selected': 0
            });
        },

        /*
         * Заполняем коллекцию структурных элементов
         * (название книги, автор, параграфы, заголовки и ...)
         */
        parseContent: function() {
            var content = this.get('content'),
                book = this,
                elements = this.get('elements');

            this.footnotes = new Backbone.Collection();
            this.html_elements.reset();

            _.each(elements, function(e) {

                var text = test_pos(e.pos_start, e.pos_end, this);
                if (e.type == 'paragraph') {
                    var cut = _.find(elements, function(el) {
                        return e.pos_start <= el.pos_start &&
                            e.pos_end >= el.pos_end &&
                            el.type != 'paragraph' && !el.delete;
                    });

                    // перенос на новую страницу
                    if (cut) {
                        text = text.substr(0, cut.get('pos_start') - e.pos_start) + text.substr(cut.get('pos_end') - e.pos_start + 1);
                    }
                }

                var element_type = _.find(available_elements_type.models, function(model) {
                    return model.get('name') == e.type;
                });

                if (typeof(element_type) != 'undefined') {
                    var html_element = new HtmlElement({
                        content: text,
                        pos_start: e.pos_start,
                        pos_end: e.pos_end,
                        type: e.type,
                        typeLvl: e.typeLvl,
                        id: e.id,
                        element_type: element_type
                    });

                    if (e.type == 'inset') console.log(element_type);
                    if (e.type == 'table') html_element.set('table', e.content);
                    if (e.type == 'epigraph') {
                        html_element.set('content', text.split('<br />').join('<br>').split('<br/>').join('<br>'));
                    }

                    this.html_elements.add(html_element);
                } else {
                    if (e.footnotes_inline || e.footnotes_style) {

                        var element_type = _.find(available_elements_type.models, function(model) {
                            return model.get('name') == 'footnote';
                        });

                        var footnote_type = e.footnotes_inline ? 'footnotes_inline' : 'footnotes_style';

                        _.each(e.footnote_type, function(model) {

                            var footnote = new Backbone.Model({
                                id: model.id,
                                content: test_pos(model['pos_start'], model['pos_end'], this),
                                text: test_pos(model['pos_start'], model['pos_end'], this),
                                pos_start: model['pos_start'],
                                pos_end: model['pos_end'],
                                pos_in_text: model['pos_in_text'],
                                symbol_position: model['pos_in_text'],
                                type: 'footnote',
                                element_type: element_type
                            });

                            this.footnotes.add(footnote);
                        }, this);
                    } else if (e.inset) {
                        var element_type = _.find(available_elements_type.models, function(model) {
                            return model.get('name') == 'inset';
                        });

                        _.each(e.inset, function(model) {
                            var html_element = new CoverPicModel({
                                content: test_pos(model['pos_start'], model['pos_end'], this),
                                pos_start: model['pos_start'],
                                pos_end: model['pos_end'],
                                type: 'inset',
                                element_type: element_type
                            });

                            this.html_elements.add(html_element);
                        }, this);
                    } else if (e.tables) {
                        var element_type = _.find(available_elements_type.models, function(model) {
                            return model.get('name') == 'table';
                        });

                        _.each(e.tables, function(model) {
                            var html_element = new HtmlElement({
                                type: 'table',
                                row: model['rows'],
                                element_type: element_type
                            });

                            book.html_elements.add(html_element);
                        });
                    } //else console.log('не найден тип элемента', e);
                }

            }, this);
        }
    })
});