define(function (require) {

	"use strict";

	var _ = require('underscore'),
		HtmlElement = require('app/models/HtmlElement');

	return HtmlElement.extend({
	    initialize:function(){

	        this.on('change:src', function(){
	            this.view.render();
	            this.view.create_preview();
	        },this)

	        this.on('change:printed', function(model,value){
	            if (value === 1) model.view.create_preview();
	        });

	        this.on('change:curent_placeholder_size', this.set_style);

	        this.select_placeholder();
	    },
	    set_style:function(){
	        this.placeholder = (this.get('placeholders').length > 1)
	            ? _.find(this.get('placeholders').models, function(model){ return model.get('selected') == 1; }) // find selected
	            : _.find(this.get('placeholders').models, function(model){ return 1; }) // or find first

	        this.style = _.clone(this.placeholder.attributes);
	        delete this.style.selected;

	        this.set({
	            style:this.style,
	            height_start:parseInt(this.style.top),
	            height_stop:parseInt(this.style.top) + parseInt(this.style.height),
	            width_start:parseInt(this.style.left),
	            width_stop:parseInt(this.style.left) + parseInt(this.style.width)
	        });
	    }
	});
});