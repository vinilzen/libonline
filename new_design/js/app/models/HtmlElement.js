define(function (require) {

	// "use strict";

	var Backbone = require('backbone');

	return Backbone.Model.extend({
	    defaults: {
	        content:"",
	        printed:0,
	        cut:0,
	        dropcap:0,
	        selected:0,
	        style:{
		        'color':'#000',
		        'font-family':'Verdana',
		        'font-weight':'normal',
		        'font-size':'14px',
		        'background-color':'#fff',
		        'text-align':'left',
	        }
	    },
	    links_to_widgets:[],
	    initialize: function(model, options) {
	        this.options = options;
	        this.make_column = 1;
	        this.on('change:printed', this.afterPrint, this);

	        this.on('change:approve', function(model,value){
	            book.save_attr_local(model, value, this, 'approve');
	        },this);

	        this.on('change:src', function(a,b,c){
	            book.save_attr_local(model, value, this, 'src');
	            this.view.render();
	            this.view.create_preview();
	        },this)

	        this.on('change:style', this.save_changes, this);

	        this.on('change:cut', this.checkForRerender, this);

	        this.on("error", function(model, error) {
	            console.log(error, model);
	        });
	    },

	    afterPrint:function(model, value, c){

	        if (0 && value === 1 || value === 2) {

	            if (value === 2) {
	                model.set({
	                    'first_part_lines_length':model.lines?model.lines.length:0,
	                    'container_first_part':model.get('container')
	                });
	            }

	            if (
	                typeof(model['additional_widget']) != 'undefined' &&
	                    model['additional_widget'] == 1 &&
	                    this.get('container').model.get('page') &&
	                    value === 1
	                ){

	                var widget = _.find(book.widget_collection.models, function(w_model){
	                    return	w_model.get('type-pos')=='intext' &&
	                        w_model.get('pos')>=model.get('pos_start') &&
	                        w_model.get('pos')<=model.get('pos_end');
	                });

	                if (widget) {
	                    var n_page = this.get('container').model.id;
	                    if (model.has('cut') && model.get('cut') > 0) {
	                        var cut_from_start = _.size(model.get('content').split(' ')) - model.get('cut'),
	                            word_array = model.get('content').split(' ', cut_from_start),
	                            first_part_length = word_array.join(' ').length;

	                        var cut_position = model.get('pos_start') + first_part_length;

	                        if (parseInt(widget.get('pos'))<cut_position) {
	                            n_page=n_page-1;
	                        }
	                    }

	                    widget.set({ 'type-pos':'page', 'pos':n_page });
	                    book.view.render(book.widget_collection);
	                } //else console.log('err');
	            }

	            if (typeof(model.view)!='undefined' && !model.has('merged') || (model.has('merged') && model.get('merged')==0)){
	                model.view.create_preview(value);
	            }
	        
	    this.setPage();
	        }

	    },

	    setPage:function(){
	        if (typeof(this.contents)!='undefined' && this.get('container').model.get('page'))
	            this.contents.set('page', this.get('container').model.get('page'));
	    },

	    /**
	     * Нужно ли перепечатывать последующие элементы
	     * @return {[type]} [description]
	     */
	    checkForRerender:function(){
	        if (this.get('printed') == 1){
	            this.page = this.get('container');
	            if (book.pages.get(this.page.model.id+1)) {
	                /*

	                 1. check first html_element
	                 2. if the same -> change
	                 2.1. old_2part.rerender();
	                 3. else create new вторую половину
	                 3.1 page.view.$el.prepend(new_2part.el)
	                 4. запуск ререндера всех послед страниц

	                 */
	            }
	            // console.log(this, this.attributes, this.get('printed'));
	        }
	    },
	    save_first_view:function(){
	        this.page.saved_view = {};
	        this.page.model.get('inside_collection').each(function(m){
	            this.page.saved_view[m.cid] = m.view.$el.html();
	        }, this)
	    },
	    restore_page:function(){
	        if (this.page.saved_view){
	            this.page.model.get('inside_collection').each(function(m){
	                if (this.page.saved_view[m.cid]){
	                    m.view.marked();
	                    m.view.de_marked();
	                    m.view.$el.html(this.page.saved_view[m.cid]);
	                }
	            }, this)
	        }
	    },

	    /**
	     * Проверим на странице след элемент с такой же колоночностью и типом
	     * 	что бы объеденить их в один блок, если они стоят рядом
	     * 	@param {Number} next_id id след элемент для слияния
	     * @return void
	     */
	    check_page:function(next_id){

	        // console.log(next_id);

	        var page_view = this.get('container'),
	            inside_collection  = page_view.model.get('inside_collection');

	        if (inside_collection)
	            return inside_collection.find(function(html_element){
	                return next_id == html_element.id &&
	                    this.get('type') == html_element.get('type') &&
	                    this.count_column == html_element.count_column;
	            }, this);
	    },

	    /**
	     * Найдем все элементы одного типа на странице и с одинаковой
	     * 	колоночностью, и в первом запустим makeColumn
	     * @param  {Number} count_column кол-во колонок
	     * @return void
	     */
	    group_columns:function(count_column){

	        this.edited_items.push(this);
	        this.edited_items = _.sortBy(this.edited_items, function(m){ return m.id; });

	        _.each(this.edited_items, function(model){
	            model.count_column = count_column;
	            if (model.make_column != 0){
	                model.make_column = 2;

	                var next = _.find(this.edited_items, function(m){
	                    return m.id == model.id+1 && m.make_column != 0 && m.get('type') == model.get('type');
	                });

	                while(next){
	                    next.make_column = 0;

	                    next.view.position = next.view.$el.position();
	                    next.view.position['width'] = next.view.$el.width();
	                    next.view.position['height'] = next.view.$el.height();
	                    next.view.line_height = $('span:first-child', next.view.$el).height();

	                    if (typeof(model.merged)!='undefined')
	                        model.merged.push(next);
	                    else
	                        model.merged = [next];

	                    var next_id = next.id+1;

	                    next = _.find(this.edited_items, function(new_m){
	                        return new_m.id == next_id && new_m.make_column != 0 && new_m.get('type') == next.get('type');
	                    });

	                    if (!next){
	                        next = this.check_page(next_id);
	                    }
	                }
	            }
	        }, this);



	        _.each(this.edited_items, function(m){
	            if (m.make_column == 2)	{
	                m.merged = _.uniq(m.merged);
	                m.makeColumn(m.count_column);
	            }
	        });

	        _.each(this.edited_items, function(m){ m.make_column = 1; });
	    },

	    /**
	     * Создаем колонки
	     * @param  {Number} count_column количество колонок
	     * @return {[type]}              [description]
	     */
	    makeColumn:function(count_column){ //1 | 2 | 3 | 4

	        var count_column = count_column || this.count_column || 1;

	        if (this.edited_items && _.size(this.edited_items)>0 && this.make_column != 2){

	            this.group_columns(count_column);

	        } else {

	            this.count_column = count_column;
	            this.page = this.get('container');
	            this.$v = this.view.$el;
	            this.available_height = this.page.content.height() - this.$v.position().top;

	            if (this.count_column > 1 && typeof(this.merged)!='undefined'){
	                _.each(this.merged, function(m){
	                    m.count_column = count_column;
	                });
	            }

	            if (this.count_column > 1 && typeof(this.merged)=='undefined') {
	                this.searchConsecutiveMultiColumn_2();
	            } else {
	                if (this.count_column == 1) { // reset view
	                    this.view.render();
	                    if (typeof(this.merged)!='undefined'){
	                        _.each(this.merged, function(m){
	                            m.count_column = 1;
	                            m.make_column = 1;
	                            m.view.render();
	                            m.view.$el.show();
	                        });
	                        this.merged = [];
	                    }
	                }
	            }

	            if (this.count_column == 1) {
	                this.restore_page();
	            } else {
	                if (this.make_column == 1 || this.make_column == 2){ // this columns draw in the prev paragraph

	                    this.all_width = this.page.$el.width();
	                    if (typeof(this.page.saved_view)=='undefined')	this.save_first_view();

	                    if (typeof(this.column_gap)=='undefined') this.column_gap = 5;
	                    this.column_width = parseInt((this.all_width-(this.column_gap*(count_column-1)))/count_column*10)/10;
	                    this.view.p = this.$v.position();
	                    this.$v.html('');
	                    this.columns = {};
	                    for (var i=1; i <= count_column; i++) {
	                        var gap = (i==count_column) ? 0 : this.column_gap;
	                        this.columns[i] = $('<column data-column="'+i+'"></column>').css({
	                            'width':parseInt(this.column_width)-1,
	                            'display':'block',
	                            'float':'left',
	                            'margin-right': gap + 'px',
	                            'min-height':'10px',
	                        }).appendTo(this.$v);
	                    }

	                    this.fill_column();
	                }
	            }
	        }
	    },

	    find_first:function(){
	        this.checked_id = this.id-1;
	        while(this.check_model()){
	            this.checked_id--;
	        }
	        return book.html_elements.get(this.checked_id+1);
	    },
	    find_next:function(){
	        this.checked_id = this.id+1;
	        if (typeof(book.edited_items)!='undefined' && _.size(book.edited_items)>0){
	            console.log(book.edited_items);
	        } else {
	            while(this.check_model()){
	                this.merged.push(book.html_elements.get(this.checked_id));
	                this.checked_id++;
	            }
	        }
	    },
	    check_model:function(){
	        var model = book.html_elements.get(this.checked_id);
	        if (typeof(model)=='undefined')
	            return false;
	        else
	        if (this.get('type') == model.get('type') && this.count_column == model.get('column')) return true;
	        else return false;
	    },
	    searchConsecutiveMultiColumn_2:function(){
	        var page = this.get('container').model;
	        if (page){
	            this.edited_items = _.filter(page.get('inside_collection').models, function(m){
	                return m.count_column && m.count_column == this.count_column;
	            }, this)
	        }

	        this.group_columns(this.count_column);
	    },
	    searchConsecutiveMultiColumn:function(){
	        var first = this.find_first();

	        if(typeof(first)!='undefined'){
	            first.merged = [];
	            first.find_next();
	            this.view.inset_menu.remove();
	            first.make_column = 1;

	            if(first.id!=this.id){
	                this.make_column = 0;
	                this.view.$el.hide();
	                first.makeColumn(this.count_column);
	            }
	        }
	    },
	    get_lines:function(){
	        this.lines = [];
	        this.content = this.get('content');
	        this.words_array = this.get('content').split(' ');

	        var line_height = $('span:first-child', this.view.$el).height(),
	            crossing_widget = this.get_widget();

	        while(this.words_array.length > 0){

	            var word =  this.words_array.shift(),
	                lines_word = [];

	            if (typeof(word)!='undefined'){
	                lines_word.push(word);
	                var line_width = this.column_width;
	                if (_.size(crossing_widget)>0) {
	                    console.log('accros -> as');
	                    line_width = this.get_line_width(crossing_widget, _.size(this.lines), line_height);
	                }

	                while( get_width(lines_word.join(' '), this.get('type')) < line_width && typeof(word)!='undefined' ){
	                    word = this.words_array.shift();
	                    lines_word.push(word);
	                }

	                if (get_width(lines_word.join(' '), this.get('type')) > line_width && lines_word.length>1){
	                    lines_word.pop();
	                    this.words_array.unshift(word);
	                }
	                this.lines.push(lines_word.join(' '));
	            }
	        }

	        if (typeof(this.merged)!='undefined' && _.size(this.merged)>0){
	            _.each(this.merged, function(m){
	                m.column_width = this.column_width;
	                m.lines = m.get_lines();
	                this.lines.push('&nbsp;');// добавим пустую строку для разрыва между параграфами
	                this.lines = this.lines.concat(m.lines);
	            }, this);
	        }

	        return this.lines;
	    },
	    get_empty_columns: function(total_columns) {
	        var columns = {};
	        for (j = 1; j <= total_columns; j++) {
	            columns[j] = [];
	        }
	        return columns;
	    },

	    get_words: function() {
	        var words = this.get('content').split(' ');

	        if (this.get('cut')>0){
	            words = words.slice(-this.get('cut'));
	        }

	        if (typeof(this.merged) != 'undefined' && _.size(this.merged) > 0) {
	            _.each(this.merged, function(m) {
	                m.view.$el.hide();
	                m.column_width = this.column_width;
	                var add_words = m.get('content').split(' ');
	                add_words.unshift('&nbsp;');
	                words = words.concat(add_words);
	            }, this);
	        }
	        return words;
	    },

	    /**
	     * Заполнение колонок текстом
	     * @return {[type]} [description]
	     */
	    fill_column:function(){
	        var total_columns = this.count_column,
	            columns = this.get_empty_columns(total_columns),
	            line_in_column = 1,
	            column = 1,
	            words = this.get_words(),
	            i = 0,
	            crossing_widget = this.crossing_widget = this.get_widget();

	        while( _.size(words)>0){
	            var line = [],
	                word = words.shift(),
	                span = new Backbone.View({ tagName:'span' });


	            line.push(word);

	            if (word != ' '){
	                this.columns[column].append(span.render().el);
	                span.$el.html(line.join(' '));
	                this.span_height = span.height = span.$el.height();
	                span.$el.css({
	                    'height':span.height,
	                    'display': 'inline-block',
	                    'white-space': 'nowrap'
	                });

	                while(
	                    span.$el.width()<this.get_width(columns, column, span) &&
	                        _.size(words)!=0 &&
	                        word != '&nbsp;'
	                    ){
	                    var word = words.shift();
	                    line.push(word);
	                    span.$el.html(line.join(' ')).css('text-indent',0);
	                }


	                if (span.$el.width()>this.get_width(columns, column, span)){
	                    // удалим последний элемент из строки и вернем его в массив всех слов
	                    words.unshift(line.pop());
	                    span.$el.html(line.join(' ')).width('100%');

	                    if (_.size(line)==0 && _.size(words)>0){
	                        span.$el.html('&nbsp;').width('100%');
	                    }
	                }

	                columns[column].push(line);
	                i++;

	                if (word == '&nbsp;'){ // разрыв строки для объединенных параграфов
	                    var new_span = new Backbone.View({tagName:'span'});
	                    this.columns[column].append(new_span.render().el);
	                    new_span.$el.html('&nbsp;').css({
	                        'height':span.height,
	                        'display': 'inline-block',
	                        'white-space': 'nowrap',
	                        'width':'100%'
	                    });
	                    if (_.size(columns[column]) != line_in_column) columns[column].push([' ']);
	                }

	                // увеличим количетво строк колонке и после этого перестроим все колонки
	                if (_.size(columns[column]) == line_in_column){
	                    column++;
	                    if (column > total_columns) {
	                        column = 1;
	                        if (_.size(words)>0){

	                            if (span.height * (line_in_column+1) > this.available_height) {
	                                this.set('cut', _.size(words));
	                                words = [];
	                            } else {
	                                words = this.get_words();
	                                columns = this.get_empty_columns(total_columns);
	                                for (x=1;x<=total_columns;x++){ this.columns[x].html(''); }
	                                line_in_column++;
	                            }
	                        }
	                    }
	                }
	            }
	        }
	    },

	    get_width: function(columns, column, span) {
	        var crossing_widget = this.crossing_widget;

	        if (_.size(crossing_widget)>0){

	            var p = this.columns[column].position(),
	                num_line = _.size(columns[column]) + 1,
	                column_pos = {
	                    h_start: p.left,
	                    h_end: p.left + this.column_width
	                },
	                line_pos = {
	                    v_start: (num_line - 1) * this.span_height + this.view.p.top,
	                    v_end: (num_line - 1) * this.span_height + this.span_height + this.view.p.top
	                },
	                img_pos = {
	                    v: { start: crossing_widget[0].top, end: crossing_widget[0].top + crossing_widget[0].height },
	                    h: { start: crossing_widget[0].left, end: crossing_widget[0].left + crossing_widget[0].width }
	                };

	            if (
	                (img_pos.v.start >= line_pos.v_start && img_pos.v.start <= line_pos.v_end) ||
	                    (img_pos.v.start <= line_pos.v_start && img_pos.v.end >= line_pos.v_end) ||
	                    (img_pos.v.end <= line_pos.v_end && img_pos.v.end >= line_pos.v_start)
	                ) {
	                if (column_pos.h_start <= img_pos.h.start && column_pos.h_end >= img_pos.h.start) {
	                    // console.log('HaccrosL-'+num_line, column);
	                    return img_pos.h.start - column_pos.h_start;
	                } else if (column_pos.h_start >= img_pos.h.start && column_pos.h_end <= img_pos.h.end) {
	                    // console.log('HaccrosAll-'+num_line, column);
	                    return 0;
	                } else if (
	                    column_pos.h_start >= img_pos.h.start &&
	                        column_pos.h_start <= img_pos.h.end && !column_pos.h_end <= img_pos.h.end
	                    ) {
	                    // console.log('HaccrosR-'+num_line, column);
	                    var new_width = column_pos.h_end - img_pos.h.end;
	                    span.$el.css('text-indent', this.column_width - new_width);
	                    return new_width;
	                } else return this.column_width;
	            } else return this.column_width;
	        } else return this.column_width;
	    },
	    get_widget:function(){
	        var widgets = this.get('container').model['curent_widget_collection'],
	            page = this.get('container').model.id,
	            crossing_widget = [];

	        this.view.position = this.view.$el.position();
	        this.view.position['width'] = this.view.$el.width();
	        this.view.position['height'] = this.view.$el.height();
	        this.view.line_height = $('span:first-child', this.view.$el).height();

	        if (_.size(widgets)>0){
	            _.each(widgets, function(widget){
	                if (widget.get('element_type') == 'inset'){

	                    var widget_element = _.find(book.html_elements.models, function(model) {
	                        return model.get('type') == 'inset' && model.get('container').model.id == page;
	                    });

	                    if (widget_element) {
	                        var placeholders = widget.get('placeholders');
	                        if (placeholders) {
	                            var placeholder = placeholders.find(function(p) {
	                                return p.get('selected') == 1;
	                            });


	                            if (placeholder) {
	                                var start_placeholder = parseInt(placeholder.get('top')),
	                                    end_placeholder = parseInt(placeholder.get('top')) + parseInt(placeholder.get('height')),
	                                    bottom = (this.view.position['top'] + this.view.position['height']);

	                                if (this.merged && _.size(this.merged) > 0) {
	                                    bottom_view = this.merged[_.size(this.merged) - 1].view;
	                                    bottom = (bottom_view.position['top'] + bottom_view.position['height']);
	                                }


	                                if (end_placeholder > this.view.position['top'] && start_placeholder < bottom) {
	                                    var placeholder_position = {};
	                                    _.each(placeholder.attributes, function(val, key) {
	                                        if (key != 'selected') placeholder_position[key] = parseInt(val);
	                                    });

	                                    crossing_widget.push(placeholder_position);
	                                }
	                            }
	                        }
	                    }

	                } else {

	                    var placeholders = widget.get('placeholders');
	                    if (placeholders) {
	                        var placeholder = placeholders.find(function(p){ return p.get('selected') == 1; });
	                        if (placeholder){
	                            var start_placeholder = parseInt(placeholder.get('top')),
	                                end_placeholder = parseInt(placeholder.get('top')) + parseInt(placeholder.get('height')),
	                                bottom = (this.view.position['top'] + this.view.position['height']);

	                            if (this.merged && _.size(this.merged)>0){
	                                bottom_view = this.merged[_.size(this.merged)-1].view;
	                                bottom = (bottom_view.position['top'] + bottom_view.position['height']);
	                            }

	                            if (end_placeholder>this.view.position['top'] && start_placeholder<bottom) {
	                                var placeholder_position = {};
	                                _.each(placeholder.attributes, function(val, key){
	                                    if (key != 'selected') placeholder_position[key] = parseInt(val);
	                                });

	                                crossing_widget.push(placeholder_position);
	                            }
	                        }
	                    }

	                }
	            }, this);
	        }
	        return crossing_widget;
	    },

	    /**
	     * Создаем вьюху нужного типа/вида
	     * @return {[type]} [description]
	     */
	    createView:function(){
	        this.type = this.get('type');

	        this.available_element_type = _.find(available_elements_type.models, function(model){
	            return model.get('name') == this.type;
	        }, this);

	        this.set('element_type',this.available_element_type);
	        this.set_content();

	        if (typeof(this.available_element_type)!='undefined'){
	            var view_name = this.view_name = this.available_element_type.get('view');

	            if (typeof(window[this.view_name]) == 'function'){
	                var FuncView;
	                if (this.view_name == 'title'){
	                    if(this.has('typeLvl')){
	                        var typeLvl = parseInt(this.get('typeLvl'));
	                        FuncView = window['H'+typeLvl+'View'];
	                    } else FuncView = window['TitleView'];
	                } else FuncView = window[this.view_name];

	                var view = new FuncView({model:this}, this.options),
	                    style = book.get('curent_style');

	                switch (this.type){
	                    case 'title':
	                        if (this.has('typeLvl')){
	                            var typeLvl = parseInt(this.get('typeLvl'));
	                            if (typeof(style.get('title_h'+typeLvl))!='undefined'){
	                                element_style = style.get('title_h'+typeLvl);
	                            }
	                        } else element_style = style.get('title_style');
	                        break;

	                    default:
	                        if	(
	                            typeof(this.get('element_type')) != 'undefined' &&
	                                typeof(this.get('element_type').get('themeContainer'))!='undefined'
	                            )
	                        {

	                            if (['illustration','video','audio','inset','latex'].indexOf(this.type) >-1){
	                                // was at that time in attributes "style"
	                                delete element_style;
	                            } else {
	                                element_style = style.get(this.get('element_type').get('themeContainer'));
	                                if (typeof(element_style) != 'undefined' && 'placeholders' in element_style){
	                                    view.placeholders = element_style.placeholders;
	                                    delete element_style.placeholders;
	                                }
	                            }
	                        }
	                }

	                if (typeof(element_style) != 'undefined'){
	                    if (_.isEqual(this.get('style'), element_style)){	// если устанавливается тот же стиль, то не срабатывает тригер
	                        view.render();
	                    } else {
	                        this.set('style',element_style); // рендер сработает тригер на change:style
	                    }
	                } else view.render();
	            }
	        } else {
	            var view = new PView({model:this}),
	                style = book.get('curent_style'),
	                element_style = style.get('text_style');

	            this.set('style',_.clone(element_style));
	        }

	        if (view){
	            view.$el.attr('data-type',this.type);
	            return view;
	        } else console.log('ERROR (need VIEW)!');
	    },

	    /**
	     * Вставка строк из кеша
	     * @param {Array} lines строки уже подготовленные для обтекания
	     * @return this
	     */
	    pasteLineFromCache:function(lines) {
	        var style = model.get('style') || {};
	        if (lines && _.size(lines)){
	            this.lines = new Backbone.Collection();
	            this.view.$el.html('');
	            _.each(lines, function(line) {
	                var span = new SpanModel(line);
	                this.view.$el.append(span.view.el).css(style);
	                this.lines.add(span);
	            }, this)
	        }
	    },

	    set_content:function(){
	        var content = test_pos(this.get('pos_start'), this.get('pos_end'));

	        // приведем перевод строки к одному виду
	        if (this.type == 'epigraph') content = content.split('<br />').join('<br>').split('<br/>').join('<br>');

	        this.set({content: content});
	    },
	    validate: function(attrs) {
	        if (attrs.pos_end < attrs.pos_start) {
	            return "Конечная координата не может быть больше начальной ("+attrs.pos_start+"-"+attrs.pos_end+")";
	        }

	        if (typeof(attrs.element_type)=='undefined'){
	            return "Неопдределен тип элемента";
	        }

	        if (!attrs.element_type.has('title') || !attrs.element_type.has('view') || !attrs.element_type.has('name')) {
	            console.log(attrs.element_type)
	            return "Неправильный тип элемента";
	        }
	    },
	    update_style:function(){
	        this.first_style.set(this.get('style'));
	        this.setFontStyle();
	        this.save_changes();
	        if (typeof(this.get('style'))!= 'undefined' &&
	            typeof(this.get('style')['position'])!='undefined' &&
	            this.get('style')['position'] == 'absolute'
	            ){
	            console.log('new propierties without render page');
	        } else {
	            book.view.render();
	        }
	    },
	    setFontStyle:function(){
	        _.each(this.get('style'), function(value,name){
	            if (name != 'top' && name != 'left' && name != 'position'){
	                this.view.$el.css(name, value);
	            }
	        }, this);
	    },
	    save_changes:function(){
	        if (typeof(this.first_style) == 'undefined'){
	            this.first_style = new Backbone.Model(this.get('style'));
	            this.first_style.on('change',function(style){
	                //TO DO add changes to  CHANGES element_type.style
	                book.save_changes_local(
	                    style.changedAttributes(),
	                    this.get('element_type').get('themeContainer'),
	                    this.get('style')
	                );
	            }, this);
	        }
	    },
	    save_curent_position:function(){
	        book.save_placeholders_local(this)
	    },
	    select_placeholder:function(){
	        if(this.has('placeholders') && this.get('placeholders').length > 0){
	            this.placeholder = (this.get('placeholders').length > 1)
	                ? _.find(this.get('placeholders').models, function(model){ return model.get('selected') == 1; }) // find selected
	                : this.get('placeholders').at(0) // or find first

	            if (typeof(this.placeholder)!= 'undefined'){

	                this.style = _.clone(this.placeholder.attributes);
	                delete this.style.selected;

	                this.set({
	                    style:this.style,
	                    height_start:parseInt(this.style.top),
	                    height_stop:parseInt(this.style.top) + parseInt(this.style.height),
	                    width_start:parseInt(this.style.left),
	                    width_stop:parseInt(this.style.left) + parseInt(this.style.width)
	                });

	                switch(this.get('type')){
	                    case 'inset':
	                        this.view = new InsetView({model:this});
	                        break
	                    case 'latex':
	                        this.view = new LatexView({model:this});
	                        break
	                    case 'illustration':
	                    default:
	                        this.view = new CoverPicView({model:this});
	                }

	                this.view.render();
	            } // else console.log('this.placeholder undefined, ', this.get('placeholders'));
	        } // else console.log('Для элемента нет места (пласехолдеров)');
	    },
	    mark_search_result:function(m){
	        this.mark_preview_search_result(m);
	        this.mark_view_search_result(m);
	    },
	    demark_search_result:function(m){
	        this.demark_view_search_result(m);
	        this.demark_preview_search_result(m);
	    },
	    demark_preview_search_result:function(m){
	        var preview_content = this.preview.$el.html();
	        var new_preview_content = preview_content.split('<span class="yellow">'+m.get('q')+'</span>').join(m.get('q'));
	        this.preview.$el.html(new_preview_content);
	    },
	    mark_preview_search_result:function(m){
	        var preview_content = this.preview.$el.html();
	        var new_preview_content = preview_content.split(m.get('q')).join('<span class="yellow">'+m.get('q')+'</span>');
	        this.preview.$el.html(new_preview_content);
	    },
	    mark_view_search_result:function(m){
	        var q = m.get('q');
	        if (typeof(this.lines)!='undefined' && this.lines.length>0){
	            this.lines.each(function(line_model){
	                var text = line_model.get('text');
	                if (text.indexOf(q)>-1){
	                    line_model.view.$el.html(text.split(q).join('<span class="yellow">'+q+'</span>'))
	                }
	            });
	        } // else console.log('dont lines', this);
	    },
	    demark_view_search_result:function(m){
	        var q = m.get('q');
	        if (typeof(this.lines)!='undefined' && this.lines.length>0){
	            this.lines.each(function(line_model){
	                var text = line_model.get('text');
	                if (text.indexOf(q)>-1){
	                    line_model.view.$el.html(line_model.get('text'))
	                }
	            });
	        }
	    },
	    set_link_to_widget:function(cid,start,end){
	        var newLink = {}
	        newLink.cid = cid
	        newLink.start = start
	        newLink.end = end
	        newLink.paragraphId = this.id
	        this.links_to_widgets.push(newLink)
	        this.view.remove_edit_menu();
	        delete book.edited;
	        this.view.add_widget_links()
	    }
	})

});