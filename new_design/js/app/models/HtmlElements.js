define(function(require) {

	"use strict";

	var HtmlElement = require('app/models/HtmlElement');


	return Backbone.Collection.extend({
		model: HtmlElement,
		initialize: function() {
			this.comparator;
		},
		comparator: function(model) {
			return model.get("pos_start");
		}
	})

});