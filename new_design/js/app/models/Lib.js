define(function (require) {

    "use strict";

    var $           = require('jquery'),
        Backbone    = require('backbone'),
        Book        = require('app/models/Book');


    return Backbone.Collection.extend({

        model:Book,

        url: "/books/",

        initialize: function(app){

            var app = this.app = app;

            this.on('set_book', function(book){
                app.book = book;
            }, this);

            this.fetch({
                reset:true,
                error:function(collection,response){
                    if (response && response.status == 401) {
                        app.errorMsg.show('Ошибка загрузки библиотеки <br><a href="/"><small class="text-danger">'+response.responseJSON.error+'</small></a>');
                        $('.modal-footer, .modal-header').remove();
                    } else {
                        app.errorMsg.show('Ошибка загрузки библиотеки <br><small class="text-danger">'+response.responseJSON.error+'</small>');
                    }
                }
            });
        },

        showLib:function(){
            var collection = this;
            require(["app/views/LibView"], function (LibView) {
                collection.view = new LibView({collection:collection});
            });
        },

        selectBook:function(book_uid){
            var book = this.get(book_uid);
            if (book) book.set('selected',1);
            return book;
        }

    })

});