define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        fileupload          = require('app/jquery.fileupload'),
        iframe_transport    = require('app/jquery.iframe-transport'),
        MediaLibItemModel   = require('app/models/MediaLibItem'),

        video_server = 'http://?????',
		upload_point = '/upload/',
		media_conf = {
			upload_video:			video_server + upload_point +'uploadVideo/',
			get_video:				video_server + upload_point +'getVideo/',
			get_video_clipart:		video_server + upload_point +'getVideoClipArts/',
			upload_video_clipart:	video_server + upload_point +'uploadVideoClipArt/',
			create_video_folder:	video_server + upload_point +'createVideoFolder/',
			delete_video_folder:	video_server + upload_point +'deleteVideoFolder/',
			delete_video_clipart:	video_server + upload_point +'deleteVideoClipArtFile/',
			delete_video:			video_server + upload_point +'deleteVideoFile/',
			upload_image: 			upload_point+'uploadImage/',
			delete_image:			upload_point+'deleteImageFile/',
			delete_image_clipart:	upload_point+'deleteImageClipArtFile/',
			upload_image_clipart:	upload_point+'uploadImageClipArt/',
			get_image_clipart: 		upload_point+'getClipArts/',
			create_image_folder:	upload_point+'createImageFolder/',
			delete_image_folder:	upload_point+'deleteImageFolder/',
			get_image: 				upload_point+'getImage/',
			upload_audio:			upload_point+'uploadMultimedia/',
			upload_audio_clipart:	upload_point+'uploadMultimediaClipArt/',
			delete_audio:			upload_point+'deleteMultimediaFile/',
			delete_audio_clipart:	upload_point+'deleteMultimediaClipArtFile/',
			get_audio:				upload_point+'getMultimedia/',
			get_audio_clipart:		upload_point+'getMultimediaClipArts/',
			create_audio_folder:	upload_point+'createMultimediaFolder/',
			delete_audio_folder:	upload_point+'deleteMultimediaFolder/',
			set_tag:				upload_point+'setFileTags/'
		},
		curent_folder = '';

    return Backbone.Collection.extend({

    	model:MediaLibItemModel,

	    initialize: function (options) {
            this.type = options.type || 'pictures';
            this.view = options.view;
            this.clipart = !!options.clipart;
            this.book = options.book;
            this.inner_dir = 0;

            this.config = media_conf;

           	this.dir_view = options.dir_view;
           	this.dir_view.collection = this;

			this.on('change_url', this.update, this);
            this.trigger('change_url', this.getUrl());
        },

        getUrl:function(){
        	switch (this.type){
            	case 'micro':
            		return media_conf.get_audio;
            		break;

            	case 'camera':
            		return media_conf.get_video;
            		break;
            		
            	case 'prezent': // TODO
            		return media_conf.get_image;
            		break;
            		
            	case 'pictures':
            	default:
            		return this.clipart ?  media_conf.get_image_clipart : media_conf.get_image;
            }
        },

        update: function(url){
			this.url = url || this.url;
			this.fetch({
				success: function(collection) {
					collection.success();
				},
				error: function(collection) {
					collection.err();
				}
			})
        },

        render:function(){
        	/* 
        	 * Если внутренняя папка обновим список файлов, 
        	 * если корневая папка создадим новые вьюхи.
        	 */
/*        	if (this.inner_dir != 1) {
	            this.view.render();
	           	this.dir_view.render();
	           	this.dir_view.collection = this;
        	}*/

        	this.view.medialist.empty();

        	this.each(function(model){
        		model.view.collection = this;
        		if (model.get('type') != 'dir') {
        			this.view.medialist.append(model.view.el);
        		} else {
        			if (this.inner_dir != 1) {
        				if (this.clipart){
        					$('.dirs.clipart_dir', this.dir_view.$el).append(model.view.el);
        				} else {
        					$('.dirs.user_dir', this.dir_view.$el).append(model.view.el);
        				}
        			}
        		}
        	}, this);
        },

        success:function(){
            this.render();
        },

        err:function(){
        	$('.popover .popover-content').html('Ошибка загрузки библиотеки');
        }

    });

});