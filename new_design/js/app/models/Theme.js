define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        ThemeBtn = require('app/views/ThemeBtn');

    return Backbone.Model.extend({
        initialize: function () {
            this.btn = new ThemeBtn({model:this});
            this.on('change:selected', this.afterSelect, this);
        },

        afterSelect:function(model,value){

            
            if (value == 1) {
                
                this.btn.$el.addClass('selected');

                // unselect other model
                _.chain(this.collection.without(this)).invoke('unselect');
                
                Backbone.history.navigate('book'+this.collection.book_uid+'/theme'+this.id, true);
            } else {
                this.btn.$el.removeClass('selected');
            }

        },
        unselect:function(){
            this.set({'selected':0});
        }
    })

});