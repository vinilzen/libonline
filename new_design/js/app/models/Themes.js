define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        Theme       = require('app/models/Theme'),
        ThemesView  = require('app/views/ThemesView');


    return Backbone.Collection.extend({

        model:Theme,

        url: "/templates/getBaseTemplates/",

        initialize: function(options){

            // for create url to #book1385381143/theme1/edit
            this.book_uid = options.book_uid;

            this.fetch({
                reset:true,
                error:function(){
                    console.log('error fetch themes')
                }
            });
        },

        showThemes:function(){
            this.showed = 1;
            this.view = new ThemesView({collection:this});
            this.trigger('showed');
        }

    })

});