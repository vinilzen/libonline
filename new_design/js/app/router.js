define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        Lib                 = require('app/models/Lib'),
        Themes              = require('app/models/Themes'),
        Header              = require('app/views/Header'),
        ErrorMsg            = require('app/views/ErrorMsg'),
        Preloader           = require('app/views/Preloader');

    var preloader = new Preloader();

    return Backbone.Router.extend({

        initialize:function(){
            this.lib = new Lib(this);           
            this.header = new Header();
            this.errorMsg = new ErrorMsg();
        },

        routes: {
            '': 'home',
            'book:book_uid':'showTheme',
            'book:book_uid/theme:theme_id':'showBtnGoEdit',
            'book:book_uid/theme:theme_id/edit':'edit',
            'editor':'edit',
        },

        home: function () {
            this.lib.on('reset', this.lib.showLib());
        },

        edit: function(book_uid, theme_id) {
            
            if (this.lib && this.lib.length > 0 ){
                this.initEditor(book_uid, theme_id);
            } else {
                this.lib.on('reset',function(){
                    this.initEditor(book_uid, theme_id);
                }, this);
            }
        },

        initEditor:function(book_uid, theme_id){

            preloader.show();

            var self = this;
            require(['app/views/MediaLibIcos','app/views/Editor'], function (MediaLibIcosView, Editor) {

                var book = self.lib.selectBook(book_uid);

                console.log(book);

                self.mediaLibIcos = new MediaLibIcosView({book:book});

                self.editor = new Editor({
                    book:self.book,
                    theme_id:theme_id,
                    app:self
                });

                self.editor.on('AllPagePrinted', function(){
                    preloader.hide();
                });
            });
        },

        showTheme: function(book_uid) {

            if (this.lib && this.lib.length > 0 ){
                this.initTheme(book_uid);
            } else {
                this.lib.on('reset',function(){
                    this.initTheme(book_uid);
                }, this);
            }
        },

        initTheme:function(book_uid){
            this.themes = new Themes({book_uid:book_uid});
            this.themes.on('reset', function(collection){
                collection.showThemes()
            }, this);
        },

        showBtnGoEdit:function(book_uid, theme_id){

            var btnView = Backbone.View.extend({
                tagName:'a',
                className:'go_edit',
                events:{ click:'goEdit' },
                goEdit:function(){
                    $(document.body).removeClass('modal_open');
                    $('.modal, .modal-backdrop').remove();
                    Backbone.history.navigate('book'+book_uid+'/theme'+theme_id+'/edit', true);
                },
                render:function(){
                    this.$el.html('Редактировать');
                    return this;
                }
            });
            var btn = new btnView();

            if (this.themes && this.themes.length > 0) {
                if (this.themes.showed){
                    $('.modal-body').append(btn.render().el);
                } else {
                    this.themes.showThemes();
                    this.themes.on('showed', function(){
                        this.themes.get(theme_id).set('selected',1);
                        $('.modal-body').append(btn.render().el);
                    }, this)
                }
            } else {
                this.initTheme(book_uid);
                this.themes.on('showed', function(){
                    this.themes.get(theme_id).set('selected',1);
                    $('.modal-body').append(btn.render().el);
                }, this)
            }
        }
    });

});