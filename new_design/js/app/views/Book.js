define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone');

    return Backbone.View.extend({

        className: "book",
        
        initialize: function () {
            console.log('init book');
        }
        
    });

});