define(function (require) {

    "use strict";

    var PageView        = require('app/views/PageView'),
        PagesPreView    = require('app/views/PagesPreView'),
        PagePreview     = require('app/views/PagePreview'),
        PageModel       = Backbone.Model.extend({
            initialize:function(){
                this.view = new PageView({model:this});
                this.preview = new PagePreview({model:this});
            }
        });

    return Backbone.View.extend({

        el:'.page_container',
       
        initialize: function(){
            this.$el.empty();
            this.model.pages = new Backbone.Collection();
        },

        render:function(){
            this.previews = new PagesPreView();
            return this;
        },

        createPages:function(){
            var num_page = 1;

            $('#main').attr('style','overflow:visible; z-index:10');

            while (this.haveSomethingToPrint() && num_page<6){

                var page_model = new PageModel({num_page:num_page});

                page_model.view.book = this.model;

                this.$el.append(page_model.view.render().el);

                page_model.view.fill();

                this.previews.$el.append(page_model.preview.el);


                html2canvas(page_model.view.el, {
                    onrendered: function(canvas) {
                        
                        var context = canvas.getContext('2d'),
                            oldCanvas = canvas.toDataURL("image/png"),
                            img = new Image();
                        
                        img.src = oldCanvas;
                        img.width = 55;
                        img.height = 77;

                        // page_model.preview.el.appendChild(img)
                        $('.prev-page', page_model.preview.$el)[0].appendChild(img)
                        // console.log(img, $('.prev-page', page_model.preview.$el)[0])
                        //$('.prev-page', page_model.preview.$el).html(img);
                        
                        // canvas.height = 77;
                        // canvas.width = 55;
                    }
                });

                this.model.pages.add(page_model);
                num_page++;
            }
            
            $('#main').attr({style:' '});
            this.model.editor.trigger('AllPagePrinted');
        },

        // есть ли еще не напечатанные элементы
        haveSomethingToPrint:function(){
            var not_printed_item = this.findNotPrinted()

            if (typeof(not_printed_item)!='undefined'){
                return true;
            } else {
                return false;
            }
        },

        findNotPrinted:function(){
            
            return _.find(this.model.html_elements.models, function(element){
                return  (
                            element.get('type') == 'paragraph' ||  
                            element.get('type') == 'title' || 
                            element.get('type') == 'epigraph' || 
                            element.get('type') == 'introduction_content' || 
                            element.get('type') == 'introduction' || 
                            element.get('type') == 'volume'
                        ) && // сначала пробуем напечатать только параграфы и заголовки
                        element.get('printed') == 0
            });
        }
    });

});