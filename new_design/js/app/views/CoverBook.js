define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone');

    return Backbone.View.extend({
        tagName:'a',
        className: 'book-cover-btn',
        events:{
            'click':'selectBook'
        },
        initialize: function () {
            this.render();
        },
        render:function(){
            this.$el.html(this.model.get('name') + ' ('+this.model.get('author')+')');
            return this;
        },
        selectBook:function(){
            this.model.set('selected',1);

            Backbone.history.navigate('book'+this.model.get('book_uid'), true);
        }
    });

});