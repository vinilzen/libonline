define(function (require) {

    "use strict";

    var TitleEditor     = require('app/views/TitleEditor'),
        TextEditor      = require('app/views/TextEditor'),
        SearchView      = require('app/views/SearchView');

    return Backbone.View.extend({

        el:'#main',

        className: "editor",
        
        initialize: function (options) {
            var app = this.app = options.app;

            if (options.book){
                this.book = options.book;
                this.book.editor = this;


                this.book.on('change', this.start, this);

                // this.book.on('parse', this.book.view.render(), this);

                this.book.fetch({
                    reset:true,
                    error:function(collection,response){
                        if (response && response.status == 401) {
                            app.errorMsg.show('Ошибка загрузки библиотеки <br><a href="/"><small class="text-danger">'+response.responseJSON.error+'</small></a>');
                            $('.modal-footer, .modal-header').remove();
                        } else {
                            app.errorMsg.show('Ошибка загрузки библиотеки <br><small class="text-danger">'+response.responseJSON.error+'</small>');
                        }
                    }
                })
            } else {
                // app.errorMsg.show('Ошибка чтения содержимого книги');
                app.errorMsg.show('Книга не найдена<br><a href="/index/"><small class="text-danger">Вернуться в библиотеку</small></a>');
                $('.modal-footer, .modal-header').remove();
            }
        },
        
        start:function(){

            console.log('book change - start');

            this.titleEditor = new TitleEditor({model:this.book});
            this.textEditor = new TextEditor({model:this.book});
            this.search = new SearchView({model:this.book});
        }
    });

});