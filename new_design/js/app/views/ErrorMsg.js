define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
    	initialize:function(){
    		this.render();
    	},
    	render:function(){
    		return this;
    	},
    	show:function(text){
    		this.$el.modal('Ошибка', text);
    	},
    	hide:function(){
    		this.$el.closeModal();
    	}
    })
})