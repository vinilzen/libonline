define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({

        el: "#top",
        
        events:{
            'click .controls':'toggleHead'
        },

        initialize: function () {
            this.render();
        },

        render:function(){
            return this;
        },

        toggleHead:function(){
            if (this.minimalize != 1){
                this.$el.height('25px');
                $('.row', this.$el).hide();
                $('#main, #left-side, #right-side').css('top','38px')
                this.minimalize = 1;
                $('.up', this.$el).addClass('down');
            } else {
                $('.row', this.$el).show();
                this.$el.height('175px');
                $('#main, #left-side, #right-side').css('top','165px')
                this.minimalize = 0;
                $('.up', this.$el).removeClass('down');
            }
        }
    });

});