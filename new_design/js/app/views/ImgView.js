define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
    	className:'media_pic',
    	events:{
    		'mouseenter':'hoverOn',
    		'mouseleave ':'hoverOff',
    		'click .gear':'showOption',
    		'click ul a':'delImg',
    	},
    	initialize:function(options){
    		this.src = options.src;
    		console.log(this.src);
    		this.render();
    	},
    	render:function(){
    		this.$el.html(
    			'<img width="300" height="200">'+
    			'<span class="gear"></span>'+
    			'<ul><li><a>Удалить фото</a></li></ul>'
    		);
    		$('img', this.$el).attr({ src:this.src });
    		return this;
    	},
    	hoverOn:function(){
    		this.$el.addClass('selected_red');
    	},
    	hoverOff:function(){
    		this.$el.removeClass('selected_red');
    	},
    	delImg:function(){
    		this.remove();
    	},
    	showOption:function(el){
    		if ($(el.target).hasClass('active')){

	    		$(el.target).removeClass('active');
	    		$('ul', this.$el).fadeOut();

    		} else {

	    		$(el.target).addClass('active');
	    		$('ul', this.$el).fadeIn();

    		}
    	}
    });
});