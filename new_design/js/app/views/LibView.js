define(function (require) {

    "use strict";

    var modal = require('modal');

    return Backbone.View.extend({

        className: "lib",
        
        initialize: function () {
            this.render();
        },

        render:function(){
            
            this.$el.empty();
            this.$el.modal('Мои книжки', '');

            if (this.collection.length > 0){
                this.collection.each(function(model){
                    this.$el.append(model.coverView.el);
                }, this);
                $('.modal-body').html(this.el);
            } else {
                $('.modal-body').html('У вас еще нет книжек<br><a href="/index/">вернуться в Библиотеку</a>');
            }

            

            return this;
        },        
    });

});