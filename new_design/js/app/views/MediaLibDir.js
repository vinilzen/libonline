define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone');

    return Backbone.View.extend({

        tagName: "li",

        events: {
            click:'goToFolder'
        },

        initialize: function () {
            this.model.on("change", this.render, this);
            this.render();
        },

        render: function () {
            this.$el.html('<div class="stick"></div><span>'+this.model.get('name')+'</span>');
            return this;
        },

        goToFolder:function(){
            this.collection.dir_view.removeActive();
            this.$el.addClass('active');
            this.collection.trigger('change_url', this.collection.getUrl() + '?folder='+this.model.get('name'));
            this.collection.inner_dir = 1;
        }

    });

});