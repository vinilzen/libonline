define(function (require) {

    "use strict";

    var popover             = require('popover'),
        MediaLib            = require('app/models/MediaLib'),
        PlaceholdersView    = require('app/views/MediaPlaceholders'),
        tpl                 = require('text!tpl/MediaLibIco.html'),
        medialib_tpl        = require('text!tpl/MediaLib.html'),
        tpl_dir             = require('text!tpl/MediaLibDir.html'),
        template_dir        = _.template(tpl_dir),
        template            = _.template(tpl),
        template_medialib   = _.template(medialib_tpl);

    // обретка для картинок
    var LibView             = Backbone.View.extend({
        className:'main',
        initialize:function(){
            this.render();
            this.medialist = $('.select-media', this.$el);
        },
        render:function(){
            this.$el.html(template_medialib());
            return this;
        }
    });


    // обретка для дерева файлов
    var DirView = Backbone.View.extend({
        className:'dir',
        events:{
            'click .root_dir':'goToRootDir'
        },
        initialize:function(){
            this.render();
        },
        render:function(){
            this.$el.html(template_dir());
            return this;
        },
        goToRootDir:function(){
            this.removeActive();
            
            $('.dirs', this.$el).empty();

            this.collection.inner_dir = 0;
            this.collection.trigger('change_url', this.collection.getUrl());
        },
        removeActive:function(){
            $('.dirs li', this.$el).removeClass('active');
        }
    });


    return Backbone.View.extend({

        tagName: "li",

        events: {
            click:'showLib'
        },

        initialize: function () {
            this.model.on("change", this.render, this);
        },

        render: function () {
            this.$el.html(template(this.model.attributes));
            return this;
        },

        showLib: function () {

            var medialib_view = new LibView(),
                mediadir_view = new DirView();

            this.lib = new MediaLib({
                type:this.model.get('name'),
                book:this.book,
                view:medialib_view,
                dir_view:mediadir_view,
            });

            this.lib_clipart = new MediaLib({
                type:this.model.get('name'),
                book:this.book,
                view:medialib_view,
                dir_view:mediadir_view,
                clipart:1
            });

            if ( placeholders_view ) placeholders_view.remove();

            var placeholders_view = new PlaceholdersView({
                config:this.lib.config,
                book:this.book,
                lib:this.lib
            });

            this.$el.popover(null,'<img src="./new_design/img/preloader.gif" class="preloader" />','left', 'medialib pic', null, 1);

            $('.popover .popover-content')
                    .html(mediadir_view.el)
                    .append(medialib_view.el)
                    .append(placeholders_view.el);
        }

    });

});