define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        MediaIcoView        = require('app/views/MediaLibIco'),

		curent_folder = '',
		init_collection = new Backbone.Collection([
			{name:'prezent', title:'slides'},
			{name:'micro', title:'audio'},
			{name:'camera', title:'video'},
			{name:'pictr', title:'pictures'},
		]),
		property_value_view_widget = 0;

	    return Backbone.View.extend({
	    	el:'.media ul',
			initialize:function(options){
				this.book = options.book;
				this.render();
			},
	        render: function () {
	        	this.$el.empty();
	        	_.each(init_collection.models, function(model){
	        		model.view = new MediaIcoView({model:model});
	        		model.view.book = this.book;
	        		this.$el.append(model.view.render().el);
	        	}, this);
	            return this;
	        }

	    });

});



function getCookie(name){
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}