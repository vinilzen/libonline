define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/MediaLibItem.html'),
        tpl_video           = require('text!tpl/MediaLibItemVideo.html'),
        tpl_audio           = require('text!tpl/MediaLibItemAudio.html'),

        template            = _.template(tpl),
        template_video      = _.template(tpl_video),
        template_audio      = _.template(tpl_audio);

    return Backbone.View.extend({

        tagName: "li",

        initialize: function () {
            this.model.on("change", this.render, this);
            this.render();
        },

        render: function () {
            this.$el.html(this.getContent());
            return this;
        },

        getContent:function(){
            switch (this.model.collection.type){
                case 'camera':
                    return template_video(this.model.attributes);

                case 'micro':
                    return template_audio(this.model.attributes);

                case 'pictr':
                default:
                    return template(this.model.attributes);
            }
        }

    });

});