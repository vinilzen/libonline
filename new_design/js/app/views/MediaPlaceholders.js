define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        ImgView             = require('app/views/ImgView'),
        tpl                 = require('text!tpl/MediaPlaceholders.html'),

        template            = _.template(tpl);

    return Backbone.View.extend({

        className: "placeholders",

        events:{
            'click .upload':'fileUpload',
            'click .page_placeholders li':'selectPosition',
            'click .paste':'pasteMedia'
        },

        initialize: function (options) {
            var lib = options.lib;
            this.book = options.book;

            this.render();
            $('#fileupload', this.$el).attr('data-url',options.config.upload_image);

            $('#fileupload', this.$el).fileupload({
                dataType: 'json',
                done: function (e, data) {
                    if (data.result && data.result.error){
                        console.log(data.result.error);
                        return false;
                    } else if (data.result) {
                        lib.update();
                    }
                },
                dropZone: $('.upload'),
                drop: function(e, data){
                    console.log(data.files);
                }
            }).error(function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            });
        },

        fileUpload:function(){
            $('#fileupload', this.$el).click();
        },

        render: function () {
            this.$el.html(template());
            return this;
        },

        selectPosition:function(e){
            $(e.target).addClass('active');
            this.pl = $(e.target).data('placement');
        },

        pasteMedia:function(e){
            var src = $('.select-media input:checked').attr('data-src');
            if (src) {
                var page = _.find(this.book.pages.models, function(m){
                    return m.get('selected');
                });
                if (!page){
                    console.log('not found page')
                    page = this.book.pages.models[0]
                } 
                
                var img = new ImgView({src:src}),
                    spacer = $('<div class="spacer">').css({
                          'height': '680px',
                          'width': '0px',
                          'float': 'right',
                    });

                $('.container', page.view.$el).prepend(img.el);

                switch (this.pl) {
                    case 'tl':
                        img.$el.css({'float':'left'})
                        break;
                    case 'tr':
                        img.$el.css({'float':'right'})
                        break;
                    case 'bl':
                        $('.container', page.view.$el).prepend(spacer);
                        img.$el.css({'float':'left'})
                        break;
                    case 'br':
                        $('.container', page.view.$el).prepend(spacer);
                        img.$el.css({'float':'right'})
                        break;
                }

                $('[data-popover="1"]').attr('data-popover',0);
                $('.popover').remove();

            } else {
                alert('Нужно выбрать изображение для вставки')
            }
        }

    });

});