define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        scrollto = require('scrollto');

    return Backbone.View.extend({

        tagName:'li',

        template:_.template(
            '<div class="prev-page">'+
                '<img src="/new_design/img/sempl_prev<%= num_page_cover %>.png" />'+
            '</div>'+
            '<span class="num-page"><%= num_page %></span>'
        ),

        events:{
            'click':'scrollTo'
        },

        initialize: function(){
            this.render();
        },

        render:function(){
            var num_page_cover = this.model.get('num_page')

            if (this.model.get('num_page')>8)  num_page_cover = this.model.get('num_page')%8+1;

            this.$el.html(this.template({
                num_page:this.model.get('num_page'),
                num_page_cover:num_page_cover
            }));
            return this;
        },

        scrollTo:function(){
            $('#main').scrollTo('.page[data-page='+this.model.get("num_page")+']', 700);
        }

    });

});