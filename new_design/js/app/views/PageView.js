define(function (require) {

    "use strict";

    var Backbone                = require('backbone'),
        TextView                = require('app/views/elements/Text'),
        Volume                  = require('app/views/elements/Volume'),
        Genre                   = require('app/views/elements/Genre'),
        Epigraph                = require('app/views/elements/Epigraph'),
        Introdaction            = require('app/views/elements/Introdaction'),
        IntrodactionContent     = require('app/views/elements/IntrodactionContent'),
        TitleView               = require('app/views/elements/Title');

    return Backbone.View.extend({

        className:'page',

        template: _.template(
            '<div class="container"></div>'+
            '<div class="page-tools">'+
                '<span class="ico select"></span><span class="ico text"></span>'+
            '</div>'
        ),

        events:{
            'click':'selectPage'
        },
        
        initialize: function(){
        },

        render:function(){
            this.$el.html(this.template()).attr('data-page',this.model.get('num_page'));

            this.container = $('.container', this.$el).css('margin','85px 145px');
            return this;
        },

        selectPage:function(){
            this.model.set('selected',1);
            var cid = this.model.cid;
            this.model.collection.each(function(m){
                if (m.cid != cid) {
                    m.set('selected',0)
                }
            })
        },

        fill:function(){

            while (this.book.view.haveSomethingToPrint() && this.container.height()+250 < this.$el.height()) {
                var element = this.book.view.findNotPrinted();

                if (element) {
                    switch (element.get('type')){
                        case 'title':
                            element.view = new TitleView({model:element});
                            break;
                        case 'volume':
                            element.view = new Volume({model:element});
                            break;
                        case 'epigraph':
                            element.view = new Epigraph({model:element});
                            break;
                        case 'introduction':
                            element.view = new Introdaction({model:element});
                            break;
                        case 'introduction_content':
                            element.view = new IntrodactionContent({model:element});
                            break;
                        case 'paragraph':
                        default:
                            element.view = new TextView({model:element});
                    }

                    this.container.append(element.view.el);

                    if (this.container.height()+250 < this.$el.height()) {
                        element.set('printed',1);
                    } else {
                        element.view.remove();
                        break;
                    }
                }


            }

            return this;
        }
    });

});