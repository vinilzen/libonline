define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({

        el:'.prev-pages',

        initialize: function(){
            this.render();
        },

        render:function(){
            this.$el.empty();
            return this;
        },

    });

});