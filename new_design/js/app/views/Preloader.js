define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
    	initialize:function(){
    		this.render();
    	},
    	render:function(){
    		this.$el.html(
    			'<h3 style="color:#000;">Загрузка редактора</h3><hr>'+
    			'<div class="progress progress-striped active">'+
  					'<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>'+
				'</div>'
    		);
    		return this;
    	},
    	show:function(text){
    		this.$el.modal(text, '');
    		$('.modal-footer, .modal-header').remove();
    		$('.modal-body').html(this.el);
    	},
    	hide:function(){
    		this.$el.closeModal();
    	}
    })
})