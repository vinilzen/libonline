define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        popover = require('popover');


    var SearchResultView = Backbone.View.extend({
        tagName:'dd',
        initialize:function(){
            this.render();
        },
        render:function(){

            return this;
        }
    });

    var SearchResultsView = Backbone.View.extend({
        tagName:'dl',
        initialize:function(){
            this.render();
            this.dl = $('dl', this.$el);
        },
        render:function(type){
            type = type || 'тексте';
            this.$el.html('<dl><dt>В '+type+':</dt></dl>');
            return this;
        }
    });

    return Backbone.View.extend({

        el:'.search',
        
        events: {
            'focus input':'showResult',
            'blur input':'hideResult',
            'keypress input':'start_search_enter'
        },

        initialize: function(){
            this.render();
            this.positions = [];
            this.input = $('input', this.$el);
        },

        render:function(){
            // this.$el.html('')
            return this;
        },

        showResult:function(){
            this.input.popover('Все результаты',
                '<dl><dt>В тексте:</dt><dd><strong>Desire For A Beter </strong><a>стр. 61</a></dd><dd><strong>Desire For A Beter </strong><a>стр. 121</a></dd><dd><strong>Desire For A Beter </strong><a>стр. 139</a></dd></dl><dl><dt>В заголовках:</dt><dd><strong>Desire For A Beter </strong><a>стр. 61</a></dd></dl><dl><dt>В подписях:</dt><dd><strong>Desire For A Beter </strong><a>стр. 115</a></dd></dl>',
                'top', 'search-result', true);
        },

        hideResult:function(){
            this.input.attr('data-popover',0);
            $('.popover').remove();
        },
        start_search_enter:function(e){
            if (e.keyCode == 13){
              this.start_search()
            }
        },
        start_search:function(){
            this.q = this.input.val();
            if (this.q!='' && this.q.length > 2){

              if (typeof(this.result_view)!='undefined'){
                this.result_view.close();
              }

              this.positions = this.search(this.q);

              if (this.positions.length>0){

                console.log(this.positions);

                // remove old marked phrase
                if (typeof(this.result_view)!='undefined' && typeof(this.result_view.result)!='undefined'){
                  this.result_view.result.demarked();
                }

                this.result_view = new SearchResultsView({
                  positions:this.positions,
                  form:this
                });
              }
            } else alert('Строка для поиска должна содержать как минимум 3 символа');

            return false;
        },
        search:function(q){
            
            var content = this.model.get('content');

            if(content.indexOf(q)>-1){

              this.positions=[content.indexOf(q)];
              this.count = content.split(q).length - 1;
              if (this.count>1) this.search_next(content, q);

            } else {
              alert('Ничего не найдено');
              this.positions = [];
              this.count = 0;
            }

            return this.positions;
        },
        search_next:function(content, q){
            var r = content.indexOf(q,  _.last(this.positions)+1);
            if (r >-1){
              this.positions.push(r);
              if (this.positions.length<this.count) this.search_next(content, q);
            }
        }
    });

});