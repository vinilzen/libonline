define(function (require) {

    "use strict";

    var tools = [
        {'title':'Семейство шрифта','name':'font-family', 'options':['Tahoma','Verdana','Arial']},
        {'title':'Насыщенность шрифта','name':'font-weight', 'options':['Normal','Bold']},
        {'title':'Размер шрифта','name':'font-size', 'options':['12px','14px','16px','18px','20px','22px','24px','28px']},
        {'title':'Цвет фона текста','name':'background-color', 'options':['#ddd','#ccc','#eee']},
        {'title':'Цвет текста','name':'color', 'options':['#ddd','#ccc','#000','#fff']},
        {'title':'Выравнивание текста','name':'text-align', 'options':['left','right','center','justify']},
        {'title':'Колоночность','name':'column', 'options':['1','2']},
        {'title':'Стили','name':'style', 'options':['1','2']},
        {'title':'Отменить','name':'undo', 'options':['1','2']},
    ];

    var PropertyValueView = Backbone.View.extend({
        tagName:'em',
        events:{
            'click':'setOption'
        },
        initialize:function(options){
            this.value = options.value;
            this.render();
        },
        render:function(){
            this.$el.html(this.value);
            return this;
        },
        setOption:function(){
            if (this.element && this.name){
                var new_style = {};
                new_style[this.name] = this.value.toLowerCase();

                _.extend(this.element.view.style,new_style);
                this.element.view.styleExtend();
                this.parent.removeOptions();
                this.parent.showedOptions = 0;
            }
        }
    });

    var ItemToolView = Backbone.View.extend({
        tagName:'li',
        initialize:function(){
            this.render();
        },
        events:{
            'click .font-family, .font-weight, .font-size':'showOptions',
            'click .text-color, .bgr-color':'showColors',
            'click #canvas_picker':'showHEX',
            'click #setColor':'setColor',
            'click .column-one, .column-two, .text-al-left, .text-al-center, .text-al-justify, .text-al-right':'setOption',
        },
        render:function(){
            switch (this.model.get('name')){
                case 'font-family':
                case 'font-weight':
                case 'font-size':
                    this.$el.html('<span class="'+this.model.get('name')+'">'+this.model.get('options')[0]+'<b class="caret"></b></span>');
                    break;

                case 'color':
                    this.$el.html('<span class="ico text-color"><i></i></span>');
                    break;

                case 'background-color':
                    this.$el.html('<span class="ico bgr-color"><i>T</i></span>');
                    break;
                    
                case 'text-align':
                    this.$el.html(
                        '<span class="ico text-al-left" data-value="left"></span>'+
                        '<span class="ico text-al-center" data-value="center"></span>'+
                        '<span class="ico text-al-right" data-value="right"></span>'+
                        '<span class="ico text-al-justify" data-value="justify"></span>');
                    break;

                case 'column':
                    this.$el.html(
                        '<span class="ico column-one" data-value="1"></span>'+
                        '<span class="ico column-two" data-value="2"></span>');
                    break;

                case 'undo':
                    this.$el.html('<span class="ico undo"></span>');
                    break;
                    
                default:
                    this.$el.html('<span>'+this.model.get('title')+'<b class="caret"></b></span>');
                    break;
            }
            return this;
        },
        setColor:function(e){

            this.color = $('#color_property').val() || this.color;

            this.model.collection.removeAllOptionsView(0);
            var el = _.find(this.model.collection.book.html_elements.models, function(m){
                return m.get('selected');
            });

            if (el){
                var new_style = {};

                new_style[this.model.get('name')] = this.color;

                _.extend(el.view.style,new_style);
                el.view.styleExtend();
                // console.log('set option', this.model.collection, this.model.get('name') ,'=' , $(e.target).data('value'));
            } else {
                console.log('Редактируемый элемент не найден')
            }
        },
        setOption:function(e){
            this.model.collection.removeAllOptionsView(0);
            var el = _.find(this.model.collection.book.html_elements.models, function(m){
                return m.get('selected');
            });

            if (el){
                var new_style = {};
                new_style[this.model.get('name')] = $(e.target).data('value')

                _.extend(el.view.style,new_style);
                el.view.styleExtend();
                // console.log('set option', this.model.collection, this.model.get('name') ,'=' , $(e.target).data('value'));
            } else {
                console.log('Редактируемый элемент не найден')
            }
        },
        showColors:function(e){
            this.model.collection.removeAllOptionsView(this.cid);
            if (this.showedOptions != 1){
                this.showedOptions = 1;
                var properties = this.model.get('options'),
                    img = new Image(),
                    $properties = $(
                        '<div class="properties" data-name="'+this.model.get('name')+'">'+
                            '<canvas width="256" height="250" id="canvas_picker"></canvas>'+
                            '<input type="text" id="color_property" >'+
                            '<a id="setColor" class="button">Применить</a>'+
                        '</div>'
                    );

                img.src = './new_design/img/map-saturation.png';

                this.$el.addClass('selected').append($properties);
                var canvas = this.canvas = document.getElementById('canvas_picker').getContext('2d');
                
                $(img).load(function(){
                  canvas.drawImage(img,0,0);
                });
            } else {
                this.showedOptions = 0;
            }
        },
        rgbToHex:function(R,G,B){
            return this.toHex(R)+this.toHex(G)+this.toHex(B);
        },
        toHex:function(n){
            n = parseInt(n,10);
            if (isNaN(n)) return "00";
            n = Math.max(0,Math.min(n,255));
            return "0123456789ABCDEF".charAt((n-n%16)/16)  + "0123456789ABCDEF".charAt(n%16);
        },
        showHEX:function(e){
            // getting user coordinates
            var x = e.pageX - $(e.target).offset().left;
            var y = e.pageY - $(e.target).offset().top;
            // getting image data and RGB values
            var img_data = this.canvas.getImageData(x, y, 1, 1).data;
            var R = img_data[0];
            var G = img_data[1];
            var B = img_data[2];
            // convert RGB to HEX
            var hex = this.rgbToHex(R,G,B);
            // making the color the value of the input
            this.color = '#'+hex;
            $('#color_property').val(this.color);
        },
        showOptions:function(e){

            this.model.collection.removeAllOptionsView(this.cid);

            if (this.showedOptions != 1){

                this.showedOptions = 1;

                var properties = this.model.get('options'),
                    $properties = $('<div class="properties" data-name="'+this.model.get('name')+'"></div>');

                this.$el.addClass('selected').append($properties);

                var el = _.find(this.model.collection.book.html_elements.models, function(m){
                    return m.get('selected');
                });

                _.each(properties, function(property){
                    var view = new PropertyValueView({value:property});
                    if (el){
                        view.element = el;
                        view.name = this.model.get('name');
                        view.parent = this;
                    }
                    $properties.append(view.el);
                }, this);

                var h = $properties.height(),
                    w = this.$el.width()+20;

                $properties.attr('style', 'top:-'+h+'px; width:'+w+'px');

            } else {
                this.showedOptions = 0;
            }
        },
        removeOptions:function(){
            $('.properties', this.$el).remove();
            this.$el.removeClass('selected');
        }
    });

    var ToolItem = Backbone.Model.extend({
        initialize:function(){
            this.view = new ItemToolView({model:this});
        }
    });

    var Tools = Backbone.Collection.extend({
        model:ToolItem,
        // initialize:function(){ },
        removeAllOptionsView:function(cid){
            this.each(function(model){
                model.view.removeOptions();
                if (cid != model.view.cid) {
                    model.view.showedOptions = 0;
                }
            });
        }
    });

    var tools_collection = new Tools(tools);


    return Backbone.View.extend({

        el:'.editor',
        
        initialize: function(){
            this.render();
        },

        render:function(){
            this.$el.empty();
            tools_collection.book = this.model;
            // this.$el.html(this.model.get('name')).append(settings.render().el);
            tools_collection.each(function(model){
                this.$el.append(model.view.el);
            }, this)
            return this;
        }
    });

});
