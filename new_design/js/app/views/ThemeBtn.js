define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone');

    return Backbone.View.extend({
        tagName:'a',
        className: 'theme-btn',
        events:{
            'click':'selectTheme'
        },
        initialize: function () {
            this.render();
        },
        render:function(){
            this.$el.html(this.model.get('theme_name'));
            return this;
        },
        selectTheme:function(){
            this.model.set('selected',1);
        }
    });
});