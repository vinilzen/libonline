define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        modal = require('modal');

    return Backbone.View.extend({

        className: "themes",
        
        initialize: function () {
            this.render();
        },

        render:function(){

            this.$el.empty();

            this.collection.each(function(model){
                this.$el.append(model.btn.el);
            }, this);

            this.$el.modal('Темы оформления', '');

            $('.modal-body').html(this.el);

            return this;
        }
    });

});