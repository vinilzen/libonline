define(function (require) {

    "use strict";

    var Settings = require('app/views/Settings');

    return Backbone.View.extend({

        el:'.book-name',
        
        initialize: function(){
            this.render();
        },

        render:function(){
            var settings = new Settings();
            this.$el.html(this.model.get('name')).append(settings.render().el);
            return this;
        }
    });

});