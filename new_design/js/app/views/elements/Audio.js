define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
        className: 'audio',
        template: _.template('<audio src="" controls></audio>'),
        render: function() {

            $(this.el)
                .html(this.template(this.model.toJSON()))
                .attr('id','audio_'+this.model.id);

            $('audio',this.$el).attr({'src': this.model.get('src') });

            if (this.model.get("style") && _.size(this.model.get("style")) > 0){
                var style_str = '', src,w,h;
                _.each(this.model.get("style"), function(value,prop){
                    style_str += prop+':'+value+'; ';
                });

                this.$el
                    .attr( {'style': style_str })
                    .css({'top':'10px','position':'relative'});
            }
            return this;
        },
        create_preview:function(){
            this.model.preview = new Backbone.View({
                tagName:'p',
                className:'audio preview_audio',
            });

            this.prev_audio = document.createElement('audio');
            $(this.prev_audio).attr('src', this.model.get('src')).css({
                width:'100%'
            });

            this.model.preview.$el
                .attr('style',this.scale_css())
                .css({
                    position:'absolute',
                    overflow:'hidden'
                }).html(this.prev_audio);

            this.model.get('container') // вьюха страницы которая содержит эту модель
                .model.preview.content.append(this.model.preview.el);
        },
    })
})