define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        TextView = require('app/views/elements/Text');

    return TextView.extend({
        tagName:'p',
        className:'epigraph',
    	style:{
    		'text-align':'right',
    	},
    })
})