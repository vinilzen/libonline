define(function(require) {

    "use strict";

    var Backbone = require('backbone');


    var ImgDescriptionView = Backbone.View.extend({
        className: 'img_descr',
        template: _.template('<p><%= text %></p>' +
            '<input type="text" value="<%= text %>" />'),
        events: {
            'click p': 'edit',
            'blur input': 'save',
            'keypress input': 'updateOnEnter'
        },
        initialize: function() {
            _.bindAll(this);
            this.render()
        },
        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            this.input = $('input', this.$el).hide();
            this.p = $('p', this.$el);
            return this;
        },
        edit: function() {
            this.p.hide();
            this.input.show();
            return false;
        },
        updateOnEnter: function(e) {
            if (e.keyCode == 13) this.save();
        },
        save: function() {
            this.model.set('text', this.input.val());
            this.close();
        },
        close: function() {
            this.p.show();
            this.input.hide();
        }
    });

    var ImgDescriptionModel = Backbone.Model.extend({
        initialize: function() {
            _.bindAll(this);
            this.view = new ImgDescriptionView({
                model: this
            });
            this.on('change', this.rerender);
        },
        rerender: function() {
            this.get('parent_model').set('description', this.get('text'))
            this.view.render();
        }
    });

    return Backbone.View.extend({
        render: function() {
            if (this.model.has('style') && this.model.get('style')) {
                this.$el.css(this.model.get('style'));
            }
            this.$el.css({
                position: 'absolute',
                overflow: 'hidden',
                background: 'rgba(221, 221, 221, 0.5)'
            });


            this.model.set({
                'curent_placeholder_size': [this.$el.width(), this.$el.height()]
            }, {
                silent: true
            });

            var thisJq = this.$el.html('');

            if (!this.options.preview) {
                this.img = new Image();
                this.img.parent = this;
                this.img.onload = function() {
                    this.parent.model.set('original_img_size', [this.width, this.height]);
                    $(this).appendTo(thisJq);
                    this.parent.fill();
                }
                this.img.src = this.model.get('src');
                this.img.title = this.model.get('title');
            } else {
                console.log('prev');
            }

            return this;
        },
        create_preview: function() {
            if (typeof(this.model.preview) != 'undefined') {
                this.model.preview.remove();
            }
            this.model.preview = new Backbone.View();

            this.prev_img = new Image();
            this.prev_img.src = this.model.get('src');

            this.model.preview.$el
                .attr('style', this.scale_css())
                .css({
                    position: 'absolute',
                    overflow: 'hidden'
                }).html(this.prev_img);

            this.model.get('container') // вьюха страницы которая содержит эту модель
            .model.preview.content.append(this.model.preview.el);
        },
        fill: function() {
            var css = this.model.get('style'),
                original_img_size = this.model.get('original_img_size'),
                curent_placeholder_size = this.model.get('curent_placeholder_size');

            if (typeof(original_img_size) != 'undefined') {

                if (typeof(this.preview) != 'undefined') {

                    _.each(css, function(a, b) {
                        css[b] = parseInt(a) * book.get('prev_scale');
                    });

                    curent_placeholder_size = [
                        parseInt(curent_placeholder_size[0] * book.get('prev_scale')),
                        parseInt(curent_placeholder_size[1] * book.get('prev_scale'))
                    ];
                }

                this.$el.css(css);
                var curent_img_size = [$(this.img).width(), $(this.img).height()];

                if (original_img_size[0] / original_img_size[1] > curent_placeholder_size[0] / curent_placeholder_size[1]) {
                    var ml = parseInt((curent_placeholder_size[0] - curent_img_size[0]) / 2);

                    $(this.img)
                        .css({
                            'height': '100%',
                            'width': 'auto'
                        })
                        .css({
                            'margin-left': ml,
                            'margin-top': 0
                        });

                    $(this.prev_img)
                        .css({
                            'height': '100%',
                            'width': 'auto'
                        })
                        .css({
                            'margin-left': parseInt(ml * book.get('prev_scale')),
                            'margin-top': 0
                        });
                } else {

                    var mt = parseInt((curent_placeholder_size[1] - curent_img_size[1]) / 2);

                    $(this.img)
                        .css({
                            'width': '100%',
                            'height': 'auto'
                        })
                        .css({
                            'margin-top': mt,
                            'margin-left': 0
                        });

                    $(this.prev_img)
                        .css({
                            'width': '100%',
                            'height': 'auto'
                        })
                        .css({
                            'margin-top': parseInt(mt * book.get('prev_scale')),
                            'margin-left': 0
                        });
                }
            }
        },
        show_inset_menu: function() {},
    })
})