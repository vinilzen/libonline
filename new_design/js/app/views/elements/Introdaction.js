define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        TextView = require('app/views/elements/Text');

    return TextView.extend({
    	tagName:'h3',
    	className:'introduction',
    	style:{
    		'font-size':'18px',
    		'font-weight':'bold'
    	}
    })
})