define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        TextView = require('app/views/elements/Text');

    return TextView.extend({
        tagName:'p',
        className:'introdaction_content',
        style:{
        	'font-size':'12px'
        }
    })
})