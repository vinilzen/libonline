define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    var EditLatexView = Backbone.View.extend({
        className:'edit_latex',
        template:_.template('<input type="text" value="<%= formula %>" /><i class="icon-edit"></i>'),
        events:{
            'click .icon-edit':'show',
            'click input':'returnfalse',
            'blur input':'save',
            'keypress input': 'updateOnEnter'
        },
        initialize:function(){
            _.bindAll(this);
            this.render();
        },
        render:function(){
            this.$el.html( this.template(this.model.toJSON()) );
            this.ico = $('.icon-edit', this.$el);
            this.input = $('input', this.$el).css('cursor','pointer');
            return this;
        },
        show:function(){
            this.input.show();
            return false;
        },
        updateOnEnter:function(e){
            if (e.keyCode == 13) this.save();
        },
        save:function(){
            this.model.set('formula',this.input.val());
            this.model.view.render();
            console.log(this.model.attributes)
            this.close();
        },
        close:function(){
            console.log('close')
            this.input.hide();
        },
        returnfalse:function(){
            return false;
        }
    });

    return Backbone.View.extend({
        template: _.template('<img src="<%= src %>" />'),
        initialize: function(model, options){
            _.bindAll(this);
            this.model.view = this;
        },
        render: function(){
            this.$el.html(this.template({
                'src': 'http://chart.apis.google.com/chart?cht=tx&chl='+encodeURIComponent(this.model.get("formula"))
            }));

            this.$el.css(this.model.get('style'))
                .css({
                    position:'absolute',
                    overflow:'hidden',
                    background:'rgba(221, 221, 221, 0.5)'
                });


            this.model.set({'curent_placeholder_size':[this.$el.width(),this.$el.height()]},{silent:true});

            var thisJq = this.$el;

            if (!this.options.preview){
                this.img = new Image();
                this.img.parent = this;
                this.img.onload = function(){
                    this.parent.model.set('original_img_size',[this.width,this.height]);
                    $(this).appendTo(thisJq);
                    this.parent.fill();
                }
                this.img.src = this.model.get('src');
                this.img.title = this.model.get('title');
            } else {
                console.log('prev');
            }

            return this;
        },
        create_edit_menu:function(){
            this.model.set('edited',1);
            this.edit_formula = new EditLatexView({model:this.model});
            this.$el.append(this.edit_formula.el);

            this.properties = new Properties(
                [],  // models
                {style:this.model.get("style"), text_model:this.model} // options
            );

            $('[data-name="path"]').hide();

            if (this.$el.hasClass('popover-active')){
                this.$el.popover('destroy').removeClass('popover-active');
            }

            this.marked();

            if (typeof(this.model.get("container"))!='undefined'){
                this.draggable(this.model.get("container"), this.model); // делаем элемент подвижным
                this.marked_page(this.model.get("container"));
            } else {
                console.log('need cantainer');
            }
        },
        remove_edit_menu:function(){
            this.model.set('edited',0);
            this.de_marked_page(this.model.get("container"));
            this.de_marked();
            if (typeof(this.properties)!= 'undefined'){
                this.properties.hide();
            }
            if (typeof(this.edit_formula)!= 'undefined'){
                this.edit_formula.remove();
            }
            this.de_draggable(this.model.get("container"));
        },
    })
})