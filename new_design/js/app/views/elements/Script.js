define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
        tagName:"div",
        initialize: function() {
            this.render();
        },
        render: function() {
            var divscript_class = this.model.get("type")+'_'+this.model.id;
            $(this.el).attr({'class': divscript_class});
            $(this.el).addClass('divscript');
            $(this.el).append($('<img>').attr('src',this.model.get("src")));

            if (this.model.get("style") && _.size(this.model.get("style")) > 0) {

                var style_str = '',src,w,h;

                _.each(this.model.get("style"), function(value,prop){
                    switch(prop){
                        case 'height':
                            if (value == 0 || value == '0' || value == ''){
                                style_str += prop+':auto; ';
                                h = 'auto';
                            } else {
                                style_str += prop+':'+value+'; ';
                                h = value;
                            }
                            break;
                        case 'width':
                            if (value == 0 || value == '0' || value == ''){
                                style_str += prop+':auto; ';
                                w = 'auto';
                            } else {
                                if (value == 100){
                                    value = '100%';
                                    w = '100%'
                                } else {
                                    w = value;
                                }
                                style_str += prop+':'+value+'; ';
                            }
                            break;
                        default:
                            style_str += prop+':'+value+'; ';
                    }
                });

                this.$el.attr( {'style': style_str, 'src': this.model.get("src") });

                var container = $(this.el);
                container.append($('<div></div>').attr('class','divscript_play').css({
                    'width':parseInt(container.css('width'))/3,
                    'height':parseInt(container.css('width'))/3,
                    'margin-left':parseInt(container.css('width'))/2 - (parseInt(container.css('width'))/3)/2,
                    'margin-top':parseInt(container.css('height'))/2 - (parseInt(container.css('width'))/3)/2
                }).append($('<img>').attr('src','/img/play.png')));
            }
            return this;
        },
        show_edit_menu: function(){
            if (!this.preview){
                var properties = new Properties(
                    [],  // models
                    {style:this.model.get("style"), text_model:this.model} // options
                );
                var text_model = this.model,
                    div_container = "."+text_model.get('type')+'_'+text_model.get('id'),
                    scripts_data = book.get('curent_style').get('scripts_data');

                if(scripts_data && scripts_data.length){
                    for(var i=0;i<scripts_data.length;i++){
                        if(scripts_data[i].pos == text_model.get("pos")){
                            var script_id = parseInt(scripts_data[i].id);
                            var data = scripts_data[i].data;
                        }
                    }
                }


                if(script_id){
                    if ($(div_container).find('.divscript_play').length != 0){
                        dispatcher_scripts('demo', script_id, div_container, text_model.get("pos"), data);
                    }

                    properties.add(new Property({
                        "name":"Play",
                        "script_name":"demo",
                        "script_id":script_id,
                        "container_id":text_model.get('id'),
                        "pos":text_model.get("pos"),
                        "container":div_container,
                        "data":data
                    }));

                    properties.add(new Property({
                        "name":"Edit",
                        "script_name":"conf",
                        "script_id":script_id,
                        "container_id":text_model.get('id'),
                        "pos":text_model.get("pos"),
                        "container":div_container,
                        "data":data
                    }));
                }

                var change_script_property = new Property({
                    "name":"Change Script",
                    "script_name":"change",
                    "script_id":script_id,
                    "container_id":text_model.get('id'),
                    "pos":text_model.get("pos"),
                    "container":div_container
                });
                properties.add(change_script_property);

                if(!script_id){
                    if ($(div_container).find('.divscript_play').length != 0){
                        var changeScript = new ChangeScriptView({model:change_script_property});
                    }
                }

                $('.properties').remove();

                properties_view = new PropertiesDivscriptView({collection: properties});
                properties_view.render();

                $('.page_container').prepend(properties_view.$el);
                properties_view.$el.css({height: '32px'});

            } else {
                return this;
            }
        },
    })
})