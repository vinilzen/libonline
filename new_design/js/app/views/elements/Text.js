define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
    	tagName:'p',
    	events:{
    		'click':'toggleSelected'
    	},
    	initialize:function(){
    		this.render();
    		this.model.on('change:selected', function(model, value){
    			if (value){
					this.$el.addClass('selected_blue');
                    this.setActualOption(model.get('style'));
    			} else {
	    			this.$el.removeClass('selected_blue')
	    			this.$el.removeClass('selected_red')	
    			}
    		}, this);

            this.model.on('change:style', function(model, value){
                this.$el.css(model.get('style'));
            }, this);

            this.styleExtend();
    	},
    	render:function(){
    		this.$el.html(this.model.get('content'))
    		return this;
    	},
        setActualOption:function(style){
            _.each(style, function(value, name){
                switch (name){
                    case 'font-family':
                    case 'font-weight':
                    case 'font-size':
                        $('.editor .'+name).html(value+'<b class="caret"></b>').css('text-transform','capitalize');

                        // $( 'properties[data-name="'+name+'"] em').removeClass('selected');
                        // $( 'properties[data-name="'+name+'"] em:contains("'+value+'")').addClass('selected');

                        break;
                    case 'color':
                        $('.editor .text-color i').css({
                            'background-color':value
                        });
                        break;

                    case 'background-color':
                        $('.editor .bgr-color i').css({
                            'background-color':value
                        });
                        break;
                }
            })
        },
        style: {'color':'#333'},
        styleExtend:function(){
            var style_attr = _.clone(this.model.get('style'));

            _.extend(style_attr, this.style);

            this.model.set('style',style_attr);
            this.setActualOption(style_attr);
        },
    	toggleSelected:function(){
    		
    		this.deselectOther();

    		if (this.model.get('selected') == 0) {
    			this.model.set('selected',1);
    		} else {
    			this.model.set('selected',0);
    		}
    	},

    	deselectOther:function(){
    		var cid = this.model.cid;
	   		this.model.collection.each(function(element){
	   			if (element.cid != cid) element.set('selected',0);
    		})
    	}
    })
})