define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        TextView = require('app/views/elements/Text');

    return TextView.extend({
    	tagName:'h4',
    	className:'title',
    	style:{
    		'font-size':'24px',
    		'font-weight':'bold'
    	},
    })
})