define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
        className: 'video',
        events:{
            'click': 'show_edit_menu',
            'contextmenu':'popover',
            'mousedown':'mousedown',
            'mouseup':'mouseup'
        },
        popover:function(e){
            if (e.button == 2){
                $('.popover').remove();

                this.$el.popover({
                    'container': 'body',
                    'placement':'bottom',
                    'html':true,
                    'trigger':'manual',
                    'content':'Заменить видео:<br>',
                    'title':'Редактирование виджета видео'
                }).popover('toggle');


                this.model.btn_video = new WidgetEditImgBtn({
                    model:this.model,
                    lib_id:3,
                    type:'video',
                    title:'Выбрать из библиотеки'
                });

                this.model.btn_clipart_video = new WidgetEditImgBtn({
                    model:this.model,
                    lib_id:4,
                    type:'video_clipart',
                    title:'Выбрать из клипартов'
                });

                $('.popover-content')
                    .append(this.model.btn_video.el)
                    .append(this.model.btn_clipart_video.el);

                return false;
            }
        },
        template: _.template('<video controls poster="<%= poster %>">'+
            '<source src="<%= src %>">'+
            '<p>Ваш браузер не поддерживает html5 видео</p>'+
            '</video>'),
        render: function() {
            if (!this.model.has('poster')){
                this.model.set('poster','')
            }
            $(this.el)
                .html(this.template(this.model.toJSON()))
                .attr('id','video_'+this.model.get('video_id'));

            if (this.model.get("style") && _.size(this.model.get("style")) > 0) {
                var style_str = '', src,w,h;
                _.each(this.model.get("style"), function(value,prop){
                    switch(prop){
                        case 'height':
                            if (value == 0 || value == '0' || value == ''){
                                style_str += prop+':auto; ';
                                h = 'auto';
                            } else {
                                style_str += prop+':'+value+'; ';
                                h = value;
                            }
                            break;
                        case 'width':
                            if (value == 0 || value == '0' || value == ''){
                                style_str += prop+':auto; ';
                                w = 'auto';
                            } else {
                                if (value == 100){
                                    value = '100%';
                                    w = '100%'
                                } else {
                                    w = value;
                                }
                                style_str += prop+':'+value+'; ';
                            }
                            break;
                        default:
                            style_str += prop+':'+value+'; ';
                    }
                });

                this.$el
                    .attr( {'style': style_str, 'src': src })
                    .css('position','absolute');

                $('video',this.$el).css({'width':w, 'height':h});
            }
            return this;
        },
        show_inset_menu:function(){},
        create_preview:function(){
            this.model.preview = new Backbone.View({
                tagName:'p',
                className:'video preview_video',
            });

            this.prev_video = document.createElement('video');
            $(this.prev_video).attr('src', this.model.get('src')).css({
                width:'100%'
            });

            this.model.preview.$el
                .attr('style',this.scale_css())
                .css({
                    position:'absolute',
                    overflow:'hidden'
                }).html(this.prev_video);

            this.model.get('container') // вьюха страницы которая содержит эту модель
                .model.preview.content.append(this.model.preview.el);
        },
        marked: function(){
            this.de_marked();
            this.$el.addClass('marked')
                .css({  'margin-top':'-1px',
                    'margin-left':'-1px'});
            this.tag_menu();
        },
        de_marked: function(){
            $('.marked').css({  'cursor':'auto',
                'margin-top':0,
                'margin-left':0}).removeClass('marked');
            this.hide_tags();
        },
        hide_tags:function(){
            if (typeof(this.tag_form)!='undefined'){
                this.tag_form.remove();
            }
        },
        tag_menu:function(){
            if (!this.model.has('tags')){
                var tags = new VideoTagCollection();
                this.model.set('tags',tags)
            }
            this.add_tag_form();
            this.show_tags();
        },
        show_tags:function(){
            if (this.model.get('tags').length>0){
                this.model.get('tags').render();
            }
        },
        add_tag_form:function(){

            this.tag_form = new AddTagForm({
                parent:this.model
            });

            this.model.get('tags').tag_form = this.tag_form;

            var position = this.$el.position();

            this.$el.closest('.content').append(this.tag_form.el);
            this.tag_form.$el.css({
                top: position.top + this.$el.height(),
                left: position.left,
                width:this.$el.width()
            });
        }
    })
})