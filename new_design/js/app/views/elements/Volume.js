define(function (require) {

    "use strict";

    var Backbone = require('backbone'),
        TextView = require('app/views/elements/Text');

    return TextView.extend({
    	tagName:'h2',
    	className:'volume',
    	style:{
    		'font-size':'28px',
    		'font-weight':'bold'
    	}
    })
})