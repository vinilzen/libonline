#= require jquery
#= require underscore
#= require backbone
#= require backbone_sync
#= require_tree ./lib
#= require start_screen
#= require editor
#= require font_size

Backbone.emulateHTTP = true;
Backbone.emulateJSON = true;



$(function() {
  
  var start_screen;

  start_screen = new StartScreen();
  start_screen.on("next", function(uploaded_file) {
    var editor, id;
    ts = uploaded_file.get("ts");

    if (typeof(ts) == "number") { // новая книга

        start_screen.hide();
        $("#editor").show();
        return editor = new Editor(ts, 0);

    } else { // сохраненная с темой

      if (typeof(ts) == 'string') {

        var param = ts.split('-');
        if (param.length > 0) {
          start_screen.hide();
          $("#editor").show();
          return editor = new Editor(param[0], param[1]);
        }

      } else {
        alert('Некорректный ответ сервера');
      }
    }
  });

  /*
  window.TextStyles = new FontSizeView({
    model: new FontSizeModel()
  });
  TextStyles.hide();

  window.ImageProperties = new ImagePropertiesView({
    model: new ImageFileUpload()
  });
  return ImageProperties.hide(); */
});