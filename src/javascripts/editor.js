#= require_tree ./editor

var book_view, content, widgets, book, user_theme = 0, user_themes, style={};
/* loads book, theme, selects theme */
this.Editor = (function() {

  function Editor(id, theme_id) {
    book = new Book({
      id: id
    });

    if (parseInt(theme_id) > 0) { // for exist book with theme
      _.each(available_themes.models, function(m){         m.set({'selected':false});      })
      
      selected_theme = _.find(available_themes.models, function(theme){ return theme.id == theme_id; });
      selected_theme.set({'selected':true});
      selected_theme.view_btn.selected();

    } else {// for new upload book
      selected_theme = _.find(available_themes.models, function(theme){ return theme.get("selected"); });
    }

    selected_theme.fetch({
               success: function(model, response){

                  book_view = new BookView({model:book});
                  user_themes = new UserThemes();
                  book.set({
                              "text_style":  selected_theme.get("text_style"),
                              "title_style": selected_theme.get("title_style"),
                              "page_style":  selected_theme.get("page_style"),
                              "cover_author_style": selected_theme.get("cover_author_style"),
                              "cover_name_style": selected_theme.get("cover_name_style"),
                              "cover_pic_style": selected_theme.get("cover_pic_style"),
                              "placeholders": selected_theme.get("placeholders"),
                              "theme_id": selected_theme.id,
                            },{silent: true});

                  book.fetch({
                             success: function(model, response){
                              content = book.get("content");
                              widgets = book.get("widgets");

                              var name = _.find(widgets, function(i){ return i['type'] == 'name'; });
                              var author = _.find(widgets, function(i){ return i['type'] == 'author'; });
                              
                              if (name && name.text && name.text != 'undefined')
                                model.set({'name': name.text},{silent: true});
                              
                              if (author && author.text && author.text != 'undefined')
                                model.set({'author': author.text},{silent: true});

                              parseContent(content, widgets);
                              
                              book_view.render();

                             }, error: function(model, response){
                                alert('error fetch book!!!');
                             }
                  });

               }, error: function(model, response){
                  alert('error fetch theme');
               }
    })
  }

  Editor.prototype.book_loaded = function() {
    this.book.off("change", this.fetch_theme, this);
    return this.select_theme();
  };

  Editor.prototype.theme_loaded = function() {
    this.theme.off("change", this.theme_loaded, this);

    if (this.user_changes) {
      this.user_changes.off();
    }
    
    this.user_changes = new UserChanges();

    this.user_changes.on("reflow", this.apply_theme, this);
    this.user_changes.set("theme_uid", this.theme.get("id"));
    this.user_changes.set("book_uid", this.book.get("id"));
    this.pages.user_changes = this.user_changes;
    this.save_button.set_model(this.user_changes);
    this.user_changes.on("loaded", this.user_changes_loaded, this);
    return this.user_changes.fetch();
  };

  Editor.prototype.user_changes_loaded = function() {
    this.user_changes.off("change", this.user_changes_loaded, this);
    return this.apply_theme();
  };

  Editor.prototype.apply_theme = function() {
    this.pages.reset();
    this.theme.apply_to_book(this.book, this.pages, this.user_changes);
    this.pages.select(this.pages.at(0));
    return this.pages.post_render_update();
  };
  return Editor;

})();