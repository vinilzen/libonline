
var TextView = Backbone.View.extend({
	events:{
		'click': 'show_edit_menu',
	},

	initialize: function(){
		this.render();
	},
	
	show_edit_menu: function(){

      properties = new Properties();

      var text_model = this.model;

      var my_page = this.model.get("container"); // Вьюха страницы на которой распологается элемент

      _.each(text_model.get("style"), function(value, name){
        
        var property_default = _.find(defaults_properties, function(p){  //console.log(p, p.name, name);
          return p.name == name;
        });

        if (property_default && property_default != 'undefined') {
          var property = new Property(property_default);
          property.set({"curent_value":value, "type":text_model.get("type"), "typeLvl":text_model.get("typeLvl")});
          properties.add(property);
        }
      });

      $('.properties').remove();
      if (properties.length > 0){
        new_tool_bar = new PropertiesView({collection: properties});
        new_tool_bar.render();
        $('.page_container').prepend(new_tool_bar.$el);
        new_tool_bar.$el.css({height: '48px'});
      }

      this.marked_page(my_page); // помечаем рамкой текущую страницу  
      
      this.draggable(my_page, text_model); // делаем элемент подвижным (или переопределяем метод)
      
      this.marked(); // помечаем рамкой 
	},

  draggable: function(my_page, text_model){

    var placeholders = book.get("placeholders"), placeholders_array_length = 0;

    if (placeholders && placeholders[text_model.get("type")]){
      placeholders_array = placeholders[text_model.get("type")];
      placeholders_array_length = placeholders_array.length;
    }

    this.$el.draggable({

      containment: ".page[data-page="+my_page.$el.attr('data-page')+"] .content",

      scroll: false,

      revert: function(){
        if (placeholders_array_length > 0)
          return true;
        else
          return false;
      },

      //snap: '.drop',
      
      stop: function( event, ui ) { // set to model curent position
        if (placeholders_array_length == 0) {
          var curent_style = text_model.get("style");
          curent_style['top'] = ui.position.top+'px';
          curent_style['left'] = ui.position.left+'px';
          curent_style['position'] = 'absolute';

          book.set(text_model.get("type")+'_style', curent_style);
          book.save_user_theme();
          book_view.render();// todo check
        }
      }

    });


    if (placeholders_array_length > 0) {

      for (i=0; i<placeholders_array_length; i++) {
        //console.log(placeholders_array[i]);
        var view = new Backbone.View;
        var el = view.make("div", {"class": "drop"});
        $('.content', my_page.$el).append(el);
        $(el).css(placeholders_array[i]);
      };

      $( ".drop", my_page.$el ).droppable({
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        
        drop: function( event, ui ) {
          var curent_style = text_model.get("style");
          curent_style['top'] = ui.position.top+'px';
          curent_style['left'] = ui.position.left+'px';
          curent_style['position'] = 'absolute';

          book.set(text_model.get("type")+'_style', curent_style);
          book.save_user_theme();
          book_view.render();// todo check
        },
        tolerance: 'fit'
      });
    }
  },

  marked_page: function(my_page){

    $('.page .drop').remove();
    $('.marked_page').css({ height: 'auto',
                            width: 'auto',
                            top:0,
                            left:0
                          }).removeClass('marked_page');

    $('.content', my_page.$el).addClass('marked_page');
    $('.content', my_page.$el).css({  height: my_page.$el.height(),
                                      width: my_page.$el.width(),
                                      top:'-1px',
                                      left:'-1px'
                                    });
  },

  marked: function(){

    $('.marked').css({  'cursor':'auto',
                        'margin-top':0,
                        'margin-left':0}).removeClass('marked');

    this.$el.addClass('marked');
    this.$el.css({  'cursor':'move',
                    'margin-top':'-1px',
                    'margin-left':'-1px'});
  },

	render: function() {
        if (this.model.get("cut")>0){
        	var content = this.model.get("content").split(' ');
        	content = content.slice(content.length-this.model.get("cut"));
        	this.$el.html(content.join(' '));
        	this.model.set("cut", -1);
        } else {
    		this.$el.html(this.model.get("content"));
        }


    	if (this.model.get("style") && _.size(this.model.get("style")) > 0) {
	    	var style_str = '';
	    	_.each(this.model.get("style"), function(value,prop){
	    		style_str += prop+':'+value+'; ';
	    	});
	    	if (this.model.get("cut") == -1){
	    		style_str += 'text-indent:0; ';
	    		this.model.set("cut", 0);
	    	}
	    	this.$el.attr( 'style', style_str );
    	}
    	return this;
	}

});

var TitleView = TextView.extend({});

var CoverNameView = TextView.extend({
  tagName: 'h1',
});

var CoverAuthorView = TextView.extend({
  tagName: 'h2',
});

var CoverPicView = TextView.extend({
  tagName: 'img',

  render: function() {

    if (this.model.get("style") && _.size(this.model.get("style")) > 0) {
      var style_str = '', src;
      _.each(this.model.get("style"), function(value,prop){
        if (prop != 'img'){
          style_str += prop+':'+value+'; ';
        } else {
          src = value;
        }
      });
     
      this.$el.attr( {'style': style_str, 'src': src });
    }
    return this;
  }
});


var H1View = TitleView.extend({
	tagName: 'h1',
  draggable: function(){ },
});


var H2View = TitleView.extend({
	tagName: 'h2',  
  draggable: function(){ },
});


var H3View = TitleView.extend({
	tagName: 'h3',  
  draggable: function(){ },
});


var H4View = TitleView.extend({
	tagName: 'h4',  
  draggable: function(){ },
});


var H5View = TitleView.extend({
	tagName: 'h5',  
  draggable: function(){ },
});


var PView = TextView.extend({
	tagName: 'p',

  draggable: function(){ },

  marked: function(){
      $('.marked').css({  'cursor':'auto',
                          'margin-top':0,
                          'margin-left':0}).removeClass('marked');

      this.$el.addClass('marked');

      this.$el.css({  'cursor':'text',
                      'margin-top':'-1px',
                      'margin-left':'-1px'});
  },

});