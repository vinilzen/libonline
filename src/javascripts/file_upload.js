#= require plupload
#= require plupload.html5


var ERRORS = {
  http: "ошибка сервера",
  unknown: "неизвестная ошибка. Код: ",
  type: "вы выбрали не тот тип файла",
  size: "файл слишком большой"
};
  
this.FileUpload = Backbone.Model.extend({

  defaults: {
    file_name: null,
    file_size: null
  },

  initialize: function() {
    var settings;
    _.bindAll(this);
    settings = {
      max_file_size: "1mb",
      runtimes: "html5",
      required_features: "dragdrop",
      multi_selection: false
    };
    settings = _.extend(this.uploader_settings, settings);
    this.uploader = new plupload.Uploader(settings);
    this.uploader.init();
    this.uploader.bind("BeforeUpload", this.before_upload);
    this.uploader.bind("FilesAdded", this.files_added);
    this.uploader.bind("FileUploaded", this.upload_complete);
    this.uploader.bind("UploadProgress", this.upload_progress);
    return this.uploader.bind("Error", this.upload_error);
  },

  before_upload: function(uploader) {},

  files_added: function(up, files) {
    var file;
    this.uploader.start();
    file = files[0];
    this.set({
      file_name: file.name,
      file_size: file.size
    });
    this.trigger("file:selected");
    return this.trigger("file:uploading");
  },

  upload_progress: function(up, file) {},

  upload_complete: function(up, file, response) {
    try {
      response = $.parseJSON(response.response);
      if (response.success) {
        this.success(response);
        return this.trigger("file:uploaded");
      } else {
        return this.trigger("error", this, response.error);
      }
    } catch (e) {
      return this.trigger("error", this, ERRORS.http);
    }
  },
  
  success: function() {

    console.log('upload success');

  },

  upload_error: function(up, error) {
    var message;
    switch (error.code) {
      case plupload.FILE_SIZE_ERROR:
        message = ERRORS.size;
        break;
      case plupload.FILE_EXTENSION_ERROR:
        message = ERRORS.type;
        break;
      case plupload.HTTP_ERROR:
        message = ERRORS.http;
        break;
      default:
        message = ERRORS.unknown + error.code;
    }
    return this.trigger("error", this, message);
  }
});