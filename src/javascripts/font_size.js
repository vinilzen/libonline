this.FontSizeModel = Backbone.Model.extend({
  defaults: {
      sizes: [6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36],
      colors: ['000000', '448ccb', '9e0b0f', '4b0049', '636363', '00a651', 'f7941d', 'ed145b', 'ff0000', '603913', 'a186be', '959595', 'fff568', 'fdc689'],
      fonts: ['Geneva', 'Arial', 'Helvetica', 'sans-serif', 'Georgia', 'Times New Roman', 'Times', 'serif'],
      current_size: 36,
      current_color: '000000',
      current_font: 'serif',
      current_align: 'left',
      current_bold: false,
      current_italic: false,
      current_underline: false
  },

  set_current: function(model) {
    this.current_model = model;
    return this.set({
      current_size: parseInt(model.get("font_size")),
      current_color: model.get("color").replace("#", ""),
      current_font: model.get("font_family"),
      current_align: model.get("text_align"),
      current_bold: model.get("font_weight") === "bold",
      current_italic: model.get("font_style") === "italic",
      current_underline: model.get("text_decoration") === "underline"
    });
  },

  set_font: function(font) {
    this.set("current_font", font);
    this.save_user_changes("font_family", font);
    return console.log(font);
  },

  set_color: function(color) {
    this.set("current_color", color);
    return this.save_user_changes("color", "#" + color);
  },

  set_size: function(size) {
    this.set("current_size", size);
    return this.save_user_changes("font_size", size + "px");
  },

  set_style: function(style) {
    switch (style) {
      case 'styleAlignLeft':
        this.set("current_align", "left");
        return this.save_user_changes("text_align", "left");
      case 'styleAlignCenter':
        this.set("current_align", "center");
        return this.save_user_changes("text_align", "center");
      case 'styleAlignRight':
        this.set("current_align", "right");
        return this.save_user_changes("text_align", "right");
      case 'styleBold':
        if (this.get("current_bold")) {
          this.set("current_bold", false);
          return this.save_user_changes("font_weight", "normal");
        } else {
          this.set("current_bold", true);
          return this.save_user_changes("font_weight", "bold");
        }
        break;
      case 'styleItalic':
        if (this.get("current_italic")) {
          this.set("current_italic", false);
          return this.save_user_changes("font_style", "normal");
        } else {
          this.set("current_italic", true);
          return this.save_user_changes("font_style", "italic");
        }
        break;
      case 'styleUnderline':
        if (this.get("current_underline")) {
          this.set("current_underline", false);
          return this.save_user_changes("text_decoration", "none");
        } else {
          this.set("current_underline", true);
          return this.save_user_changes("text_decoration", "underline");
        }
    }
  },

  save_user_changes: function(prop_name, prop_value) {
    var changes;
    this.current_model.set(prop_name, prop_value);
    changes = this.current_model.page.user_changes.get_by_key(this.current_model.parent_key);
    return changes.set_for(this.current_model.widget_key, this.get("type"), prop_name, prop_value);
  }
});


this.FontSizeView = Backbone.View.extend({

  el: '#text_styles',

  initialize: function() {
    Hideable.include_here(this);
    this.model.on("change", this.render, this);
    _.each(this.model.get('sizes'), function(size, key, list) {
      return this.$el.find('#fontSize').append($('<div></div>').attr({
        "class": 'fontSizeStick',
        id: 'size' + size,
        value: size
      }).css('height', ((39 / list.length) * key) + 5));
    }, this);
    this.$el.find('#fontSize').append($('<input>').attr({
      id: 'fontSizeNumber',
      maxlength: 3
    }));
    _.each(this.model.get('colors'), function(color) {
      return this.$el.find('#fontColor').append($('<div></div>').attr({
        "class": 'colorRect',
        id: 'color' + color,
        value: color
      }).css({
        'background-color': '#' + color
      }));
    }, this);
    this.$el.find('#fontFamily').append($('<select></select>').attr('id', 'fontSelect'));
    _.each(this.model.get('fonts'), function(font) {
      return $('#fontSelect').append($('<option></option>').attr({
        "class": 'fontOption',
        id: 'font' + font.replace(/\s/g, ''),
        value: font
      }).html(font));
    }, this);
    return this.render();
  },

  render: function() {
    this.$el.find('.fontSizeStickActive').attr('class', 'fontSizeStick');
    _.each(this.model.get('sizes'), function(size) {
      var obj;
      obj = this.$el.find('#size' + size);
      if (obj.val() <= this.model.get('current_size')) {
        return obj.attr('class', 'fontSizeStickActive');
      }
    }, this);
    this.$el.find('#fontSizeNumber').css({
      'font-family': this.model.get('current_font'),
      'color': '#' + this.model.get('current_color')
    }).val(this.model.get('current_size'));
    this.$el.find('#font' + this.model.get('current_font').replace(/\s/g, '')).attr('selected', 'selected');
    $('.styleFontButton').css('background-color', '#FFFFFF');
    $('#align' + this.model.get('current_align') + 'cont').css('background-color', '#e1e1e1');
    if (this.model.get('current_bold')) {
      $('#styleBoldCont').css('background-color', '#e1e1e1');
    }
    if (this.model.get('current_italic')) {
      $('#styleItalicCont').css('background-color', '#e1e1e1');
    }
    if (this.model.get('current_underline')) {
      return $('#styleUnderlineCont').css('background-color', '#e1e1e1');
    }
  },

  events: {
    'click .fontSizeStick': 'change_size',
    'click .fontSizeStickActive': 'change_size',
    'click .colorRect': 'change_color',
    'keydown #fontSizeNumber': 'change_font_input',
    'click #styleAlignLeft': 'change_style',
    'click #styleAlignCenter': 'change_style',
    'click #styleAlignRight': 'change_style',
    'click #styleBold': 'change_style',
    'click #styleItalic': 'change_style',
    'click #styleUnderline': 'change_style',
    'change #fontSelect': 'change_font'
  },

  change_size: function(event) {
    return this.model.set_size(event.target.value);
  },

  change_color: function(event) {
    return this.model.set_color(event.target.value);
  },

  change_font: function(event) {
    return this.model.set_font(event.target.value);
  },

  change_style: function(event) {
    return this.model.set_style(event.target.id);
  },

  change_font_input: function(event) {
    var newFontSize;
    if (event.keyCode === 13) {
      newFontSize = $('#fontSizeNumber').val();
      if (newFontSize > 100 || newFontSize.toString().search(/^-?[0-9]+$/)) {
        return $('#fontSizeNumber').val(this.model.get('current_size'));
      } else {
        return this.model.set_size(event.target.value);
      }
    }
  },
  
  set_position: function(top, left) {
    return this.$el.css({
      left: left,
      top: top
    });
  }
});