this.Module = (function() {

  function Module() {}

  Module.included = function(func) {
    return this.prototype[this.included_key()] = func;
  };

  Module.module_scope_key = function() {
    var module_name, string_representation;
    string_representation = this.toString();
    module_name = string_representation.match(/function\s*(.+)\s*\(/)[1];
    return module_name;
  };

  Module.included_key = function() {
    return this.module_scope_key() + "_included_callback";
  };

  Module.private_scope_key = function() {
    return this.module_scope_key() + "_private";
  };

  Module.call_private = function() {
    return this.prototype[this.private_scope_key()];
  };

  Module["private"] = function(private_scope) {
    return this.prototype[this.private_scope_key()] = private_scope;
  };

  Module.include_here = function(scope_to_extend) {
    var prototype;
    prototype = _(this.prototype);
    prototype.each(function(value, key) {
      if (key !== "constructor") {
        return scope_to_extend[key] = value;
      }
    });
    if (this.prototype[this.private_scope_key()]) {
      this.prototype[this.private_scope_key()]["public"] = scope_to_extend;
    }
    if (this.prototype[this.included_key()]) {
      return this.prototype[this.included_key()].apply(scope_to_extend, [this.prototype[this.private_scope_key()]]);
    }
  };

  return Module;

})();
