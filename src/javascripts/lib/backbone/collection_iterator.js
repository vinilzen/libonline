// Backbone.Collection mixin
this.CollectionIterator = {
  next: function(model) {
    var index;
    index = this.indexOf(model) + 1;
    if (index >= this.length) {
      index = this.length - 1;
    }
    return this.at(index);
  },
  prev: function(model) {
    var index;
    index = this.indexOf(model) - 1;
    if (index < 0) {
      index = 0;
    }
    return this.at(index);
  }
};