this.CollectionView = Backbone.View.extend({

  collection_item_class: null,

  append_collection_items_to: null,

  initialize: function() {
    this.collection.on("reset", this.render, this);
    this.collection.on("add", this.render_item, this);
    return this.render();
  },

  render: function() {
    this.get_element().empty();
    this.collection.each(this.render_item, this);
    return this;
  },

  remove: function() {
    this.collection.off("reset", this.render, this);
    this.collection.off("add", this.render_item, this);
    Backbone.View.prototype.remove.call(this);
  },

  render_item: function(item) {
    var item_view, klass;
    klass = this.collection_item_class;
    if (!klass) {
      throw new Error("You must specify collection_item_class");
    }
    item_view = new klass({
      model: item
    });
    return this.get_element().append(item_view.render().el);
  },
  
  get_element: function() {
    if (!this.element) {
      this.element = this.$el.find(this.append_collection_items_to);
      if (this.element.size() < 1) {
        this.element = this.$el;
      }
    }
    return this.element;
  }
});