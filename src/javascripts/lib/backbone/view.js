this.View = Backbone.View.extend({

  initialize: function() {
    if (this.template_path) {
      this.template = JST[this.template_path];
    }
    this.model.on("change", this.render, this);
    return this.model.on("destroy", this.remove, this);
  },

  render: function() {
    if (this.template) {
      this.$el.html(this.template(this.model.toJSON()));
    }
    return this;
  },
  
  remove: function() {
    this.model.off("change", this.render, this);
    this.model.off("destroy", this.remove, this);
    Backbone.View.prototype.remove.call(this);
  }
});