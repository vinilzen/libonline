this.GeometryUtil = (function() {

  function GeometryUtil() {}

  GeometryUtil.fit = function(container_position, container_size, new_position, size) {
    var max_position_for_object;
    if (new_position < container_position) {
      new_position = container_position;
    }
    max_position_for_object = container_size - size + container_position;
    if (new_position > max_position_for_object) {
      new_position = max_position_for_object;
    }
    if (container_size < size) {
      new_position = container_position;
    }
    return new_position;
  };

  GeometryUtil.fit_rect = function(target, rect) {
    var height, left, top, width;
    width = Math.min(target.get("width"), rect.get("width"));
    height = Math.min(target.get("height"), rect.get("height"));
    top = GeometryUtil.fit(target.get("top"), target.get("height"), rect.get("top"), height);
    left = GeometryUtil.fit(target.get("left"), target.get("width"), rect.get("left"), width);
    return {
      top: top,
      left: left,
      width: width,
      height: height
    };
  };

  return GeometryUtil;

})();
