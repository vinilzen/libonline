var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) {
    for (var key in parent) {
      if (__hasProp.call(parent, key)) child[key] = parent[key];
    }
    function ctor() {
      this.constructor = child;
    }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
  };

this.Hideable = (function(_super) {

  __extends(Hideable, _super);

  function Hideable() {
    return Hideable.__super__.constructor.apply(this, arguments);
  }

  Hideable.prototype.hide = function() {
    return this.$el.hide();
  };

  Hideable.prototype.show = function() {
    return this.$el.show();
  };

  return Hideable;

})(Module);

this.HideableModule = {
  hide: function() {
    return this.$el.hide();
  },
  show: function() {
    return this.$el.show();
  }
};