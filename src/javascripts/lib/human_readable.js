this.HumanReadable = (function() {

  function HumanReadable() {}

  HumanReadable.file_size = function(bytes) {
    var i, units;
    units = ['байт', 'Кб', 'Мб', 'Гб', 'Тб'];
    i = 0;
    while (bytes >= 1024) {
      bytes /= 1024;
      ++i;
    }
    return bytes.toFixed(1) + " " + units[i];
  };

  return HumanReadable;

})();