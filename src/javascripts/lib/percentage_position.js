// Backbone.Model mixin
this.PercentagePosition = {
  update_px_values: function() {
    return this.set({
      top_px: this.vertical_to_px(this.get("top")),
      left_px: this.horizontal_to_px(this.get("left")),
      height_px: this.vertical_to_px(this.get("height")),
      width_px: this.horizontal_to_px(this.get("width"))
    });
  },
  update_percent_values: function() {
    return this.set({
      top: this.vertical_to_percent(this.get("top_px")),
      left: this.horizontal_to_percent(this.get("left_px")),
      height: this.vertical_to_percent(this.get("height_px")),
      width: this.horizontal_to_percent(this.get("width_px"))
    });
  },
  vertical_to_percent: function(h) {
    return Math.round(h * 100 / this.page.get("height"));
  },
  horizontal_to_percent: function(w) {
    return Math.round(w * 100 / this.page.get("width"));
  },
  vertical_to_px: function(h) {
    return Math.round(h * this.page.get("height") / 100);
  },
  horizontal_to_px: function(w) {
    return Math.round(w * this.page.get("width") / 100);
  }
};
