/* manage views for start screen */
#= require_tree ./start_screen

var available_themes;

this.StartScreen = (function() {

  StartScreen.prototype.themes = 

  function StartScreen() {
    _.extend(this, Backbone.Events);
    
    this.uploader_model = new DocFileUpload();
    this.uploader_model.on("file:uploaded", this.show_themes, this);
    this.uploader_model.on("error", this.hide_themes, this.z);

    this.file_upload = new DocFileUploadView({
      model: this.uploader_model
    });

    available_themes = this.themes;

    this.themes_view = new SelectThemeView({
      collection: this.themes
    });
    
    this.themes_view.on("next", this.next, this);

    this.file_upload.show();
    this.mybooks = new MyBooks();
    this.mybooks_view = new MyBooksView({
      collection: this.mybooks,
      el: $("#my_books .books")
    });
    this.mybooks_view.show();
    this.mybooks.on("book_selected", this.book_selected, this);
    this.mybooks.fetch();
  }

  StartScreen.prototype.book_selected = function(book) {
    return this.trigger("next", book);
  };

  StartScreen.prototype.next = function() {
    return this.trigger("next", this.uploader_model);
  };

  StartScreen.prototype.hide = function() {
    $("#my_books").hide();
    $("#select_theme").hide();
    return this.file_upload.hide();
  };

  StartScreen.prototype.show_themes = function() {
    return $('#select_theme').show();
  };

  StartScreen.prototype.hide_themes = function() {
    return this.themes_view.hide();
  };

  return StartScreen;

})();