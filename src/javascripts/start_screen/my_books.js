#= require editor/book
/* start_screen/my_books.js */
this.MyBook = Backbone.Model.extend({});

this.MyBooks = Backbone.Collection.extend({

  model: MyBook,

  url: function() {
    return ENV.api_url + "/getbooks/";
  }
});


this.MyBookView = View.extend({
  className: "book",

  events: {
    "click": "click"
  },

  render: function() {
    this.$el.html(this.model.get("ts"));
    return this;
  },

  click: function() {
    return this.model.collection.trigger("book_selected", this.model);
  }
});


this.MyBooksView = CollectionView.extend({
  collection_item_class: MyBookView
});


_.extend(MyBooksView.prototype, HideableModule);