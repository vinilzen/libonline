#= require file_upload
var ERRORS = {
  http: "ошибка сервера",
  unknown: "неизвестная ошибка. Код: ",
  type: "вы выбрали не *.docx файл",
  size: "файл слишком большой"
};

this.DocFileUpload = FileUpload.extend({

  uploader_settings: {
    url: ENV.api_url + "/upload/uploadFile/",
    file_data_name: "doc_file",
    browse_button: "browse_for_files",
    drop_element: "file_upload",
    filters: [
      {
        title: "*.docx files",
        extensions: "docx"
      }
    ]
  },

  success: function(response) {
    
    console.log(response);

    return this.set("ts", response.ts);
  }
});


this.DocFileUploadView = Backbone.View.extend({

  el: "#file_upload",

  events: {
    "dragover": "highlight_on_drop_area",
    "dragleave": "highlight_off_drop_area",
    "click .next": "move_next"
  },

  initialize: function() {
    Hideable.include_here(this);
    this.drop = this.$el.find(".drop");
    this.input = this.$el.find("#file_input").first();
    this.select_file = this.$el.find(".drop a");
    this.error = this.$el.find(".error");
    this.error_explanation = this.$el.find(".error .explanation");
    this.summary = this.$el.find(".summary");
    this.file_name = this.$el.find(".summary .name");
    this.file_size = this.$el.find(".summary .size");
    this.progress = this.$el.find(".summary .progress");
    this.next_button = this.$el.find(".summary .next");
    this.model.on("file:selected", this.display_file_summary, this);
    this.model.on("file:uploading", this.show_progress, this);
    this.model.on("file:uploaded", this.hide_progress, this);
    this.model.on("file:uploaded", this.show_next, this);
    return this.model.on("error", this.show_error, this);
  },

  move_next: function() {},

  highlight_on_drop_area: function() {
    return this.drop.addClass("highlighted");
  },

  highlight_off_drop_area: function() {
    return this.drop.removeClass("highlighted");
  },

  display_file_summary: function() {
    this.error.hide();
    this.file_name.html(this.model.get("file_name"));
    this.file_size.html(HumanReadable.file_size(this.model.get("file_size")));
    return this.summary.show();
  },

  show_error: function(model, error) {
    this.highlight_off_drop_area();
    this.summary.hide();
    this.error_explanation.html(error);
    return this.error.show();
  },

  show_progress: function() {
    this.highlight_off_drop_area();
    this.next_button.hide();
    return this.progress.show();
  },

  hide_progress: function() {
    return this.progress.hide();
  },

  show_next: function() {
    return this.next_button.show();
  }
});