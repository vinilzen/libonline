<?
require_once('base.view.php');

class AdminView extends BaseView{
    public function _default(){?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <?=$this->head(array('statistic', 'scripts'));?>
    <body>
    <div class="container-narrow-admin">
        <div class="masthead">
            <h3 class="muted">LibOnline</h3>
        </div>
        <hr>

        <div class="row-fluid marketing" id="templates" style="text-align: center; border: none;">
            <div class="row-fluid hide" id="base_template_1366361870" style="display: block;">
                <h3>Administrator Page</h3>
                <ul class="nav nav-tabs themes">
                    <li id="admin_1" class="span2 active"><a onclick="show_statistic();" href="" data-toggle="tab">Reassignment Statistics</a></li>
                    <li id="admin_2" class="span2"><a onclick="show_books_statistic();" href="" data-toggle="tab">Books Statistics</a></li>
                    <li id="admin_3" class="span2"><a onclick="show_image_clipart();" href="" data-toggle="tab">Image Clipart<br>&nbsp;</a></li>
                    <li id="admin_4" class="span2"><a onclick="show_multimedia_clipart();" href="" data-toggle="tab">Audio Clipart<br>&nbsp;</a></li>
                    <li id="admin_5" class="span2"><a onclick="show_video_clipart();" href="" data-toggle="tab">Video Clipart<br>&nbsp;</a></li>
                    <li id="admin_6" class="span2"><a onclick="show_templates();" href="" data-toggle="tab">Templates<br>&nbsp;</a></li>
                    <li id="admin_7" class="span2"><a onclick="show_scripts();" href="" data-toggle="tab">Edit Scripts<br>&nbsp;</a></li>
                    <!--                            <li id="admin_8" class="span2"><a onclick="show_thesaurus();" href="" data-toggle="tab">Thesaurus<br>&nbsp;</a></li>-->
                </ul>
            </div>
        </div>

        <div id="admin_content">
            <!--Reassignment Statistics-->
            <div id="admin_stat" class="cont_admin"></div>

            <!--Books Statistics-->
            <div id="admin_books_stat" class="cont_admin hide"></div>

            <!--Images Clipart upload-->
            <div id="admin_image_clipart" class="cont_admin hide">
                <div class="container-narrow">
                    <div class="masthead">
                        <h3 class="muted">LibOnline</h3>
                    </div>
                    <hr>
                    <div class="jumbotron">
                        <h3>Загрузите клипарт-файл:</h3>
                        <button class="btn btn-large btn-link btn-block drop" id="dropzone_image_clipart">
                            Перетащите файл/файлы сюда или выберите с диска
                        </button>
                        <input type="file" name="file_name" class="hidden" id="fileupload_image_clipart" data-url="<?=Dispatcher::getURI('upload', 'uploadClipArt');?>" />
                        <div class="alert alert-block hide" id="upload_alert_image_clipart"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                        <div class="fix_block">
                            <div id="progress_image_clipart" class="progress progress-striped hide">
                                <div class="bar" style="width: 0%;"></div>
                            </div>
                        </div>
                        <p class="lead">Поддерживаются *.jpg/*.png/*.gif файлы, максимальный размер 3Mb.</p>
                    </div>
                </div>
                <div class="admin_files"></div>
            </div>

            <!--Multimedia Clipart upload-->
            <div id="admin_multimedia_clipart" class="cont_admin hide">
                <div class="container-narrow">
                    <div class="masthead">
                        <h3 class="muted">LibOnline</h3>
                    </div>
                    <hr>
                    <div class="jumbotron">
                        <h3>Загрузите клипарт-файл:</h3>
                        <button class="btn btn-large btn-link btn-block drop" id="dropzone_multimedia_clipart">
                            Перетащите файл/файлы сюда или выберите с диска
                        </button>
                        <input type="file" name="file_name" class="hidden" id="fileupload_multimedia_clipart" data-url="<?=Dispatcher::getURI('upload', 'uploadMultimediaClipArt');?>" />
                        <div class="alert alert-block hide" id="upload_alert_multimedia_clipart"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                        <div class="fix_block">
                            <div id="progress_multimedia_clipart" class="progress progress-striped hide">
                                <div class="bar" style="width: 0%;"></div>
                            </div>
                        </div>
                        <p class="lead">Поддерживаются *.mp3 файлы, максимальный размер 5Mb.</p>
                    </div>
                </div>
                <div class="admin_files"></div>
            </div>

            <!--Video Clipart upload-->
            <div id="admin_video_clipart" class="cont_admin hide">

                <div class="container-narrow">
                    <div class="masthead">
                        <h3 class="muted">LibOnline</h3>
                    </div>
                    <hr>
                    <div class="jumbotron">
                        <h3>Загрузите видео-файл:</h3>
                        <button class="btn btn-large btn-link btn-block drop" id="dropzone_video_clipart">
                            Перетащите файл сюда или выберите с диска
                        </button>
                        <input type="file" name="file_name" class="hidden" id="fileupload_video_clipart" data-url="<?=SERVER_VIDEO.'upload/uploadVideoClipArt/?user_uid='.$_COOKIE['lib_user_uid'];?>" />
                        <div class="alert alert-block hide" id="upload_alert_video_clipart"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                        <div class="fix_block">
                            <div id="progress_video_clipart" class="progress progress-striped hide">
                                <div class="bar" style="width: 0%;"></div>
                            </div>
                        </div>
                        <p class="lead">Файлы форматов отличных от *.mp4, будут автоматически сконвертированы, максимальный размер 30Mb.</p>
                    </div>
                </div>

                <div id="upload_clipart_image" class="hide">
                    <div class="container-narrow">
                        <hr>
                        <div class="jumbotron">
                            <h3>Загрузите картинку для выбранного видео:</h3>
                            <button class="btn btn-large btn-link btn-block drop" id="dropzone_video_clipart_image">
                                Перетащите файл сюда или выберите с диска
                            </button>
                            <input type="file" name="file_name_img" class="hidden" id="fileupload_video_clipart_image" data-url="<?=SERVER_VIDEO.'upload/uploadVideoImageClipart/?user_uid='.$_COOKIE['lib_user_uid'];?>" />
                            <div class="alert alert-block hide" id="upload_alert_video_clipart_image"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                            <div class="fix_block">
                                <div id="progress_video_clipart_image" class="progress progress-striped hide">
                                    <div class="bar" style="width: 0%;"></div>
                                </div>
                            </div>
                            <p class="lead">Поддерживаются *.jpg/*.png/*.gif файлы, максимальный размер 1Mb.</p>
                        </div>
                    </div>
                </div>



                <div class="admin_files"></div>
            </div>

            <!--Template upload-->
            <div id="admin_template" class="cont_admin hide">
                <div class="container-narrow">
                    <div class="masthead">
                        <h3 class="muted">LibOnline</h3>
                    </div>
                    <hr>

                    <!--Template-->
                    <div class="jumbotron" id="jumbotron_template">
                        <h3>Загрузите шаблон:</h3>
                        <button class="btn btn-large btn-link btn-block drop" id="dropzone_template">
                            Перетащите файл сюда или выберите с диска
                        </button>
                        <input type="file" name="file_name" class="hidden" id="fileupload_template" data-url="<?=Dispatcher::getURI('upload', 'uploadTemplate', array('type' => 'book'));?>" />
                        <div class="alert alert-block hide" id="upload_alert_template"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                        <div class="fix_block">
                            <div id="progress_template" class="progress progress-striped hide">
                                <div class="bar" style="width: 0%;"></div>
                            </div>
                        </div>
                        <p class="lead">Поддерживаются только *.xml файлы.</p>
                    </div>

                    <!--Image-->
                    <div class="jumbotron hide" id="jumbotron_template_image">
                        <h3>Загрузите картинку:</h3>
                        <button class="btn btn-large btn-link btn-block drop" id="dropzone_template_image">
                            Перетащите файл сюда или выберите с диска
                        </button>
                        <input type="file" name="file_name" class="hidden" id="fileupload_template_image" data-url="<?=Dispatcher::getURI('upload', 'uploadBaseImageTheme');?>" />
                        <div class="alert alert-block hide" id="upload_alert_template_image"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                        <div class="fix_block">
                            <div id="progress_template_image" class="progress progress-striped hide">
                                <div class="bar" style="width: 0%;"></div>
                            </div>
                        </div>
                        <p class="lead">Поддерживаются только *.jpg/*.png/*.gif файлы,<br /> максимальный размер 1Mb.</p>
                    </div>
                </div>
                <div class="admin_files"></div>
            </div>

            <!--Edit scripts-->
            <div id="admin_scripts" class="cont_admin hide"></div>

            <!--Thesaurus-->
            <!--                    <div id="admin_thesaurus" class="cont_admin hide"></div>-->
        </div>
    </div>

    <script type="text/javascript" src="<?=PATH_JS;?>jquery.min.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>jquery.ui.widget.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>jquery.fileupload.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>jquery_custom_ui.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>underscore-min.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>backbone-min.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>admin.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>statistic.js"></script>
    <script type="text/javascript" src="<?=PATH_JS;?>scripts.js"></script>
    <!--            <script type="text/javascript" src="--><?//=PATH_JS;?><!--thesaurus.js"></script>-->
    <!--            <script type="text/javascript" src="--><?//=PATH_JS;?><!--medialib.js"></script>-->
    <script type="text/javascript">
        function showCont(container){
            if(container.css('display') == 'none'){
                $('.cont_admin').hide();
                container.show('fast');
            }
        }

        function show_statistic(){
            var container = $('#admin_stat');
            showCont(container);
        }

        function show_books_statistic(){
            var container = $('#admin_books_stat');
            showCont(container);
            var booksStatistics = new StatisticBooks();
        }

        function show_image_clipart(){
            var container = $('#admin_image_clipart');
            showCont(container);
            var ids = {
                'dropzone':'dropzone_image_clipart',
                'upload_alert':'upload_alert_image_clipart',
                'fileupload':'fileupload_image_clipart',
                'progress':'progress_image_clipart'
            };

            var checkFormats = function(name){ return /^.*\.(jpg)$/.test(name) || /^.*\.(png)$/.test(name) || /^.*\.(gif)$/.test(name) };
            var show_images = new ShowImageClipart({model:{
                'container':container,
                'ids':ids,
                'formats':checkFormats,
                'max_loaded':10
            }});
            var clipart_images = new GetFiles({
                'view':show_images,
                'get_url':'getClipArts',
                'delete_folder_url':'deleteClipArtFolder',
                'make_folder':'createClipArtFolder',
                'delete_file_url':'deleteClipArtFile',
                'sub_folder':''
            });
        }

        function show_multimedia_clipart(){
            var container = $('#admin_multimedia_clipart');
            showCont(container);
            var ids = {
                'dropzone':'dropzone_multimedia_clipart',
                'upload_alert':'upload_multimedia_image_clipart',
                'fileupload':'fileupload_multimedia_clipart',
                'progress':'progress_multimedia_clipart'
            };

            var checkFormats = function(name){ return /^.*\.(mp3)$/.test(name) };
            var show_images = new ShowMultimediaClipart({model:{
                'container':container,
                'ids':ids,
                'formats':checkFormats,
                'max_loaded':10
            }});
            var clipart_images = new GetFiles({
                'view':show_images,
                'get_url':'getMultimediaClipArts',
                'delete_folder_url':'deleteMultimediaFolder',
                'make_folder':'createClipMultimediaFolder',
                'delete_file_url':'deleteMultimediaClipArtFile',
                'sub_folder':''
            });
        }

        function show_video_clipart(){
            var container = $('#admin_video_clipart');
            showCont(container);
            var ids = {
                'dropzone':'dropzone_video_clipart',
                'upload_alert':'upload_alert_video_clipart',
                'fileupload':'fileupload_video_clipart',
                'progress':'progress_video_clipart'
            };
            var checkFormats = function(name){ return /^.*\.(mp4)$/.test(name) || /^.*\.(avi)$/.test(name) || /^.*\.(divx)$/.test(name) || /^.*\.(webm)$/.test(name)  || /^.*\.(ogvf)$/.test(name)};

            var idsImg = {
                'dropzone':'dropzone_video_clipart_image',
                'upload_alert':'upload_alert_video_clipart_image',
                'fileupload':'fileupload_video_clipart_image',
                'progress':'progress_video_clipart_image'
            };
            var checkFormatsImages = function(name){ return /^.*\.(jpg)$/.test(name) || /^.*\.(png)$/.test(name) || /^.*\.(gif)$/.test(name) };

            var show_video = new ShowVideoClipart({model:{
                'container':container,
                'ids':ids,
                'formats':checkFormats,
                'idsImg':idsImg,
                'formats_img':checkFormatsImages,
                'max_loaded':1
            }});

            var clipart_video = new GetFiles({
                'view':show_video,
                'external_host':'<?=SERVER_VIDEO;?>',
                'user_uid':'<?=$_COOKIE['lib_user_uid'];?>',
                'get_url':'getVideoClipArts',
                'delete_folder_url':'deleteVideoClipArtFolder',
                'fileupload':'fileupload_video_clipart',
                'make_folder':'createVideoClipArtFolder',
                'delete_file_url':'deleteVideoClipArtFile',
                'sub_folder':''
            });
        }

        function show_templates(){
            var container = $('#admin_template');
            showCont(container);

            var ids_template = {
                'dropzone':'dropzone_template',
                'upload_alert':'upload_alert_template',
                'fileupload':'fileupload_template',
                'progress':'progress_template'
            };

            var ids_template_image = {
                'dropzone':'dropzone_template_image',
                'upload_alert':'upload_alert_template_image',
                'fileupload':'fileupload_template_image',
                'progress':'progress_template_image'
            };

            var checkFormatsXML = function(name){ return /^.*\.(xml)$/.test(name) };
            var checkFormatsImg = function(name){ return /^.*\.(jpg)$/.test(name) || /^.*\.(png)$/.test(name) || /^.*\.(gif)$/.test(name)};
            var show_template = new ShowTemplate({model:{
                'container':container,
                'ids_template':ids_template,
                'ids_template_image':ids_template_image,
                'formats_xml':checkFormatsXML,
                'formats_img':checkFormatsImg,
                'max_loaded_xml':1,
                'max_loaded_img':3
            }});
            var template = new GetTemplates({
                'view':show_template,
                'make_folder':'createTemplateFolder',
                'sub_folder':''
            });
        }

        function show_scripts(){
            var container = $('#admin_scripts');
            showCont(container);
            var scripts = new Scripts();
        }

        //                function show_thesaurus(){
        //                    var container = $('#admin_thesaurus');
        //                    showCont(container);
        //                    var thesaurus = new Thesaurus();
        //                }

        show_statistic();
    </script>
    <script type="text/javascript">
        function my_alert(message, container){
            $('#upload_alert_image_clipart h4, #upload_alert_image_clipart p').remove();
            var alert = '<h4>Warning!</h4><p>'+message+'</p>';
            $('#'+container).append(alert).show();
        }

        $(document).bind('dragover', function (e) {
            var dropZone = $('.drop'), timeout = window.dropZoneTimeout;
            if (!timeout) {
                dropZone.addClass('in');
            } else {
                clearTimeout(timeout);
            }
            if (e.target === dropZone[0]) {
                dropZone.addClass('hover');
            } else {
                dropZone.removeClass('hover');
            }
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        });
    </script>
    </body>
    </html>
    <?}
}
?>