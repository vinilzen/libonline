<?
	require_once('inc/constant.inc.php');
	
	abstract class BaseView{
		protected function head($addCss = NULL){?>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Lib Online</title>

                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">
                <!-- Le styles -->
                <link href="<?=PATH_CSS;?>bootstrap.css" rel="stylesheet">
                <link href="<?=PATH_CSS;?>bootstrap-responsive.css" rel="stylesheet">
                <link href="<?=PATH_CSS;?>libonline.css" rel="stylesheet">
                <?
                if($addCss){
                   if(is_array($addCss)){
                       for($i=0;$i<count($addCss);$i++){?><link href="<?=PATH_CSS.$addCss[$i];?>.css" rel="stylesheet" type="text/css" /><?}
                   }else{?><link href="<?=PATH_CSS.$addCss;?>.css" rel="stylesheet" type="text/css" /><?}
                }
                ?>
                <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
                <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->

                <link rel="icon" href="/favicon.ico" type="image/x-icon">
			</head>
		<?}
		
		protected function input($name, $descr=NULL, $value=NULL, $type='text', $maxlength=NULL, $stylesDescr=NULL, $stylesInput=NULL, $actions=NULL){
			if ($stylesDescr) $stl1 = $this->setStyles($stylesDescr);
			if ($stylesInput) $stl2 = $this->setStyles($stylesInput);
			if ($actions) $actns = $this->setActions($actions);
			if ($type == 'checkbox'){
				if ($value) $checked = 'checked = "checked"';
				else $checked = NULL;
			}
			if($type != 'submit' && $type != 'file'){?>
				<div class="descrInput" style="<?=$stl1;?>"><?=$descr;?></div>
			<?}?>
			<input <? if($type != 'submit'){?>name="<?=$name;?>"<?}?> type="<?=$type;?>" <? if($type != 'checkbox' && $type != 'file'){?>value="<?=$value;?>" maxlength="<?=$maxlength;?>"<? echo ' '.$actns;}?> <?=$checked;?> style="<?=$stl2;?>" />
		<?}
		
		protected function select($name, $values, $names, $select=NULL, $disable=NULL, $multiple=false, $descr=NULL, $stylesDescr=NULL, $stylesSelect=NULL, $actions=NULL){
			if (!$values || !$names) return;

			if ($stylesDescr) $stl1 = $this->setStyles($stylesDescr);
			if ($stylesSelect) $stl2 = $this->setStyles($stylesSelect);
			if ($actions) $actns = $this->setActions($actions); ?>
			
			<div class="descrInput" style="<?=$stl1;?>"><?=$descr;?></div>
			<select <?if($multiple) echo 'multiple'?> style="<?=$stl2;?>" <?=$actns;?> name=<?=$name;?>>
				<? for($i=0;$i<count($values);$i++){
					if($values[$i] == $select) $attr = 'selected';
					else if($values[$i] == $disable && $values[$i] != NULL) $attr = 'disabled';
					else $attr = '';
					?>
					<option <?=$attr;?> value="<?=$values[$i];?>"><?=$names[$i];?></option>
				<?}?>
			</select>
		<?
		}
		
		protected function textarea($name, $descr=NULL, $value=NULL, $maxlength=NULL, $stylesDescr=NULL, $stylesTA=NULL, $actions=NULL){
			if (!$name) return;
			if ($stylesDescr) $stl1 = $this->setStyles($stylesDescr);
			if ($stylesTA) $stl2 = $this->setStyles($stylesTA);
			if ($actions) $actns = $this->setActions($actions);
			?>
			<div class="descrInput" style="<?=$stl1;?>"><?=$descr;?></div>
			<textarea name="<?=$name;?>" maxlength="<?=$maxlength;?>" style="<?=$stl2;?>" <?=$actions;?>><?=$value;?></textarea>
		<?}
		
		protected function setStyles($styles){
			if(!is_array($styles) || !$styles) return;
			$stl = '';
			foreach ($styles as $style => $val){
				$stl .= $style.': '.$val.'; ';
			}
			return $stl;
		}
	}
?>