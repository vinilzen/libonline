<?
	require_once('base.view.php');
	
	class IndexView extends BaseView{
		public function _default(){?><!DOCTYPE html>
            <html>
            <?=$this->head();?>
            <body>
                <div class="container-narrow">
                    <div class="masthead">
                        <div class="row-fluid">
                            <div class="span8">
                                <h3 class="muted">LibOnline</h3>
                            </div>
                            <div class="span4">
 <!--                                <a href="#" title="Буквицы" onclick="$('#settingsModal').modal('show')" class="btn btn-link pull-right settings">
                                    <i class="icon-cog"></i>
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="jumbotron">
                        <h2>Загрузите файл:</h2>
                        <button class="btn btn-large btn-link btn-block drop" id="dropzone">
                            Перетащите файл сюда или выберите с диска
                        </button>
                        <input type="file" name="doc_file" class="hidden" id="fileupload" data-url="<?=Dispatcher::getURI('upload', 'uploadFile');?>" />
                        <div class="alert alert-block hide" id="upload_alert"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

                        <div class="fix_block">
                            <div id="progress" class="progress progress-striped hide">
                                <div class="bar" style="width: 0%;"></div>
                            </div>
                        </div>
                        <p class="lead">Поддерживаются только *.docx файлы, максимальный размер 1Mb.</p>
                </div>
                <hr>

                <div class="row-fluid marketing libconteiner hide">
                    <h2>Выберите книгу и тему:</h2>
                    <div class="my_books" data-toggle="buttons-radio"></div>
                </div>

                <div class="row-fluid marketing" id="templates">
                    <button class="btn hide btn-block" id="goto_edit">Редактировать</button>
                </div>

            </div>
            <div class="container-editor hide">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="navbar">
                            <div class="navbar-inner lib_navbar">
                                
                                <a class="brand" href="#">Home</a>
                                
                                <a href="#" class="refresh"><i class="icon-refresh  icon-white"></i></a>
                                
                                <!-- меню для библиотеки с семпловым и пользовательским контентом -->
                                <div class="lib_buttons btn-group">
                                    <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">
                                        Медиа Библиотека
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu"></ul>
                                </div>
                                
                                <!-- список базовых и пользовательских тем -->
                                <div class="theme_select btn-group">
                                    <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">
                                        Темы оформления
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu"></ul>
                                </div>
                                <div class="saver"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span2 well" id="preview_span">
                            <fieldset class="filter_default_content">
                                <label class="checkbox">
                                  <input type="checkbox" id="filter_def"> <small>Показать страницы с дефолтным контентом</small>
                                </label>
                            </fieldset>
                            <div class="page_previews"></div>
                        </div>
                        <div class="span10" id="editor_span">
                            <div class="editor_main">
                                <div class="page_container"></div>
                            </div>
                        </div>
                    </div><!--/row-->
                </div>
            </div>

            <script type="text/javascript" src="<?=PATH_JS;?>jquery-2.0.3.min.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery.ui.widget.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery.iframe-transport.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery.form.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery.fileupload.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery.scrollTo.min.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery_custom_ui.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>bootstrap.min.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>underscore-min.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>backbone-min.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>multiline.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>page.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>widget.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>placeholder.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>change.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>contents.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>filter_def.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>book.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>theme.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>page.jq.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>inset_menu.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>text.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>elements_type.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>remapping.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>properties.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>scripts.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>tag.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>medialib.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>dropcap.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>footnote.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>search.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>main.js"></script>
            <script type="text/javascript" src="<?=PATH_JS;?>admin.js"></script>
            <script type="text/javascript" id="customs_js"></script>
            <div class="modal-backdrop fade in hide"></div>
        </body>
        </html>
		<?}
	}
?>