<?
	require_once('view/base.view.php');
	
	class LoginView extends BaseView{
		public function _default($message=false){?>
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <?=$this->head();?>
            <body>
                <? if($message){?>
                    <div id="blackShadow">
                        <div id="messageFrame">
                            <div id="innerMessageFrame">
                                <div id="closeButton">[X]</div>
                                <?=$message;?>
                            </div>
                        </div>
                    </div>
                <?}?>
                <div class="container">
                    <form class="form-signin" action="<?=Dispatcher::getURI('login', 'enter');?>" method="POST" id="loginFrame">
                        <img id="logo" src="/img/logo.png">
                        <h2 class="form-signin-heading">Please sign in</h2>
                        <input name="login" type="text" class="input-block-level" placeholder="Enter your login">
                        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
                    </form>
                </div>
            </body>
            <script type="text/javascript" src="<?=PATH_JS;?>jquery.min.js"></script>
            <script type="text/javascript">
                if(jQuery('#blackShadow').html()){
                    jQuery('#closeButton').bind('click', function(){
                        jQuery('#blackShadow').remove();
                    });
                }
            </script>
            </html>
        <?}
	}
?>