<?
require_once('view/base.view.php');

class TestView extends BaseView{
    public function _default(){?>
    <!DOCTYPE html>
    <html>
    <head>
        <script type="text/javascript" src="<?=PATH_JS;?>jquery.min.js"></script>
        <meta charset="utf-8">
        <title>Test</title>
    </head>
        <?//=$this->head();?>
    <body>
    <!--        <video src="--><?//=SERVER_VIDEO;?><!--clipart_video/test.mp4" controls></video>-->
    <!--        <div id="test1" style="width: 500px; height: 500px; border: 1px solid red;"></div>-->
    <!--            <h1>Upload docx</h1>-->
    <!--            <form style="height: 35px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadFile');?><!--" method="POST">-->
    <!--                --><?//=$this->input('test', '', 'elements', 'hidden', '', array(), array());?>
    <!--                --><?//=$this->input('doc_file', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
    <!--                --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
    <!--            </form>-->
    <!--            <hr>-->
    <!--            <h1>Parser</h1>-->
    <!--            <form style="height: 75px;" action="--><?//=Dispatcher::getURI('parser', 'parseXML');?><!--" method="POST">-->
    <!--                --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
    <!--                --><?//=$this->input('test', '', 'elements', 'hidden', '', array(), array());?>
    <!--                --><?//=$this->input('test_stat', '', true, 'hidden', '', array(), array());?>
    <!--                <input type="submit" value="ЖГИ!">-->
    <!--            </form>-->
    <!--            <hr>-->
    <!--            <h1>Get Content</h1>-->
    <!--            <form style="height: 80px;" action="--><?//=Dispatcher::getURI('getbooks', 'getContent');?><!--" method="GET">-->
    <!--                --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?>
    <!--                <input type="submit" value="ЖГИ!">-->
    <!--            </form>-->
    <!--            <hr>-->
    <!--        <h1>Save Book</h1>-->
    <!--        <form style="height: 270px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'saveBook');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('name', 'Name', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('author', 'Author', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('elements', 'Elements', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('content', 'Content', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Thesaurus</h1>-->
    <!--        <form style="height: 270px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('thesaurus');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '100px'));?><!--<br />-->
    <!---->
    <!--            --><?//=$this->input('low_limit', 'Low Limit', '', '', '', array(), array('width' => '100px'));?><!--<br />-->
    <!--            --><?//=$this->input('high_limit', 'Hight Limit', '', '', '', array(), array('width' => '100px'));?><!--<br />-->
    <!---->
    <!--            --><?//=$this->input('text_pos_start', 'Text pos min', '', '', '', array(), array('width' => '100px'));?><!--<br />-->
    <!--            --><?//=$this->input('text_pos_end', 'Text pos max', '', '', '', array(), array('width' => '100px'));?><!--<br />-->
    <!---->
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->

    <!--        <h1>Add Thesaurus Word</h1>-->
    <!--        <form style="height: 270px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books','addThesaurusWord');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
    <!--            --><?//=$this->input('word', 'Word', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
    <!--        -->
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Books Statistic</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'getBooksStatistic');?><!--" method="POST">-->
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Reasign Phrase</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'setReassignments');?><!--" method="POST">-->
    <!--            <input type="hidden" name="book_uid" value="1372758024">-->
    <!--            <input type="hidden" name="reassign" value='[{"phrase":"Вступление", "turn":"author", "was":"title", "start_pos":520}]'>-->
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Books</h1>-->
    <!--        <form style="height: 80px;" action="/books/" method="POST"">-->
    <!--        <input type="submit" value="ЖГИ!">-->
    <!--        </form>-->
    <!--        <hr>-->
<!--            <h1>Get Base Templates</h1>-->
<!--            <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('templates', 'getBaseTemplates');?><!--" method="POST">-->
<!--                --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--            </form>-->
<!--            <hr>-->
<!--            <h1>Get User Templates</h1>-->
<!--            <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('templates', 'getUserTemplates');?><!--" method="POST">-->
<!--                --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--            </form>-->
<!--            <hr>-->

    <!--        <h1>Get Cliparts</h1>-->
    <!--        <form style="height: 60px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'getcliparts');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Set Files Tags</h1>-->
    <!--        <form style="height: 110px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'setFileTags');?><!--" method="POST">-->
    <!--            <input type="hidden" name="model" value='{"id":1,"tags":["dark","green","orange","blue"]}'>-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Complete conversion</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--filechange/completeConversion/" method="POST">-->
    <!--            --><?//=$this->input('file_id', 'File ID', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Upload image</h1>-->
    <!--        <form style="height: 110px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadImage');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('file_name', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
    <!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Upload Clipart</h1>-->
    <!--        <form style="height: 110px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadClipArt');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('clipart_file', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
    <!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->

<!--            <h1>Get Template</h1>-->
<!--            <form style="height: 120px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('templates');?><!--" method="POST">-->
<!--                --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '100px'));?>
<!--                --><?//=$this->input('theme_uid', 'Theme UID', '1', '', '', array(), array('width' => '100px'));?><!--<br>-->
<!--                --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--            </form>-->
<!--            <hr>-->
            <h1>Upload Template</h1>
            <form style="height: 100px;" enctype="multipart/form-data" action="<?=Dispatcher::getURI('books', 'saveHtml');?>" method="POST">
                <?=$this->input('file_name', '', 'Select theme', 'file', 0, array(), array('width' => '250px'));?><br>
                <?=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
            </form>
            <hr>

    <!--        <h1>Delete Template</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'deleteThemeFile');?><!--" method="POST">-->
    <!--            --><?//=$this->input('name', 'Delete Template', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Update Data</h1>-->
    <!--        <form style="height: 180px;" action="--><?//=Dispatcher::getURI('templates', 'updateTemplate');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('theme_uid', 'Theme UID', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('data', 'Data', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
    <!--            <input type="submit" value="ЖГИ!">-->
    <!--        </form>-->
    <!--        <hr>-->

    <!--        <h1>Insert Content</h1>-->
    <!--        <form style="height: 180px;" action="--><?//=Dispatcher::getURI('templates', 'updateTheme');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('theme_uid', 'Theme UID', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('data', 'Data', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
    <!--            <input type="submit" value="ЖГИ!">-->
    <!--        </form>-->
    <!--        <hr>-->

<!--        <h1>Get Template</h1>-->
<!--        <form style="height: 120px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('templates');?><!--" method="POST">-->
<!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '100px'));?>
<!--            --><?//=$this->input('theme_uid', 'Theme UID', '1', '', '', array(), array('width' => '100px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Upload Template</h1>-->
<!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadTemplate');?><!--" method="POST">-->
<!--            --><?//=$this->input('file_name', '', 'Select theme', 'file', 0, array(), array('width' => '250px'));?>
<!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Delete Template</h1>-->
<!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'deleteThemeFile');?><!--" method="POST">-->
<!--            --><?//=$this->input('name', 'Delete Template', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Update Data</h1>-->
<!--        <form style="height: 180px;" action="--><?//=Dispatcher::getURI('templates', 'updateTemplate');?><!--" method="POST">-->
<!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?>
<!--            --><?//=$this->input('theme_uid', 'Theme UID', '', '', '', array(), array('width' => '500px'));?>
<!--            --><?//=$this->input('data', 'Data', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
<!--            <input type="submit" value="ЖГИ!">-->
<!--        </form>-->
<!--        <hr>-->

    <!--        <h1>Upload Preview Theme</h1>-->
    <!--        <form style="height: 110px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadPreviewTheme');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('image_theme', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
    <!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Statistic</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'getStatistic');?><!--" method="POST">-->
    <!--            <input type="hidden" name="book_uid" value="1371559136">-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Reassign Statistic Book</h1>-->
    <!--        <form style="height: 60px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'getReassignStatistic');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '100px'));?>
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Reassign Statistic</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'getReassignStatistic');?><!--" method="POST">-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Scripts Names</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('scripts', 'getNamesScripts');?><!--" method="POST">-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Save Script</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('books', 'getReassignStatistic');?><!--" method="POST">-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Scripts</h1>-->
    <!--        <form style="height: 30px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('scripts', 'getScriptsPublic');?><!--" method="POST">-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->

    <!--        <h1>Get Images</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'getImage');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Delete Folder</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'deleteImageFolder');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'Deleting folder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->

    <!--        <h1>Create Image Folder</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'createImageFolder');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'Create folder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Создать', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->

    <!--        <h1>Upload Multimedia</h1>-->
    <!--        <form style="height: 100px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadMultimedia');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('file_name', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!---->
    <!--        <h1>Get Multimedia</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'getMultimedia');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Multimedia Cliparts</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'getMultimediaClipArts');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->


    <!--        <h1>Get Video</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/getvideo/" method="POST">-->
    <!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
<!--    <h1>Get Video Cliparts</h1>-->
<!--    <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/getVideoClipArts/" method="POST">-->
<!--        --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--        --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--        --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--    </form>-->
<!--    <hr>-->
<!---->

<!--        <h1>Get Multimedia</h1>-->
<!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'getMultimedia');?><!--" method="POST">-->
<!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Get Multimedia Cliparts</h1>-->
<!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'getMultimediaClipArts');?><!--" method="POST">-->
<!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->


<!--        <h1>Get Video</h1>-->
<!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/getvideo/" method="POST">-->
<!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'ДАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Get Video Cliparts</h1>-->
<!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/getVideoClipArts/" method="POST">-->
<!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'ДАВАЙ!', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->

<!--        <h1>Upload Video</h1>-->
<!--        <form style="height: 130px;" enctype="multipart/form-data" action="--><?////=SERVER_VIDEO;?><!--<!--upload/uploadVideo/" method="POST">-->
<!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            <snan>Select Video file</snan>--><?//=$this->input('file_name', '', 'Select Video file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
<!--            <snan>Select Image file</snan>--><?//=$this->input('file_name_img', '', 'Select Image file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->

<!--    <h1>Upload Video</h1>-->
<!--    <form style="height: 130px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/uploadVideo/" method="POST">-->
<!--        --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--        --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--        <snan>Select Video file</snan>--><?//=$this->input('file_name', '', 'Select Video file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
<!--        <snan>Select Image file</snan>--><?//=$this->input('file_name_img', '', 'Select Image file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
<!--        --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
<!--    </form>-->
<!--    <hr>-->

<!--    <h1>Upload Video Image</h1>-->
<!--    <form style="height: 150px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/uploadVideoImage/" method="POST">-->
<!--        --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--        --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '300px'));?><!--<br>-->
<!--        --><?//=$this->input('id', 'Video File ID', '', '', '', array(), array('width' => '300px'));?><!--<br>-->
<!--        <snan>Select Image file</snan>--><?//=$this->input('file_name_img', '', 'Select Image file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
<!--        --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
<!--    </form>-->
<!--    <hr>-->

<!--    <h1>Delete Video Clipart Folder</h1>-->
<!--    <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/deleteVideoClipArtFolder/" method="POST">-->
<!--        --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
<!--        --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--        --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
<!--    </form>-->
<!--    <hr>-->

    <!--        <h1>Upload Video Clipart</h1>-->
    <!--        <form style="height: 100px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/uploadVideoClipArt/" method="POST">-->
    <!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('file_name', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Delete Video</h1>-->
    <!--        <form style="height: 170px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/deleteVideoFile/" method="POST">-->
    <!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
    <!--            --><?//=$this->input('file_name', 'File Name', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('id', 'File Id', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Delete Video Clipart</h1>-->
    <!--        <form style="height: 170px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/deleteVideoClipArtFile/" method="POST">-->
    <!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
    <!--            --><?//=$this->input('file_name', 'File Name', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('id', 'File Id', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Create Video Sub Folder</h1>-->
    <!--        <form style="height: 50px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/createVideoFolder/" method="POST">-->
    <!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'Create', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Delete Video Sub Folder</h1>-->
    <!--        <form style="height: 50px;" enctype="multipart/form-data" action="--><?//=SERVER_VIDEO;?><!--upload/deleteVideoFolder/" method="POST">-->
    <!--            --><?//=$this->input('user_uid', '', $_COOKIE['lib_user_uid'], 'hidden', '', array(), array());?>
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Create Images Sub Folder</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'createImagesFolder');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'Create', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Create Multimedia Sub Folder</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'createMultimediaFolder');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'Create', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Delete Images Sub Folder</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'deleteImagesFolder');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Create Template Sub Folder</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'createTemplateFolder');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('submit', '', 'Create', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->


        <h1>Upload docx</h1>
        <form style="height: 70px;" enctype="multipart/form-data" action="<?=Dispatcher::getURI('upload', 'uploadFile');?>" method="POST">
            <?=$this->input('doc_file', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
            <?=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
        </form>
        <hr>
<!--        <h1>Upload Multimedia</h1>-->
<!--        <form style="height: 110px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadMultimedia');?><!--" method="POST">-->
<!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('multimedia_file', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
<!--            --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
<!---->
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Get Base Drop Cap Config</h1>-->
<!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('dropcaps', 'getBaseConfig');?><!--" method="POST">-->
<!--            --><?//=$this->input('config', 'Config', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--            --><?//=$this->input('submit', '', 'Давай!', 'submit', 0, array(), array('width' => '100px'));?>
<!--        </form>-->
<!--        <hr>-->

<!--            <h1>Upload Multimedia Clipart</h1>-->
<!--            <form style="height: 90px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'uploadMultimediaClipArt');?><!--" method="POST">-->
<!--                --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
<!--                --><?//=$this->input('file_name', '', 'Select file', 'file', 0, array(), array('width' => '250px'));?>
<!--                --><?//=$this->input('submit', '', 'Upload', 'submit', 0, array(), array('width' => '100px'));?>
<!--            </form>-->
<!--            <hr>-->
    <!--        <h1>Get Base Drop Cap Config</h1>-->
    <!--        <form style="height: 80px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('dropcaps', 'getBaseConfig');?><!--" method="POST">-->
    <!--            --><?//=$this->input('config', 'Config', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Давай!', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->

    <!--        <h1>Delete image</h1>-->
    <!--        <form style="height: 150px;" enctype="multipart/form-data" action="--><?//=Dispatcher::getURI('upload', 'deleteImageFile');?><!--" method="POST">-->
    <!--            --><?//=$this->input('folder', 'SubFolder', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('file', 'delete', '', '', '', array(), array('width' => '500px'));?><!--<br>-->
    <!--            --><?//=$this->input('submit', '', 'Delete', 'submit', 0, array(), array('width' => '100px'));?>
    <!--        </form>-->
    <!--        <hr>-->
    <!--        <h1>Get Themes</h1>-->
    <!--        <form style="height: 140px;" action="--><?//=Dispatcher::getURI('getbooks', 'getThemes');?><!--" method="POST">-->
    <!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?>
    <!--            --><?//=$this->input('theme_uid', 'Theme UID', '', '', '', array(), array('width' => '500px'));?>
    <!--            <input type="submit" value="ЖГИ!">-->
    <!--        </form>-->
    <!--        <hr>-->

<!--        <h1>Parser</h1>-->
<!--        <form style="height: 75px;" action="--><?//=Dispatcher::getURI('parser', 'parseXML');?><!--" method="POST">-->
<!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
<!--            --><?//=$this->input('test', '', 'elements', 'hidden', '', array(), array());?>
<!--            --><?//=$this->input('test_stat', '', true, 'hidden', '', array(), array());?>
<!--            <input type="submit" value="ЖГИ!">-->
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Add Thesaurus Word</h1>-->
<!--        <form style="height: 120px;" action="--><?//=Dispatcher::getURI('books', 'addThesaurusWord');?><!--" method="POST">-->
<!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
<!--            --><?//=$this->input('word', 'Add Word', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
<!--            <input type="submit" value="ЖГИ!">-->
<!--        </form>-->
<!--        <hr>-->
<!--        <h1>Insert Theme</h1>-->
<!--        <form style="height: 180px;" action="--><?//=Dispatcher::getURI('savedata', 'insertTheme');?><!--" method="POST">-->
<!--            --><?//=$this->input('book_uid', 'Book UID', '', '', '', array(), array('width' => '500px'));?>
<!--            --><?//=$this->input('theme_uid', 'Theme UID', '', '', '', array(), array('width' => '500px'));?>
<!--            --><?//=$this->input('data', 'Data', '', '', '', array(), array('width' => '500px'));?><!--<br />-->
<!--            <input type="submit" value="ЖГИ!">-->
<!--        </form>-->
<!--        <hr>-->
        </body>
        </html>
		<?
	}
}
?>
